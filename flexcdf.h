/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXCDF_H__
#define __FLEXCDF_H__

//#include "netcdf.h"
#include "netCDF/netcdf.h"

#define MAX_ATT_LEN 64 

class flexCDF
{
public:
    //! Constructor
	flexCDF();

    //! Destructor
	~flexCDF();

	/** Opens netCDF file for input or output.
	 @param fname File name.
	*/
	void open(const char *fname);

	/** Check field variable.
	 @param vname Variable name.
	*/
	bool isVariable(const char *vname);

	/** Reads grid variable.
	 @param vname Variable name.
	 @param ptr Pointer to memory for results.
	 @param np Number of points in the variable.
	*/
	int readGrid(const char *vname, double *ptr, int np);

	/** Reads field variable.
	 @param vname Variable name.
	 @param ptr Pointer to memory.
	 @param np Number of points.
	*/
	void readVariable(const char *vname, float *ptr, int np);

private:

	void checkVariable(const char *vname);

	void checkBuffer(int size);

	void getAttribute(const char *aname, double *value, int *ivalue, int n);

private:
	int _ncid, _varid, _ndims;

	int _nl, _nr, _nc, _np;

	nc_type _type;

	int _dimids[NC_MAX_VAR_DIMS];

	size_t _dimlen[NC_MAX_VAR_DIMS];

	double _scale, _offset;
	double _gdas1_scale[MAX_ATT_LEN], _gdas1_offset[MAX_ATT_LEN];
	int _missing;

	void *_buf;
	int _size;
};


#endif
