/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXCLASS_H__
#define __FLEXCLASS_H__

#include "flexocl.h"
#include "flexarr.h"
#include "flexgrid.h"
#include "flexparam.h"
#include "flexfield.h"
#include "flexcount.h"

/**
 flexClass represents a particle class released to the model.
 */

class flexClass
{
public:
    //! Constructor
	flexClass();

    //! Destructor
	~flexClass();

    /** Models particle dispersion for one synchronization time interval.
     @param G flexGrid object.
     @param P flexParam object.
     @param F flexField object.
     @param Ct flexCount object.
     */
    void dispersion(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &Ct);

    /** Calculates trajectory without turbulence for one synchronization time interval.
     @param G flexGrid object.
     @param P flexParam object.
     @param F flexField object.
     */
    void trajectory(const flexGrid &G, const flexParam &P, const flexField &F);
    
    /**
     * Calculates each particles radioactive level for one sunchronisation time 
     * interval from the previous level
     */
    void radioactive_decay(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &Ct);

    /** Releases particles randomly.
     @param n Number of particles.
     @param x0 Longitude center [degree].
     @param y0 Latitude center [degree].
     @param ds Radius from the center [m].
     @param z0 Lower altitude [m].
     @param dz Altitude range above z0 [m].
     */
	void release(int n, float x0, float y0, float ds, float z0, float dz, float mass, int halflife);

	/** Sorts particles by grid index.
	 @param Pdx Sorted particle ID index.
	 @param Sdx Segment index of grid index.
	 @param cuda Flag indicating whether GPU device is used.
	 <br>
	 Particles with id of Pdx(Sdx(i)) to Pdx(Sdx(i+1)) exclusive have the same grid index. 
	 */
	int sort(flexArr<int> &Pdx, flexArr<int> &Sdx, int cuda);

    /** Releases particles on grids.
     @param x0 Initial longitude [degree].
     @param dx Longitude grid [degree].
     @param nx Number of grids along the longitude.
     @param y0 Initial latitude [degree].
     @param dy Latitude grid [degree].
     @param ny Number of grids along the latitude.
     @param z0 Initial altitude [m].
     @param dz Altitude grid [m].
     @param nz Number of vertical grids.
     */
	void relgrid(float x0, float dx, int nx, float y0, float dy, int ny, float z0, float dz, int nz);

    /** Sets grid for counting particles.
     @param Ct flexCount object.
     */
	void cgrid(const flexCount &Ct) { C.set(Ct); }

    /** Sets air mass.
     @param G flexGrid object.
     @param F flexField object.
     */
	void mass(const flexGrid &G, const flexField &F) { C.mass(G,F); }

    /** Saves particle positions.
     @param fname File name.
     @param G flexGrid object.
     @param P flexParam object.
     @param F flexField object.
     @param ctime Current time.
     */
	void saveXYZ(const char *fname, const Grid &G, const Parameter &P, const Field &F, double ctime);

    /** Saves particle counting results.
     @param fname File name.
	 @param sec Output interval [s].
     @param ctime Current time.
     */
	void saveCNT(const char *fname, const Parameter &P, double ctime)
	{
		C.save(fname,P,age(),ctime);
	}

    /** Sets the ID of the particle class.
     @param name Particle class name.
     */
	void id(const char *name);

    //! Gets particle positions from NVIDIA device.
	void xyz();

    //! Gets the ID of the particle class.
	const char *id() const { return _id; }

    /** Gets the longitude of a particle.
     @param i Particle array index (starts from zero).
     */
	float x(int i) const { return X(i); }

    /** Gets the latitude of a particle.
     @param i Particle array index (starts from zero).
     */
	float y(int i) const { return Y(i); }

    /** Gets the altitude of a particle.
     @param i Particle array index (starts from zero).
     */
	float z(int i) const { return Z(i); }

    /** Gets the surface height of a particle.
     @param i Particle array index (starts from zero).
     */
	float zs(int i) const { return Zs(i); }

    /** Gets the boundary layer height of a particle.
     @param i Particle array index (starts from zero).
     */
	float zb(int i) const { return Zb(i); }

    //! Gets the age [s] of the particle class.
	int age() const { return _age; }

        //update the host data
        void updateHost();
private:

// update device data
	void updateDevice();

        void checkResults(const flexGrid &G, const flexParam &P, const flexField &F);
        
// clear device data
	void clearDevice();

// resize array
	void resize(int n);

// set class pointers
	void setPtr(bool cuda); 
        
        void copyClass(bool fromHostToDevice);

	void wetDeposition(const flexGrid &G, const flexParam &P, const flexField &F);
	void dryDeposition(const flexGrid &G, const flexParam &P, const flexField &F);
        
        void runDispersionKernel(cl_mem G);
        
        void runTrajectoryKernel(Class C, int np, cl_mem G, const flexParam *P, DeviceField *F);
        
        void runRadioactiveKernel(Class C, cl_mem G, const flexParam *P, DeviceField *F);
        
        //pure the unused arguments, and write description
        void dryDepositionMassLoss(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &Ct);

public:
	Class
		//! Class data in host computer memory
		host;
        
        DeviceClass
                //temporary structure used to copy device pointers to device
                *dev_struct;
private:

	flexCount
		C;					// particle count

	flexArr<float>
                Mass,                           //Mass of the particle [Kg]
                Radioactivity,                  //Radioactivity in Bequerels (See note 1)
		X, Y, Z,			// parcel positions [degree, degree, m] (Z above ground, i.e., from 0 to ztop)
		Fg,					// virtical turbulent gradient flag
		Up, Vp, Wp,			// turbulent component of wind
		Um, Vm, Wm,			// mesoscale turbulent component of wind
		Zs, Zb;				// surface and boundary layer 

	flexArr<int>
		Idx,				// random number index
		Gdx;				// grid index
    
        int
		_age,							// class age
                _halflife;                      //halflife of particles
        
        float   _specific_molar_mass;

	long
		_idum, _iy, _iv[32];			// for random number generator

	char
		_id[256];						// class id
        
};

/**
 * Note 1
 * 
 * At present, starting mass of the particles is not given only their type.
 * Consequently, the number recorded here is the bequerels calculated from a %
 * of the original mass, rather than the absolute value:
 * 
 * Normally 
 * 
 * bq = (mass_current/mass_molar) * avogadro * (ln(2) / halflife);
 * 
 * but we calculate the remaining mass after decay (mass_current) as a %, rather 
 * than an absolute value. 
 * 
 * So get the correct balue for Bq, this should be multiplied by the starting 
 * mass.
 */

#endif
