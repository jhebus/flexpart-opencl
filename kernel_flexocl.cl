/**
 * The ONLY opencl file for the flexocl largrangian code.
 * 
 * First an appology. The number of arguments in this code make me sad, so i can
 * only image what you are thinking.
 */

#define INTERPOLATION_KERNEL            ( 0 )       
#define INTERPOLATION_KERNEL_WITH_FLUX  ( 1 )       
#define DISPERSION_KERNEL               ( 2 )
#define TRAJECTORY_KERNEL               ( 3 )
#define CONVECTION_KERNEL               ( 4 )
#define RADIOACTIVE_KERNEL              ( 5 )
#define MASS_KERNEL                     ( 6 )
#define WET_DEPOSITION_KERNEL           ( 7 )
#define DRY_DEPOSITION_MATTER_KERNEL    ( 8 )
#define DRY_DEPOSITION_GAS_KERNEL       ( 9 ) 
#define RELEASE_KERNEL                  ( 10 )
/*==================================================================*/
#define DEBUG 0

#if DEBUG
#define DEBUG_COMMA						,
#define DEBUG_PARAMS					__global int *int_probe, __global float *float_probe
#else
#define DEBUG_COMMA						
#define DEBUG_PARAMS					
#endif
/*==================================================================*/
#define CONVECTION 0

#define USE_SWITCH 0

#ifdef ON_CPU
#define STATIC_INLINE	static inline
#else
#define STATIC_INLINE
#endif
/*==================================================================*/
//enable the use of doubles
//#pragma OPENCL EXTENSION cl_khr_fp64: enable

//constants for calculation
#define AVOGADROS_NUM                   ( 6.022e+23 )

//constants for Interpolation length index
#define INTERP_U_LENGTH             ( 0 )
#define INTERP_PS_LENGTH            ( 8 )

//These are for the convection kernel
#define dmax(a,b) ((a)>(b)?(a):(b))
#define dmin(a,b) ((a)<(b)?(a):(b))
//#define max(a,b) ((a)>(b)?(a):(b))
//#define min(a,b) ((a)<(b)?(a):(b))
#define dabs(a) (fabs((a)))

//To tralsate 3d index into 1d index
#define CONVERT(i, j, k, rows, columns)        ((i * rows * columns) + (j * columns) + k)

float pow_dd(float *a, float *b) { return pow(*a, *b); }

// An attempt to make the function invocation smaller, by hiding the parameters 
// in macros...
//
// PARAMS are passed to functions
// ARGS are used in definitions

#define CLASS_PARAMS    class_np, class_Idx, class_Gdx, class_Mass, class_Radioactivity, class_X,  \
                        class_Y,  class_Z,   class_Fg,  class_Up,   class_Vp, \
                        class_Wp, class_Um,  class_Vm,  class_Wm,   class_Zs, \
                        class_Zb 

#define FIELD_PARAMS    field_U,     field_V,     field_W,   field_Z,   field_Rho,   \
                        field_DRho,  field_T,     field_RH,  field_PBL, field_Tropo, \
                        field_Ustar, field_Wstar, field_Oli, field_Ps 

#define SCALAR_PARAM_PARAMS     param_ctl,                      \
                                param_d_strat,                  \
                                param_d_trop,                   \
                                param_dispersion,               \
                                param_ifine,                    \
                                param_lconv,                    \
                                param_ldirect,                  \
                                param_ldt,                      \
                                param_lensec,                   \
                                param_lflx,                     \
                                param_lsynctime,                \
                                param_ng,                       \
                                param_outsec,                   \
                                param_timing,                   \
                                param_turbmesoscale

#define CLASS_ARGS       int class_np,                          \
                        __global int *class_Idx,                \
                        __global int *class_Gdx,                \
                        __global float *class_Mass,             \
                        __global float *class_Radioactivity,    \
                        __global float *class_X,                \
                        __global float *class_Y,                \
                        __global float *class_Z,                \
                        __global float *class_Fg,               \
                        __global float *class_Up,               \
                        __global float *class_Vp,               \
                        __global float *class_Wp,               \
                        __global float *class_Um,               \
                        __global float *class_Vm,               \
                        __global float *class_Wm,               \
                        __global float *class_Zs,               \
                        __global float *class_Zb

#define FIELD_ARGS      __global float *field_U,                \
                        __global float *field_V,                \
                        __global float *field_W,                \
                        __global float *field_Z,                \
                        __global float *field_Rho,              \
                        __global float *field_DRho,             \
                        __global float *field_T,                \
                        __global float *field_RH,               \
                        __global float *field_PBL,              \
                        __global float *field_Tropo,            \
                        __global float *field_Ustar,            \
                        __global float *field_Wstar,            \
                        __global float *field_Oli,              \
                        __global float *field_Ps

#define SCALAR_PARAM_ARGS       int param_ctl,                  \
                                float param_d_strat,            \
                                float param_d_trop,             \
                                int param_dispersion,           \
                                int param_ifine,                \
                                int param_lconv,                \
                                int param_ldirect,              \
                                int param_ldt,                  \
                                int param_lensec,               \
                                int param_lflx,                 \
                                int param_lsynctime,            \
                                int param_ng,                   \
                                int param_outsec,               \
                                int param_timing,               \
                                int param_turbmesoscale
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
//Duplicated from flexocl.h
typedef struct {
    int
        //! Hybrid grid flag
        hybrid,
        //! Global grid indicator
        gl,
        //! Number of vertical levels
        nl,
        //! Number of latitude grids (data columns)
        nc,
        //! Number of longitude grid (data rows)
        nr,
        //! nxy=nr*nc;
        nxy,
        //! number of seconds between field datasets
        ns;
    float
        //! Start longitude [deg]
        x0,
        //! End longitude [deg]
        x1,
        //! Longitude grid [deg]
        dx,
        //! Start latitude [deg]
        y0,
        //! End latitude [deg]
        y1,
        //! Latitude grid [deg]
        dy;
    float
        //! Pressure levels [Pa]
        P[256],
        //! half pressure level parameters [P=A+B*Ps]
        A[256], B[256];
} Grid;
/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/
typedef struct {
    float
        //! Turbulent diffusivity for horizontal components in the troposphere [m2/s]
        d_trop,
        //! Turbulent diffusivity for vertical component in the stratosphere [m2/s]
        d_strat,
        //! Factor by which standard deviations of winds at grid points surrounding
        //! particle positions are scaled to yield the scales for the mesoscale wind velocity fluctuations  
        turbmesoscale;

    int
        //! Mode flag: 1 for forward and -1 for backward simulation
        ldirect,
        //! Synchronisation time of all particles [s]
        lsynctime,
        //! Maximum time step for integration [s]
        ldt,
        //! Fine loops for integration of turbulent wind (must >1)
        ifine,
        //! Time step control factor
        ctl,
        //! Moist convection flag: 1 for yes or 0 no.
        lconv,
        //! Flux interpolation flag: 1 for yes or 0 no.
        lflx,
        //! Size of Gauss random data array
        ng;
    int
        //! Output interval [s]
        outsec,
        //! Trajectory length [s]
        lensec,
        //! NVIDIA device flag: 1 for yes or 0 for no
        openCL,
        //! Number of threads for using OpenMP
        openmp,
        //! Performance timing flag: 1 for yes or 0 for no.
        timing,
        // Dispersion flag: 1 for yes or 0 for no.
        dispersion; 
} DeviceParameter;

/*----------------------------------------------------------------------------*/
 /*Grid indexes
 <pre>
    f4               f3
 (i2,j1)-----------(i2,j2)
    |                 |
    |                 |
    |                 |
    |                 |
 (i1,j1)-----------(i1,j2)
   f1                f2
 </pre>
 */
typedef struct {
	int
		i1,
		//! Latitude grid indexes
		i2,
		j1,
		//! Longitude grid indexes
		j2;
	int
		k1, k2, k3,
		//! Vertical grid indexes
		k4;
	float
		f1, f2, f3,
		//! Horizontal interpolation factors
		f4;
	float
		fa1, fa2, fa3, fa4,
		fb1, fb2, fb3,
		//! Vertical interpolation factors
		fb4;
} Factor;
/*----------------------------------------------------------------------------*/
typedef struct {
	float
		sigu, sigv, sigw, dsigwdz, dsigw2dz, tlu, tlv, tlw;
} Turb;
/*----------------------------------------------------------------------------*/
typedef struct {
	float
		x, y;  //latitude/longitude data
} XY;
/*----------------------------------------------------------------------------*/
#define NA 70		// this must equal NA in convect43c.f
#define NTHR 16

/**
 Structures for moist convection
 */
typedef struct {
    float
        FSUM[NA],
        ZIJ[NA],
        MASS[NA],
        SUB[NA],
        FMASS[NA*NA],
        FRAC[NA*NA],
        MENT[NA*NA],
        QENT[NA*NA],
        ELIJ[NA*NA],
        SIJ[NA*NA];
} Convect;

/**
 Structures for moist convection
 */
typedef struct {
    Convect cpa[NTHR];
} ConvectArray;
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/********************* FUNCTIONS PROTOTYPES ***********************************/
/******************************************************************************/
STATIC_INLINE Factor  index_f( __global Grid *g, FIELD_ARGS, float x, float y, float z DEBUG_COMMA DEBUG_PARAMS);

STATIC_INLINE Factor  zindex_f( __global Grid *g, FIELD_ARGS, float z, int i, int j);

/*----------------------------------------------------------------------------*/
STATIC_INLINE float   interpo2d_f( Factor *f,  __global Grid *g,  __global float* v);
STATIC_INLINE float   interpo3d_f( Factor *f,  __global Grid *g,  __global float* v, int k);

STATIC_INLINE Turb    hanna_f(float z, float h, float ol, float ust, float wst);

STATIC_INLINE XY      xy2ll_f( __global Grid *g, float lon, float lat, float dx, float dy, float zt);

STATIC_INLINE float   hq_f(float P/*[Pa]*/, float T/*[K]*/, float Hum/*[%]*/);

STATIC_INLINE float   field3d_f(Factor *f, __global Grid *g, __global float* v);
STATIC_INLINE float   reflect_f(float z, float zmax);
STATIC_INLINE float   std3d_f(Factor f, __global Grid *g, __global float* v);
STATIC_INLINE float   wvp_f(float T/*[K]*/, float Hum/*[%]*/);


/*------Main functions-----------------*/
STATIC_INLINE 
void    dispersion_function(    int index, 
                                CLASS_ARGS,
                                __global Grid *G,
                                SCALAR_PARAM_ARGS,
                                __global float *param_gauss,
                                FIELD_ARGS
                                DEBUG_COMMA DEBUG_PARAMS);
STATIC_INLINE 
void    trajectory_function(    int index, CLASS_ARGS,
                                __global Grid *G,
                                SCALAR_PARAM_ARGS,                        
                                __global float *param_gauss,
                                FIELD_ARGS
                                DEBUG_COMMA DEBUG_PARAMS);

#if CONVECTION
STATIC_INLINE 
void    convection_function(int index, CLASS_ARGS,        
                                __global Grid *G,
                                SCALAR_PARAM_ARGS,
                                __global float *param_gauss,
                                __global float *param_rand,
                                FIELD_ARGS, 
                                __global int *Sdx, 
                                __global int *Pdx, 
                                __global float *CBMF
                                DEBUG_COMMA DEBUG_PARAMS);
#endif

//this is not used as it is faster on 
STATIC_INLINE 
void    mass_function(  int index,
                        __global Grid *G, 
                        FIELD_ARGS,
                        __global float *mass_X,
                        __global float *mass_Y, 
                        __global float *mass_Z,
                        __global float *mass_M,
                        int mass_x_len,
                        int mass_y_len, 
                        int mass_z_len, 
                        int mass_m_len,  
                        int mass_m_rows,
                        int mass_m_column,
                        float mass_deg DEBUG_COMMA DEBUG_PARAMS);
/******************************************************************************/
/************************ THE KERNEL ******************************************/
/******************************************************************************/
__kernel void super_kernel( 
                                //which kernel to run 1
                                int kernel_choice  
                                DEBUG_COMMA DEBUG_PARAMS,
                                      
                                //for interpolation 84
                                float interp_mul1,
                                float interp_mul2,
                                __global int *interp_lengths,
                                __global float *interp_U_f1,
                                __global float *interp_U_f2,
                                __global float *interp_V_f1,
                                __global float *interp_V_f2,
                                __global float *interp_W_f1,
                                __global float *interp_W_f2,
                                __global float *interp_Z_f1,
                                __global float *interp_Z_f2,
                                __global float *interp_T_f1,
                                __global float *interp_T_f2,
                                __global float *interp_RH_f1,
                                __global float *interp_RH_f2,
                                __global float *interp_RHO_f1,
                                __global float *interp_RHO_f2,
                                __global float *interp_DRHO_f1,
                                __global float *interp_DRHO_f2,
                                __global float *interp_PS_f1,
                                __global float *interp_PS_f2,
                                __global float *interp_PBL_f1,
                                __global float *interp_PBL_f2,
                                __global float *interp_TROPO_f1,
                                __global float *interp_TROPO_f2,
                                __global float *interp_USTAR_f1,
                                __global float *interp_USTAR_f2,
                                __global float *interp_WSTAR_f1,
                                __global float *interp_WSTAR_f2,
                                __global float *interp_OLI_f1,
                                __global float *interp_OLI_f2,
                                
                                //the grid 1
                                __global Grid *g,
        
                                //the parameters 3
                                __global float *param_gauss,
                                __global float *param_rand,
                                SCALAR_PARAM_ARGS,
        
                                //Class Struct 16
                                CLASS_ARGS,
        
                                //Field struct 14
                                FIELD_ARGS,

                                //For convection 3
                                //__global int *conv_pdx,
                                //__global int *conv_sdx,
                                //__global float *conv_cbmf,        
                                //int conv_segments,
        
                                //for mass
                                //__global float *mass_x,        
                                //__global float *mass_y,       
                                //__global float *mass_z,        
                                //__global float *mass_m,        
                                //int mass_x_len,
                                //int mass_y_len, 
                                //int mass_z_len,
                                //int mass_m_len,
                                //int mass_m_rows,
                                //int mass_m_columns,
                                //float mass_deg, 

                                //radiation
                                int halflife,
                                float species_molar_weight,
                                float rad_const_1,
                                float rad_const_2
                                                                ){
    int i = get_global_id(0);

#if USE_SWITCH
    switch(kernel_choice){
        case INTERPOLATION_KERNEL:
#else
	if(kernel_choice == INTERPOLATION_KERNEL){
#endif
            if(i < interp_lengths[INTERP_U_LENGTH]){		
                field_U[i]     = (interp_mul1 * interp_U_f1[i]) + (interp_mul2 * interp_U_f2[i]);
                field_V[i]     = (interp_mul1 * interp_V_f1[i]) + (interp_mul2 * interp_V_f2[i]);
                field_W[i]     = (interp_mul1 * interp_W_f1[i]) + (interp_mul2 * interp_W_f2[i]);
                field_Z[i]     = (interp_mul1 * interp_Z_f1[i]) + (interp_mul2 * interp_Z_f2[i]);
                field_T[i]     = (interp_mul1 * interp_T_f1[i]) + (interp_mul2 * interp_T_f2[i]);
                field_RH[i]    = (interp_mul1 * interp_RH_f1[i]) + (interp_mul2 * interp_RH_f2[i]);
                field_Rho[i]   = (interp_mul1 * interp_RHO_f1[i]) + (interp_mul2 * interp_RHO_f2[i]);
                field_DRho[i]  = (interp_mul1 * interp_DRHO_f1[i]) + (interp_mul2 * interp_DRHO_f2[i]);

                 //these arrays are smaller than the previous arrays (but the same size)
                if( i < interp_lengths[INTERP_PS_LENGTH]){
                    field_Ps[i]    = (interp_mul1 * interp_PS_f1[i]) + (interp_mul2 * interp_PS_f2[i]);
                    field_PBL[i]   = (interp_mul1 * interp_PBL_f1[i]) + (interp_mul2 * interp_PBL_f2[i]);
                    field_Tropo[i] = (interp_mul1 * interp_TROPO_f1[i]) + (interp_mul2 * interp_TROPO_f2[i]);                
                }
            }
#if USE_SWITCH
            break;
        case INTERPOLATION_KERNEL_WITH_FLUX:
#else
	}
	else if(kernel_choice == INTERPOLATION_KERNEL){
#endif
	        if(i < interp_lengths[INTERP_U_LENGTH]){
                field_U[i]     = (interp_mul1 * interp_U_f1[i]) + (interp_mul2 * interp_U_f2[i]);
                field_V[i]     = (interp_mul1 * interp_V_f1[i]) + (interp_mul2 * interp_V_f2[i]);
                field_W[i]     = (interp_mul1 * interp_W_f1[i]) + (interp_mul2 * interp_W_f2[i]);
                field_Z[i]     = (interp_mul1 * interp_Z_f1[i]) + (interp_mul2 * interp_Z_f2[i]);
                field_T[i]     = (interp_mul1 * interp_T_f1[i]) + (interp_mul2 * interp_T_f2[i]);
                field_RH[i]    = (interp_mul1 * interp_RH_f1[i]) + (interp_mul2 * interp_RH_f2[i]);
                field_Rho[i]   = (interp_mul1 * interp_RHO_f1[i]) + (interp_mul2 * interp_RHO_f2[i]);
                field_DRho[i]  = (interp_mul1 * interp_DRHO_f1[i]) + (interp_mul2 * interp_DRHO_f2[i]);

                 //these arrays are smaller than the previous arrays (but the same size)
                if( i < interp_lengths[INTERP_PS_LENGTH]){
                    field_Ps[i]    = (interp_mul1 * interp_PS_f1[i]) + (interp_mul2 * interp_PS_f2[i]);
                    field_PBL[i]   = (interp_mul1 * interp_PBL_f1[i]) + (interp_mul2 * interp_PBL_f2[i]);
                    field_Tropo[i] = (interp_mul1 * interp_TROPO_f1[i]) + (interp_mul2 * interp_TROPO_f2[i]);                       

                    //Flux
                    field_Ustar[i] = (interp_mul1 * interp_USTAR_f1[i]) + (interp_mul2 * interp_USTAR_f2[i]);
                    field_Wstar[i] = (interp_mul1 * interp_WSTAR_f1[i]) + (interp_mul2 * interp_WSTAR_f2[i]);
                    field_Oli[i]   = (interp_mul1 * interp_OLI_f1[i]) + (interp_mul2 * interp_OLI_f2[i]);
                }
            }
#if USE_SWITCH
            break;
        case DISPERSION_KERNEL:
#else
	}
	else if(kernel_choice == DISPERSION_KERNEL){
#endif
            if( i < class_np){
               dispersion_function(i, CLASS_PARAMS, g, SCALAR_PARAM_PARAMS, param_gauss, FIELD_PARAMS DEBUG_COMMA DEBUG_PARAMS);
            }
#if USE_SWITCH
            break;
#endif
#if 0
        case TRAJECTORY_KERNEL:
            if(i < class_np){
                trajectory_function(i, CLASS_PARAMS, g, SCALAR_PARAM_PARAMS, param_gauss,  FIELD_PARAMS DEBUG_COMMA DEBUG_PARAMS);
            }
            break;
        case MASS_KERNEL:
            //if(i < mass_y_len){
            //    mass_function(i, g, FIELD_PARAMS, mass_x, mass_y, mass_z, mass_m, mass_x_len, mass_y_len, mass_z_len, mass_m_len, mass_m_rows, mass_m_columns, mass_deg DEBUG_COMMA DEBUG_PARAMS);
            //}
            break;
        case RELEASE_KERNEL:
            if(i < class_np){
                /*
                float dx, dy, dxy = ds+1.0f;
		while (dxy > ds) {
			dx  = (ran2(_idum, _iy, _iv, 32) - 0.5) * 2.0 * ds;
			dy  = (ran2(_idum, _iy, _iv, 32) - 0.5) * 2.0 * ds;
			dxy = sqrt(dx * dx + dy * dy);
		}
		class_Z[i] = z0 + ran2(_idum,_iy,_iv,32) * dz;
		XY xy      = xy2ll_f(&G,x0,y0,dx,dy,class_Z[i]);
		class_X[k] = xy.x;
		class_Y[i] = xy.y;
                */ 
            }
            break;
        case RADIOACTIVE_KERNEL:
            // constants are calculated on the cpu and passed to save time
            if( i < class_np){
                //float half_life_idoine-131 = 60 * 60 * 24 * 8.0197;                  //wikipedia
                //float half_life_ceasium-137 = 60 * 60 * 24 * 7 * 365.25 * 30.17      //wikipedia
                
                //first the new mass
                class_Mass[i] = class_Mass[i] * rad_const_1;// * exp((-param_ldt * log(2.0)) / halflife);
                
                //then the value in Bequerels (this value is a % of the final value. It should be multiplied by the initial mass to get the correct result)
                class_Radioactivity[i] = (class_Mass[i]/species_molar_weight) * rad_const_2;// * AVOGADROS_NUM * (ln(2) / halflife);
            }
            break;
        case WET_DEPOSITION_KERNEL:
            break;
        case DRY_DEPOSITION_MATTER_KERNEL:
            break;
        case DRY_DEPOSITION_GAS_KERNEL:
            break;
        case CONVECTION_KERNEL:
#if CONVECTION
            if(i > 1 && i < conv_segments){
                convection_function(i, CLASS_PARAMS, g, SCALAR_PARAM_PARAMS, param_gauss, param_rand,
                                     FIELD_PARAMS,                                        
                                    conv_sdx, 
                                    conv_pdx, 
                                    conv_cbmf
                                    DEBUG_COMMA DEBUG_PARAMS);
            }
#endif
            break;               
#endif // unused kernel functions


    }
}
/******************************************************************************/
/********************* FUNCTIONS DEFINTIONS ***********************************/
/******************************************************************************/
/*----------------------------------------------------------------------------*/
//this is also used for dispersion, so can possibly be optimised
STATIC_INLINE 
void    dispersion_function(int idx, CLASS_ARGS,
                            __global Grid *G,
                            SCALAR_PARAM_ARGS,
                            __global float *param_Gauss,
                            FIELD_ARGS
                            DEBUG_COMMA DEBUG_PARAMS){
        
    
    
	int k   = class_Idx[idx] % param_ng;
	if (k<0) k=0;

	
// particle position
	float x0 = class_X[idx];
	float y0 = class_Y[idx];
	float z0 = class_Z[idx];
	float xt = x0;
	float yt = y0;
	float zt = z0;
        
// turbulent wind components
	float up = class_Up[idx];          //along wind ??
	float vp = class_Vp[idx];          //cross wind??
	float wp = class_Wp[idx];          //vertical comp of turbulance ???

// time steps
	float dt  = param_ldt;             //Max time step for integration
	float dtf = dt/param_ifine;        //Number of fine itterations allowed per time step
	int   sdt = param_ldt;

	
         
// interpolation factors
	Factor fc = index_f(G, FIELD_PARAMS, xt, yt, zt DEBUG_COMMA DEBUG_PARAMS);
    
      
    //should/can this be check prior to deployment???
	if (fc.i1 < 0 || fc.j1 < 0){
            //int_probe[idx] = 64;
            return;
        }

	int     ldt;
	float   u, v, w;
	float   cosphi, sinphi;
	float   r, zsfc, zpbl, ztop, trop;
	float   dx  = 0.0f,
                dy  = 0.0f,
                d_along_wind = 0.0f,                    //delta along wind
                d_cross_wind = 0.0f;                    //delta cross wind
	XY      xy;
		
//for the specified time
	for (ldt=0; ldt < param_lsynctime; ldt += sdt) {
// boundaries
		zsfc = interpo3d_f(&fc, G, field_Z, 0);
		ztop = interpo3d_f(&fc, G, field_Z, G->nl-1) - zsfc;
		zpbl = interpo2d_f(&fc, G, field_PBL);
		trop = interpo2d_f(&fc, G, field_Tropo);
                
// winds
		u = field3d_f(&fc, G, field_U);
		v = field3d_f(&fc, G, field_V);
		w = field3d_f(&fc, G, field_W);
		
		if (param_ctl > 0) {
// re-estimate time step for integration
			sdt     = (int) (zpbl/(1.e-10+param_ctl*fabs(w)));
                        
			if (sdt<1)                      sdt = 1;
			if (sdt>param_ldt)                 sdt = param_ldt;
			if (sdt>param_lsynctime-ldt)       sdt = param_lsynctime - ldt;
                        
			dt      = sdt;
			dtf     = sdt/param_ifine;
		}

		dx += u * dt * param_ldirect;
		dy += v * dt * param_ldirect;
		zt += w * dt * param_ldirect;

		if (zt >= zpbl) {
// z goes out of PBL, complete the current interval above PBL.
			ldt += sdt;
			break;
		}

		if (zt < 0.0f) zt = -zt;

                
// turbulent components
		float oli                       = interpo2d_f(&fc, G, field_Oli);
		float friction_velocity         = interpo2d_f(&fc, G, field_Ustar);
		float convection_velocity       = interpo2d_f(&fc, G, field_Wstar);
		float rhoaux                    = field3d_f(&fc, G, field_DRho) / (1.e-10f + field3d_f(&fc, G, field_Rho));

		Turb tb = hanna_f(zt, zpbl, oli, friction_velocity, convection_velocity);

// Langevin horizontal wind component
		r = dt/tb.tlu;
                int ndx_gauss; 
		if (r < 0.5f) {
                    ndx_gauss = ++k % param_ng;
                    up = (1.0f-r) * up + param_Gauss[ndx_gauss] * tb.sigu * sqrt(2.0f * r);
		}
		else {
                    r  = exp(-r);
                    up = r * up + param_Gauss[++k % param_ng] * tb.sigu * sqrt(1.0f-r * r);
		}

		r = dt / tb.tlv;
		if (r < 0.5f) {
			vp = (1.0f-r) * vp + param_Gauss[++k % param_ng] * tb.sigv * sqrt(2.0f * r);
		}
		else {
			r  = exp(-r);
			vp = r * vp + param_Gauss[++k % param_ng] * tb.sigv * sqrt(1.0f-r * r);
		}

		d_along_wind += up * dt;
		d_cross_wind += vp * dt;


// Langevin vertical wind component
		float cbt = class_Fg[idx];
		int ifine;


                //can we skip the first loop?
		for (ifine=0; ifine < param_ifine; ifine++) {

			if (ifine > 0) {
                            tb = hanna_f(zt, zpbl, oli, friction_velocity, convection_velocity);

                        }

			r = dtf/tb.tlw;
			if (r < 0.5f) {
				wp = cbt * ((1.0f-r) * wp + param_Gauss[++k % param_ng] * sqrt(2.f * r) + dtf * (tb.dsigwdz + rhoaux * tb.sigw));
			}
			else {
				r = exp(-r);
				wp = cbt * (r * wp + param_Gauss[++k % param_ng] * sqrt(1.f - r * r ) + tb.tlw * (1.f - r) * (tb.dsigwdz + rhoaux * tb.sigw));
			}

			zt += wp * tb.sigw * dtf;

			if (zt<0.f || zt>zpbl) {
				cbt=-1.0f;
			}
			else {
				cbt=1.0f;
			}
   
			zt = reflect_f(zt,zpbl);

		}

                //set the reflection flag?
		class_Fg[idx] = cbt;


		fc=index_f(G, FIELD_PARAMS, xt,yt,zt DEBUG_COMMA DEBUG_PARAMS);

            
                 //should this be check prior to deployment???
		if (fc.i1<0 || fc.j1<0) {
			class_X[idx]=x0;
			class_Y[idx]=y0;
			class_Z[idx]=z0;
			return;
		}
	}
        




// save random disturbances

        
        
	class_Up[idx]=up;
	class_Vp[idx]=vp;
	class_Wp[idx]=wp;

// convert along/cross wind dispplacement to dx and dy

	r       = max(1.e-5f, sqrt(dx*dx+dy*dy));
	cosphi  = dx/r;
	sinphi  = dy/r;

	dx += cosphi * d_along_wind - sinphi * d_cross_wind;
	dy += sinphi * d_along_wind + cosphi * d_cross_wind;

// advance to next sync time for particle outside mixing layer

	if (ldt < param_lsynctime) {
		dt=param_lsynctime-ldt;

		if (zt<trop) {
			r       = sqrt(2.f *param_d_trop/dt);
			up      = param_Gauss[++k % param_ng] * r;
			vp      = param_Gauss[++k % param_ng] * r;
//			wp=0.0f;
			// different from flexpart: vertical perturbation even in troposphere.
			r       = sqrt(2. * param_d_trop * 0.01/dt);
			wp      = param_Gauss[++k % param_ng] * r;
		}
		else if (zt < trop + 1000.0f) {
			float weight    = (zt-trop)/1000.0f;
			r               = sqrt(2.f * param_d_trop * (1.-weight) /dt);
			up              = param_Gauss[++k % param_ng] * r;
			vp              = param_Gauss[++k % param_ng] * r;
//			r=sqrt(2.*param_d_strat*weight/dt);
			// different from flexpart.
			r               = sqrt(2.f * param_d_strat*weight/dt)+sqrt(2.*param_d_trop*0.01*(1.-weight)/dt);
			wp              = param_Gauss[++k % param_ng] * r;
		}
		else {
			up      = 0.0f;
			vp      = 0.0f;
			r       = sqrt(2.*param_d_strat/dt);
			wp      = param_Gauss[++k % param_ng] * r;
		}

		dx += (u+up) * dt * param_ldirect;
		dy += (v+vp) * dt * param_ldirect;
		zt += (w+wp) * dt * param_ldirect;

	}
    
           
// add mesoscale random disturbances

	r               = exp(-2.f*param_lsynctime/G->ns);
	float rs        = sqrt(1.-r*r);

	float sig       = std3d_f(fc,G,field_U); 
	class_Um[idx]   = r * class_Um[idx] + rs * param_Gauss[++k % param_ng] * sig * param_turbmesoscale;

	sig             = std3d_f(fc,G,field_V);
	class_Vm[idx]   = r * class_Vm[idx] + rs * param_Gauss[++k % param_ng] * sig * param_turbmesoscale;

	sig             = std3d_f(fc,G,field_W);
	class_Wm[idx]   = r * class_Wm[idx] + rs * param_Gauss[++k % param_ng] * sig * param_turbmesoscale;
	
	dx             += class_Um[idx] * param_lsynctime * param_ldirect;
	dy             += class_Vm[idx] * param_lsynctime * param_ldirect;
	zt             += class_Wm[idx] * param_lsynctime * param_ldirect;

 
	zt              = reflect_f(zt,ztop);

	xy              = xy2ll_f(G, xt, yt, dx, dy, zt + zsfc);
	xt              = xy.x;
	yt              = xy.y;

// save coordinates
   
	class_X[idx]   = xt;
	class_Y[idx]   = yt;
	class_Z[idx]   = zt;
 	class_Zs[idx]  = zsfc;
	class_Zb[idx]  = zpbl;
	class_Idx[idx] = k;
                       
// update grid index
   
	if (param_lconv != 0) {
		int i = (int) ((yt + 0.5 * G->dy - G->y0) / G->dy);
		int j = (int) ((xt + 0.5 * G->dx - G->x0) / G->dx);
                
#if defined(__FLEX__) || 1
		if (i<0 || j<0){
                    //int_probe[0] = xt;
                    return;//TODO SHOULD CHECK BEFORE KERENL LAUNH errmsg("Bad grid index!");
                } 
#endif
		if (i>=G->nr) i=G->nr-1;
		if (j>=G->nc) j=G->nc-1;
                
		class_Gdx[idx] = i * G->nc + j;
	}
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/** Advances one synchronize time step for trjaectory calculation using mean wind fields.
 @param C Class object.
 @param idx Particle index.
 @param G Grid object.
 @param P Parameter object.
 @param F Field obejct.
 */
STATIC_INLINE void trajectory_function(int idx, CLASS_ARGS,
                            __global Grid *G,
                            SCALAR_PARAM_ARGS,
                            __global float *param_Gauss,
                            FIELD_ARGS
                            DEBUG_COMMA DEBUG_PARAMS)
{
    float dt = param_ldt;
    int  sdt = param_ldt;

    float x0 = class_X[idx];
    float y0 = class_Y[idx];
    float z0 = class_Z[idx];

    for (int ldt = 0; ldt < param_lsynctime; ldt += sdt) {
// first guess, save winds.
        float xt = class_X[idx];
        float yt = class_Y[idx];
        float zt = class_Z[idx];
        
        
        Factor fc = index_f(G, FIELD_PARAMS, xt,yt,zt DEBUG_COMMA DEBUG_PARAMS);

        //can i ditch this???
        if (fc.i1<0 || fc.j1<0) {
            class_X[idx] = x0;
            class_Y[idx] = y0;
            class_Z[idx] = z0;
            return;
        }

        float zs        = interpo3d_f(&fc, G, field_Z,0);
        float zb        = interpo2d_f(&fc, G, field_PBL);
        float ztop      = interpo3d_f(&fc, G, field_Z,G->nl-1)-zs;

        float u = field3d_f(&fc, G, field_U);
        float v = field3d_f(&fc, G, field_V);
        float w = field3d_f(&fc, G, field_W);

        if (param_ctl > 0) {
// re-estimate time step for integration
            sdt = (int) (zb/(1.e-10+param_ctl*fabs(w)));
            if (sdt < 1)                     sdt = 1;
            if (sdt > param_ldt)             sdt = param_ldt;
            if (sdt > param_lsynctime - ldt) sdt = param_lsynctime-ldt;
            
            dt = sdt;
        }
        
        XY xy    = xy2ll_f(G, xt, yt, dt * u * param_ldirect, dt * v * param_ldirect, zs + zt);
        float ze = reflect_f(zt + dt * w * param_ldirect, ztop);

// final position
        fc = index_f(G, FIELD_PARAMS, xy.x ,xy.y, ze DEBUG_COMMA DEBUG_PARAMS);

        //can i dtich this???? NO CREATE AN ERROR Flag!!!
        if (fc.i1<0 || fc.j1<0) {
            class_X[idx] = x0;
            class_Y[idx] = y0;
            class_Z[idx] = z0;
            return;
        }

        u = 0.5 * (u + field3d_f(&fc, G, field_U));
        v = 0.5 * (v + field3d_f(&fc, G, field_V));
        w = 0.5 * (w + field3d_f(&fc, G, field_W));

        xy = xy2ll_f(G, xt, yt, dt * u * param_ldirect, dt * v * param_ldirect, zs + zt);
        zt = reflect_f(zt + dt * w * param_ldirect, ztop);

        class_X[idx]    = xy.x;
        class_Y[idx]    = xy.y;
        class_Z[idx]    = zt;
        class_Zs[idx]   = interpo3d_f(&fc, G, field_Z, 0);
        class_Zb[idx]   = interpo2d_f(&fc, G, field_PBL);
    }
}
/*----------------------------------------------------------------------------*/
// this is parallelised on the length of y
STATIC_INLINE void    mass_function(  int index, __global Grid *G, FIELD_ARGS,
                        __global float *mass_X,
                        __global float *mass_Y, 
                        __global float *mass_Z,
                        __global float *mass_M,
                        int mass_x_len,
                        int mass_y_len, 
                        int mass_z_len, 
                        int mass_m_len,  
                        int mass_m_rows,
                        int mass_m_columns,
                        float mass_deg DEBUG_COMMA DEBUG_PARAMS){
    
    for (int j = 0; j< mass_x_len; j++) {
        float x         = mass_X[j] + mass_deg/2;
        float y         = mass_Y[index] + mass_deg/2;
        Factor f        = index_f(G, FIELD_PARAMS, x, y, 0.0 DEBUG_COMMA DEBUG_PARAMS);
        if (f.i1 < 0) continue;
        float p0        = interpo2d_f(&f, G, field_Ps);
        float z0        = interpo3d_f(&f, G, field_Z, 0);
        float zl        = interpo3d_f(&f, G, field_Z, G->nl - 1) - z0;
        mass_M[CONVERT(0, index, j, mass_m_rows, mass_m_columns)] = 0;
        
        for (int k = 1; k < mass_z_len; k++) {
                if (mass_Z[k] >= zl) {
                        mass_M[CONVERT(k, index, j, mass_m_rows, mass_m_columns)] = p0/9.81;
                }
                else {
                        for (int l = 1; l < G->nl ; l++) {
                                float z2 = interpo3d_f(&f, G, field_Z, l) - z0;
                                if (z2 >= mass_Z[k]) {
                                        float z1 = interpo3d_f(&f, G, field_Z, l - 1) - z0;
                                        float a = fabs((z2 - mass_Z[k]) / (z2 - z1));
                                        float b = fabs((z1 - mass_Z[k]) / (z2 - z1));
                                        float p;
                                        if (G->hybrid) {
                                                float p1 = G->A[l-1] + G->B[l-1] * p0;
                                                float p2 = G->A[l]   + G->B[l]   * p0;
                                                float p3;
                                                if (l < G->nl-1) {
                                                        p3 = G->A[l+1] + G->B[l+1] * p0;
                                                }
                                                else {
                                                        p3 = 0.0;
                                                }
                                                p1 = 0.5 * (p1 + p2);
                                                p2 = 0.5 * (p2 + p3);
                                                p  = a * log(p1) + b * log(p2);
                                        }
                                        else {
                                                p  = a * log(G->P[l-1]) + b * log(G->P[l]);
                                        }
                                        p = exp(p);
                                        mass_M[CONVERT(k, index, j, mass_m_rows, mass_m_columns)] = (p0-p)/9.81;
                                        break;
                                }
                        }
                }
        }
        if(mass_M[CONVERT(mass_z_len - 1, index, j, mass_m_rows, mass_m_columns)] < mass_M[CONVERT(mass_z_len - 2, index, j, mass_m_rows, mass_m_columns)]){ 
           mass_M[CONVERT(mass_z_len - 2, index, j, mass_m_rows, mass_m_columns)] = mass_M[CONVERT(mass_z_len - 1, index, j, mass_m_rows, mass_m_columns)];
        }
    }
}
/*----------------------------------------------------------------------------*/
/** Finds x-y grid indices and calculates interpolation factors (z refer to surface)
 @param g Grid object.
 @param f Field object.
 @param x Longitude.
 @param y Latitude.
 @param z Altitude.
 */
STATIC_INLINE 
Factor index_f( __global Grid *g, FIELD_ARGS, float x, float y, float z DEBUG_COMMA DEBUG_PARAMS)
{
	Factor fc, f2;

	fc.j1= (int) ((x-g->x0)/g->dx);
	fc.j2=fc.j1+1;
	
        
        
	if (fc.j1<0 || fc.j2>=g->nc) {
		// x goes out of range
		if (!g->gl) {
			// for non-global grid, return negative index.
			fc.i1=-1;
			fc.j1=-1;
			return fc;
		}
		// it must be embraced by the two end grids along x-axis.
		fc.j1=g->nc-1;
		fc.j2=0;
	}

	float r=fabs((x-(g->x0+fc.j1*g->dx))/g->dx);

	fc.i1= (int) ((y-g->y0)/g->dy);
	fc.i2=fc.i1+1;

	if (fc.i1<0 || fc.i2>=g->nr) {
		if (!g->gl) {
			fc.i1=-1;
			fc.j1=-1;
			return fc;
		}
#ifdef __FLEX__
//		errmsg("i-index problem");
#endif
		fc.i1=0;
		fc.i2=1;
	}

	float s=fabs((y-(g->y0+fc.i1*g->dy))/g->dy);

	fc.f1=(1.0f-r)*(1.0f-s);
	fc.f2=r*(1.0f-s);
	fc.f3=r*s;
	fc.f4=(1.0f-r)*s;

// vertical indexes and factors

	f2=zindex_f(g, FIELD_PARAMS, z,fc.i1,fc.j1);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k1=f2.k1;
	fc.fa1=f2.fa1;
	fc.fb1=f2.fb1;

	f2=zindex_f(g, FIELD_PARAMS, z,fc.i1,fc.j2);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k2=f2.k1;
	fc.fa2=f2.fa1;
	fc.fb2=f2.fb1;

	f2=zindex_f(g, FIELD_PARAMS, z, fc.i2,fc.j2);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k3=f2.k1;
	fc.fa3=f2.fa1;
	fc.fb3=f2.fb1;

	f2=zindex_f(g, FIELD_PARAMS, z,fc.i2,fc.j1);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k4=f2.k1;
	fc.fa4=f2.fa1;
	fc.fb4=f2.fb1;

	return fc;
}
/*----------------------------------------------------------------------------*/
/** Interpolates 2D field data
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 */
STATIC_INLINE 
float interpo2d_f( Factor *f,  __global Grid *g, __global float* v)
{
	int n1  = f->i1 * g->nc;
	int n2  = n1 + g->nc;
	float d = f->f1 * v[n1 + f->j1] +
                  f->f2 * v[n1 + f->j2]+
                  f->f3 * v[n2 + f->j2]+
                  f->f4 * v[n2 + f->j1];
        
	return d;
}
/*----------------------------------------------------------------------------*/
/** Interpolates 3D field data on a level.
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 @param k Level index of v.
 */
STATIC_INLINE 
float interpo3d_f( Factor *f, __global Grid *g, __global float* v, int k)
{
	int n1  = k * g->nxy + f->i1 * g->nc;
	int n2  = n1 + g->nc;
        
	float d = f->f1 * v[n1 + f->j1] +
			  f->f2 * v[n1 + f->j2] +
			  f->f3 * v[n2 + f->j2] +
			  f->f4 * v[n2 + f->j1];
	return d;
}
/*----------------------------------------------------------------------------*/
//! Hanna function. Refer to hanna.f of flexpart6.4-gfs version
/*
 * Parameterisation of wind fluctuations - Section 7.4 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
 * the acronyms are explained in that doc
 */
STATIC_INLINE 
Turb hanna_f(float z, float h, float L, float ustar, float wstar)
{
	Turb t;

	if ((h / fabs(L)) < 1.0) {
// Neutral conditions
            ustar               = max(ustar, 1.e-4f);
            float corr          = z/ustar;
            
            //eqn 26 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
            t.sigu              = 1.e-2f + 2.0f * ustar * exp(-3.e-4f * corr);
            
            //eqn 27 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
            t.sigw              = 1.3f * ustar * exp(-2.e-4f * corr);
            t.dsigwdz           = -2.e-4f * t.sigw;
            t.sigw              = t.sigw + 1.e-2f;
            t.sigv              = t.sigw;
            
            //eqn 28 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
            t.tlu               = (0.5f * z/t.sigw) / (1.0f + 1.5e-3f * corr);
            t.tlv               = t.tlu;
            t.tlw               = t.tlu;
	}
	else if (L < 0.0f) {
// Unstable conditions
		float zeta      = z/h;
                
                //??????
		t.sigu          = 1.e-2f + ustar * pow(12.0f - 0.5f * h/L, 0.33333f);
		t.sigv          = t.sigu;
                
                //eqn 22 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
		t.sigw          = sqrt(1.2f * wstar * wstar * (1.0f - 0.9f * zeta) * pow(zeta, 0.66666f) + (1.8f - 1.4f * zeta) * ustar * ustar) + 1.e-2f;
		t.dsigwdz       = 0.5f/t.sigw/h * (-1.4f * ustar * ustar + wstar * wstar * (0.8f * pow(max(zeta, 1.e-3f), -.33333f) - 1.8f * pow(zeta, 0.66666f)));
                
                //eqn 21 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
		t.tlu           = 0.15f * h/t.sigu;
                t.tlv           = t.tlu;
                
		if (z < fabs(L)){
                    //eqn 23 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
                    t.tlw   = 0.1f * z/(t.sigw * (0.55f - 0.38f * fabs(z/L)));
                }
		else if (zeta<0.1f){
                    //eqn 24 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
                    t.tlw   = 0.59f * z/t.sigw;
                }
		else{
                    //eqn 25 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
                    t.tlw   = 0.15f * h/t.sigw * (1.0f - exp(-5.0f * zeta)); 
                }
	}
	else {
// Stable conditions
		float zeta=z/h;
                
                //eqn 29 - 33 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
		t.sigu          = 1.e-2f + 2.0f * ustar * (1.0f - zeta);
		t.sigv          = 1.e-2f + 1.3f * ustar * (1.0f - zeta);
		t.sigw          = t.sigv;
		t.dsigwdz       = -1.3f * ustar/h;
		t.tlu           = 0.15f * h/t.sigu * sqrt(zeta);
		t.tlv           = 0.467f * t.tlu;               //??
		t.tlw           = 0.1f * h/t.sigw * pow(zeta, 0.8f);
	}

	t.tlu = max(10.0f, t.tlu);
	t.tlv = max(10.0f, t.tlv);
	t.tlw = max(30.0f, t.tlw);
	
	if (t.dsigwdz == 0.0f) t.dsigwdz = 1.e-10f;

        //CAN'T RETURN T!
	return t;
}
/*----------------------------------------------------------------------------*/
/** Converts distance change in m to degree.
 @param g Grid object.
 @param lon Longitude [deg]
 @param lat Latitude [deg]
 @param dx Est-west displacement [m]
 @oaram dy North-south displacement [m]
 @param zt Altitude [m]
 */
STATIC_INLINE 
XY xy2ll_f( __global Grid *g, float lon, float lat, float dx, float dy, float zt)
{
	 float R2D=57.2957795f;		// radius to degree 180/pi;
	 float D2R=0.0174532925f;		// radius to degree pi/180;

	float a=lon*D2R,
		  b=lat*D2R,
		  re=6378137.0f+zt,				// earth radius (m)
		  r=re*cos(b),
		  x, y;

	if (lat>75.0f) {
		// point near north pole
		x=r*cos(a)-dx*sin(a)-dy*cos(a);
		y=r*sin(a)+dx*cos(a)-dy*sin(a);
		lon=R2D*atan2(y,x);
		lat=R2D*acos(sqrt(x*x+y*y)/re);
	} else if (lat<-75.0f) {
		// point near south pole
		x=r*cos(a)-dx*sin(a)+dy*cos(a);
		y=r*sin(a)+dx*cos(a)+dy*sin(a);
		lon=R2D*atan2(y,x);
		lat=-R2D*acos(sqrt(x*x+y*y)/re);
	} else {
		lon+=R2D*dx/r;
		lat+=R2D*dy/re;
	}

	if (lat>90.0f) {
		lat=180.0f-lat;
		lon+=180.0f;
	}
	else if (lat<-90.0f) {
		lat=-(180.0f+lat);
		lon+=180.0f;
	}

	if (g->gl) {
		if (lon<g->x0) {
			lon+=360.0f;
		}
		else if (lon>g->x1+g->dx) {
			lon-=360.0f;
		}
	}

	XY xy={lon,lat};

	return xy;
}
/*----------------------------------------------------------------------------*/
/** Calculate vertical interpolation factors.
 @param g Grid object.
 @param f Field object.
 @param z Altitude.
 @param i Field row index.
 @param j Field column index.
 */
STATIC_INLINE 
Factor zindex_f( __global Grid *g, FIELD_ARGS, float z, int i, int j)
{
	// todo: implementation for hybrid or sigma vertical grid.

	int k, n, nxy;
	float z1, z2, dz;
	Factor fc;

	if (z<0.0f) {
#ifdef __FLEX__
	//errmsg("z<0 problem");
#else
		z=-z;
#endif
	}

	n       = i * g->nc + j;
	nxy     = g->nxy;
	z1      = field_Z[n];
	z       += z1 + 1.0f;
	
	for (k=1 ; k < g->nl ; k++) {
		z2 = field_Z[n + k * nxy];
		if (z2 > z) {
			dz      = z2-z1;
			fc.k1   = k-1;
			fc.fa1  = fabs((z2-z)/dz);
			fc.fb1  = fabs((z1-z)/dz);
			return fc;
		}
		z1 = z2;
	}

	if (g->gl || z-z2<100.0f) {
		fc.k1=g->nl-2;
		fc.fa1=0.0;
		fc.fb1=1.0;
	}
	else {
		fc.k1=-1;
		fc.fa1=1.0;
		fc.fb1=0.0;
	}

	return fc;
}
/*----------------------------------------------------------------------------*/
/** Interpolate 3D field data
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 */
STATIC_INLINE 
float field3d_f(Factor *f, __global Grid *g, __global float* v)
{
	int n1, n2, nxy;
	float v1, v2, v3, v4;

	nxy=g->nxy;
	
	n1=f->k1*nxy+f->i1*g->nc+f->j1;
	n2=n1+nxy;
	v1=f->fa1*v[n1]+f->fb1*v[n2];
	
	n1=f->k2*nxy+f->i1*g->nc+f->j2;
	n2=n1+nxy;
	v2=f->fa2*v[n1]+f->fb2*v[n2];

	n1=f->k3*nxy+f->i2*g->nc+f->j2;
	n2=n1+nxy;
	v3=f->fa3*v[n1]+f->fb3*v[n2];

	n1=f->k4*nxy+f->i2*g->nc+f->j1;
	n2=n1+nxy;
	v4=f->fa4*v[n1]+f->fb4*v[n2];

	return f->f1*v1+f->f2*v2+f->f3*v3+f->f4*v4;
}
/*----------------------------------------------------------------------------*/
/** Reflect excessive altitude displacement on boundaries (z refer to the surface)
 * 
 * fmod is not compiling with Apple amd, hence the shamefull code
 @param z Altitude.
 @param zmax Altitude maximum.
 */
STATIC_INLINE 
float reflect_f(float z, float zmax)
{
	if (z < 0.0f) z = -z;
        
	if (z > zmax) {
		z = fmod(z, zmax);
                //while( (z - (z/zmax)) > 0){
                //    z = z - (z/zmax);
                //}
		z = zmax - z;
	}
	return z;
}
/*----------------------------------------------------------------------------*/
/** Calculates standard deviation for a grid box.
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to 3D field data.
 */
STATIC_INLINE 
float std3d_f(Factor f, __global Grid *g, __global float* v)
{
	int n1, n2, nxy;
	float vm, v1, v2, v3, v4, v5, v6, v7, v8;

	nxy = g->nxy;

	n1 = f.k1 * nxy + f.i1 * g->nc + f.j1;
	n2 = n1 + nxy;
	v1 = v[n1];
	v2 = v[n2];

	n1 = f.k2 * nxy + f.i1 * g->nc + f.j2;
	n2 = n1 + nxy;
	v3 = v[n1];
	v4 = v[n2];

	n1 = f.k3 * nxy + f.i2 * g->nc + f.j2;
	n2 = n1 + nxy;
	v5 = v[n1];
	v6 = v[n2];

	n1 = f.k4 * nxy + f.i2 * g->nc + f.j1;
	n2 = n1 + nxy;
	v7 = v[n1];
	v8 = v[n2];

	vm = (v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8)/8.0f;

	v1 -= vm;
	v2 -= vm;
	v3 -= vm;
	v4 -= vm;
	v5 -= vm;
	v6 -= vm;
	v7 -= vm;
	v8 -= vm;

	return 0.0;//sqrt((v1*v1 + v2*v2 + v3*v3 + v4*v4 + v5*v5 + v6*v6 + v7*v7 + v8*v8)/8.0f);
}
/*----------------------------------------------------------------------------*/
/** Calculates specific humidity
 @param P Air pressure [Pa]
 @param T Air temperature [K]
 @param Hum Relative humidity [%]
 <br>
  Ref: http://en.wikipedia.org/wiki/Humidity
*/
STATIC_INLINE float hq_f(float P/*[Pa]*/, float T/*[K]*/, float Hum/*[%]*/)
{
	float pw=wvp_f(T,Hum);
	// mixing ratio
	float mr=0.62197*pw/(P-pw);
	return mr/(1.0+mr);
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/** Calculates water vapor pressure [Pa]
 @param T Air temperature [K]
 @param Hum Relative humidity [%]
 <br>
 Ref:  http://en.wikipedia.org/wiki/Density_of_air
*/
STATIC_INLINE 
float wvp_f(float T/*[K]*/, float Hum/*[%]*/)
{
	// saturated water vapor pressure [Pa]
	float pw=610.78*pow(10.0,(7.5*T-2048.625)/(T-35.85));
	// water vapor pressure
	return (float) (pw*Hum/100.0);
}
/* --------------------------------------------------------------------------- */
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
