# Makefile for Flexocl

# This does not seem to be working?????
VPATH = . netCDF

#cobj: ./netCDF/convect43c.c ./netCDF/attr.c ./netCDF/dim.c ./netCDF/error.c ./netCDF/libvers.c ./netCDF/nc.c ./netCDF/ncio.c ./netCDF/ncx.c \
#	./netCDF/putget.c ./netCDF/string.c ./netCDF/v1hpg.c ./netCDF/v2i.c ./netCDF/var.c
#	gcc $(CFLAGS) -c $^

UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
# This was for my previous openCL with linux and cuda
#OPENCL = -l OpenCL
#INCLUDES = -I /usr/local/cuda/include/

# This is for machine "togian"
OPENCL = -l OpenCL
INCLUDES = -I/opt/AMDAPP/include/
LIB_LOCATION = -L /opt/AMDAPP/lib/x86_64/
endif

ifeq ($(UNAME), Darwin)
OPENCL = -framework OpenCL
INCLUDES = -I /usr/local/cuda/include/
endif

ifeq ($(UNAME), CYGWIN_NT-6.1-WOW64)
OPENCL = -l OpenCL
INCLUDES = -I "/cygdrive/c/Program Files (x86)/AMD APP/include"
LIB_LOCATION = -L "/cygdrive/c/Program Files (x86)/AMD APP/lib/x86" -L "/cygdrive/c/Program Files (x86)/AMD APP/lib/x86_64"
endif

OPTIMISATION_LEVEL = -O2

CFLAGS = -g $(OPTIMISATION_LEVEL) -DOPENCL

CXX = g++

OPENMP = -fopenmp

GOMP = -lgomp

OBJECTS = attr.o  convect43c.o  dim.o  error.o  flexcdf.o  flexocl.o  \
          flexconv.o  flexcount.o  flexfield.o  flexgrid.o  flexlist.o  \
          flexmodel.o  flexclass.o  flexparam.o  flexutility.o \
           \
          libvers.o  main.o  ncio.o  nc.o  ncx.o  putget.o  string.o  v1hpg.o  v2i.o  var.o #  kernel_trajectory_functions.o kernel_meterological_functions.o

all: cobj cppo flexocl

cobj: convect43c.c attr.c dim.c error.c libvers.c nc.c ncio.c ncx.c \
	putget.c string.c v1hpg.c v2i.c var.c
	gcc $(CFLAGS) -c $^


#%.o: %.c ./netCDF/%.c
#	$(CC) $(CFLAGS) $(INCLUDES) -c $^ 


#%.o: %.cpp
#	$(CXX) $(CFLAGS) $(INCLUDES) -c $^


cppo: flexcdf.cpp flexclass.cpp flexconv.cpp flexcount.cpp flexocl.cpp\
	flexfield.cpp flexgrid.cpp flexlist.cpp main.cpp \
	flexmodel.cpp flexparam.cpp flexutility.cpp \
	#kernel_meterological_functions.cpp kernel_trajectory_functions.cpp \
	#flexgl.cpp
	g++ $(CFLAGS) $(INCLUDES) $(OPENMP) -c $^



flexocl: $(OBJECTS) 
	g++ $(CFLAGS)  $^ -o $@ $(LIB_LOCATION) $(OPENCL) $(OPENMP) $(GOMP) 

clean:
	rm -f *.o
	rm flexocl
