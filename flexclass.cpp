/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flextraj_kernel.h"
#include "flexclass.h"
#include "flexutility.h"
#include "flexsort.h"
#include "./netCDF/netcdf.h"
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

#include "constants.h"

#include <stdlib.h>                     //for exit

#include "definitions.h"

#define __device__ static inline
#define __FLEX__


// timing functions
#include <sys/time.h>
struct timeval start_to, stop_to;
struct timeval start_kernel, stop_kernel;
struct timeval start_from, stop_from;
#define TO_RESET        (start_to = (struct timeval){ 0 }; stop_to = (struct timeval){ 0 })
#define KERNEL_RESET    (start_kernel = (struct timeval){ 0 }; stop_kernel = (struct timeval){ 0 })
#define FROM_RESET      (start_from = (struct timeval){ 0 }; stop_from = (struct timeval){ 0 })
#define START_TO gettimeofday(&start_to, NULL)
#define STOP_TO  gettimeofday(&stop_to, NULL)
#define START_KERNEL gettimeofday(&start_kernel, NULL)
#define STOP_KERNEL  gettimeofday(&stop_kernel, NULL)
#define START_FROM gettimeofday(&start_from, NULL)
#define STOP_FROM  gettimeofday(&stop_from, NULL)
 

#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif
/*============================================================================*/
#define FLOAT(a)                ((float)a)
#define INT(a)                  ((int)a)
/*============================================================================*/
//this is a hack till i understand how to pass scalars in structs
static Parameter static_p;
extern cl_mem float_probe, int_probe;

static float static_rad_const_1;
static float static_rad_const_2;


typedef struct land_use {
    int    type;
    float  percent;
}LAND_USE;

//constants for calculation
#define AVOGADROS_NUM                   ( 6.022e+23 )

//this is temp until i figure out what it is and implement
#define rel_diff(a)                     ( 1 )

// import common kernel functions

#include "kernel_meterological_functions.h"
#include "kernel_trajectory_functions.h"

flexClass::flexClass()
{
	_age            = 0;
	_id[0]          = 0;
	_idum           = -7;
	_iy             = 0;
	dev_struct       = NULL;
        /*
	d_X             = NULL;
	d_Y             = NULL;
	d_Z             = NULL;
	d_Fg            = NULL;
	d_Up            = NULL;
	d_Vp            = NULL;
	d_Wp            = NULL;
	d_Um            = NULL;
	d_Vm            = NULL;
	d_Wm            = NULL;
	d_Zs            = NULL;
	d_Zb            = NULL;
	d_Idx           = NULL;
	d_Gdx           = NULL;
         */         
        
        //this is fixed just now for I-131
        _specific_molar_mass = 131.0;
        
        PRINTF("FLEXCLASS : Create\n");
}
/*----------------------------------------------------------------------------*/
flexClass::~flexClass()
{
    PRINTF("FLEXCLASS : destroy\n");
    clearDevice();
}
/*----------------------------------------------------------------------------*/
void flexClass::id(const char *name)
{
	if (strlen(name)>sizeof(_id)-1) errmsg("ID name is too long");
	strcpy(_id,name);
}
/*----------------------------------------------------------------------------*/
void flexClass::release(int n, float x0, float y0, float ds, float z0, float dz, float mass, int halflife)
{
	if (x0<-180.0f || x0>360.0f ||
		y0<-90.0f || y0>90.0f ||
		ds<.0f || z0<.0f || dz<.0f) errmsg("Bad release parameters");

	resize(n);

	Grid G;
	G.y0=90.0f;
	G.y1=-90.0f;
	G.dy=1.0f;
	G.x0=0.0f;
	G.x1=359.0f;
	G.dx=1.0f;

	if (x0 < 0) {
		G.x0=-180.0f;
		G.x1=180.0f;
	}
        
#if RADIOACTIVE
        _halflife = halflife;
#endif
        //updateDevice();
        
        //runReleaseKernel();
        
        
	for (int k=0; k<host.np; k++) {
		float dx, dy, dxy=ds+1.0f;
		while (dxy>ds) {
			dx=(ran2(_idum,_iy,_iv,32)-0.5)*2.0*ds;
			dy=(ran2(_idum,_iy,_iv,32)-0.5)*2.0*ds;
			dxy=sqrt(dx*dx+dy*dy);
		}
		Z(k)    = z0+ran2(_idum,_iy,_iv,32)*dz;
		XY xy   = xy2ll_f(&G,x0,y0,dx,dy,Z(k));
		X(k)    = xy.x;
		Y(k)    = xy.y;
                
#if RADIOACTIVE
                Mass(k) = mass;
#endif
	}
}
/*----------------------------------------------------------------------------*/
void flexClass::relgrid(float x0, float dx, int nx, float y0, float dy, int ny, float z0, float dz, int nz)
{
	if (x0<-180.0f || x0>360.0f || nx<1 || dx<0.f || dx*nx>360.0f ||
		y0<-90.0f || y0>90.0f || ny<1 || dy<0.f || dy*ny>180.0f ||
		z0<.0f || dz<0.0 || nz<1) errmsg("Bad release parameters");

	resize(nx*ny*nz);

	int i, j, l, k=0;

	for (i=0; i<ny; i++) {
		for (j=0; j<nx; j++) {
			for (l=0; l<nz; l++) {
				X(k)=x0+j*dx;
				Y(k)=y0+i*dy;
				Z(k)=z0+l*dz;
				k++;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void flexClass::dispersion(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &Ct)
{
    PRINTF("FLEXCLASS : dispersion\n");

	if (Idx(0)<0 && Idx(1)<0) {
// initialize random number indexes
		for (int i=0; i<Idx.nd(); i++) {
			Idx(i) = int(ran2(_idum,_iy,_iv,32)*P.host.ng);
		}
	}

//        time_t tick=time(NULL);
        if (P.host.timing) {
		START_TO;
        }

        //timestamp_t start = get_timestamp();
    if(P.host.openCL){
#if KERNEL_CALCULATIONS
        //check if the other have set what they need to
        if (!G.device || !P.d_Gauss || !P.d_Rand || !F.dev_struct)
            errmsg("Not all device pointers have been set properly");

        if(!dev_struct){
            updateDevice();
        }

	if (P.host.timing) {
                STOP_TO;
		START_KERNEL;
        }

        runDispersionKernel(G.device);


	 if (P.host.timing) {
                STOP_KERNEL;
                START_FROM;
         }


        //by examining the code, values from the class are only copied
        //back when necessary, eg sort, and xyz. So no need to do it here
        //updateHost();

        if (C.nd()>0) {
                xyz();
                C.count(host,Ct, P.host.lsynctime);
        }

	if (P.host.timing) {
        	STOP_FROM;
        }
#endif
    }
    else {
		if (dev_struct) clearDevice();

		if (P.host.timing) {
                	STOP_TO;
	                START_KERNEL;
	        }


#pragma omp parallel for

// dispersion for all particles
		for (int i=0; i<host.np; i++) {
			if (G.nested) {
				if (!dispersion_f(&host,i,&G.nested->host,&P.host,&F.nested->host)) {
					dispersion_f(&host,i,&G.host,&P.host,&F.host);
				}
			}
			else {
				dispersion_f(&host,i,&G.host,&P.host,&F.host);
			}
		}

		if (P.host.timing) {
               		STOP_KERNEL;
	                START_FROM;
	         }




		if (C.nd()>0) {
// count particles in grids
			C.count(host,Ct,P.host.lsynctime);
		}

		if (P.host.timing) {
                	STOP_FROM;
	        }

	}
    
	if (P.host.timing){
            //printf("Dispersion, %.4fms\n", (get_timestamp() - start)/ 1000.0L);
            //printf("Dispersion, %.1f\n", difftime(::time(NULL), tick));
             printf("TRAJECTORY TO    : %lu.%06lu\n", stop_to.tv_sec     - start_to.tv_sec,     stop_to.tv_usec     - start_to.tv_usec);
             printf("TRAJECTORY KERNEL: %lu.%06lu\n", stop_kernel.tv_sec - start_kernel.tv_sec, stop_kernel.tv_usec - start_kernel.tv_usec);
             printf("TRAJECTORY FROM  : %lu.%06lu\n", stop_from.tv_sec   - start_from.tv_sec,   stop_from.tv_usec   - start_from.tv_usec);

        }
	_age+=P.host.lsynctime;
}
/*----------------------------------------------------------------------------*/
void flexClass::runDispersionKernel(cl_mem G){

    PRINTF("CLASS : dispersion kernel\n");

    cl_uint var = 0;
    
    int choice = DISPERSION_KERNEL;
    //the first argument indicates the kernel code to run
    setKernelArgs(choice, var++, sizeof(int), &choice);
    
    var += ERROR_ARGS + INTERPOLATION_ARGS + GRID_ARGS;

    //The grid
    //setKernelArgs(choice, var++, sizeof(cl_mem), &G);       //should be able to pass struct directly
    
    var += PARAM_ARGS;
    
    //class variables
    setKernelArgs(choice, var++, sizeof(int), &host.np);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Idx);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Gdx);
#if RADIOACTIVE
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Mass);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Radioactivity);
#else
    var += 2;
#endif
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->X);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Y);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Z);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Fg);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Up);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Vp);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Wp);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Um);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Vm);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Wm);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Zs);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Zb); //96
    
    //printf("This should be calculated by querying the device!!\n");
    //const size_t local_ws = 256;	// Number of work-items per work-group
    
    // shrRoundUp returns the smallest multiple of local_ws bigger than size
    //const size_t global_ws = shrRoundUp(local_ws, host.np);	// Total number of work-items
    
    size_t global_ws = host.np;
    runKernelBlocking(&global_ws, 0);
}
/*----------------------------------------------------------------------------*/
void flexClass::runRadioactiveKernel(Class C, cl_mem G, const flexParam *P, DeviceField *F){
    
    //Beware of this kernel, it is radioactive!!
    int choice = RADIOACTIVE_KERNEL;
    int var = 0;
    
     
    //the first argument indicates the kernel code to run
    setKernelArgs(choice, var++, sizeof(int), &choice);
    
    var += ERROR_ARGS + INTERPOLATION_ARGS + GRID_ARGS + PARAM_ARGS;
    
     
    //class variables
    setKernelArgs(choice, var++, sizeof(int), &host.np);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Idx);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Gdx);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Mass);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Radioactivity);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->X);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Y);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Z);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Fg);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Up);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Vp);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Wp);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Um);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Vm);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Wm);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Zs);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Zb); //96
    
    var += FIELD_ARGS + CONVECTION_ARGS + MASS_ARGS;
    
    setKernelArgs(choice, var++, sizeof(int), &_halflife); 
    setKernelArgs(choice, var++, sizeof(float), &_specific_molar_mass); 
    setKernelArgs(choice, var++, sizeof(float), &static_rad_const_1); 
    setKernelArgs(choice, var++, sizeof(float), &static_rad_const_2); 
    
    
    
    //printf("This should be calculated by querying the device!!\n");
    const size_t local_ws = 256;	// Number of work-items per work-group
    
    // shrRoundUp returns the smallest multiple of local_ws bigger than size
    size_t global_ws = shrRoundUp(local_ws, host.np);	// Total number of work-items
    
    
    runKernelBlocking(&global_ws, local_ws);
    
}
/*----------------------------------------------------------------------------*/
void flexClass::runTrajectoryKernel(Class C, int np, cl_mem G, const flexParam *P, DeviceField *F){

    PRINTF("CLASS : trajectory kernel\n");
    
     
    cl_uint var = 0;
    int choice = TRAJECTORY_KERNEL;
    //the first argument indicates the kernel code to run
    setKernelArgs(choice, 0, sizeof(int), &choice);
    
    var += ERROR_ARGS + INTERPOLATION_ARGS;

    //The grid
    setKernelArgs(choice, var++, sizeof(cl_mem), &G);       //should be able to pass struct directly

    var += PARAM_ARGS;
    
    //class variables
    setKernelArgs(choice, var++, sizeof(int), &C.np);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Idx);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Gdx);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Mass);    
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Radioactivity);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->X);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Y);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Z);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Fg);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Up);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Vp);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Wp);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Um);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Vm);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Wm);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Zs);
    setKernelArgs(choice, var++, sizeof(cl_mem), &dev_struct->Zb);

    //printf("This should be calculated by querying the device!!\n");
    const size_t local_ws = 256;	// Number of work-items per work-group
    
    // shrRoundUp returns the smallest multiple of local_ws bigger than size
    size_t global_ws = shrRoundUp(local_ws, np);	// Total number of work-items
    
    
    runKernelBlocking(&global_ws, local_ws);
    
}
/*----------------------------------------------------------------------------*/
int flexClass::sort(flexArr<int> &Pdx, flexArr<int> &Sdx, int openCL)
{
	if (openCL) {
		getDeviceData(Gdx.ptr(), dev_struct->Gdx, Gdx.nd()*sizeof(int));
	}

	if (Pdx.nd()<Gdx.nd()) errmsg("Pdx.nd<Gdx.nd");

// sort grid index so that Gdx(i) is the grid index of particle pds[i]
	HeapSort(Gdx.ptr(),Pdx.ptr(),Gdx.nd(),1);

// save sorted grid index segments
	Sdx(0) = 0;
	int i, ns = 1, gdx = Gdx(0);
        
	for (i = 1; i < host.np; i++) {
            if (Gdx(i) != gdx) {
                gdx = Gdx(i);
                if (Sdx.nd() <= ns) 
                    errmsg("Sdx.nd<ns");
                Sdx(ns++) = i;
            }
	}
        
	Sdx(ns++)=host.np;

	return ns;
}
/*----------------------------------------------------------------------------*/
void flexClass::trajectory(const flexGrid &G, const flexParam &P, const flexField &F)
{
	time_t tick=time(NULL);

	if (P.host.openCL) {
		if (!G.device || !P.d_Gauss || !P.d_Rand || !F.dev_struct)
			errmsg("Not all device pointers have been set properly");
		if (!dev_struct) updateDevice();

                runTrajectoryKernel(host, host.np, G.device, &P, F.dev_struct);

                //updateHost();
	}
	else {
		if (dev_struct) clearDevice();

#pragma omp parallel for

		for (int i=0; i<host.np; i++) {
			if (G.nested) {
				if (!trajectory_f(&host,i,&G.nested->host,&P.host,&F.nested->host))
					trajectory_f(&host,i,&G.host,&P.host,&F.host);
			}
			else {
				trajectory_f(&host,i,&G.host,&P.host,&F.host);
			}
		}
	}

	if (P.host.timing) printf("Trajectory, %.1f\n",difftime(time(NULL),tick));

	_age+=P.host.lsynctime;
}
/*----------------------------------------------------------------------------*/
/**
 * Please note that this can only be used for homogeneous particle types. 
 * @param G
 * @param P
 * @param F
 */
void flexClass::radioactive_decay(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &Ct){
    
    if (!G.device || !P.d_Gauss || !P.d_Rand || !F.dev_struct)
        errmsg("Not all device pointers have been set properly");
    
    timestamp_t start = get_timestamp();

    //this is simple just now, as there is only 1 species
    static_rad_const_1 = exp((-P.host.lsynctime * log10(2.0)) / _halflife);
    static_rad_const_2 = AVOGADROS_NUM * (log(2) / _halflife);
    
    //no need to update device, we are called after dispersion/trajectory which has done this
    
    runRadioactiveKernel(host, G.device, &P, F.dev_struct);
    
    //we should only have to copy data back, just before a save 
    //getDeviceData(Radioactivity.ptr(), dev_struct->Radioactivity, sizeof(float) * Radioactivity.nd());
    //getDeviceData(Mass.ptr(), dev_struct->Mass, sizeof(float) * Mass.nd());
    
   // printf("%f pc:Bq\n", Radioactivity(0));
   // printf("%f, mass\n", Mass(0));
    
    if (P.host.timing) printf("Irradiation, %.4fms\n", (get_timestamp() - start) / 1000.0);
}
/*----------------------------------------------------------------------------*/
void flexClass::updateDevice()
{
        PRINTF("FLEXCLASS : updateDevice\n");

	size_t size = X.nd() * sizeof(float);

	PRINTF("FLEXCLASS: size %d \n", size);
        
        if (!dev_struct) dev_struct = new DeviceClass();
#if RADIOACTIVE
        if(!dev_struct->Mass) mallocOnDevice(&dev_struct->Mass, size, host.Mass);
        setDeviceData(dev_struct->Mass, host.Mass, size);
        
        if(!dev_struct->Radioactivity) mallocOnDevice(&dev_struct->Radioactivity, size, host.Radioactivity);
        setDeviceData(dev_struct->Radioactivity, host.Radioactivity, size);        
#endif
	if (!dev_struct->X) mallocOnDevice(&dev_struct->X,size, host.X);
	setDeviceData(dev_struct->X, host.X,size);

	if (!dev_struct->Y) mallocOnDevice(&dev_struct->Y,size, host.Y);
	setDeviceData(dev_struct->Y, host.Y,size);

	if (!dev_struct->Z) mallocOnDevice(&dev_struct->Z,size, host.Z);
	setDeviceData(dev_struct->Z, host.Z, size);

	if (!dev_struct->Fg) mallocOnDevice(&dev_struct->Fg,size, Fg.ptr());
	setDeviceData(dev_struct->Fg,Fg.ptr(),size);

	if (!dev_struct->Up) mallocOnDevice(&dev_struct->Up,size, Up.ptr());
	setDeviceData(dev_struct->Up,Up.ptr(),size);

	if (!dev_struct->Vp) mallocOnDevice(&dev_struct->Vp,size, Vp.ptr());
	setDeviceData(dev_struct->Vp,Vp.ptr(),size);

	if (!dev_struct->Wp) mallocOnDevice(&dev_struct->Wp,size, Wp.ptr());
	setDeviceData(dev_struct->Wp,Wp.ptr(),size);

	if (!dev_struct->Um) mallocOnDevice(&dev_struct->Um,size, Um.ptr());
	setDeviceData(dev_struct->Um,Um.ptr(),size);

	if (!dev_struct->Vm) mallocOnDevice(&dev_struct->Vm,size, Vm.ptr());
	setDeviceData(dev_struct->Vm,Vm.ptr(),size);

	if (!dev_struct->Wm) mallocOnDevice(&dev_struct->Wm,size, Wm.ptr());
	setDeviceData(dev_struct->Wm,Wm.ptr(),size);

	if (!dev_struct->Zs) mallocOnDevice(&dev_struct->Zs,size, Zs.ptr());
	setDeviceData(dev_struct->Zs,Zs.ptr(),size);

	if (!dev_struct->Zb) mallocOnDevice(&dev_struct->Zb,size, Zb.ptr());
	setDeviceData(dev_struct->Zb,Zb.ptr(),size);

	size=X.nd()*sizeof(int);

	if (!dev_struct->Idx) mallocOnDevice(&dev_struct->Idx,size, Idx.ptr());
	setDeviceData(dev_struct->Idx,Idx.ptr(),size);
        
        //this is only ever written to
	if (!dev_struct->Gdx) mallocOnDevice(&dev_struct->Gdx,size, Gdx.ptr());
        //setDeviceData(dev_struct->Gdx,Gdx.ptr(),size);
        
        dev_struct->np = host.np;
        setPtr(false);
}

/*
 * These are the values updated by dispersion and need to be brought back
 */
void flexClass::updateHost(){
    PRINTF("FLEXCLASS : updatehost\n");
    
    size_t size = X.nd() * sizeof(float);
    
    getDeviceData(Mass.ptr(), dev_struct->Mass, size);
    getDeviceData(Radioactivity.ptr(), dev_struct->Radioactivity, size);
    getDeviceData(X.ptr(), dev_struct->X, size);
    getDeviceData(Y.ptr(), dev_struct->Y, size);
    getDeviceData(Z.ptr(), dev_struct->Z, size);
    getDeviceData(Fg.ptr(), dev_struct->Fg, size);
    getDeviceData(Up.ptr(), dev_struct->Up, size);
    getDeviceData(Vp.ptr(), dev_struct->Vp, size);
    getDeviceData(Wp.ptr(), dev_struct->Wp, size);
    getDeviceData(Um.ptr(), dev_struct->Um, size);
    getDeviceData(Vm.ptr(), dev_struct->Vm, size);
    getDeviceData(Wm.ptr(), dev_struct->Wm, size);
    getDeviceData(Zs.ptr(), dev_struct->Zs, size);
    getDeviceData(Zb.ptr(), dev_struct->Zb, size);

    size=X.nd()*sizeof(int);
    getDeviceData(Idx.ptr(), dev_struct->Idx, size);    
    getDeviceData(Gdx.ptr(), dev_struct->Gdx, size);    
  }

void flexClass::clearDevice()
{
    PRINTF("FLEXCLASS : clearDevice\n");
    
    if(!dev_struct)
        return;
#if RADIOACTIVE
    freeOnDevice(dev_struct->Mass);
    dev_struct->Mass    = NULL;
    freeOnDevice(dev_struct->Radioactivity);
    dev_struct->Radioactivity    = NULL;
#endif
    freeOnDevice(dev_struct->X);
    dev_struct->X       = NULL;
    freeOnDevice(dev_struct->Y);
    dev_struct->Y=NULL;
    freeOnDevice(dev_struct->Z);
    dev_struct->Z=NULL;
    freeOnDevice(dev_struct->Fg);
    dev_struct->Fg=NULL;
    freeOnDevice(dev_struct->Up);
    dev_struct->Up=NULL;
    freeOnDevice(dev_struct->Vp);
    dev_struct->Vp=NULL;
    freeOnDevice(dev_struct->Wp);
    dev_struct->Wp=NULL;
    freeOnDevice(dev_struct->Um);
    dev_struct->Um=NULL;
    freeOnDevice(dev_struct->Vm);
    dev_struct->Vm=NULL;
    freeOnDevice(dev_struct->Wm);
    dev_struct->Wm=NULL;
    freeOnDevice(dev_struct->Zs);
    dev_struct->Zs=NULL;
    freeOnDevice(dev_struct->Zb);
    dev_struct->Zb=NULL;
    freeOnDevice(dev_struct->Idx);
    dev_struct->Idx=NULL;
    freeOnDevice(dev_struct->Gdx);
    dev_struct->Gdx=NULL;

    delete dev_struct;
    dev_struct              = NULL;
}

void flexClass::setPtr(bool cuda)
{
#if RADIOACTIVE
    host.Mass           = Mass.ptr();
    host.Radioactivity  = Radioactivity.ptr();
#endif
    host.X              = X.ptr();
    host.Y              = Y.ptr();
    host.Z              = Z.ptr();
    host.Fg             = Fg.ptr();
    host.Up             = Up.ptr();
    host.Vp             = Vp.ptr();
    host.Wp             = Wp.ptr();
    host.Um             = Um.ptr();
    host.Vm             = Vm.ptr();
    host.Wm             = Wm.ptr();
    host.Zs             = Zs.ptr();
    host.Zb             = Zb.ptr();
    host.Idx            = Idx.ptr();
    host.Gdx            = Gdx.ptr();
}

void flexClass::resize(int n)
{
    PRINTF("CLASS : resize %d (%d)\n", n, host.np);
    
    if (n<=0 ) {
        printf("CLASS : warning, can't resize to length 0\n");
        exit(-1);
    }
            
    //if the array are already allocated to the correct size, no need to do anything
    if(n == host.np && X.ptr() != NULL){
        return;
    }


    host.np=n;
#if RADIOACTIVE
    Mass.resize(n);
    Radioactivity.resize(n);
#endif
    X.resize(n);
    Y.resize(n);
    Z.resize(n);

    Fg.resize(n);
    Fg.fill(1.f);

    Up.resize(n);
    Up.clear();
    Vp.resize(n);
    Vp.clear();
    Wp.resize(n);
    Wp.clear();

    Um.resize(n);
    Um.clear();
    Vm.resize(n);
    Vm.clear();
    Wm.resize(n);
    Wm.clear();

    Zs.resize(n);
    Zs.clear();
    Zb.resize(n);
    Zb.clear();

    Idx.resize(n);
    Idx(0)=-999;
    Idx(1)=-999;

    Gdx.resize(n);
    Gdx.fill(-1);

    setPtr(false);
    clearDevice();
}

void flexClass::xyz()
{
	size_t bytes=host.np*sizeof(float);
	getDeviceData(X.ptr(),dev_struct->X,bytes);
	getDeviceData(Y.ptr(),dev_struct->Y,bytes);
	getDeviceData(Z.ptr(),dev_struct->Z,bytes);
	getDeviceData(Zs.ptr(),dev_struct->Zs,bytes);
	getDeviceData(Zb.ptr(),dev_struct->Zb,bytes);
        
#if RADIATION
        getDeviceData(Mass.ptr(),dev_struct->Mass, bytes);
        getDeviceData(Radioactivity.ptr(),dev_struct->Radioactivity, bytes);
#endif
        
}

void flexClass::saveXYZ(const char *fname, const Grid &G, const Parameter &P, const Field &F, double ctime)
{
	if (_age%P.outsec!=0) return;

	if (_age==0) {
		for (int k=0; k<host.np; k++) {
			Factor fc=index_f(&G,&F,X(k),Y(k),Z(k));
			Zs(k)=interpo3d_f(&fc,&G,F.Z,0);
			Zb(k)=interpo2d_f(&fc,&G,F.PBL);
		}
	}

	int i, len=1+P.lensec/P.outsec;
	int ncid, tid, pid, xid, yid, zid, sid, bid, dimid[2];

	const char	*value=NULL,
				*xname="x",
				*yname="y",
				*zname="z",
				*hname="zs",
				*bname="zb",
				*tname="t",
				*pname="id",
				*lname="long_name",
				*units="units",
				*comment="comment",
				*mode="direction";

	printf("save xyz: %s\n",fname);

	if (nc_open(fname,NC_WRITE,&ncid)!=NC_NOERR) {
		
		if (nc_create(fname,NC_NOCLOBBER,&ncid)!=NC_NOERR) errmsg("Failed to open ",fname," for saving data");
		
		nc_redef(ncid);
		
		char str[1024];
		value="FLEXCPP by Jiye Zeng <zeng@nies.go.jp>";
		nc_put_att_text(ncid,NC_GLOBAL,comment,strlen(value),value);

		nc_def_dim(ncid,tname,len,&tid);
		dimid[0]=tid;
		nc_def_var(ncid,tname,NC_INT,1,dimid,&tid);
		value="time";
		nc_put_att_text(ncid,tid,lname,strlen(value),value);
		int yy, mm, dd, hh, mi;
		jul2cal(yy,mm,dd,hh,mi,ctime);
		sprintf(str,"seconds since %d-%02d-%02d %02d:%02d:00",yy,mm,dd,hh,mi);
		nc_put_att_text(ncid,tid,units,strlen(str),str);
		nc_put_att_int(ncid,tid,mode,NC_INT,1,&P.ldirect);

		nc_def_dim(ncid,pname,host.np,&pid);
		dimid[0]=pid;
		nc_def_var(ncid,pname,NC_INT,1,dimid,&pid);
		value="particle_index";
		nc_put_att_text(ncid,pid,lname,strlen(value),value);
		value="none";
		nc_put_att_text(ncid,pid,units,strlen(value),value);
		
		dimid[0]=tid; dimid[1]=pid;
		
		nc_def_var(ncid,xname,NC_FLOAT,2,dimid,&xid);
		value="longitude";
		nc_put_att_text(ncid,xid,lname,strlen(value),value);
		value="degree_east";
		nc_put_att_text(ncid,xid,units,strlen(value),value);

		nc_def_var(ncid,yname,NC_FLOAT,2,dimid,&yid);
		value="latitude";
		nc_put_att_text(ncid,yid,lname,strlen(value),value);
		value="degree_north";
		nc_put_att_text(ncid,yid,units,strlen(value),value);

		nc_def_var(ncid,zname,NC_FLOAT,2,dimid,&zid);
		value="height_above_surface";
		nc_put_att_text(ncid,zid,lname,strlen(value),value);
		value="m";
		nc_put_att_text(ncid,zid,units,strlen(value),value);

		nc_def_var(ncid,hname,NC_FLOAT,2,dimid,&sid);
		value="surface_height";
		nc_put_att_text(ncid,sid,lname,strlen(value),value);
		value="m";
		nc_put_att_text(ncid,sid,units,strlen(value),value);

		nc_def_var(ncid,bname,NC_FLOAT,2,dimid,&bid);
		value="planetary_boundary_layer_height";
		nc_put_att_text(ncid,bid,lname,strlen(value),value);
		value="m";
		nc_put_att_text(ncid,bid,units,strlen(value),value);

		nc_enddef(ncid);

		flexArr<int> arr(X.nd());
		for (i=0; i<X.nd(); i++) arr(i)=i;
		nc_put_var_int(ncid,pid,arr.ptr());

		arr.resize(len);
		arr.clear();
		nc_put_var_int(ncid,tid,arr.ptr());
	}
	else {
		if (nc_inq_varid(ncid,tname,&tid)!=NC_NOERR ||
			nc_inq_varid(ncid,pname,&pid)!=NC_NOERR ||
			nc_inq_varid(ncid,xname,&xid)!=NC_NOERR ||
			nc_inq_varid(ncid,yname,&yid)!=NC_NOERR ||
			nc_inq_varid(ncid,zname,&zid)!=NC_NOERR ||
			nc_inq_varid(ncid,hname,&sid)!=NC_NOERR ||
			nc_inq_varid(ncid,bname,&bid)!=NC_NOERR) errmsg("Failed to get variable IDs");

		size_t tlen, plen;

		nc_inq_dimlen(ncid,tid,&tlen);
		nc_inq_dimlen(ncid,pid,&plen);

		if (tlen!=len || plen!=host.np) errmsg("CDF dimensions != array dimensions");

		if (_age==0) {
// overwrite coordinate data to an old file.
			flexArr<int> arr(X.nd());
			for (i=0; i<X.nd(); i++) arr(i)=i;
			nc_put_var_int(ncid,pid,arr.ptr());

			nc_put_att_int(ncid,tid,mode,NC_INT,1,&P.ldirect);

			arr.resize(len);
			arr.clear();
			nc_put_var_int(ncid,tid,arr.ptr());
		}
	}

	if (P.openCL!=0 && _age>0) xyz();

	size_t start[2]={_age/P.outsec,0}, count[2]={1,host.np};

	if (nc_put_var1_int(ncid,tid,start,&_age)!=NC_NOERR ||
		nc_put_vara_float(ncid,xid,start,count,X.ptr())!=NC_NOERR ||
		nc_put_vara_float(ncid,yid,start,count,Y.ptr())!=NC_NOERR ||
		nc_put_vara_float(ncid,zid,start,count,Z.ptr())!=NC_NOERR ||
		nc_put_vara_float(ncid,sid,start,count,Zs.ptr())!=NC_NOERR ||
		nc_put_vara_float(ncid,bid,start,count,Zb.ptr())!=NC_NOERR) errmsg("Failed to save xyz");

	nc_close(ncid);
}

void flexClass::wetDeposition(const flexGrid &G, const flexParam &P, const flexField &F)
{
	// not implemented yet
}
/*----------------------------------------------------------------------------*/
void flexClass::dryDeposition(const flexGrid &G, const flexParam &P, const flexField &F)
{
	// not implemented yet
    
    //this can be constant
    //float deposition_vel = flux/concentration[height];
    
    //if(gas_deposition){
        //todo
    //}
    //else if(matter_deposition){
        
    //    float dynamic_viscosity_of_air = 0.000018;
     //   float   
     //   float quasilaminar_sublayer_resistance;
     //   float bulk_surface_resistance;
        
     //   float gravitational_settling_velocity = (g * particle_density * (particle_diameter * particle_diameter) * cunningham_slip_flow_correction) / 18 * dynamic_viscosity_of_air;
        
     //   (1 / ()) + gravitational_settling_velocity;
    //}
    ;
}
/*----------------------------------------------------------------------------*/
void particleDepositionVelocity(
                                    flexArr<float> density,                     //1d
                                    flexArr<float> interval_mass_fraction,      //2d
                                    flexArr<float> schmidt,                     //2d
                                    flexArr<float> v_settling,                  //2d
                                    float aerodynamical_resistance,
                                    float ustar,
                                    float nyl,
                                    flexArr<float> deposition_velocity          //1d
                                ){
    
    float interval_deposition_velocity;
    float stokes_num;
    float alpha;
    float deposition_layer_resistance;
    float eps = 1.e-5;
    
    
    for(int i = 0 ; i < NUM_SPECIES ; i++){
        for(int j = 0 ; j < num_diameter_intervals && density(i) > 0 ; j++){
            if(ustar > eps){
                stokes_num = v_settling(i, j)/ga * ustar * ustar /nyl;          //what is the precidence here????
                alpha      = -3/stokes_num; 
                
                if(alpha <=  log10(eps)){
                    deposition_layer_resistance = 1 / (schmidt(i, j) * ustar);
                }
                else{
                    //rdp=1./((schmi(ic,j)+10.**alpha)*ustar)
                    //is the power translation correct??
                    deposition_layer_resistance = 1 / (pow((schmidt(i, j) + 10), alpha) * ustar);
                }
                
                //vdepj=vset(ic,j)+1./(ra+rdp+ra*rdp*vset(ic,j))
                //Again, is the translation correct
                interval_deposition_velocity = v_settling(i, j) + 1 / 
                        ((deposition_layer_resistance + aerodynamical_resistance) + (deposition_layer_resistance * aerodynamical_resistance * v_settling(i ,j)));
                        
            }
            else {
                interval_deposition_velocity = v_settling(i, j);
            }
            
            //deposition velocities of each interval are weighted with mass fraction
            deposition_velocity(i) = deposition_velocity(i) * interval_deposition_velocity * interval_mass_fraction(i, j);   
        }       
    }
}
/*----------------------------------------------------------------------------*/
/**
 * For all particle types and diameters, get the rate at which they fall, and if
 * they have moved below the "reference_height", remove them
 * @param G
 */
void flexClass::dryDepositionMassLoss(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &Ct){
    
    //so some of this can be lifted to the init of the class, but will put it here for the moment
    
    //this is to be calculated, represetnts the total deposition velcoities of 
    //all types of deposition
    flexArr<float> deposition_velocity;
    deposition_velocity.resize(NUM_SPECIES);
    
    //Dry deposition velocities
    flexArr<float> dry_deposition_velocity;
    dry_deposition_velocity.resize(NUM_SPECIES);

    //globals that i need to define and populate somewhere else...
    flexArr<float> density;                     //1d
    density.resize(NUM_SPECIES);
    
    //This is also known as rb
    flexArr<float> surfalce_layer_resistance; 
    surfalce_layer_resistance.resize(NUM_SPECIES);
    
    /*
  ! Read landuse inventory
  !***********************
  ! The landuse information is saved in a compressed format and written
  ! out by records of the length of 1 BYTE. Each grid cell consists of 3
  ! Bytes, which include 3 landuse categories (val 1-13 and 16 percentage
  ! categories) So one half byte is used to store the Landusecat the other
  ! for the percentageclass in 6.25 steps (100/6.25=16)
  ! e.g.
  ! 4 3  percentage 4 = 4*6.25 => 25% landuse class 3
  ! 2 1  percentage 2 = 2*6.25 => 13% landuse class 1
  ! 1 12 percentage 1 = 1*6.26 => 6.25% landuse class 12
     * 
     * NOTE: for just now this is an array of structs, and need to be looked at
     * again when i understand what is going on
     */
    flexArr<LAND_USE> land_use;
    land_use.resize(NUM_SPECIES);
    
    //this is specific to Iodine-131
    density(1) = DENSITY_IODINE_131;
    
    flexArr<float> interval_mass_fraction;      //2d
    flexArr<float> schmidt;                     //2d
    flexArr<float> v_settling;                  //2d
    float aerodynamical_resistance = 0;
    float ustar;                                //i think that this should just be in the field
    float nyl;                                  //kinematic viscosity  
 
    
    
    //locals for the setup
    float ra;
    float rhoa;
    float temp_celcius;
    float dynamic_viscosity;
    float diffusivity_h20;
    float deposition_layer_resistance;
    float alpha;
    float stokes_num;
    float interval_deposition_velocity;
    
    // For every particle 
    for(int i = 0 ; i < host.np ; i++){
        
        //Calculate diffusivity of water vapor (raise to power))
        //diffusivity_h20 = 2.11e-5 * (F.host.T[i]/273.15)**1.94 * (101325/F.host.Ps[i]);
        diffusivity_h20 = 2.11e-5 * pow((F.host.T[i]/273.15),1.94) * (101325/F.host.Ps[i]);

        //convert temp to celcius from kelvin
        temp_celcius = F.host.T[i] - 273.15;
        
        //Calculate dynamic viscosity
        if(temp_celcius < 0){
            //need to put a pow in here, get precidence!!
            //dynamic_viscosity = (1.718 + 0.0049 * temp_celcius - 1.2e-05 * temp_celcius**2) * 1.e-05;
            dynamic_viscosity = (1.718 + 0.0049 * temp_celcius - 1.2e-05 * pow(temp_celcius,2)) * 1.e-05;
        }
        else{
            dynamic_viscosity = (1.718 + 0.0049 * temp_celcius) * 1.e-05;
        }

        //calculate kinematic air viscosity
        rhoa = F.host.Ps[i]/(287. * F.host.T[i]);
        nyl  = dynamic_viscosity/rhoa;
        
        //set deposition_velocity to 0
        deposition_velocity.fill(0);
        

        float raquer = 0;
        //loop over all landuse classes       
        for(int j = 0 ; j <  NUM_OF_LAND_TYPES ; j++){
            
            //compute surface layer resistance "rb"
            if( rel_diff(j) > 0){
                printf("schimdt : these indicies are not correct - check");
                schmidt(i, j) = nyl/diffusivity_h20 * rel_diff(i);
                //surfalce_layer_resistance(i) = 2.0 * (schmidt(i, j) / pr)**0.67 / (karman * F.host.Ustar[i]);
                surfalce_layer_resistance(i) = 2.0 * pow((schmidt(i, j) / pr),0.67) / (karman * F.host.Ustar[i]);
            }
            
            //we should also adjust the land use cases for SNOW
            ////////////TODO
            ////////////TODO
            ////////////TODO
            
            //calculate aerodynamic resistance
            if (land_types[j]  > eps){
                //psih cna be found in flexfield, perhaps this should all be moved there
                //aerodynamical_resistance = (log(href/land_use(j)) - psih(href, F.host.Oli[i]) + psih(land_use(j), F.host.Oli[i])) / (karman * F.host.Ustar[i])
                raquer                  += aerodynamical_resistance * land_types[j];
                
                //Calculate the surface resistance for gases
                ////////////TODO
                ////////////TODO
                ////////////TODO
                
                // Calculate deposition velocities for gases and sum deposition velocities for all landuse classes
                ////////////TODO
                ////////////TODO
                ////////////TODO

            }
            
            //now calculate the dry deposition velocities
            for(int j = 0 ; j <  NUM_SPECIES ; j++){
                if(density(j) > 0){
                    for(int k = 0 ; k <  NUM_DIAMETER_INTERVALS ; k++){
                        if(F.host.Ustar[i] > eps){
                            stokes_num = v_settling(i, j)/ga * F.host.Ustar[i] * F.host.Ustar[i] /nyl;          //what is the precidence here????
                            alpha      = -3/stokes_num; 

                            if(alpha <=  log10(eps)){
                                deposition_layer_resistance = 1 / (schmidt(i, j) * F.host.Ustar[i]);
                            }
                            else{
                                //rdp=1./((schmi(ic,j)+10.**alpha)*ustar)
                                //is the power translation correct??
                                deposition_layer_resistance = 1 / (pow((schmidt(i, j) + 10), alpha) * F.host.Ustar[i]);
                            }

                            //vdepj=vset(ic,j)+1./(ra+rdp+ra*rdp*vset(ic,j))
                            //Again, is the translation correct
                            interval_deposition_velocity = v_settling(i, j) + 1 / 
                                    ((deposition_layer_resistance + aerodynamical_resistance) + (deposition_layer_resistance * aerodynamical_resistance * v_settling(i ,j)));

                        }
                        else {
                            interval_deposition_velocity = v_settling(i, j);
                        }

                        //deposition velocities of each interval are weighted with mass fraction
                        deposition_velocity(i) = deposition_velocity(i) * interval_deposition_velocity * interval_mass_fraction(i, j); 
                    }
                }
            }            
        }
        
        //If no detailed parameterization available, take constant deposition velocity if that is available
        for(int j = 1 ; j < NUM_SPECIES ; j++){
            if((rel_diff(j) < 0) && (density(j) < 0) && dry_deposition_velocity(j) > 0){
                deposition_velocity(j) = dry_deposition_velocity(j);
            }
        }      
    }
    
    
    //particleDepositionVelocity();
    
    //this should be done in an kernel, the species loop should be unrolled, and done in sequence
    for(int i = 0 ; i < NUM_SPECIES ; i++){
        for(int j = 0 ; j < host.np ; j++){
            Mass(j) = Mass(j) * ( 1 - exp((-deposition_velocity(i) * href * P.host.ldt)/2 * href));
        }
    }
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#if 0
void deposition(){
    // These should be passed in, having been read from a file
    
    // operational switches
    bool dryDepoEnabled = false;                 // if true, calculate dry deposition
    bool wetDepoEnabled = false;                 // if true, calculate wet deposition
    flexArr<bool> dryDepoSpeciesEnabled;            // if true, calculate the dry deposition for the given species
    dryDepoSpeciesEnabled.resize(NUM_SPECIES);
    
    // Radioactive decay
    flexArr<int> decay;                         // radioactive decay constant of species
    decay.resize(NUM_SPECIES);
    
    // Wet deposition of particles
    flexArr<int> weta, wetb;                    // parameters to determine the wet scavenging coefficient
    weta.resize(NUM_SPECIES);
    wetb.resize(NUM_SPECIES);
    
    // Dry deposition of gases
    flexArr<int> relativeDiff;                  //              diffusivity relative to H2O
    flexArr<int> henry;                         // [M/atm]      Henry constant
    flexArr<int> realReactivityO3;              //              reactivity relative to that of O3
    flexArr<int> avgGravSetV;                   // [m/s]        average gravitational settling velocity
    
    relativeDiff.resize(NUM_SPECIES);
    henry.resize(NUM_SPECIES);
    realReactivityO3.resize(NUM_SPECIES);
    avgGravSetV.resize(NUM_SPECIES);
    // ?? RM
    
    // Dry deposition of particles
    flexArr<int> density;                       // [kg/m3]      density of particles                
    flexArr<int> avgParticleDiameter;           // [m]          mean diameter of particles
    flexArr<int> sigmaParticleDiameter;         // sigmaParticleDiameter=10 or sigmaParticleDiameter=0.1 means 
                                                // that 68% of the mass is between 
                                                // 0.1*avgParticleDiameter and 10*avgParticleDiameter
    flexArr<int> massFractionPerInterval;       // mass fraction of each diameter interval
    flexArr<int> schmidt;                       // Schmidt number**2/3 of each diameter interval
    flexArr<int> gravSettlingVel;               // gravitational settling velocity in ni intervals
    flexArr<int> avgGravSettlingVel;            // average gravitational settling velocity
    
    density.resize(NUM_SPECIES);
    avgParticleDiameter.resize(NUM_SPECIES);    
    sigmaParticleDiameter.resize(NUM_SPECIES);
    massFractionPerInterval.resize(NUM_SPECIES, NUM_DIAMETER_INTERVALS);
    schmidt.resize(NUM_SPECIES, NUM_DIAMETER_INTERVALS);
    gravSettlingVel.resize(NUM_SPECIES, NUM_DIAMETER_INTERVALS);
    avgGravSettlingVel.resize(NUM_SPECIES);
    
    // Dry deposition for constant deposition velocity
    //flexArr<int>
    
    // nclass (maxpart)        one of nclassunc classes to which the particle is attributed
    flexArr<int> nclass;
    nclass.resize(MAX_PARTICLES);
    
    // Dry
    flexArr<float> dryDepoMass;
    dryDepoMass.resize(NUM_SPECIES);
    
    
    // variables used in the calculations
    float decayFactor;
    int lastRadioActiveCalc;
    int temp_num_particles;
    
    
    
    // Dry deposition and radioactive decay for each species
    int particle, species;
    
    if(wetDepoEnabled /* and other things : line 276, timemanager*/){
        wetDepo();
    }
    
    // for each particle
    for(particle = 0 ; particle < temp_num_particles ; particle++){
        for(species = 0 ; species < NUM_SPECIES ; species++){
            // calculate the new decay factor for the current time step, if applicable
            if(decay(particle) > 0){
                decayFactor = exp(-FLOAT(syncTime) * decay(species));             
            }
            else {
                decayFactor = 1;
            }

            // if dry depo enabled for this species
            if(dryDepoSpeciesEnabled(species)){
                dryDepoMass(species) = xmass(particle, species) * prob(species) * decayFactor;

                printf("Need to check the middle term: line 116\n");
                xmass(particle, species) = xmass(particle, species) * (1 - prob(species)) * decayFactor;    

                if(decay(species) > 0){
                    xmass(particle, species) = dryDepoMass(species) * exp(FLOAT(abs(ldeltat)) * decay(species));
                }
            }
            else{
                xmass(particle, species) = xmass(particle, species) * decayFactor;
            }
        }
        
        if(dryDepoEnabled){
            dryDepoKernel(nclass(particle), dryDepoMass, sngl(xtra1(particle)),sngl(ytra1(particle)), 
                    nage, dx, xoutshift, dxout, dy, youtshift, dyout, numxgrid, numygrid, nspec, drydepspec, drygridunc);
        }
        
        printf("Should check for nested input at this point!\n");
    }
}
/*----------------------------------------------------------------------------*/
//  Variables:                                                                   *
//                                                                               *
//  nunc             uncertainty class of the respective particle                *
//  nage             age class of the respective particle                        *
//  deposit          amount (kg) to be deposited                                 *
//                                                                               *
void dryDepoKernel(int nunc, flexArr<float> deposit, float x, float y, nage, float dx, float xoutshift, float dxout, float dy, float youtshift, 
        float dyout, numxgrid, numygrid, nspec, drydepspec, drygridunc){
    
    
    
    float w, wx, ddx, x, ddy, wy, y, y1, x1, deposit[NUM_SPECIES];
    int ix, ixp, jy, jyp, k, nage, nunc;
    
    xl = (x * dx + xoutshift) / dxout;
    yl = (y * dy + youtshift) / dyout;
    ix = INT(xl);
    jy = INT(yl);
    ddx = xl - FLOAT(ix);
    ddy = yl - FLOAT(jy);

    if(ddx > 0.5){
        ixp = ix  + 1;
        wx  = 1.5 - ddx;
    } else {
        ixp = ix  - 1;
        wx  = 0.5 + ddx;
    }

    if(ddy > 0.5){
        jyp = jy  + 1;
        wy  = 1.5 - ddy;
    } else {
        jyp = jy  - 1;
        wy  = 0.5 + ddy;
    }
    
    //  Determine mass fractions for four grid points

    int species;
    
    if((ix >= 0) && (jy >= 0) && (ix <= (numxgrid-1)) && (jy <= (numygrid-1))){
        w = wx * wy;
        for(species = 0 ; species < NUM_SPECIES ; species++){
          if (drydepspec(species)){ 
              drygridunc(ix,jy,species,nunc,nage) = drygridunc(ix, jy, species, nunc, nage) + deposit(species) * w;
          }
        }
      }

    if ((ixp >= 0) && (jyp >= 0) && (ixp <= (numxgrid-1)) && (jyp <= (numygrid-1))){
        w = (1.-wx) * (1.-wy);
        for(species = 0 ; species < NUM_SPECIES ; species++){
            if (drydepspec(species)){
                drygridunc(ixp,jyp,species,nunc,nage)= drygridunc(ixp,jyp,species,nunc, nage)+deposit(species)*w;
            }
        }
    }

    if ((ixp >= 0) && (jy >= 0) && (ixp <= (numxgrid-1)) && (jy <= (numygrid-1))){
        w = (1.-wx) * wy;
        for(species = 0 ; species < NUM_SPECIES ; species++){
            if (drydepspec(species)){
                drygridunc(ixp,jy,species,nunc,nage) = drygridunc(ixp,jy,species,nunc, nage) + deposit(species) * w;
            }
        }
    }

    if ((ix >= 0) && (jyp >= 0) && (ix <= (numxgrid-1)) && (jyp <= (numygrid-1))){
        w = wx * (1.-wy);
        for(species = 0 ; species < NUM_SPECIES ; species++){
          if (drydepspec(species)) 
              drygridunc(ix,jyp,species,nunc,nage)= drygridunc(ix,jyp,species,nunc, nage)+deposit(species)*w;
        }
    }
}
/*============================================================================*/
/*
 Calculation of wet deposition using the concept of scavenging coefficients.  *
!  For lack of detailed information, washout and rainout are jointly treated.   *
!  It is assumed that precipitation does not occur uniformly within the whole   *
!  grid cell, but that only a fraction of the grid cell experiences rainfall.   *
!  This fraction is parameterized from total cloud cover and rates of large     *
!  scale and convective precipitation.                                          *
!                                                                               *
!     Author: A. Stohl                                                          *
!                                                                               *
!     1 December 1996                                                           *
!                                                                               *
!  Correction by Petra Seibert, Sept 2002:                                      *
!  use centred precipitation data for integration                               *
!  Code may not be correct for decay of deposition!                             *
!                                                                               *
! *******************************************************************************
!                                                                               *
!  Variables:                                                                   *
!  cc [0-1]           total cloud cover                                         *
!  convp [mm/h]       convective precipitation rate                             *
!  fraction [0-1]     fraction of grid, for which precipitation occurs          *
!  ix,jy              indices of output grid cell for each particle             *
!  itime [s]          actual simulation time [s]                                *
!  jpart              particle index                                            *
!  ldeltat [s]        interval since radioactive decay was computed             *
!  lfr, cfr           area fraction covered by precipitation for large scale    *
!                     and convective precipitation (dependent on prec. rate)    *
!  loutnext [s]       time for which gridded deposition is next output          *
!  loutstep [s]       interval at which gridded deposition is output            *
!  lsp [mm/h]         large scale precipitation rate                            *
!  ltsample [s]       interval over which mass is deposited                     *
!  prec [mm/h]        precipitation rate in subgrid, where precipitation occurs *
!  wetdeposit         mass that is wet deposited                                *
!  wetgrid            accumulated deposited mass on output grid                 *
!  wetscav            scavenging coefficient                                    *
 */
void wetDepo(){
    int i, itime, ix, j, jpart, jy, k, ldeltat, loutnext, ltsample;
    int itage, nage, ngrid;
    float cc, convp, fraction, lsp, prec, wetscav, xtn, ytn;
    float restmass;
    float lfr = {0.5, 0.65, 0.8, 0.9, 0.95};
    float smallnum = 1.e-38;
    float wetdeposit[NUM_SPECIES];
    float cfr = {0.4, 0.55, 0.7, 0.8, 0.9};
    
    // temp variables
    int jpart, nage, j;
    
    // Compute interval since radioactive decay of deposited mass was computed
    if (itime <= loutnext){
        ldeltat = itime - (loutnext-loutstep);
    } else {                                   
        ldeltat = itime - loutnext;
    }
    
    //!  Loop over all particles
    for(jpart = 1 ; jpart < numpart ; jpart++){
//! AF        if (itra1(jpart).eq.-999999999) goto 20
        if(ldirect == 1){
            if (itra1(jpart) > itime) goto 20;
        } else {
            if (itra1(jpart) < itime) goto 20;
        }
        //!  Determine age class of the particle
        itage = abs(itra1(jpart)-itramem(jpart));
        for(nage = 0 ; nage < nageclass ; nage++){
            if (itage < lage(nage)) goto 33;//  !Break
        }
33://    call noop
        
        //!  Determine which nesting level to be used
        ngrid = 0
        for(j = numbnests,1,-1){
            if((xtra1(jpart) > xln(j)) && (xtra1(jpart) < xrn(j)) && (ytra1(jpart) > yln(j)) && (ytra1(jpart) < yrn(j))){
                ngrid = j;
                goto 23;//  !Break
            }
        }
23://    call noop


        //!  Determine nested grid coordinates
        if(ngrid > 0){
            xtn = (xtra1(jpart)-xln(ngrid))*xresoln(ngrid);
            ytn = (ytra1(jpart)-yln(ngrid))*yresoln(ngrid);
            ix = INT(xtn);
            jy = INT(ytn);
        } else {
            ix = INT(xtra1(jpart));
            jy = INT(ytra1(jpart));
        }
        
        //!  Interpolate large scale precipitation, convective precipitation and
        //!  total cloud cover
        //!  Note that interpolated time refers to itime-0.5*ltsample [PS]
        if (ngrid == 0){
            interpol_rain(lsprec,convprec,tcc,nxmax,nymax,1,nx,ny,memind,sngl(xtra1(jpart)), sngl(ytra1(jpart)),1,memtime(1),memtime(2),nint(itime-0.5*ltsample),lsp,convp,cc);
        } else {
            interpol_rain_nests(lsprecn,convprecn,tccn,nxmaxn,nymaxn,1,maxnests,ngrid,nxn,nyn, memind,xtn,ytn,1,memtime(1),memtime(2),nint(itime-0.5*ltsample),lsp,convp,cc);
        }

        if((lsp < 0.01) && (convp < 0.01)) goto 20;

        //!  1) Parameterization of the the area fraction of the grid cell where the
        //!     precipitation occurs: the absolute limit is the total cloud cover, but
        //!     for low precipitation rates, an even smaller fraction of the grid cell
        //!     is used. Large scale precipitation occurs over larger areas than
        //!     convective precipitation.
        //! **********************************************************

        if(lsp > 20.){
          i = 5;
        } else if(lsp > 8.){
          i = 4;
        } else if (lsp > 3.){
          i = 3;
        } else if (lsp > 1.){
          i = 2;
        } else {
          i = 1;
        }

        if (convp > 20.){
          j = 5;
        } else if (convp > 8.){
          j = 4;
        } else if (convp > 3.){
          j = 3;
        } else if (convp > 1.){
          j = 2;
        } else { 
          j = 1;
        }

        fraction = max(0.05,cc*(lsp*lfr(i)+convp*cfr(j))/(lsp+convp));
        
        //!  2) Computation of precipitation rate in sub-grid cell
        //! **********************************************************
        prec = (lsp+convp)/fraction;


        //!  3) Computation of scavenging coefficients for all species
        //!     Computation of wet deposition
        //! **********************************************************

        for(k = 1; k < nspec ; k++){
            if (weta(k) > 0.){
              wetscav = weta(k)*prec**wetb(k);
              wetdeposit(k) = xmass1(jpart,k)* (1.-exp(-wetscav*abs(ltsample)))*fraction;
    //!            new particle mass:
              restmass = xmass1(jpart,k)-wetdeposit(k);
              if (restmass  >  smallnum){
                xmass1(jpart,k) = restmass;
              } else {
                xmass1(jpart,k) = 0.;
              }

              //!  Correct deposited mass to the last time step when radioactive decay of 
              //!  gridded deposited mass was calculated
              //! ***********************************************************************
              if (decay(k) > 0.){
                  wetdeposit(k) = wetdeposit(k)*exp(abs(ldeltat)*decay(k));
              }
            } else {
                wetdeposit(k) = 0.;
            }
        }
        
        //!  Add the wet deposition to accumulated amount on output grid and nested output grid
        //! ***********************************************************************************

        wetdepokernel(nclass(jpart),wetdeposit,sngl(xtra1(jpart)),sngl(ytra1(jpart)),nage,dx, xoutshift,dxout,dy,youtshift,dyout,numxgrid,numygrid,nspec,wetgridunc);
        
        if(nested_output == 1) 
            wetdepokernel_nest(nclass(jpart),wetdeposit,sngl(xtra1(jpart)), sngl(ytra1(jpart)),nage,dx,xoutshiftn,dxoutn,dy,youtshiftn,dyoutn,numxgridn,numygridn,nspec, wetgriduncn);

20:     //    call noop
    }



        



}
#endif
/*============================================================================*/
/*============================================================================*/
/*============================================================================*/
/*============================================================================*/
