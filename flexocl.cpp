#include "flexocl.h"
#include "flexutility.h"
#include "flexparam.h"
#include <stdio.h>
#include <stdlib.h>
 #include <sys/time.h>

/**
 * Implementation of the flexOcl class and methods
 */

#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

// boolean to use the map instead of copy
#define USE_MAP_BUFFER 0

//these are statics used to pass scalars to the kernel
static Parameter static_p;
static int static_num_particles;
static cl_mem null_mem;

//Globals for openCL
cl_context              context;
cl_command_queue        command_queue;
cl_program              program;
cl_kernel               super_kernel;

cl_uint                 num_platforms;
cl_uint                 num_devices;

cl_platform_id*         clPlatformIDs;
cl_device_id*           clDeviceIDs;

cl_int                  errorNum;

int                     platform;
int                     device;
        
const char*             main_kernel_source_file = "kernel_flexocl.cl" ;


extern cl_mem float_probe, int_probe;
/*----------------------------------------------------------------------------*/
timestamp_t get_timestamp()
{
  struct timeval now;
  gettimeofday (&now, NULL);
  return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}
/*----------------------------------------------------------------------------*/    
void initOpenCL(int platform_choice, int device_choice, bool list_details){
    
    printf("OPENCL : initOpenCL => platform<%d>, device<%d>\n", platform_choice, device_choice);
    
    char chBuffer[1024];
    
    platform    = platform_choice;
    device      = device_choice;
    
#if DEBUG || 1
    list_details = true;
#endif
    
    // Get OpenCL platform count
    errorNum = clGetPlatformIDs (0, NULL, &num_platforms);
    if(errorNum != CL_SUCCESS){
        printf("clGetPlatformIDs (get platforms): %s\n", decode_error(errorNum));
        exit(-1);
    }
    
    
    if(num_platforms == 0)
    {
        //shrLog("No OpenCL platform found!\n\n");
        printf("No OpenCL platform found!\n\n");
        exit(-2000);
    }
    else {
        // if there's a platform or more, make space for ID's
        if ((clPlatformIDs = (cl_platform_id*)malloc(num_platforms * sizeof(cl_platform_id))) == NULL)
        {
            //shrLog("Failed to allocate memory for cl_platform ID's!\n\n");
            printf("Failed to allocate memory for cl_platform ID's!\n\n");
            exit(-3000);
        }
        
        if ((clDeviceIDs = (cl_device_id*)malloc(20 * sizeof(cl_device_id))) == NULL)
        {
            //shrLog("Failed to allocate memory for cl_platform ID's!\n\n");
            printf("Failed to allocate memory for clDeviceIDs ID's!\n\n");
            exit(-3000);
        }
        
        // get platform info for each platform 
        errorNum = clGetPlatformIDs (num_platforms, clPlatformIDs, NULL);
        
        if(list_details) printf("%d Available platforms:\n", num_platforms);
        
        for(int i = 0; i < num_platforms; ++i)
        {
            if((errorNum = clGetPlatformInfo (clPlatformIDs[i], CL_PLATFORM_VERSION, 1024, &chBuffer, NULL)) != CL_SUCCESS){
                printf("clGetPlatformInfo (CL_PLATFORM_VERSION): %s\n", decode_error(errorNum));
                exit(-1);
            }
            
            printf("VERSION : %s\n", chBuffer);
            
            //Get the info for this platform
            errorNum = clGetPlatformInfo (clPlatformIDs[i], CL_PLATFORM_NAME, 1024, &chBuffer, NULL);
            if(errorNum == CL_SUCCESS)
            {
                if(list_details)printf("platform %d: %s  ", i, chBuffer);

                //get the Devices of this platform
                errorNum = clGetDeviceIDs(clPlatformIDs[i], CL_DEVICE_TYPE_ALL, 20, clDeviceIDs, &num_devices);
                
                if (errorNum != CL_SUCCESS)
                {
                    printf("clGetDeviceIDs: %s\n", decode_error(errorNum));
                    exit(-1000);
                }
                else {
                    if(list_details) printf("[Devices  : %d]\n", num_devices);
                    
                    //for each device
                    for(int j = 0; j < num_devices; ++j){
                        //get the info
                        errorNum = clGetDeviceInfo (clDeviceIDs[j], CL_DEVICE_NAME, 1024, &chBuffer, NULL);
                        if(errorNum == CL_SUCCESS)
                        {
                            if(list_details) printf("\t* device %d ID: %s\n", j, chBuffer);
                        }
                        else {
                            printf("clGetDeviceInfo: %s\n", decode_error(errorNum));
                            exit(-1000);
                        }
			
			cl_int clint;
			errorNum = clGetDeviceInfo (clDeviceIDs[j], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_int), &clint, NULL);
                        if(errorNum == CL_SUCCESS)
                        {
                            printf("\t* device %d CORES: %d\n", j, clint);
                        }
                        else {
                            printf("clGetDeviceInfo: %s\n", decode_error(errorNum));
                            exit(-1000);
                        }

                        errorNum = clGetDeviceInfo (clDeviceIDs[j], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_int), &clint, NULL);
                        if(errorNum == CL_SUCCESS)
                        {
                            printf("\t* device %d CLOCK_FREQ: %d\n", j, clint);
                        }
                        else {
                            printf("clGetDeviceInfo: %s\n", decode_error(errorNum));
                            exit(-1000);
                        }  

			size_t workitem_size[3];
                        errorNum = clGetDeviceInfo (clDeviceIDs[j], CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(workitem_size), &workitem_size, NULL);
                        if(errorNum == CL_SUCCESS)
                        {
                            printf("\t* device %d WORK_ITEMS_PER_DIMENSION: %d\n", j, workitem_size[0]);
                        }
                        else {
                            printf("clGetDeviceInfo: %s\n", decode_error(errorNum));
                            exit(-1000);
                        }  


                    }
                }
            }
            else {
                printf("clGetPlatformInfo (): %s\n", decode_error(errorNum));
            }
            
            if( i == platform_choice)
                break;
        }
    }

    //if(list_details) exit(0);

    
    //we now have platforms and devices, create the context and command queue
    cl_context_properties properties[] = { CL_CONTEXT_PLATFORM, ((cl_context_properties)clPlatformIDs[platform_choice]), 0 };	
    context = clCreateContext(properties, num_devices, clDeviceIDs, NULL, NULL, &errorNum);
    if(errorNum != CL_SUCCESS){
        printf("clCreateContext (create context): %s\n", decode_error(errorNum));
        exit(-1);
    }
        
    
    
    command_queue = clCreateCommandQueue(context, clDeviceIDs[device_choice], 0, &errorNum);
    if(errorNum != CL_SUCCESS){
        printf("clCreateCommandQueue (create command queue): %s\n", decode_error(errorNum));
        exit(-1);
    }
    
    
    size_t src_size = 0;
    const char* source = file_contents(main_kernel_source_file, &src_size);
    program = clCreateProgramWithSource(context, 1, &source, &src_size, &errorNum);
    
    if(errorNum != CL_SUCCESS){
        printf("clCreateProgramWithSource (%s): %s\n", main_kernel_source_file, decode_error(errorNum));
        exit(-1);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    
    //build the program
    if((errorNum = clBuildProgram(program, 1, &clDeviceIDs[device_choice], "-cl-fast-relaxed-math", NULL, NULL) != CL_SUCCESS)){
        printf("clBuildProgram: %s\n", decode_error(errorNum));
    }
        
    //get the log and print
    char* build_log;
    size_t log_size;

    errorNum = clGetProgramBuildInfo(program, clDeviceIDs[device_choice], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
    if(errorNum != CL_SUCCESS){
        printf("clGetProgramBuildInfo-A: %s\n", decode_error(errorNum));
        exit(-1);
    }

    build_log = new char[log_size+1];
    errorNum = clGetProgramBuildInfo(program, clDeviceIDs[device_choice], CL_PROGRAM_BUILD_LOG, log_size, build_log, &log_size);
    if(errorNum != CL_SUCCESS){
        printf("clGetProgramBuildInfo-B: %s\n",  decode_error(errorNum));
        exit(-1);
    }

    build_log[log_size] = '\0';
    printf("\nlog:\n%s\n", build_log);
        
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    
    /*
     * There is only 1 kernel file, and one kernel function. The "kernel_choice"
     * argument will indiacte which code to run
     */
    
    //get the kernel
    super_kernel = clCreateKernel(program, "super_kernel", &errorNum);
    if(errorNum != CL_SUCCESS){
        printf("clCreateKernel: %s\n", decode_error(errorNum));
        exit(-1);
    }
    
    cl_ulong max;
    errorNum = clGetDeviceInfo (clDeviceIDs[device_choice], CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong), &max, NULL);
    if(errorNum == CL_SUCCESS)
    {
        printf("MAX SIZE %lu\n", max);
    }
    else {
        printf("clGetDeviceInfo: %s\n", decode_error(errorNum));
        exit(-1000);
    }
            
    
    //SHOULD BE REMOVED : these are for debug purposes
    //mallocOnDevice(&float_probe, sizeof(float) * 10000, NULL);
    //mallocOnDevice(&int_probe, sizeof(int) * 10000, NULL);
    
    //this is a placeholder for null memory kernel arguments
    mallocOnDevice(&null_mem, sizeof(int), NULL);
    
    printf("\n****** openCLInit - complete ******\n\n");
    
}
/*----------------------------------------------------------------------------*/
int getMaxComputeUnits(){
    
    PRINTF("OPENCL : getMaxComputeUnits :: ");
    cl_int num;
    
    errorNum = clGetDeviceInfo (clDeviceIDs[device], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_int), &num, NULL);
    if(errorNum != CL_SUCCESS){
        printf("clGetDeviceInfo (CL_DEVICE_MAX_COMPUTE_UNITS): %s\n", decode_error(errorNum));
        exit(-1);
    }
    
    PRINTF("%d \n", num);
    return (int)num;
} 
/*----------------------------------------------------------------------------*/
void tearDownOpenCL(){
    
    PRINTF("OPENCL : tearDown\n");
    
    clReleaseCommandQueue(command_queue);
    clReleaseContext(context);
   
    PRINTF("OPENCL : tearDown - done\n");
}
/*----------------------------------------------------------------------------*/
void createKernel(char* kernelFunction, cl_kernel *kernel){
    
    PRINTF("OPENCL : createKernel\n");
    
    
    
    //get the kernel
    *kernel = clCreateKernel(program, kernelFunction, &errorNum);
    if(errorNum != CL_SUCCESS){
        printf("clCreateKernel: %s\n", decode_error(errorNum));
        printf("%s\n", kernelFunction);
        exit(-1);
    }
    else{
        printf("Success creating kernel %s\n", kernelFunction);
    }
    
}
/*----------------------------------------------------------------------------*/
cl_kernel getSuperKernel(){
    return super_kernel;
}
/*----------------------------------------------------------------------------*/
const char* kernelName(int kernel){
    if(kernel == MASS_KERNEL)
        return "MASS_KERNEL";
    if(kernel == INTERPOLATION_KERNEL)
        return "INTERPOLATION_KERNEL";
    if(kernel == INTERPOLATION_KERNEL_WITH_FLUX)
        return "INTERPOLATION_KERNEL_WITH_FLUX";
    if(kernel == DISPERSION_KERNEL)
        return "DISPERSION_KERNEL";
    if(kernel == CONVECTION_KERNEL)
        return "CONVECTION_KERNEL";
    
    //TODO: fill in the rest
}
/*----------------------------------------------------------------------------*/
void setKernelArgs(int kernel_choice, int argNum, int dataSize, void* data){
#ifdef OPENCL
    //errorNum  = clSetKernelArg(super_kernel, argNum, dataSize, data);
    
    if(data == NULL){
        errorNum  = clSetKernelArg(super_kernel, argNum, sizeof(cl_mem), &null_mem);
    }
    else{
        errorNum  = clSetKernelArg(super_kernel, argNum, dataSize, data);
    }
    
    if(errorNum != CL_SUCCESS){
        printf("clSetKernelArg:%s-[%d] %s\n", kernelName(kernel_choice), argNum, decode_error(errorNum));
        exit(-1);
    }
#endif
}
/*----------------------------------------------------------------------------*/
/**
 * 
 * @param global_size   This is the total number of work units (array indicies)
 * @param local_size    This is the size of items in each work group
 */
void runKernelBlocking(size_t* global_size, size_t local_size){
    
    PRINTF("OPENCL : runBlockingKernel\n");
    PRINTF("OPENCL : global_size %d, local_size (inferred)\n", *global_size);
    //run
    errorNum = clEnqueueNDRangeKernel(command_queue, super_kernel, 1, NULL, global_size, NULL, 0, NULL, NULL);
    if(errorNum != CL_SUCCESS){
        printf("clEnqueueNDRangeKernel: %s\n", decode_error(errorNum));
        exit(-1);
    }    
    
   // int probe[100];
   // getDeviceData(probe, int_probe, sizeof(int) * 100);
   // printf("probe: %s\n", kernelName(probe[0]));
    
    PRINTF("OPENCL: now wait for kernel to finish\n");   
 
    //block
    //errorNum = clFinish(command_queue);
    errorNum = clEnqueueBarrier(command_queue);
    if(errorNum != CL_SUCCESS){
        printf("clFinish: %s\n", decode_error(errorNum));
        exit(-1);
    }    
    
}
/*----------------------------------------------------------------------------*/
// on the device, copy the data from one buffer to another
void copyDeviceData(cl_mem dst, const void *src, size_t size){
    
    printf("Not sure that this function can exists\n");
}
/*----------------------------------------------------------------------------*/
/**
 * Allocate size bytes of space on the device, and save the pointer to ptr
 * 
 * @param ptr
 * @param size
 */
void mallocOnDevice(cl_mem *ptr, size_t size, void* hostPtr){
    
    PRINTF("OPENCL : MallocOnDevice ");
#ifdef OPENCL
#if !USE_MAP_BUFFER 
    *ptr = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &errorNum);
#else
    if(hostPtr == NULL){
	*ptr = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &errorNum);	
    }	
    else {
    	// we are on the cpu so map the host pointer, don't copy
    	*ptr = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, size, hostPtr, &errorNum);
    }
#endif
    if(errorNum != CL_SUCCESS){
        printf("clCreateBuffer : %s\n", decode_error(errorNum));
        exit(-1);
    }
#if USE_MAP_BUFFER
// the dest has been created with the use_host_ptr so we don't need to get anything back??
     clEnqueueMapBuffer(command_queue, *ptr, CL_TRUE, CL_MAP_READ|CL_MAP_WRITE, 0, size, 0 , NULL, NULL, &errorNum);

     if(errorNum != CL_SUCCESS){
	printf("clEnqueueMapBuffer : %s\n", decode_error(errorNum));
        exit(-1);
     }
#endif
	 PRINTF("of size %d, host<%p> device<%p> DONE\n", size, hostPtr, ptr);
#endif
}
/*----------------------------------------------------------------------------*/
/**
 * Copy size bytes from src on the host to dest on the device. Dest should 
 * already be allocated.
 * 
 * @param dest
 * @param src
 * @param size
 */
void setDeviceData(cl_mem dest, void* src, size_t size){
    
    PRINTF("OPENCL : setDeviceData of size %d host<%p> device<%p>", size, src, dest);
#ifdef OPENCL
#if !USE_MAP_BUFFER    
    errorNum = clEnqueueWriteBuffer(command_queue, dest, CL_TRUE, 0, size, src, 0, NULL, NULL);
    
    if(errorNum != CL_SUCCESS){
        printf("clEnqueueWriteBuffer : %s\n", decode_error(errorNum));
        exit(-1);
    }   
#else
   // the dest has been created with the use_host_ptr so we don't need to get anything back??
//    clEnqueueMapBuffer(command_queue, dest, CL_TRUE, CL_MAP_READ|CL_MAP_WRITE, 0, size, 0 , NULL, NULL, &errorNum);

//    if(errorNum != CL_SUCCESS){
//        printf("clEnqueueMapBuffer : %s\n", decode_error(errorNum));
//        exit(-1);
//    }

#endif
	 PRINTF(" DONE\n");
#endif
}
/*----------------------------------------------------------------------------*/
/**
 * Copy size bytes from src on the device to dest on the host
 * 
 * @param dest
 * @param src
 * @param size
 */
void getDeviceData(void* dest, cl_mem src, size_t size){
    
    PRINTF("OPENCL : getDeviceData of size %d <%p>", size, src);
#if !USE_MAP_BUFFER    
    errorNum = clEnqueueReadBuffer(command_queue, src, CL_TRUE, 0, size, dest, 0, NULL, NULL);
    
    if(errorNum != CL_SUCCESS){
        printf("clEnqueueReadBuffer : %s\n", decode_error(errorNum));
        exit(-1);
    }    
#else
    // we should just be able to read from the mapped memory here - i.e. memcpy
    //errorNum = clEnqueueReadBuffer(command_queue, src, CL_TRUE, 0, size, dest, 0, NULL, NULL);

    //if(errorNum != CL_SUCCESS){
    //    printf("clEnqueueReadBuffer : %s\n", decode_error(errorNum));
    //    exit(-1);
    //}
    //
    // As the memory is mapped, the data should already be is dest??

#endif

	PRINTF(" DONE\n");
}
/*----------------------------------------------------------------------------*/
/**
 * Set size bytes of memory pointed to by ptr to 0s on the device
 * @param ptr
 * @param size
 */
void clearDeviceMemory(void *ptr, size_t size){
    PRINTF("OPENCL : clearDeviceMemory\n");

#if USE_MAP_BUFFER
	memset(ptr, 0, size);
#endif

}
/*----------------------------------------------------------------------------*/
/**
 * Free memory allocated on the device
 * @param ptr
 */
void freeOnDevice(cl_mem ptr){
    
    PRINTF("OPENCL : freeOnDevice\n");
#ifdef OPENCL
    if(ptr == NULL)
        return;
#if 0//USE_MAP_BUFFER
    if((errorNum = clEnqueueUnmapMemObject(command_queue, ptr, memOBJ, NULL, NULL)) != CL_SUCCESS){
	printf("clEnqueueUnmapObject : %s\n", decode_error(errorNum));
	exit(-1);
    }
#endif 
    
    if((errorNum = clReleaseMemObject(ptr)) != CL_SUCCESS){;
        printf("clReleaseMemObject : %s\n", decode_error(errorNum));
        exit(-1);
    }
#endif
}
/*----------------------------------------------------------------------------*/
void runKernel( int itterations, 
                int choice, 
                DeviceClass *C, 
                cl_mem G, 
                const flexParam *P, 
                DeviceField *F, 
                DeviceField *F1, 
                DeviceField *F2, 
                float field_mul_1, 
                float field_mul_2){
    
    PRINTF("FLEXOCL : Run kernel %d (this code is not ready)\n", choice);
    
    cl_int errorNum; 
    cl_uint var = 0;
    cl_kernel kernel = getSuperKernel();
    float dummyVal = 0.0;
    
    
  //  int choice = DISPERSION_KERNEL;
    //the first argument indicates the kernel code to run
    errorNum  = clSetKernelArg(kernel, var++, sizeof(int), &choice);
    
    //this should be removed after debugging
    //errorNum  = clSetKernelArg(kernel, var++, sizeof(cl_mem), &float_probe);
    //errorNum  = clSetKernelArg(kernel, var++, sizeof(cl_mem), &int_probe);
    
    if(F && F1 && F2){
        int sizes[14];
        unsigned int sizeNdx = 0;
        /*
         //group by length
        sizes[sizeNdx++] = U.nd();
        sizes[sizeNdx++] = V.nd();
        sizes[sizeNdx++] = W.nd();
        sizes[sizeNdx++] = Z.nd();
        sizes[sizeNdx++] = T.nd();
        sizes[sizeNdx++] = RH.nd();
        sizes[sizeNdx++] = Rho.nd();
        sizes[sizeNdx++] = DRho.nd();

        //group by length
        sizes[sizeNdx++] = Ps.nd();
        sizes[sizeNdx++] = PBL.nd();
        sizes[sizeNdx++] = Tropo.nd();
        sizes[sizeNdx++] = Ustar.nd();
        sizes[sizeNdx++] = Wstar.nd();
        sizes[sizeNdx++] = Oli.nd();
         */
        sizeNdx = 0;
        
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->U);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->U);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->U);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->V);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->V);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->V);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->W);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->W);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->W);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Z);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->Z);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->Z);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->T);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->T);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->T);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->RH);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->RH);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->RH);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Rho);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->Rho);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->Rho);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->DRho);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->DRho);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->DRho);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Ps);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->Ps);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->Ps);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->PBL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->PBL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->PBL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Tropo);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->Tropo);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->Tropo);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Ustar);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->Ustar);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->Ustar);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Wstar);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->Wstar);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->Wstar);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Oli);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F1->Oli);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F2->Oli);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_1);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &field_mul_2);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &sizes[sizeNdx++]);
    }
    else{
        //the interpolation kernel arguments
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
    }


    //The grid
    if(G){
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &G);       //should be able to pass struct directly
    }
    else {
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
    }
    
    //The Parameter
    if(P->d_Gauss){
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &P->d_Gauss); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &P->d_Rand); 

        //set the static values
        static_p.ctl = P->host.ctl;
        static_p.d_strat = P->host.d_strat;
        static_p.d_trop = P->host.d_trop;
        static_p.dispersion = P->host.dispersion;
        static_p.ifine = P->host.ifine;
        static_p.lconv = P->host.lconv;
        static_p.ldirect = P->host.ldirect;
        static_p.ldt = P->host.ldt;
        static_p.lensec = P->host.lensec;
        static_p.lflx = P->host.lflx;
        static_p.lsynctime = P->host.lsynctime;
        static_p.ng = P->host.ng;
        static_p.openCL = P->host.openCL;
        static_p.openmp = P->host.openmp;
        static_p.outsec = P->host.outsec;
        static_p.timing = P->host.timing;
        static_p.turbmesoscale = P->host.turbmesoscale;

        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.ctl); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &static_p.d_strat); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &static_p.d_trop); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.dispersion); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.ifine); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.lconv); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.ldirect); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.ldt); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.lensec); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.lflx); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.lsynctime); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.ng); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.openCL); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.openmp); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.outsec); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_p.timing); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &static_p.turbmesoscale); 
    }
    else{
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum); 
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal); 
    }
    //class variables
    if(C){
        static_num_particles = C->np;
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &static_num_particles);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Idx);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Gdx);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);                   //never used, should be removed
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->X);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Y);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Z);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Fg);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Up);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Vp);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Wp);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Um);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Vm);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Wm);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Zs);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &C->Zb);
    }
    else {
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum);          //TODO!!!!!! errornum just a place holder!
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
    }
    
    //field variables
    if(F){
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->U);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->V);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->W);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Z);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Rho);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->DRho);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->T);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->RH);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->PBL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Tropo);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Ustar);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Wstar);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Oli);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), &F->Ps);
    }
    else{
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
    }
        
    //Convection Params
    if(0){
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum);
    }
    else{
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &errorNum);
    }
    
    //Mass Parameters
    if(0){
        
    }
    else{
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(cl_mem), NULL);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(int), &var);
        errorNum |= clSetKernelArg(kernel, var++, sizeof(float), &dummyVal);
    }
    if(errorNum != CL_SUCCESS){
        printf("clSetKernelArg- dispersion: %s\n", decode_error(errorNum));
        exit(-1);
    }
    
    //printf("This should be calculated by querying the device!!\n");
    const size_t local_ws = 256;	// Number of work-items per work-group
    
    // shrRoundUp returns the smallest multiple of local_ws bigger than size
    size_t global_ws = shrRoundUp(local_ws, itterations);	// Total number of work-items
    
    
    runKernelBlocking(&global_ws, local_ws);
    
    
}
/*----------------------------------------------------------------------------*/
void initKernelArgs(){
    
    int var = 0, choice = -1;
    float dummyFloat;
    double dummyDouble;
    
    //the first argument indicates the kernel code to run
    setKernelArgs(choice, var++, sizeof(int), &var);
    
//    setKernelArgs(choice, var++, sizeof(cl_mem), &float_probe);
//    setKernelArgs(choice, var++, sizeof(cl_mem), &int_probe);

    // These are debug values that should be removed from the final code
    //setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    //setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    
    //the interpolation kernel arguments
    setKernelArgs(choice, var++, sizeof(float), &dummyDouble);
    setKernelArgs(choice, var++, sizeof(float), &dummyDouble);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    
    //the grid
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    
    //The Parameter 
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL); 
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(float), &dummyFloat); 
    setKernelArgs(choice, var++, sizeof(float), &dummyFloat); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(int), &var); 
    setKernelArgs(choice, var++, sizeof(float), &dummyFloat); 
    
    //the Class
    setKernelArgs(choice, var++, sizeof(int), &var);          
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    
    //the field
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    setKernelArgs(choice, var++, sizeof(cl_mem), NULL);
    //setKernelArgs(choice, var++, sizeof(int), &var);
    //setKernelArgs(choice, var++, sizeof(int), &var);
    //setKernelArgs(choice, var++, sizeof(int), &var);
    //setKernelArgs(choice, var++, sizeof(float), &dummyFloat);
            
    //radiation halflife
    setKernelArgs(choice, var++, sizeof(int), &var);        
    setKernelArgs(choice, var++, sizeof(float), &dummyFloat);
    setKernelArgs(choice, var++, sizeof(float), &dummyFloat);
    setKernelArgs(choice, var++, sizeof(float), &dummyFloat);
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
