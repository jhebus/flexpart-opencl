/* 
 * File:   constants.h
 * Author: jhebus
 *
 * Created on August 6, 2013, 3:43 PM
 * 
 * The purpose of this file is to contain constant values used in the simulator.
 * it is really the equivalent of the flexpart "includepar"
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#define DENSITY_IODINE_131      (2.5e03)
/**
 * The different types of chemical that can be in the air
 */
#define NUM_SPECIES             ( 1 )
#define NUM_DIAMETER_INTERVALS  ( 13 )

#define MAX_PARTICLES           ( -1 )

/**
 * The different sizes of diameter (need to know what this really is and give a better explanation) 
 */
static int num_diameter_intervals = 1;

/*
  ! pi                      number "pi"
  ! pi180                   pi/180.
  ! r_earth                 radius of earth [m]
  ! r_air                   individual gas constant for dry air [J/kg/K]
  ! ga                      gravity acceleration of earth [m/s**2]
  ! cpa                     specific heat for dry air
  ! kappa                   exponent of formula for potential temperature
  ! vonkarman               von Karman constant
*/
static float pi                 = 3.14159265;
static float r_earth            = 6.371e6;
static float r_air              = 287.05;
static float ga                 = 9.81;
static float cpa                = 1004.6;
static float kappa              = 0.286;
static float pi180              = pi/180;
static float vonkarman          = 0.4;



/*
  ! karman                  Karman's constant
  ! href [m]                Reference height for dry deposition
  ! konvke                  Relative share of kinetic energy used for parcel lifting
  ! hmixmin,hmixmax         Minimum and maximum allowed PBL height
  ! turbmesoscale           the factor by which standard deviations of winds at grid
  !                    points surrounding the particle positions are scaled to
  !                    yield the scales for the mesoscale wind velocity fluctuations
  ! d_trop [m2/s]           Turbulent diffusivity for horizontal components in the troposphere
  ! d_strat [m2/s]          Turbulent diffusivity for vertical component in the stratosphere
 */
static float karman             = 0.40;
static float href               = 15;
static float convke             = 2.0;
static float hmixmin            = 100;
static float hmixmax            = 4500;
static float turbmesoscale      = 0.16;
static float d_trop             = 50;
static float d_strat            = 0.1;

//prandalt number
static float pr                 = 0.72;

//constant used in deposition, don't know what though
static float eps = 1.5e-5;


// The different types of land (for deposition)
#define NUM_OF_LAND_TYPES       ( 13  )
#define CHARNOCK                ( 0.0 )

static float land_types[NUM_OF_LAND_TYPES] = {
                                0.7,            //urban land
                                0.1,            //Agricultural land
                                0.1,            //range land
                                1.0,            //Deciduous Forest
                                1.0,            //Coniferous Forest
                                0.7,            //Mixed forest including wet land
                                CHARNOCK,       //Water (salt and fresh)
                                0.01,           //Barren land, mostly desert
                                0.1,            //Non-forested wetland
                                0.1,            //Mixed Agricultural & range land
                                0.05,           //Rocky open areas with long growing shrubs
                                0.001,          //snow and ice
                                1.0             //Rainforest
};
   
// The seasons
#define SPRING                  ( 0 )
#define SUMMER                  ( 1 )
#define AUTUMN                  ( 2 )
#define WINTER                  ( 3 )


static int      numClassUncert          =       10;             // number of classes used to calcuate the uncertatinty
#endif	/* CONSTANTS_H */

