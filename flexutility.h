/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXUTILITY_H__
#define __FLEXUTILITY_H__

#ifdef __APPLE__
#include <OpenCL/CL.h>
#else
#include <CL/cl.h>
#endif

#define DAYSEC	86400.0		// seconds in a day

#ifdef _WIN32
#pragma warning(disable: 4018)
#pragma warning(disable: 4244)
#pragma warning(disable: 4267)
#pragma warning(disable: 4305)
#pragma warning(disable: 4554)
#pragma warning(disable: 4996)
#endif

#define MISSING -1.e15

#ifndef max
#define max(a,b) (a)>(b)?(a):(b)
#endif

#ifndef min
#define min(a,b) (a)<(b)?(a):(b)
#endif


/*! \addtogroup utility
 Documentation for utility functions.
 @{
 */

/** Calculates Julian day number from Gregorian date.
 @param yy Year
 @param mm Month
 @param dd Day
 */
int cal2jul(int yy, int mm, int dd);

/** Calculates Gregorian date from Julian day number.
 @param yy Year
 @param mm Month
 @param dd Day
 @param hh Hour
 @param mi minute
 @param jul Julian day number
 */
void jul2cal(int &yy, int &mm, int &dd, int &hh, int &mi, double jul);

/** Generates uniform random number (0 to 1).
 @param idum Random number seed
 */
float ran1(long *idum);

/** Generates Gaussian random number (0 to 1).
 @param idum Random number seed
 */
float gasdev1(long *idum);

/**
 Thread-safe version of the uniform random number generator.
 */
float ran2(long &idum, long &iy, long *iv, int nv);

/**
 Thread-safe version of the Gaussian random number generator.
 */
void gasdev2(long &idum, long &iy, long *iv, int nv, float &gset1, float &gset2);

/**
 Throws exception.
 */
void errmsg(const char *msg1, const char *msg2=0, const char *msg3=0);

/**
 Print variable values for debugging.
 */
void print_variable(const char *vname, float *ptr, int np, int l);

/**
 Clamp variable values.
 */
void clamp_variable(float *ptr, int np, float vmin, float vmax);

/**
 * Return a string of the openCL error string
 */
const char* decode_error(cl_int error);


/**
 * Return the contents of the file specified by filename, and the size of the 
 * file is stored into length
 * 
 * @param filename
 * @param length
 * @return 
 */
char *file_contents(const char *filename, size_t *length);

/**
 * 
 * @param group_size
 * @param global_size
 * @return 
 */
size_t shrRoundUp( int group_size, int global_size );

#endif
