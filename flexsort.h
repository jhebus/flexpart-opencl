#ifndef __FLEXSORT_H__
#define __FLEXSORT_H__


template <typename T>
inline void Swap(T& a, T& b)
{
	T c=a; a=b; b=c;
}

/*
 * For heap sort of assending order
 */
template <typename T>
void SiftDown(T *v, int *idx, int parent, int n)
{
	// 0 based child-parent relationship
	int child=2*parent+1;
	
	// find where element being sifted belongs
	while (child<n) {
		// use right child if it is bigger
		if (child!=n-1 && v[child]<v[child+1]) child++;
		// check if it is done
		if (v[parent]>=v[child]) break;
		// if not, swap elements and move parent down
		Swap(v[parent],v[child]);
//		if (idx) Swap (idx[parent], idx[child]);
		Swap(idx[parent],idx[child]);
		parent=child;
		child=2*parent+1;
	}
}


/*
 * For heap sort of decending order
 */
template <typename T>
void SiftUp(T *v, int *idx, int parent, int n)
{
	// 0 based child-parent relationship
	int child=2*parent+1;
	// find where element being sifted belongs
	while (child<n) {
		// use right child if it is smaller
		if (child!=n-1 && v[child]>v[child + 1]) child++;
		if (v[parent]<=v[child]) break;
		// if not, swap elements and move parent down
		Swap(v[parent],v[child]);
//		if (idx) Swap(idx[parent],idx[child]);
		Swap(idx[parent],idx[child]);
		parent=child;
		child=2*parent+1;
	}
}

/*
 * Sort v[] in assending or dessending order according to the flag.
 * If an index vector is provided, corresponding sorted indices can be
 * used to rematange accompany vectors.
 */
template <typename T>
void HeapSort(T *v, int *idx, int n, int flag=1)
{
	int i;
	
//	if (idx) { for (i=0; i<n; i++) idx[i]=i; }
	for (i=0; i<n; i++) idx[i]=i;

	// construct heap
	for (i=(n-1)/2; i>=0; i--) {
		if (flag > 0)
			SiftDown(v,idx,i,n);
		else
			SiftUp(v,idx,i,n);
	}

	for (i=n-1; i>=1; i--) {
		// put top element in its correct position
		Swap(v[0],v[i]);
//		if (idx) Swap(idx[0],idx[i]);
		Swap(idx[0],idx[i]);
		// rebuild heap
		if (flag>0)
			SiftDown(v,idx,0,i);
		else
			SiftUp(v,idx,0,i);
	}
}

#endif
