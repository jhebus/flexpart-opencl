/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexgrid.h"
#include "flexutility.h"
#include "flexcdf.h"
#include <math.h>
#include <string.h>
#include <stdio.h>


#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define MAX_GRID 1024		// maximum grid poind

flexGrid::flexGrid()
{
	nested=NULL;
	device=NULL;
	setName(P_NAME,"Pre");
	setName(Lat_NAME,"Lat");
	setName(Lon_NAME,"Lon");
	setName(U_NAME,"U");
	setName(Us_NAME,"U10");
	setName(V_NAME,"V");
	setName(Vs_NAME,"V10");
	setName(W_NAME,"W");
	setName(Ws_NAME,"W9950");
	setName(Z_NAME,"GPH");
	setName(Zs_NAME,"GPHs");
	setName(T_NAME,"T");
	setName(Ts_NAME,"T2");
	setName(RH_NAME,"RH");
	setName(RHs_NAME,"RH2");
	setName(Ps_NAME,"Ps");
	setName(SHF_NAME,"SHF");
	setName(ZMF_NAME,"ZMF");
	setName(MMF_NAME,"MMF");
	setName(PBL_NAME,"PBL");
	setName(TROP_NAME,"ZTROP");
	setName(UST_NAME,"Ustar");
	setName(WST_NAME,"Wstar");
	setName(OLI_NAME,"OLI");
	setName(Rho_NAME,"Rho");
	setName(A_NAME,"A");
	setName(B_NAME,"B");
}

flexGrid::~flexGrid()
{
	clearDevice();
}

void flexGrid::setName(int idx, const char *name)
{
	if (idx<0 || idx>=END_NAME || strlen(name)>=NAME_LENGTH)
		errmsg("Bad parameters in calling flexGrid::setName");
	strcpy(Names[idx],name);
	if (nested) nested->setName(idx,name);
}


void flexGrid::readData(const char *fname, int ns, bool cuda)
{
	if (ns<3600 || ns>=86400 || ns%3600!=0) errmsg("Bad field time resolution");

	host.ns=ns;

	flexCDF cdf;
	cdf.open(fname);

	flexArr<double> D(MAX_GRID);

	int i;
	
	if (cdf.isVariable(Names[P_NAME])) {
		host.hybrid=0;
		host.nl=cdf.readGrid(Names[P_NAME],D.ptr(),MAX_GRID);
		if (host.nl<3 || host.nl>256) errmsg("Vertical levels < 3 || levels > 256");
		for (i=0; i<host.nl; i++) {
			host.P[i]=float(D(i)*100);		// hPa to Pa
			if (i>0) {
				if (host.P[i]>=host.P[i-1]) errmsg("Bad pressure grid data");
			}
		}
	}
	else {
		host.nl=cdf.readGrid(Names[A_NAME],D.ptr(),MAX_GRID);
		for (i=0; i<host.nl; i++) host.A[i]=D(i);
		host.hybrid=cdf.readGrid(Names[B_NAME],D.ptr(),MAX_GRID);
		if (host.nl!=host.hybrid) errmsg("A & B have difference dimensions");
		for (i=0; i<host.nl; i++) host.B[i]=D(i);
	}

	host.nr=cdf.readGrid(Names[Lat_NAME],D.ptr(),MAX_GRID);
	if (host.nr<3) errmsg("Latitude grid < 3");
	host.y0=D(0);
	host.y1=D(host.nr-1);
	host.dy=D(1)-D(0);

	host.nc=cdf.readGrid(Names[Lon_NAME],D.ptr(),MAX_GRID);
	if (host.nc<3) errmsg("Longitude grid < 3");
	host.x0=D(0);
	host.x1=D(host.nc-1);
	host.dx=D(1)-D(0);
	if (host.x1<=host.x0 || host.dx<=0) errmsg("Longitude grid must be from west to east");

	host.nxy=host.nr*host.nc;

	if (fabs(host.y0-host.y1)<179.99f || fabs(host.x1-host.x0+host.dx)<359.55f) {
		host.gl=0;
	}
	else {
		host.gl=1;
	}

	if (cuda) {
		updateDevice();
	}
	else {
		if (device) clearDevice();
	}
#if 0
        printf("Grid_readData\n");
        printf("dx: %g\n", host.dx);
        printf("dy: %g\n", host.dy);
        printf("gl: %d\n", host.gl);
        printf("hybrid: %d\n", host.hybrid);
        printf("nc: %d\n", host.nc);
        printf("nl: %d\n", host.nl);
        for(i=0; i < host.nl ; i++){
            printf("%g, ", host.P[i]);
        }
        printf("\n");
        printf("nr: %d\n", host.nr);
        printf("ns: %d\n", host.ns);
        printf("nxy: %d\n", host.nxy);
        printf("x0: %g\n", host.x0);
        printf("x1: %g\n", host.x1);
        printf("y0: %g\n", host.y0);
        printf("y1: %g\n", host.y1);       
#endif
}

void flexGrid::updateDevice()
{
    printf("FLEXGRID : updateDevice()\n");
 
    
    if (!device) {
        mallocOnDevice(&device,sizeof(Grid), &host);
    }
    setDeviceData(device,&host,sizeof(Grid));

    int var = KERNEL_CHOICE_ARGS + ERROR_ARGS + INTERPOLATION_ARGS;
    setKernelArgs(0, var, sizeof(cl_mem), &device);
    
#ifdef _DEBUG
	Grid g;
	getDeviceData(&g,device,sizeof(Grid));
	if (g.nl!=host.nl || g.P[0]!=host.P[0]) errmsg("device!=host");
#endif
}

void flexGrid::clearDevice()
{
    
    PRINTF("FLEXGRID : clear\n");
    
    freeOnDevice(device);
    device=NULL;
}
