/* 
 * File:   kernel_meterological_functions.h
 * Author: jhebus
 *
 * Created on 03 July 2013, 22:09
 */

#ifndef KERNEL_METEROLOGICAL_FUNCTIONS_H
#define	KERNEL_METEROLOGICAL_FUNCTIONS_H

#include <math.h>

#ifndef __device__
#define __device__ static inline
#endif


/*! \addtogroup meteo_kernel
 Documentation for meteorological kernel functions used by
 host computer CPU and NVIDIA GPU device.
 @{
 */


/** Calculates potential temperature
 @param T Air temperature [K]
 @param P Air pressure [Pa]
 <br>
 Ref: http://amsglossary.allenpress.com/glossary/search?id=potential-temperature1
 */
__device__
float pt_f(float T/*[K]*/, float P/*[Pa]*/)
{
	return T*pow(100000.0/P,0.286);
}

/** Calculates water vapor pressure [Pa]
 @param T Air temperature [K]
 @param Hum Relative humidity [%]
 <br>
 Ref:  http://en.wikipedia.org/wiki/Density_of_air
*/
__device__
float wvp_f(float T/*[K]*/, float Hum/*[%]*/)
{
	// saturated water vapor pressure [Pa]
	double pw=610.78*pow(10.0,(7.5*T-2048.625)/(T-35.85));
	// water vapor pressure
	return float(pw*Hum/100.0);
}

/** Calculates air density [kg/m3]
 @param P Air pressure [Pa]
 @param T Air temperature [K]
 @param Hum Relative humidity [%]
 <br>
 Ref:  http://en.wikipedia.org/wiki/Density_of_air
*/
__device__
float rho_f(float P/*[Pa]*/, float T/*[K]*/, float Hum/*[%]*/)
{
	float pw=wvp_f(T,Hum);
	// dry air pressure
	float pa=P-pw;
	// humid air density (specific gas constant in J/kg/K)
	pa=(pa/287.05f+pw/461.495f)/T;
	return pa;
}

/** Calculates virtual potential temperature [K]
 @param P Air pressure [Pa]
 @param T Air temperature [K]
 @param Hum Relative humidity [%]
 <br>
 Ref:
 <br>
 http://amsglossary.allenpress.com/glossary/search?id=mixing-ratio1
 <br>
 http://amsglossary.allenpress.com/glossary/search?id=virtual-potential-temperature1
*/
__device__
float pvt_f(float P/*[Pa]*/, float T/*[K]*/, float Hum/*[%]*/)
{
	float pw=wvp_f(T,Hum);
	// mixing ratio
	float r=0.622*pw/(P-pw);
	// potential temperature
	float Tp=pt_f(T,P);
	// virtual potential temperature
	return Tp*(1.0+0.61*r);
}


/** Calculates specific humidity
 @param P Air pressure [Pa]
 @param T Air temperature [K]
 @param Hum Relative humidity [%]
 <br>
  Ref: http://en.wikipedia.org/wiki/Humidity
*/
__device__
float hq_f(float P/*[Pa]*/, float T/*[K]*/, float Hum/*[%]*/)
{
	float pw=wvp_f(T,Hum);
	// mixing ratio
	float mr=0.62197*pw/(P-pw);
	return mr/(1.0+mr);
}




#endif	/* KERNEL_METEROLOGICAL_FUNCTIONS_H */

