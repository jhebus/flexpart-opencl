/* 
 * File:   flexSim.h
 * Author: jhebus
 *
 * Created on 08 July 2013, 18:17
 */

#ifndef FLEXSIM_H
#define	FLEXSIM_H

#include "flexgrid.h"
#include "flexparam.h"
#include "flexfield.h"
#include "flexcount.h"
#include "flexlist.h"
//#include "flexgl.h"

#include <string>
#include <vector>

#define MAX_GMT 256

#include "flexocl.h"

class flexSim {
    
public:
    
    flexSim();
    
    ~flexSim();
    
    void start();
    
    bool setOptions(int nargs, char* args[]);
    
    void glxy(float x, float y)	{ L.newXY(x,y); }
    
private:
    // Reset starting time

	void reset(double julian);

// Update fields
	
	void update(double julian);

//private:

	bool setOption(const std::vector<std::string> &vs);

	flexCount C;                                    // particle count

	flexGrid G;					// Grid definition of data (global)
	flexGrid Gn;                                    // Nested grid definition of data (regional)

	flexParam P;                                    // Model parameters

	flexField F, F1, F2;                            // Data field (must be global)
	flexField Fn, F1n, F2n;         		// Nested data field (must be regional)

	flexList L;					// particles

        //flexOCL ocl;                                    // The openCL device and params
        
	//flexGL GL;					// display

	std::string
		_input,					// input path
		_input2,				// nested input path
		_output;				// output paths

	std::vector<double>
		_jul1,					// start time (Julian date & time)
		_jul2;					// end time

	int
		_itvsec,				// continuous release interval [s]
		_dump,					// dumping flag
		_mobile,				// mobile release flag
		_minute,				// minute flag
		_loop,					// loop status (interactive mode)
		_addsec,				// interval of adding release [s]
		_insec;					// input interval (time resolution of field) [s]
};


#endif	/* FLEXSIM_H */

