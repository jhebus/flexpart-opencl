#ifndef __CONVECT43C_H__
#define __CONVECT43C_H__

#include <math.h>

#ifdef _WIN32
#pragma warning(disable: 4013)
#pragma warning(disable: 4244)
#pragma warning(disable: 4554)
#endif

#define dmax(a,b) ((a)>(b)?(a):(b))
#define dmin(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
#define dabs(a) (fabs((a)))

typedef float real;
typedef float doublereal;
typedef int integer;


#ifdef __cplusplus
extern "C" {
#endif 

int convect_(integer *nd, integer *nl, real *delt, integer *
	iflag, real *precip, real *wd, real *tprime, real *qprime, real *cbmf,
	 real *ft, real *fq, real *sub, real *fmass, real *tconv, real *qconv,
	 real *qsconv, real *pconv_hpa__, real *phconv_hpa__, integer *
	nconvtop);

#ifdef __cplusplus
}
#endif 

#endif