/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexfield.h"
#include "flexutility.h"
#include "flexcdf.h" 
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#define PBL_MIN 100.0
#define PBL_MAX 5000.0

#define __device__ static inline
#define __FLEX__

// timing functions
#include <sys/time.h>
struct timeval start_to_field, 		stop_to_field;
struct timeval start_kernel_field, 	stop_kernel_field;
struct timeval start_from_field,	stop_from_field;
#define TO_RESET        (start_to_field = (struct timeval){ 0 }; stop_to_field = (struct timeval){ 0 })
#define KERNEL_RESET    (start_kernel_field = (struct timeval){ 0 }; stop_kernel_field = (struct timeval){ 0 })
#define FROM_RESET      (start_from_field = (struct timeval){ 0 }; stop_from_field = (struct timeval){ 0 })
#define START_TO gettimeofday(&start_to_field, NULL)
#define STOP_TO  gettimeofday(&stop_to_field, NULL)
#define START_KERNEL gettimeofday(&start_kernel_field, NULL)
#define STOP_KERNEL  gettimeofday(&stop_kernel_field, NULL)
#define START_FROM gettimeofday(&start_from_field, NULL)
#define STOP_FROM  gettimeofday(&stop_from_field, NULL)


#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

cl_mem float_probe, int_probe;
static float older = 64.0;

static float static_f1;
static float static_f2;

//number of elements in the field struct
#define num_field_elements 14   
static int sizes[num_field_elements];

//list of the offsets for the field arrays
#define field_U 0
#define field_V 1
#define field_W 2
#define field_Z 3
#define field_Rho 4
#define field_DRho 5
#define field_T 6
#define field_RH 7
#define field_PBL 8
#define field_Tropo 9
#define field_Ustar 10
#define field_Wstar 11
#define field_Oli 12
#define field_Ps 13  
/*
static char* field_names[] = {
    "U",
    "V",
    "W",
    "Z",
    "T",
    "Rho",
    "Drho",
    "RH",
    "PBL",
    "Tropo",
    "Ustar",
    "Wstar",
    "Oli",
    "PS"
};

#define getFieldName(a)         (field_names[(a)])
*/


// import common kernel functions

#include "kernel_meterological_functions.h"

static
void interp_f(flexArr<float> &F, const flexArr<float> &F1, const flexArr<float> &F2, double f1, double f2)
{
#pragma omp parallel for
	for (int k=0; k<F.nd(); k++) {
            F(k) = f1 * F1(k) + f2 * F2(k);         
        }
}

static
void range_f(const char* name, const flexArr<float> &F)
{
	float f1=1.e9;
	float f2=-1.e9;
	for (int k=0; k<F.nd(); k++) {
		f1=min(f1,F(k));
		f2=max(f2,F(k));
	}
	printf("%s, %f1, %f2\n",name,f1,f2);
}


// refer to psim.f of flexpart6.4-gfs;
static
double psim(double z, double L)
{
	const double eps=1.e-5;
	if (L>=0.0 && L<eps) {
        L=eps;
	}
	else if (L<0.0 && L>-eps) {
		L=-eps;
	}
	double zeta=z/L;
	if (zeta<=0.0) {
// UNSTABLE CASE
        double x=pow(1.-15.*zeta,0.25);
        double a1=(1.+x)/2.;
        a1=a1*a1;
        double a2=(1.+x*x)/2.;
        return (log(a1*a2)-2.*atan(x)+3.14159/2.);
	}
// STABLE CASE
    return (-4.7*zeta);
}

// refer to psih.f of flexpart6.4gfs;
static
double psih(double z, double L)
{
	const double a=1., b=0.667, c=5., d=0.35, eps=1.e-5;
	if (L>=0.0 && L<eps) {
        L=eps;
	}
	else if (L<0.0 && L>-eps) {
		L=-eps;
	}
	double zeta=z/L;
	if (zeta<=0.0) {
// UNSTABLE CASE
		double x=pow(1.-16.*zeta,.25);
		return 2.*log((1.+x*x)/2.);
	}
// STABLE CASE
	return -pow(1.+0.667*a*zeta,1.5)-b*(zeta-c/d)*exp(-d*zeta)-b*c/d+1.;
}


////////////////////////////////////////////////////////////////////////////////
flexField::flexField()
	: ga(9.81), cpa(1004.6), karman(0.4)
{
	nested            = NULL;
	dev_struct        = NULL;
/* These are inside the dev_struct

	d_U             = NULL;
	d_V             = NULL;
	d_W             = NULL;
	d_Z             = NULL;
	d_Rho           = NULL;
	d_DRho          = NULL;
	d_T             = NULL;
	d_RH            = NULL;
	d_PBL           = NULL;
	d_Ustar         = NULL;
	d_Wstar         = NULL;
	d_Tropo         = NULL;
	d_Oli           = NULL;
	d_Ps            = NULL;
 */ 

}

flexField::~flexField()
{
    PRINTF("FIELD : destroy!!\n");
    clearDevice();
        
        
}


void flexField::readData(const char *fname, double time, const flexGrid &G, const flexParam &P)
{
	int k, nl=G.host.nl, nr=G.host.nr, nc=G.host.nc;

	if (U.nl()!=nl || U.nr()!=nr || U.nc()!=nc) {
		initField(nl,nr,nc);
		clearDevice();
	}

	_time=time;

	time_t tick=::time(NULL);

	flexCDF cdf;
	cdf.open(fname);

	cdf.readVariable(G.name(Ps_NAME),Ps.ptr(),Ps.nd());

	cdf.readVariable(G.name(Z_NAME),Z.ptr(),Z.nd());
	cdf.readVariable(G.name(Zs_NAME),Zs.ptr(),Zs.nd());
	zindex(G.host);

	cdf.readVariable(G.name(U_NAME),U.ptr(),U.nd());
	cdf.readVariable(G.name(Us_NAME),Wrk.ptr(),Wrk.nd());
	setSurface(U,Wrk,G.host);

	cdf.readVariable(G.name(V_NAME),V.ptr(),V.nd());
	cdf.readVariable(G.name(Vs_NAME),Wrk.ptr(),Wrk.nd());
	setSurface(V,Wrk,G.host);

	cdf.readVariable(G.name(W_NAME),W.ptr(),W.nd());

	cdf.readVariable(G.name(T_NAME),T.ptr(),T.nd());
	cdf.readVariable(G.name(Ts_NAME),Wrk.ptr(),Wrk.nd());
	setSurface(T,Wrk,G.host);

	cdf.readVariable(G.name(RH_NAME),RH.ptr(),RH.nd());
	cdf.readVariable(G.name(RHs_NAME),Wrk.ptr(),Wrk.nd());
	setSurface(RH,Wrk,G.host);

	rho(G.host);

	if (cdf.isVariable(G.name(PBL_NAME))) {
		cdf.readVariable(G.name(PBL_NAME),PBL.ptr(),PBL.nd());
		for (k=0; k<PBL.nd(); k++) {
			if (PBL(k)<PBL_MIN) PBL(k)=PBL_MIN;
		}
	}
	else {
		pbl(G.host);
#ifdef _DEBUG
		print_variable(G.name(PBL_NAME),PBL.ptr(),PBL.nd(),1);
#endif
	}

	if (cdf.isVariable(G.name(TROP_NAME))) {
		cdf.readVariable(G.name(TROP_NAME),Tropo.ptr(),Tropo.nd());
	}
	else {
		trop(G.host);
#ifdef _DEBUG
		print_variable(G.name(TROP_NAME),Tropo.ptr(),Tropo.nd(),1);
#endif
	}

	if (cdf.isVariable(G.name(SHF_NAME)) &&
		cdf.isVariable(G.name(ZMF_NAME)) &&
		cdf.isVariable(G.name(MMF_NAME))) {

		cdf.readVariable(G.name(SHF_NAME),SHF.ptr(),SHF.nd());
		cdf.readVariable(G.name(ZMF_NAME),ZMF.ptr(),ZMF.nd());
		cdf.readVariable(G.name(MMF_NAME),MMF.ptr(),MMF.nd());

		ustar(G.host);
		wstar(G.host);
		obukhov(G.host);
	}
	else {
		profile(G.host);
	}
	
	pa2m(G.host);
	drhodz(G.host);

#ifdef _DEBUG
	print_variable("Ustar",Ustar.ptr(),Ustar.nd(),1);
	print_variable("Wstar",Wstar.ptr(),Wstar.nd(),1);
	print_variable("Oli",Oli.ptr(),Oli.nd(),1);
	for (k=0; k<nl; k++) {
		print_variable("W[m/s]",W.ptr()+nr*nc*k,nr*nc,k+1);
	}
	for (k=0; k<nl; k++) {
		print_variable("dRho/dZ",DRho.ptr()+nr*nc*k,nr*nc,k+1);
	}
#endif

	if (P.host.openCL) {
		updateDevice();
	}
	else {
		clearDevice();
	}

	if (P.host.timing) {
		printf("Reading, %.1f\n",difftime(::time(NULL),tick));
	}
}
/*----------------------------------------------------------------------------*/

void flexField::interpolate(flexField &F1, flexField &F2, double time, const flexGrid &G, const flexParam &P, bool flag)
{
	_time = time;

	int nl=G.host.nl, nr=G.host.nr, nc=G.host.nc;

	if (U.nl()!=nl || U.nr()!=nr || U.nc()!=nc ) {
		initField(nl,nr,nc);
		clearDevice();
	}

	float df = fabs(F2.time()-F1.time());
	static_f1 = fabs(F2.time()-time)/df;
	static_f2 = fabs(F1.time()-time)/df;

	if (fabs(static_f1 + static_f2 - 1.0)>1.e-6) errmsg("Bad time interpolation factors");

	size_t size2=nr*nc*sizeof(float);

	//timestamp_t start = get_timestamp();
//	time_t tick =::time(NULL);

	if (P.host.timing) {
                START_TO;
        }

	if (P.host.openCL) {     
#if KERNEL_CALCULATIONS
            //copy the variables to the device
            if(!dev_struct){
                updateDevice();          
            }

            if (P.host.timing) {
                   STOP_TO;
                   START_KERNEL;
            }


            runInterpolationKernel(dev_struct, F1.dev_struct, F2.dev_struct, static_f1, static_f2, (P.host.lflx ? INTERPOLATION_KERNEL_WITH_FLUX : INTERPOLATION_KERNEL) );
            PRINTF("FLEXFIELD: done runInterpolationKernel()\n");    
            //now copy the data back from the device to the host pointers
            //apparently, we just leave the data on the device
            //updateHost();

            if (!P.host.lflx) {
                // fluxes must be forecast with valid time from file_time to file_time+forecast_period
                if (F1.time() < F2.time()) {
                    setDeviceData(dev_struct->Ustar, F1.Ustar.ptr(), size2);
                    setDeviceData(dev_struct->Wstar, F1.Wstar.ptr(), size2);
                    setDeviceData(dev_struct->Oli, F1.Oli.ptr(), size2);

                }
                else {
                    setDeviceData(dev_struct->Ustar, F2.Ustar.ptr(), size2);
                    setDeviceData(dev_struct->Wstar, F2.Wstar.ptr(), size2);
                    setDeviceData(dev_struct->Oli, F2.Oli.ptr(), size2);
                }
            }

            if (P.host.timing) {
                    STOP_KERNEL;
                    START_FROM;
            }


            //i don't understand why this is being recalcalculated! Flux does not affect this
            PRINTF("FLEXFIELD : check that i don't need this calca\n");

            if (flag) {
                    // interpolate Ps and PBL for saving flexClass x-y-z 
                    //interp_f(Z , F1.Z, F2.Z, static_f1, static_f2);
                    //interp_f(PBL, F1.PBL, F2.PBL, static_f1, static_f2);

                    getDeviceData(Z.ptr(), dev_struct->Z, sizeof(float) * Z.nd());
                    getDeviceData(PBL.ptr(), dev_struct->PBL, sizeof(float) * PBL.nd());
            }
            PRINTF("FLEXFIELD: done reading data\n");


            if (P.host.timing) {
                    STOP_FROM;
            }
#endif
	}
        else
	 {
		clearDevice();

		if (P.host.timing) {
	               STOP_TO;
        	       START_KERNEL;
        }



		interp_f(U,F1.U,F2.U,static_f1,static_f2);
		interp_f(V,F1.V,F2.V,static_f1,static_f2);
		interp_f(W,F1.W,F2.W,static_f1,static_f2);
		interp_f(Z,F1.Z,F2.Z,static_f1,static_f2);
		interp_f(T,F1.T,F2.T,static_f1,static_f2);
		interp_f(RH,F1.RH,F2.RH,static_f1,static_f2);

		interp_f(Ps,F1.Ps,F2.Ps,static_f1,static_f2);
		interp_f(Rho,F1.Rho,F2.Rho,static_f1,static_f2);
		interp_f(DRho,F1.DRho,F2.DRho,static_f1,static_f2);
		interp_f(PBL,F1.PBL,F2.PBL,static_f1,static_f2);
		interp_f(Tropo,F1.Tropo,F2.Tropo,static_f1,static_f2);

		if (P.host.lflx) {
			interp_f(Ustar,F1.Ustar,F2.Ustar,static_f1,static_f2);
			interp_f(Wstar,F1.Wstar,F2.Wstar,static_f1,static_f2);
			interp_f(Oli,F1.Oli,F2.Oli,static_f1,static_f2);
		}
		else {
			// fluxes must be forecast with valid time from file_time to file_time+forecast_period
			if (F1.time()<F2.time()) {
				memcpy(Ustar.ptr(),F1.Ustar.ptr(),size2);
				memcpy(Wstar.ptr(),F1.Wstar.ptr(),size2);
				memcpy(Oli.ptr(),F1.Oli.ptr(),size2);
			}
			else {
				memcpy(Ustar.ptr(),F2.Ustar.ptr(),size2);
				memcpy(Wstar.ptr(),F2.Wstar.ptr(),size2);
				memcpy(Oli.ptr(),F2.Oli.ptr(),size2);
			}
		}

		if (P.host.timing) {
	                STOP_KERNEL;
        	        START_FROM;
	        }

		if (P.host.timing) {
                        STOP_FROM;
                }


	}

	if (P.host.timing) {
           // printf("Interpolation, %.4fms\n", (get_timestamp() - start)/ 1000.0L);
//		printf("Interpolation, %.1f\n", difftime(::time(NULL), tick));


		printf("INTERPOLATION TO    : %lu.%06lu\n", stop_to_field.tv_sec     - start_to_field.tv_sec,     stop_to_field.tv_usec     - start_to_field.tv_usec);
                printf("INTERPOLATION KERNEL: %lu.%06lu\n", stop_kernel_field.tv_sec - start_kernel_field.tv_sec, stop_kernel_field.tv_usec - start_kernel_field.tv_usec);
                printf("INTERPOLATION FROM  : %lu.%06lu\n", stop_from_field.tv_sec   - start_from_field.tv_sec,   stop_from_field.tv_usec   - start_from_field.tv_usec);

	}	
}


void flexField::setSurface(flexArr<float> &V, const flexArr<float> &Vs, const Grid &G)
{
#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			int n=Idx(i,j);
			float vs=Vs(i,j);
			for (int k=0; k<=n; k++) {
				V(k,i,j)=vs;
			}
		}
	}
}


void flexField::initField(int nl, int nr, int nc)
{
	U.resize(nl,nr,nc);
	V.resize(nl,nr,nc);
	W.resize(nl,nr,nc);
	T.resize(nl,nr,nc);
	Z.resize(nl,nr,nc);
	RH.resize(nl,nr,nc);
	Rho.resize(nl,nr,nc);
	DRho.resize(nl,nr,nc);
	Ps.resize(nr,nc);
	Zs.resize(nr,nc);
	PBL.resize(nr,nc);
	Tropo.resize(nr,nc);
	SHF.resize(nr,nc);
	ZMF.resize(nr,nc);
	MMF.resize(nr,nc);
	Ustar.resize(nr,nc);
	Wstar.resize(nr,nc);
	Oli.resize(nr,nc);
	Wrk.resize(nr,nc);
	Idx.resize(nr,nc);
	Idx.clear();
	setPtr(false);
}

void flexField::updateDevice()
{
    PRINTF("FIELD : updateDevice():\n");
    size_t size2 = U.nr() * U.nc() * sizeof(float);
    size_t size3 = U.nd() * sizeof(float);
  
    if(!dev_struct)
        dev_struct = new DeviceField();

    if (!dev_struct->U) mallocOnDevice(&dev_struct->U,size3, U.ptr());
    setDeviceData(dev_struct->U,U.ptr(),size3);

    if (!dev_struct->V) mallocOnDevice(&dev_struct->V,size3, V.ptr());
    setDeviceData(dev_struct->V,V.ptr(),size3);

    if (!dev_struct->W) mallocOnDevice(&dev_struct->W,size3, W.ptr());
    setDeviceData(dev_struct->W,W.ptr(),size3);

    if (!dev_struct->Z) mallocOnDevice(&dev_struct->Z,size3, Z.ptr());
    setDeviceData(dev_struct->Z,Z.ptr(),size3);

    if (!dev_struct->Rho) mallocOnDevice(&dev_struct->Rho,size3, Rho.ptr());
    setDeviceData(dev_struct->Rho,Rho.ptr(),size3);

    if (!dev_struct->DRho) mallocOnDevice(&dev_struct->DRho,size3, DRho.ptr());
    setDeviceData(dev_struct->DRho,DRho.ptr(),size3);

    if (!dev_struct->PBL) mallocOnDevice(&dev_struct->PBL,size2, PBL.ptr());
    setDeviceData(dev_struct->PBL,PBL.ptr(),size2);

    if (!dev_struct->Tropo) mallocOnDevice(&dev_struct->Tropo,size2, Tropo.ptr());
    setDeviceData(dev_struct->Tropo,Tropo.ptr(),size2);

    if (!dev_struct->Ustar) mallocOnDevice(&dev_struct->Ustar,size2, Ustar.ptr());
    setDeviceData(dev_struct->Ustar,Ustar.ptr(),size2);

    if (!dev_struct->Wstar) mallocOnDevice(&dev_struct->Wstar,size2, Wstar.ptr());
    setDeviceData(dev_struct->Wstar,Wstar.ptr(),size2);

    if (!dev_struct->Oli) mallocOnDevice(&dev_struct->Oli,size2, Oli.ptr());
    setDeviceData(dev_struct->Oli,Oli.ptr(),size2);

    if (!dev_struct->T) mallocOnDevice(&dev_struct->T,size3, T.ptr());
    setDeviceData(dev_struct->T,T.ptr(),size3);

    if (!dev_struct->RH) mallocOnDevice(&dev_struct->RH,size3, RH.ptr());
    setDeviceData(dev_struct->RH,RH.ptr(),size3);

    if (!dev_struct->Ps) mallocOnDevice(&dev_struct->Ps,size2, Ps.ptr());
    setDeviceData(dev_struct->Ps,Ps.ptr(),size2);
    
    //group by length
    sizes[0] = U.nd();
    //sizes[1] = V.nd();
    //sizes[2] = W.nd();
    //sizes[3] = Z.nd();
    //sizes[4] = T.nd();
    //sizes[5] = RH.nd();
    //sizes[6] = Rho.nd();
    //sizes[7] = DRho.nd();
    
    //group by length
    sizes[8] = Ps.nd();
    //sizes[9] = PBL.nd();
    //sizes[10] = Tropo.nd();
    //sizes[11] = Ustar.nd();
    //sizes[12] = Wstar.nd();
    //sizes[13] = Oli.nd();
    
    if(!lengths) mallocOnDevice(&lengths, sizeof(int) * num_field_elements, sizes);
    setDeviceData(lengths, sizes, sizeof(int) * num_field_elements);
    /*
     * This was what happened origionally. If this is the case, then this value
     * should always be execpted to be 0. From the way that the code seems to be
     * used, it would see that this is not the intention. The on device value is
     * now copied from the host array.
     */
    //clearDeviceMemory(dev_struct->Ps,size2);
    PRINTF("FLEXFIELD: updateDevice-done\n");
    
    
    
}

void flexField::updateHost(){
    
    PRINTF("FIELD : updateHost():\n");
    
#if 1
    //put it back into the host pointers  -- But only what we need to!!
    size_t size2 = U.nr() * U.nc() * sizeof(float);
    size_t size3 = U.nd() * sizeof(float);
    
    getDeviceData(U.ptr(), dev_struct->U, size3);
    getDeviceData(V.ptr(), dev_struct->V, size3);
    getDeviceData(W.ptr(), dev_struct->W, size3);
    getDeviceData(Z.ptr(), dev_struct->Z, size3);
    getDeviceData(Rho.ptr(), dev_struct->Rho, size3);
    getDeviceData(DRho.ptr(), dev_struct->DRho, size3);
    getDeviceData(PBL.ptr(), dev_struct->PBL, size2);
    getDeviceData(Tropo.ptr(), dev_struct->Tropo, size2);
    getDeviceData(Ustar.ptr(), dev_struct->Ustar, size2);
    getDeviceData(Wstar.ptr(), dev_struct->Wstar, size2);
    getDeviceData(Oli.ptr(), dev_struct->Oli, size2);
    getDeviceData(T.ptr(), dev_struct->T, size3);
    getDeviceData(RH.ptr(), dev_struct->RH, size3);
    getDeviceData(Ps.ptr(), dev_struct->Ps, size2);
    
    /*
     * This is the same issue as in the updateDevice
     * 
     * The host value is now copied from the array.
     */
    //clearDeviceMemory(dev_struct->Ps, size2);
    
#if DEBUG && 0
    float choice = 0;
    getDeviceData(&choice, test, sizeof(float));
    
    printf("Choice %g| %g | %g\n", choice,f1, f2);
#endif
#else    
    
    //flexArr<float> list[14];
    int *offset, val = 0;
    float *packed;
    int data_size = 0, offset_size = 0;
    
    
    data_size += U.nd();
    data_size += V.nd();
    data_size += W.nd();
    data_size += Z.nd();
    data_size += Rho.nd();
    data_size += DRho.nd();
    data_size += T.nd();
    data_size += RH.nd();
    data_size += PBL.nd();
    data_size += Tropo.nd();
    data_size += Ustar.nd();
    data_size += Wstar.nd();
    data_size += Oli.nd();
    data_size += Ps.nd();

    offset_size = num_field_elements + 1;
            
            
    packed = (float*)malloc(sizeof(float) * (data_size));// new float[*data_size];
    offset = (int*)malloc(sizeof(int) * (offset_size)); //new int[num_field_elements + 1];        //number of required offsets
/*
    offset[0] = 0;
    offset[1] = U.nd() + offset[0];
    offset[2] = V.nd() + offset[1];
    offset[3] = W.nd() + offset[2];
    offset[4] = Z.nd() + offset[3];
    offset[5] = Rho.nd() + offset[4];
    offset[6] = DRho.nd() + offset[5];
    offset[7] = T.nd() + offset[6];
    offset[8] = RH.nd() + offset[7];
    offset[9] = PBL.nd() + offset[8];
    offset[10] = Tropo.nd() + offset[9];
    offset[11] = Ustar.nd() + offset[10];
    offset[12] = Wstar.nd() + offset[11];
    offset[13] = Oli.nd() + offset[12];
    offset[14] = Ps.nd() + offset[13];                  //added this one so was can also calc the sizes of the arrays
 */   
   /* 
    list[field_U] = U;
    list[field_V] = V;
    list[field_W] = W;
    list[field_Z] = Z;
    list[field_Rho] = Rho;
    list[field_DRho] = DRho;
    list[field_T] = T;
    list[field_RH] = RH;
    list[field_PBL] = PBL;
    list[field_Tropo] = Tropo;
    list[field_Ustar] = Ustar;
    list[field_Wstar] = Wstar;
    list[field_Oli] = Oli;
    list[field_Ps] = Ps;
    */
    //read the data off the device
    getDeviceData(packed, d_field_data, sizeof(float) * data_size);
    getDeviceData(offset, d_field_offsets, sizeof(int) * offset_size);
    
/*   
    for(int i = 0 ; i < U.nd() ; i++){
        U(i) = packed[i + offset[0]];
        V(i) = packed[i + offset[1]];
        W(i) = packed[i + offset[2]];
        Z(i) = packed[i + offset[3]];
        Rho(i) = packed[i + offset[4]];
        DRho(i) = packed[i + offset[5]];
        T(i) = packed[i + offset[6]];
        RH(i) = packed[i + offset[7]];
    }
    
    for(int i = 0 ; i < PBL.nd() ; i++){
        PBL(i) = packed[i + offset[8]];
        Tropo(i) = packed[i + offset[9]];
        Ustar(i) = packed[i + offset[10]];
        Wstar(i) = packed[i + offset[11]];
        Oli(i) = packed[i + offset[12]];
        Ps(i) = packed[i + offset[13]];
    }
 */
    
    memcpy(U.ptr(),(packed + offset[val++]), U.nd() * sizeof(float));
    memcpy(V.ptr(),(packed + offset[val++]) ,  V.nd() * sizeof(float));
    memcpy(W.ptr(),(packed + offset[val++]) ,  W.nd() * sizeof(float));
    memcpy(Z.ptr(),(packed + offset[val++]) ,  Z.nd() * sizeof(float));
    memcpy(Rho.ptr(),(packed + offset[val++]) ,  Rho.nd() * sizeof(float));
    memcpy(DRho.ptr(),(packed + offset[val++]) ,  DRho.nd() * sizeof(float));
    memcpy(T.ptr(),(packed + offset[val++]) ,  T.nd() * sizeof(float));
    memcpy(RH.ptr(),(packed + offset[val++]) ,  RH.nd() * sizeof(float));
    memcpy(PBL.ptr(),(packed + offset[val++]) ,  PBL.nd() * sizeof(float));
    memcpy(Tropo.ptr(),(packed + offset[val++]) ,  Tropo.nd() * sizeof(float));
    memcpy(Ustar.ptr(),(packed + offset[val++]) ,  Ustar.nd() * sizeof(float));
    memcpy(Wstar.ptr(),(packed + offset[val++]) ,  Wstar.nd() * sizeof(float));
    memcpy(Oli.ptr(),(packed + offset[val++]) ,  Oli.nd() * sizeof(float));
    memcpy(Ps.ptr(), (packed + offset[val++]) , Ps.nd() * sizeof(float));
    
#endif
    //this can be optimised to just assigning properly in the first place
    setPtr(false);
}


void flexField::setPtr(bool cuda)
{
        host.U=U.ptr();
        host.V=V.ptr();
        host.W=W.ptr();
        host.Z=Z.ptr();
        host.Rho=Rho.ptr();
        host.DRho=DRho.ptr();
        host.T=T.ptr();
        host.RH=RH.ptr();
        host.PBL=PBL.ptr();
        host.Tropo=Tropo.ptr();
        host.Ustar=Ustar.ptr();
        host.Wstar=Wstar.ptr();
        host.Oli=Oli.ptr();
        host.Ps=Ps.ptr();
}


void flexField::clearDevice()
{
	if (!dev_struct) return;
        
        PRINTF("FIELD : clearDevice():\n");
        
	freeOnDevice(dev_struct->U);
	dev_struct->U             = NULL;
	freeOnDevice(dev_struct->V);
	dev_struct->V             = NULL;
	freeOnDevice(dev_struct->W);
	dev_struct->W             = NULL;
	freeOnDevice(dev_struct->Z);
	dev_struct->Z             = NULL;
	freeOnDevice(dev_struct->Rho);
	dev_struct->Rho           = NULL;
	freeOnDevice(dev_struct->DRho);
	dev_struct->DRho          = NULL;
	freeOnDevice(dev_struct->T);
	dev_struct->T             = NULL;
	freeOnDevice(dev_struct->RH);
	dev_struct->RH            = NULL;
	freeOnDevice(dev_struct->PBL);
	dev_struct->PBL           = NULL;
	freeOnDevice(dev_struct->Ustar);
	dev_struct->Ustar         = NULL;
	freeOnDevice(dev_struct->Wstar);
	dev_struct->Wstar         = NULL;
	freeOnDevice(dev_struct->Tropo);
	dev_struct->Tropo         = NULL;
	freeOnDevice(dev_struct->Oli);
	dev_struct->Oli           = NULL;
	freeOnDevice(dev_struct->Ps);
	dev_struct->Ps            = NULL;
        freeOnDevice(lengths);
        lengths = NULL;
        
	delete dev_struct;
	dev_struct        = NULL;
}

void flexField::runInterpolationKernel(DeviceField *results, DeviceField *F1, DeviceField *F2, double field_mul_1, double field_mul_2, int kernel_choice){
    
    PRINTF("FLEXFIELD: Interpolation\n");
    int var = 0;

    //the first argument indicates the kernel code to run
    setKernelArgs(kernel_choice, var++, sizeof(int), &kernel_choice);

    var += ERROR_ARGS;
    
    //the interpolation kernel arguments
    setKernelArgs(kernel_choice, var++, sizeof(float), &static_f1);
    setKernelArgs(kernel_choice, var++, sizeof(float), &static_f2);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &lengths);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->U);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->U);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->V);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->V);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->W);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->W);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->Z);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->Z);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->T);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->T);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->RH);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->RH);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->Rho);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->Rho);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->DRho);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->DRho);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->Ps);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->Ps);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->PBL);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->PBL);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->Tropo);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->Tropo);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->Ustar);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->Ustar);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->Wstar);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->Wstar);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F1->Oli);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &F2->Oli);

    var += GRID_ARGS + PARAM_ARGS + CLASS_ARGS;
    
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->U);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->V);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->W);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->Z);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->Rho);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->DRho);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->T);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->RH);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->PBL);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->Tropo);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->Ustar);    
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->Wstar);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->Oli);
    setKernelArgs(kernel_choice, var++, sizeof(cl_mem), &results->Ps);
    
    //const size_t local_ws = getMaxComputeUnits();//256;	// Number of work-items per work-group
    //const size_t local_ws = 256;	// Number of work-items per work-group
    
    // shrRoundUp returns the smallest multiple of local_ws bigger than size
    //printf("HACK ON THE GLOBALS SIZE == NEED TO CALCULATE THIS\n");
    //const size_t global_ws = shrRoundUp(local_ws, U.nd());	// Total number of work-items
    
    size_t global_ws = U.nd();
    runKernelBlocking(&global_ws, 0);

}

void flexField::profile(const Grid &G)
{
// Refer to profile.f

#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			int k=Idx(i,j)+1;
			double z1=Z(k,i,j)-Z(0,i,j);
			if (z1<20.0) z1=20.0;
			double du=sqrt(U(k,i,j)*U(k,i,j)+V(k,i,j)*V(k,i,j))-sqrt(U(0,i,j)*U(0,i,j)+V(0,i,j)*V(0,i,j));
			double rho=Rho(0,i,j);
			if (du<=0.001) {
				// Monin-Obukhov Theory not applicable
				Ustar(i,j)=0.01;
				SHF(i,j)=0.0;
				Wstar(i,j)=0.0;
				Oli(i,j)=9999.0;
				continue;
			}
			double q0=pt_f(T(0,i,j),Ps(i,j));
			double q1=pt_f(T(k,i,j),pk(G,k,i,j));
		    double dq=q1-q0;
			if (fabs(dq)<=0.03) {
				// Neutral conditions
				double L=9999.0;
				Ustar(i,j)=(karman*du)/(log(z1/10.)-psim(z1,L)+psim(10.,L));
				SHF(i,j)=0.0;
				Wstar(i,j)=0.0;
				Oli(i,j)=L;
				continue;
			}
			double tm=0.5*(T(0,i,j)+T(k,i,j));
			double cr=(0.0219*tm*(z1-2.0)*du*du)/(dq*(z1-10.0)*(z1-10.0));
			if (dq>0.0 && cr<=1.0) {
				// Successive approximation will not converge
				double L=50.0;
				Ustar(i,j)=(karman*du)/(log(z1/10.)-psim(z1,L)+psim(10.,L));
				// follow NCEP definition; oposite sign from ECMWF
				SHF(i,j)=-rho*cpa*Ustar(i,j)*(karman*dq/0.74)/(log(z1/2.)-psih(z1,L)+psih(2.,L));
				if (SHF(i,j)>=0.0) {
					Wstar(i,j)=pow(SHF(i,j)*PBL(i,j)*ga/(pt_f(Ps(i,j),T(0,i,j))*cpa*rho),0.3333);
				}
				else {
					Wstar(i,j)=0;
				}
				Oli(i,j)=L;
				continue;
			}
			// Start iteration assuming neutral conditions
			double L=9999.;
			double ustar, qstar, Lold;
			for (int iter=1; iter<=20; iter++) {
				Lold=L;
				ustar=(karman*du)/(log(z1/10.)-psim(z1,L)+psim(10.,L));
				qstar=(karman*dq/0.74)/(log(z1/2.)-psih(z1,L)+psih(2.,L));
				L=(tm*ustar*ustar)/(ga*karman*qstar);
				if (fabs((L-Lold)/Lold)<0.01) break;		// Successive approximation successful
			}
			Ustar(i,j)=ustar;
			// follow NCEP definition; oposite sign from ECMWF;
			SHF(i,j)=-rho*cpa*ustar*qstar;
			if (SHF(i,j)>=0.0) {
				Wstar(i,j)=pow(SHF(i,j)*PBL(i,j)*ga/(pt_f(Ps(i,j),T(0,i,j))*cpa*rho),0.3333);
			}
			else {
				Wstar(i,j)=0;
			}
			Oli(i,j)=L;
		}
	}
}

void flexField::ustar(const Grid &G)
{
#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			double r1=sqrt(ZMF(i,j)*ZMF(i,j)+MMF(i,j)*MMF(i,j));
			double r2=Rho(0,i,j);
			Ustar(i,j)=sqrt(r1/r2);
		}
	}
}

void flexField::wstar(const Grid &G)
{
// Refer to richardson.f and hysplit
// Warning: be aware of the oposite sign of SHF!
#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			if (SHF(i,j)>0.0) {
				Wstar(i,j)=pow(SHF(i,j)*PBL(i,j)*ga/(pt_f(Ps(i,j),T(0,i,j))*cpa*Rho(0,i,j)),0.3333);
			}
			else {
				Wstar(i,j)=0.0;
			}
		}
	}
}

void flexField::obukhov(const Grid &G)
{
// Refer to obukhov.f
// Warning: NCEP GFS define surface->air heat flux as positive, but ECMWF as negative.
// read SHF and call ustar(i,j) first!
#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			float ust=Ustar(i,j);
			ust=max(ust,1.e-8);
			float qst=-SHF(i,j)/(Rho(0,i,j)*cpa*ust);	// scale temperature; oposite sign from flexpart
			if (fabs(qst)>1.e-10) {
				// here document says temperature, but code use potential temperature!
				float obu=pt_f(T(0,i,j),Ps(i,j))*ust*ust/(karman*ga*qst);
				if (obu > 9999.f) obu= 9999.f;
				if (obu < -9999.f) obu=-9999.f;
				Oli(i,j)=obu;
			}
			else {
				Oli(i,j)=9999.0f;
			}
		}
	}
}

void flexField::pbl(const Grid &G)
{
// Refer to HySplit

#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			PBL(i,j)=PBL_MIN;
			float q1, q2, qc;
			int k;
			q1=pt_f(T(0,i,j),Ps(i,j));
			qc=q1+2.0;
			for (k=Idx(i,j)+1; k<G.nl; k++) {
				float p=pk(G,k,i,j);
				q2=pt_f(T(k,i,j),p);
				if (q2>=qc) {
					double f1=fabs((q2-qc)/(q2-q1));
					double f2=fabs((q1-qc)/(q2-q1));
					float dz=f1*Z(k-1,i,j)+f2*Z(k,i,j)-Z(0,i,j);
					PBL(i,j)=dz;
					if (dz>PBL_MAX) PBL(i,j)=PBL_MAX;
					break;
				}
				q1=q2;
			}
			if (k>=G.nl) {
				// it may happen to dataset that does not have sufficient number of layers
				PBL(i,j)=Z(G.nl-1,i,j)-Z(0,i,j);
			}
		}
	}
}

void flexField::trop(const Grid &G)
{
// Refer to http://en.wikipedia.org/wiki/Tropopause.
// call pbl(const Grid &G) first!

#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			Tropo(i,j)=PBL(i,j)+3000.0;
			int k;
			float z0=Z(0,i,j);
			for (k=Idx(i,j)+1; k<G.nl; k++) {
				if (Z(k,i,j)-z0>Tropo(i,j)) break;
			}
			if (k<G.nl) {
				for (; k<G.nl; k++) {
					// lapse rate=-dT/dZ
					float dq=T(k-1,i,j)-T(k,i,j);
					float dz=Z(k,i,j)-Z(k-1,i,j);
					if (dq/dz<0.002) {
						dz=Z(k,i,j)-z0;
						if (dz>Tropo(i,j)) Tropo(i,j)=dz;
						break;
					}
				}
			}
			else {
				Tropo(i,j)=Z(G.nl-1,i,j)-z0;
			}
		}
	}
}

void flexField::zindex(const Grid &G)
{
#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			if (G.hybrid) {
				Idx(i,j)=0;
				Z(0,i,j)=Zs(i,j);
				if (Z(1,i,j)-Zs(i,j)<1.0) errmsg("Bad surface height data - 1");
			}
			else {
				Idx(i,j)=0;
				float a=Zs(i,j)+20.0;
				float b=Ps(i,j)-2.0;
				int k, kn;
// find the layer index under the surface
				for (kn=1; kn<G.nl; kn++) {
					if (Z(kn,i,j)>=a && G.P[kn]<=b) break;
					Idx(i,j)=kn;
				}
				if (kn>=G.nl-1){
					printf("ndx %d, max %d\n", kn, G.nl-1);
					 errmsg("Bad surface height data - 2");
					
				}
// set heights and w-wind under surface 
				for (k=0; k<kn; k++) {
					Z(k,i,j)=Zs(i,j);
				}
			}
		}
	}
}

void flexField::rho(const Grid &G)
{
#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			for (int k=0; k<G.nl; k++) {
				Rho(k,i,j)=rho_f(pk(G,k,i,j),T(k,i,j),RH(k,i,j));
			}
		}
	}
}

void flexField::pa2m(const Grid &G)
{
	const double fc=-286.04/9.8;

#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			for (int k=0; k<G.nl; k++) {
				W(k,i,j)=fc*W(k,i,j)*T(k,i,j)/pk(G,k,i,j);
			}
		}
	}
}

void flexField::drhodz(const Grid &G)
{
#pragma omp parallel for

	for (int i=0; i<G.nr; i++) {
		for (int j=0; j<G.nc; j++) {
			int n=Idx(i,j);
			for (int k=n+1; k<G.nl; k++) {
				float dz=Z(k,i,j)-Z(k-1,i,j);
				if (dz<1.0f) errmsg("Bad geopotential height data");
				DRho(k-1,i,j)=(Rho(k,i,j)-Rho(k-1,i,j))/dz;
			}
			DRho(G.nl-1,i,j)=DRho(G.nl-2,i,j);
			for (int k=n-1; k>=0; k--) {
				DRho(k,i,j)=DRho(k+1,i,j);
			}
		}
	}
}

