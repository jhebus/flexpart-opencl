/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexmodel.h"
#include <stdio.h>
#include <math.h>
#include <string.h>

#define __CL_ENABLE_EXCEPTIONS

#ifdef _WIN32
#include <windows.h>
//#define _WIN32_WINNT 0x0400     // target NT4.0 version or over
#endif




static int flexcpp(int argc, char* argv[])
{
    
    
	try {
            //flexModel m;
            flexModel *m = new flexModel();
            if(m->setOption(argc,argv)) 
                m->start();
	}
	catch (const char*msg) {
		printf("%s\n",msg);
		return 1;
	}
	catch (...) {
		printf("Unexpected execution error!\n");
		return 1;
	}
	return 0;
}


int main(int argc, char* argv[])
{
#ifdef _DEBUG
	flexModel m;
	char *arg0="flexcpp";
	char *arg1="-ini";
	char *arg2="c:/projects/flexcpp/bin/hateruma-ncep2.ini";
	char *args[3]={arg0, arg1, arg2};
	return flexacpp(3,args);
#endif

	if (argc<2) {
// try to *.ini to initialize and run the model
		char fname[1024];
		strcpy(fname,argv[0]);
		char *ext=strstr(fname,".exe");
		if (!ext) ext=strstr(fname,".out");
		if (ext && ext-fname==strlen(fname)-4) {
			strncpy(ext,".ini",4);
		}
		else {
			strcat(fname,".ini");
		}
		FILE *fp=fopen(fname,"r");
		if (fp) {
			fclose(fp);
			char *arg0=argv[0];
			char *arg1="-ini";
			char *arg2=fname;
			char *args[3]={arg0, arg1, arg2};
			flexcpp(3,args);
			puts("\nPress any key to continue...");
			getc(stdin);
			return 0;
		}
// show help
		printf("Usage: flexgpu [-option value ...]\n");
		printf("  -ini file_name\n");
		printf("       Initializes model using parameters in the file.\n");
		printf("  -dev platform device\n");
		printf("       Sets platform and device for parallel computing.\n");
		printf("       Show available platforms and devices if id<0\n");
		printf("  -in  path hour\n");
		printf("       Sets input path and field time resolution.\n");
		printf("  -out path hour\n");
		printf("       Sets output path and interval.\n");
		printf("  -t1  year month day hour minute\n");
		printf("       Sets start date and time (GMT).\n");
		printf("  -t2  year month day hour minute\n");
		printf("       Sets end date and time (GMT).\n");
		printf("  -itv hour\n");
		printf("       Sets continuous release interval (default: 0)\n");
		printf("  -len hours\n");
		printf("       Sets trajectory length.\n");
		printf("  -add hours\n");
		printf("       Sets interval for continuous release.\n");
		printf("  -bw  flag\n");
		printf("       Sets backward mode (1: yes; 0: no; default: 1).\n");
		printf("  -dsp flag\n");
		printf("       Sets dispersion mode (1: yes; 0: no; default: 1).\n");
		printf("  -omp threads\n");
		printf("       Sets thread numbers for OpenMP (default: 1).\n");
		printf("  -tm  flag\n");
		printf("       Shows execution timing (1: yes; 0: no; default: 0).\n");
		printf("  -gl  w h fname\n");
		printf("       Animates particles by openG in interactive mode.\n");
		printf("       w: window width\n");
		printf("       h: window height\n");
		printf("       fname: image to be used as globe texture\n");
		printf("  -cv  flag\n");
		printf("       Sets moist convection flag (1: yes; 0: no; default: 0).\n");
		printf("  -dt  syn stp\n");
		printf("       Sets time steps\n");
		printf("       syn: synchronization step in second\n");
		printf("       stp: maximal integration step in second\n");
		printf("  -ctl fac\n");
		printf("       Sets auto time step adjustment factor (default: 0).\n");
		printf("       fav <= 0 means using fixed time step.\n");
		printf("  -xyz flag\n");
		printf("       Sets x-y-z dumping (default: 0).\n");
		printf("       flag != 0 means dumping x-y-z in output.\n");
		printf("  -rel name np x0 y0 ds z0 dz\n");
		printf("       Releases particles randomly.\n");
		printf("       name: particle class name\n");
		printf("       np: number of points\n");
		printf("       x0: center longitude [degree]\n");
		printf("       y0: center latitude [degree]\n");
		printf("       ds: distance radius from the center [m]\n");
		printf("       z0: start height [m]\n");
		printf("       dz: height above z0[m]\n");
		printf("  -rls name x0 dx nx y0 dy ny z0 dz nz mass half_life\n");
		printf("       Releases particles at grids\n");
		printf("       name: particle class name\n");
		printf("       x0: left longitude [degree]\n");
		printf("       dx: longitude grid [degree]\n");
		printf("       nx: number of longitude grids\n");
		printf("       y0: bottom latitude [degree]\n");
		printf("       dy: latitude grid [degree]\n");
		printf("       ny: number of latitude grids\n");
		printf("       z0: height [m]\n");
		printf("       dz: altitude grid [degree]\n");
		printf("       nz: number of vertical grids\n");
                printf("       mass (g): The mass of each particle to be released (assumes homogeneous particles). Set to 0 if not used\n");
                printf("       half_life (s): The half-life for the released particles (assumes homogeneous particles). Set to 0 if not used\n");
		printf("  -mb  flag\n");
		printf("       Sets mobile release flag (1: yes; 0: no; default: 0).\n");
		printf("  -flx flag\n");
		printf("       Sets flux data interpolation flag (1: yes; 0: no; default: 1).\n");
		printf("  -ct  x0 nx y0 ny deg z0\n");
		printf("       Sets grid and hight limit for counting particles\n");
		printf("       x0: longitude (0 to 360 degree)\n");
		printf("       nx: number of longitude grids\n");
		printf("       y0: latitude (-90 to 90 degree)\n");
		printf("       ny: number of latitude grids\n");
		printf("       deg: horizontal grid\n");
		printf("       z0: altitude of the first particle counting level\n");
		printf("  -vn  fname vname\n");
		printf("       Sets netCDF variable names (vname) to correspond to\n");
		printf("       input field (fname)\n");
		printf("\n    If there is a file with the same name as the program file\n");
		printf("but the extension of ini, the program will try to use the file to\n");
		printf("initialize and run the model.\n");

		puts("\nPress any key to continue...");
		getc(stdin);

		return 0;
	}
        
	return flexcpp(argc,argv);
}

