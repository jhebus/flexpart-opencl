/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexparam.h"
#include "flexutility.h"
#include <stdio.h>

#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define RAND_AND_GAUSS_LEN 2000000

flexParam::flexParam()
{
	d_Rand                  = NULL;
	d_Gauss                 = NULL;
	
	host.outsec             = 24  * 3600;
	host.lensec             = 120 * 3600;
	host.openCL             = 0;
	host.openmp             = 1;
	host.timing             = 0;
	host.dispersion         = 1;

	host.ctl                = 0;
	host.turbmesoscale      = 0.16;
	host.d_trop             = 50.0;
	host.d_strat            = 0.1;
	host.ldirect            = -1;
	host.lsynctime          = 60;
	host.ldt                = 10;
	host.ifine              = 5;
	host.lconv              = 0;
	host.lflx               = 1;
	host.ng                 = RAND_AND_GAUSS_LEN;

	Rand.resize(host.ng);
        Gauss.resize(host.ng);


	long    k,
                idum            = -7,
                iy              = 0,
                iv[32];
        
	float g1, g2;

	for (k=1; k<host.ng; k+=2) {
		gasdev2(idum,iy,iv,32,g1,g2);
		Gauss(k-1) = g1;
		Gauss(k)   = g2;
	}

	idum = -7;
	iy   = 0;

	for (k=0; k<host.ng; k++) {
		Rand(k) = ran2(idum,iy,iv,32);
	}

        host.Rand               = Rand.ptr();
        host.Gauss              = Gauss.ptr();
}

flexParam::~flexParam()
{
    PRINTF("PARAM : destroy me\n");
	//clearDevice();
}

void flexParam::update()
{
    if(host.openCL)
	updateDevice();
}

void flexParam::updateDevice()
{
    PRINTF("PARAM : UpdateDevice\n");
    
    if(!host.openCL)
        return;
    size_t size=host.ng*sizeof(float);
    int var = 0;
    
    //copy the random arrays
    if (!d_Rand) mallocOnDevice(&d_Rand, size, Rand.ptr());
    setDeviceData(d_Rand, Rand.ptr(), size);

    if (!d_Gauss) mallocOnDevice(&d_Gauss, size, Gauss.ptr());
    setDeviceData(d_Gauss, Gauss.ptr(), size);

    host.Rand = Rand.ptr();
    host.Gauss = Gauss.ptr();
    
    var += KERNEL_CHOICE_ARGS + ERROR_ARGS + INTERPOLATION_ARGS + GRID_ARGS;
    setKernelArgs(0, var++, sizeof(cl_mem), &d_Gauss); 
    setKernelArgs(0, var++, sizeof(cl_mem), &d_Rand);     
    setKernelArgs(0, var++, sizeof(int), &host.ctl); 
    setKernelArgs(0, var++, sizeof(float), &host.d_strat); 
    setKernelArgs(0, var++, sizeof(float), &host.d_trop); 
    setKernelArgs(0, var++, sizeof(int), &host.dispersion); 
    setKernelArgs(0, var++, sizeof(int), &host.ifine); 
    setKernelArgs(0, var++, sizeof(int), &host.lconv); 
    setKernelArgs(0, var++, sizeof(int), &host.ldirect); 
    setKernelArgs(0, var++, sizeof(int), &host.ldt); 
    setKernelArgs(0, var++, sizeof(int), &host.lensec); 
    setKernelArgs(0, var++, sizeof(int), &host.lflx); 
    setKernelArgs(0, var++, sizeof(int), &host.lsynctime); 
    setKernelArgs(0, var++, sizeof(int), &host.ng); 
    setKernelArgs(0, var++, sizeof(int), &host.outsec); 
    setKernelArgs(0, var++, sizeof(int), &host.timing); 
    setKernelArgs(0, var++, sizeof(float), &host.turbmesoscale); 
        /*
        //then copy the rest of the parameters into the struct to be copied across
        DeviceParameter dev_parameter;
        
        dev_parameter.ctl = host.ctl;
        dev_parameter.d_strat = host.d_strat;
        dev_parameter.d_trop = host.d_trop;
        dev_parameter.dispersion = host.dispersion;
        dev_parameter.ifine = host.ifine;
        dev_parameter.lconv = host.lconv;
        dev_parameter.ldirect = host.ldirect;
        dev_parameter.ldt = host.ldt;
        dev_parameter.lensec = host.lensec;
        dev_parameter.lflx = host.lflx;
        dev_parameter.lsynctime = host.lsynctime;
        dev_parameter.ng = host.ng;
        dev_parameter.openCL = host.openCL;
        dev_parameter.openmp = host.openmp;
        dev_parameter.outsec = host.outsec;
        dev_parameter.timing = host.timing;
        dev_parameter.turbmesoscale = host.turbmesoscale;
        
        //if (!d_params) mallocOnDevice(&d_params, size);
	//setDeviceData(d_params, &(dev_parameter), sizeof(DeviceParameter));
        */
        
        
#ifdef _DEBUG
	Parameter p;
	getDeviceData(&p,device,sizeof(Parameter));
	if (p.ng!=host.ng) errmsg("device.ng!=host.ng");
	checkDeviceData(Gauss.ptr(),p.Gauss,host.ng,false);
#endif
}

void flexParam::updateHost(){
    PRINTF("Flexparam: Updatehost - there should be nothing to copy back\n");   
}

void flexParam::clearDevice()
{
    PRINTF("PARAM : clearDevice\n");
    if(host.openCL){
	freeOnDevice(d_Rand);
	freeOnDevice(d_Gauss);
    }
        
    d_Rand=NULL;
    d_Gauss=NULL;
}

