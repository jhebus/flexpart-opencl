/* 
 * File:   kernel_trajectory_functions.h
 * Author: jhebus
 *
 * Created on 03 July 2013, 22:12
 */

#ifndef KERNEL_TRAJECTORY_FUNCTIONS_H
#define	KERNEL_TRAJECTORY_FUNCTIONS_H

#include "flexocl.h"                    //For the structs

/*! \addtogroup traj_kernel
 Documentation for trajjectory kernel functions used by
 host computer and GPU device.
 @{
 */

/** Interpolates 2D field data
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 */

float interpo2d_f(const Factor *f, const Grid *g, const float* v);


/** Interpolates 3D field data on a level.
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 @param k Level index of v.
 */

float interpo3d_f(const Factor *f, const Grid *g, const float* v, int k);

/** Calculates standard deviation for a grid box.
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to 3D field data.
 */

float std3d_f(const Factor *f, const Grid *g, const float* v);

/** Interpolate 3D field data
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 */

float field3d_f(const Factor *f, const Grid *g, const float* v);

/** Calculate vertical interpolation factors.
 @param g Grid object.
 @param f Field object.
 @param z Altitude.
 @param i Field row index.
 @param j Field column index.
 */

Factor zindex_f(const Grid *g, const Field *f, float z, int i, int j);


/** Finds x-y grid indices and calculates interpolation factors (z refer to surface)
 @param g Grid object.
 @param f Field object.
 @param x Longitude.
 @param y Latitude.
 @param z Altitude.
 */

Factor index_f(const Grid *g, const Field *f, float x, float y, float z);


/** Reflect excessive altitude displacement on boundaries (z refer to the surface)
 @param z Altitude.
 @param zmax Altitude maximum.
 */

float reflect_f(float z, float zmax);


//! Hanna function. Refer to hanna.f of flexpart6.4-gfs version

Turb hanna_f(float z, float h, float ol, float ust, float wst);

/** Converts distance change in m to degree.
 @param g Grid object.
 @param lon Longitude [deg]
 @param lat Latitude [deg]
 @param dx Est-west displacement [m]
 @oaram dy North-south displacement [m]
 @param zt Altitude [m]
 */

XY xy2ll_f(const Grid *g, float lon, float lat, float dx, float dy, float zt);

/** Advances one synchronize time step for dispersion (refer to advance.f of flexpart6.4-gfs version)
 @param C Class object.
 @param G Grid object.
 @param P Parameter object.
 @param F Field obejct.
 */

int dispersion_f(Class *C, int idx, const Grid *G, const Parameter *P, const Field *F);


/** Advances one synchronize time step for trjaectory calculation using mean wind fields.
 @param C Class object.
 @param idx Particle index.
 @param G Grid object.
 @param P Parameter object.
 @param F Field obejct.
 */

int trajectory_f(Class *C, int idx, const Grid *G, const Parameter *P, const Field *F);


#endif	/* KERNEL_TRAJECTORY_FUNCTIONS_H */

