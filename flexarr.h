/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXARR_H__
#define __FLEXARR_H__

#include <string.h>

#define MISSING		-1.e15

/**
 flexArr template is for 1D to 3D array of data.
 <br>
 <br>
 A 2D array is a row-major matrix. To access a 2D array value by one index,
 you should calculate the index as:
 <pre>
    index = row_index*number_of_columns + column_index;
 </pre>
 A 3D array is a stack of 2D arrays. To access a 3D array value by one index,
 you should calculate the index as:
 <pre>
    index = layer_index*number_of_rows*number_of_columns + row_index*number_of_columns + column_index;
 </pre>
 */

template<class T> class flexArr
{
	T* _ptr;			// data

	int	_nl,			// number of levels
		_nr,			// number of rows
		_nc,			// number of columns
		_nd;			// number of data

	void dealloc()
	{
		if (_ptr) {
			delete[] _ptr;
			_ptr=0;
			_nl=_nr=_nc=_nd=0;
		}
	}

public:

    //! Constructor
	flexArr() : _ptr(0), _nl(0), _nr(0), _nc(0), _nd(0)
	{ }

    //! Constructor
	flexArr(int n) : _ptr(0), _nl(0), _nr(0), _nc(0), _nd(0)
	{
		resize(n);
	}

    //! Constructor
	flexArr(int nr, int nc) : _ptr(0), _nl(0), _nr(0), _nc(0), _nd(0)
	{
		resize(nr,nc);
	}

    //! Constructor
	flexArr(int nl, int nr, int nc) : _ptr(0), _nl(0), _nr(0), _nc(0), _nd(0)
	{
		resize(nl,nr,nc);
	}

    //! Constructor
	~flexArr()
	{
		dealloc();
	}

    /** Resizes array and sets it to 1D.
     @param n Number of data.
     */
	void resize(int n)
	{
		// one dimensional
		dealloc();
		if (n>0) {
			_nl=n;
			_nr=1;
			_nc=1;
			_nd=n;
			_ptr=new T[_nd];
		}
	}

    /** Resizes array and sets it to 2D.
     @param nr Number of rows.
     @param nc Number of columns.
     */
	void resize(int nr, int nc)
	{
		// two dimensional
		dealloc();
		if (nr>0 && nc>0) {
			_nl=1;
			_nr=nr;
			_nc=nc;
			_nd=nr*nc;
			_ptr=new T[_nd];
		}
	}

    /** Resizes array and sets it to 3D.
     @param nl Number of levels.
     @param nr Number of rows.
     @param nc Number of columns.
     */
	void resize(int nl, int nr, int nc)
	{
		// three dimensional
		dealloc();
		if (nl>0 && nr>0 && nc>0) {
			_nl=nl;
			_nr=nr;
			_nc=nc;
			_nd=nl*nr*nc;
			_ptr=new T[_nd];
		}
	}

    //! Returns the number of levels.
	int nl() const { return _nl; }

    //! Returns the number of rows.
	int nr() const { return _nr; }

    //! Returns the number of columns.
	int nc() const { return _nc; }

    //! Returns the number of data.
	int nd() const { return _nd; }

    //! Returns the pointer to data.
	const T * ptr() const { return _ptr; }
    //! Returns the pointer to data.
	      T * ptr()       { return _ptr; }

    /** Returns a value.
     @param k 1D array index index.
     */
	const T & operator()(int k) const { return _ptr[k]; }
	      T & operator()(int k)       { return _ptr[k]; }

    /** Returns a value.
     @param i Row index.
     @param j Column index.
     */
	const T & operator()(int i, int j) const { return _ptr[i*_nc+j]; }
	      T & operator()(int i, int j)       { return _ptr[i*_nc+j]; }

    /** Returns a value.
     @param k Level index.
     @param i Row index.
     @param j Column index.
     */
	const T & operator()(int k, int i, int j) const { return _ptr[k*_nr*_nc+i*_nc+j]; }
	      T & operator()(int k, int i, int j)       { return _ptr[k*_nr*_nc+i*_nc+j]; }

    /** Fills the array with a constant.
     @param v A constant value.
     */
	void fill(T v) { for (int i=0; i<_nd; i++) _ptr[i] = v; }

    //! Fills the array with zeros.
    void clear() { if (_ptr) memset(_ptr,0,_nd*sizeof(T)); }
};


#endif	// __FLEX_H__
