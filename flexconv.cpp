/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "convect43c.h"
#include "flexconv.h"
#include "./netCDF/netcdf.h"
#include "flexutility.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define __device__ static inline
#define __FLEX__

// import common kernel functions

#include "kernel_meterological_functions.h"

#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


extern cl_mem float_probe, int_probe;

//this is a hack till i understand how to pass scalars in structs
static Parameter static_p;
static int static_itterations;

flexConv::flexConv()
{
    PRINTF("FLEXCONV : create\n");
	dev_convect_array       = NULL;
	d_Pdx                   = NULL;
	d_Sdx                   = NULL;
	d_CBMF                  = NULL;
}

flexConv::~flexConv()
{
    PRINTF("FLEXCONV : destroy\n");
	clearDevice();
}

void flexConv::convection(flexClass &C, const flexGrid &G, const flexParam &P, const flexField &F)
{
    
    PRINTF("FLEXCONV : convection\n");

    // todo: it does not work with NVIDIA device, which gives "unspecified launch failure" error;
	// too much local memory use?

	

	updateDevice(C.host.np,G.host.nxy,P.host.openCL);

	time_t tick=time(NULL);

	int ns=C.sort(Pdx,Sdx,P.host.openCL);
/*
	if (P.host.cuda) {
            if(P.host.cuda) errmsg("Convection cannot be executed on NVIDIA GPU");

		setDeviceData(d_Sdx,Sdx.ptr(),Sdx.nd()*sizeof(int));
		setDeviceData(d_Pdx,Pdx.ptr(),Pdx.nd()*sizeof(int));
		int i=1, nt;
		while (ns>i) {
			nt=(ns-i);
			nt=min(nt,NTHR);
			redistDevice(nt,d_Sdx,i,d_Pdx,d_CBMF,device,C.device,G.device,P.device,F.device);
			synchronizeDeviceThread();
			i+=nt;
		}
	}
 */ 
	//else
        {
#pragma omp parallel for
		for (int i=1; i<ns; i++) {
			redistribute(i,&C.host,&G.host,&P.host,&F.host);
		}
	}

	if (P.host.timing) printf("Convection, %.1f\n",difftime(time(NULL),tick));
}
/*----------------------------------------------------------------------------*/
void flexConv::runConvectionKernel(int itterations, DeviceClass *C, cl_mem G, const flexParam *P, DeviceField *F){
    
    PRINTF("FLEXCONV : runKernel\n");
    
    //cl_kernel kernel = getSuperKernel();
    //cl_int errorNum;
    
    cl_int choice = CONVECTION_KERNEL;
    int var = 0;
    float dummyVal = 0.0;
    //set the static values
    static_p.ctl = P->host.ctl;
    static_p.d_strat = P->host.d_strat;
    static_p.d_trop = P->host.d_trop;
    static_p.dispersion = P->host.dispersion;
    static_p.ifine = P->host.ifine;
    static_p.lconv = P->host.lconv;
    static_p.ldirect = P->host.ldirect;
    static_p.ldt = P->host.ldt;
    static_p.lensec = P->host.lensec;
    static_p.lflx = P->host.lflx;
    static_p.lsynctime = P->host.lsynctime;
    static_p.ng = P->host.ng;
    static_p.openCL = P->host.openCL;
    static_p.openmp = P->host.openmp;
    static_p.outsec = P->host.outsec;
    static_p.timing = P->host.timing;
    static_p.turbmesoscale = P->host.turbmesoscale;

    static_itterations = itterations;
    
    //the first argument indicates the kernel code to run
    setKernelArgs(choice, var++, sizeof(int), &choice);
 
    var = 61;
    
    //The grid
    setKernelArgs(choice, var++, sizeof(cl_mem), &G);       //should be able to pass struct directly
    
    //The Parameter  var += 19;
    setKernelArgs(choice, var++, sizeof(cl_mem), (void*)&P->d_Gauss); 
    setKernelArgs(choice, var++, sizeof(cl_mem), (void*)&P->d_Rand); 
    
    setKernelArgs(choice, var++, sizeof(int), &static_p.ctl); 
    setKernelArgs(choice, var++, sizeof(float), &static_p.d_strat); 
    setKernelArgs(choice, var++, sizeof(float), &static_p.d_trop); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.dispersion); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.ifine); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.lconv); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.ldirect); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.ldt); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.lensec); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.lflx); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.lsynctime); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.ng); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.openCL); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.openmp); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.outsec); 
    setKernelArgs(choice, var++, sizeof(int), &static_p.timing); 
    setKernelArgs(choice, var++, sizeof(float), &static_p.turbmesoscale); 
   
    //class variables
    setKernelArgs(choice, var++, sizeof(int), &C->np);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Idx);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Gdx);
   // setKernelArgs(choice, var++, sizeof(cl_mem), NULL);                   //need to figure out what is going on here
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->X);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Y);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Z);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Fg);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Up);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Vp);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Wp);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Um);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Vm);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Wm);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Zs);
    setKernelArgs(choice, var++, sizeof(cl_mem), &C->Zb);
    
    //field variables
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->U);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->V);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->W);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Z);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Rho);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->DRho);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->T);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->RH);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->PBL);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Tropo);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Ustar);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Wstar);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Oli);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Ps);
    
    //Convection Array
    setKernelArgs(choice, var++, sizeof(cl_mem), &d_Pdx);
    setKernelArgs(choice, var++, sizeof(cl_mem), &d_Sdx);
    setKernelArgs(choice, var++, sizeof(cl_mem), &d_CBMF);
    setKernelArgs(choice, var++, sizeof(int), &static_itterations);
    
    //printf("This should be calculated by querying the device!!\n");
    const size_t local_ws = 256;	// Number of work-items per work-group
    
    // shrRoundUp returns the smallest multiple of local_ws bigger than size
    
    size_t global_ws = shrRoundUp(local_ws, Pdx.nd());	// Total number of work-items
    
    
    runKernelBlocking(&global_ws, local_ws);
}
/*----------------------------------------------------------------------------*/
void flexConv::updateDevice(int np, int nxy, int cuda)
{
    if (np!=Pdx.nd()) {
            Pdx.resize(np);
    }
    if (nxy!=CBMF.nd()) {
            CBMF.resize(nxy);
            CBMF.clear();
            Sdx.resize(nxy);
    }
}
/*
void flexConv::updateDevice(int resize_a, int resize_b)
{    
    PRINTF("FLEXCONV : updateDevice\n");
    
    if(resize_a){
       freeOnDevice(d_Pdx);
	d_Pdx=NULL;
    }
    
    if(resize_b){
        freeOnDevice(d_Sdx);
	d_Sdx=NULL;
	freeOnDevice(d_CBMF);
	d_CBMF=NULL;
    }
    
    if(!d_Pdx) mallocOnDevice(&d_Pdx, Pdx.nd() * sizeof(int));
    setDeviceData(d_Pdx, Pdx.ptr(),   Pdx.nd() * sizeof(int));
    
    if(!d_Sdx) mallocOnDevice(&d_Sdx, Sdx.nd() * sizeof(int));
    setDeviceData(d_Sdx, Sdx.ptr(),   Sdx.nd() * sizeof(int));
    
    if(!d_CBMF) mallocOnDevice(&d_CBMF, CBMF.nd() * sizeof(float));
    setDeviceData(d_CBMF, CBMF.ptr(), CBMF.nd() * sizeof(float));
}
 */ 
/*----------------------------------------------------------------------------*/
void flexConv::updateHost(){
    PRINTF("FLEXCONV : updateHost\n");

    //check if only cbmf comes back
    
    getDeviceData(Pdx.ptr(),  d_Pdx,  Pdx.nd()  * sizeof(int));
    getDeviceData(CBMF.ptr(), d_CBMF, CBMF.nd() * sizeof(float));
    getDeviceData(Sdx.ptr(),  d_Sdx,  Sdx.nd()  * sizeof(int));  
}
/*----------------------------------------------------------------------------*/
void flexConv::clearDevice()
{
    PRINTF("FLEXCONV : clearDevice\n");
	freeOnDevice(d_Pdx);
	d_Pdx = NULL;
	freeOnDevice(d_Sdx);
	d_Sdx = NULL;
	freeOnDevice(d_CBMF);
	d_CBMF = NULL;
}
/*----------------------------------------------------------------------------*/
int flexConv::convmatix(Convect &Cv, const Field *F, int nij, const Grid *G, const Parameter *P)
{
	int i, k, k0, m, ncv, nd, nl, iflag;
	float t[NA], q[NA], qs[NA], p[NA], ph[NA];
	
	if (G->nl>=NA) errmsg("nl>=NA");

	float ps=F->Ps[nij]-10.0f;
	float zs=F->Z[nij];
	float cbmf=CBMF(nij);
	float delt=P->lsynctime;

// find the first level above surface

	for (k0=1; k0<G->nl; k0++) {
		if (G->P[k0] < ps) break;
	}

	if (k0>=G->nl) errmsg("k0>=G->nl");


// surface level
	ps=F->Ps[nij];
	t[0]=F->T[nij];
	q[0]=hq_f(ps,t[0],F->RH[nij]);
	qs[0]=hq_f(ps,t[0],100.0f);
	p[0]=0.01*ps;					// Pa to hPa
	ph[0]=p[0]+1.0f;
	Cv.ZIJ[0]=0.0;

	for (k=k0; k<G->nl; k++) {
		m=k*G->nxy+nij;
		i=k-k0+1;
		t[i]=F->T[m];
		q[i]=hq_f(G->P[k],t[i],F->RH[m]);
		qs[i]=hq_f(G->P[k],t[i],100.0f);
		p[i]=0.01*G->P[k];
		ph[i]=0.5*(p[i]+p[i-1]);
		Cv.ZIJ[i]=F->Z[m]-zs;
		Cv.MASS[i-1]=(ph[i-1]-ph[i])*100.0/9.81;		// dp=-rho*g*dz -> p in Pa, mass in kg/m2. 
	}

	ph[i+1]=0.99*p[i];
	Cv.MASS[i]=(ph[i]-ph[i+1])*100.0/9.81;
	nd=i+1;
	nl=nd-1;
	ncv=0;

// calculate convective level

	float precip, wd, tprime, qprime;
	float ft[NA], fq[NA]; 

	convect_(&nd,&nl,&delt,&iflag,&precip,&wd,&tprime,&qprime,&cbmf,
		ft,fq,Cv.SUB,Cv.FMASS,t,q,qs,p,ph,&ncv);

	CBMF(nij)=cbmf;

	if (iflag!=1 || ncv<1) return 0;

	if (ncv>nd) errmsg("ncv > nd");

// FRAC(i,j) is mass flux from i to j in FORTRAN, but j to i in C.

	memset(Cv.FRAC,0,NA*NA*sizeof(float));

	for (i=0; i<ncv; i++) {
		Cv.FSUM[i]=0.0;
		for (k=0; k<ncv; k++) {
			m=i*NA+k;
// mass flux to mass displacement from i to j
			Cv.FRAC[m]=delt*Cv.FMASS[k*NA+i];
			Cv.FSUM[i]+=Cv.FRAC[m];
		}
		Cv.FRAC[i*NA+i]+=Cv.MASS[i]-Cv.FSUM[i];
		Cv.SUB[i]*=-287.06*t[i]/(p[i]*100.0);
	}

#ifdef _DEBUG
/*
printf("T,Q,QS,P,PH,ZIJ,MASS\n");
for (i=0; i<G->nl; i++) {
	printf("%.2f,%.5f,%.5f,%.1f,%.1f,%.2f,%.2f\n",t[i],q[i],qs[i],p[i],ph[i],Cv.MASS[i]);
}
for (i=0; i<G->nl; i++) {
	for (k=0; k<G->nl; k++) {
		printf(" %.7f",Cv.FMASS[k*NA+i]);
	}
	printf("\n");
}
for (i=0; i<G->nl; i++) {
	for (k=0; k<G->nl; k++) {
		printf(" %.7f",Cv.FMASS[k*NA+i]);
	}
	printf("\n");
}
printf("**************\n");
for (i=0; i<G->nl; i++) {
	for (k=0; k<G->nl; k++) {
		printf(" %.7f",Cv.FRAC[i*NA+k]);
	}
	printf("\n");
}
*/
#endif
	return ncv;
}
/*----------------------------------------------------------------------------*/
void flexConv::redistribute(int is, Class *C, const Grid *G, const Parameter *P, const Field *F)
{
	Convect Cv;

        //i think that n1 and n2 are the levels
	int n1=Sdx(is-1);                       
	int n2=Sdx(is);
	int nij=C->Gdx[n1];
	float ztop=F->Z[nij+(G->nl-1)*G->nxy];

	int ncv=convmatix(Cv,F,nij,G,P);

	if (ncv>0) {
           // printf("FLEXCONV :  CONVECTION!\n");
           // exit(-1);
// convection occurs
		int i, k, id, lev;
		for (i=n1; i<n2; i++) {
			id=Pdx(i);
			float zt=C->Z[id];
			if (zt>=Cv.ZIJ[ncv-1]) continue;
// inside convection level, find zt level.
			for (lev=1; lev<ncv; lev++) {
				if (Cv.ZIJ[lev]>zt) {
					lev--;
					break;
				}
			}
// relocate particle; refer to redist.f of flexpart.
			bool relocated=false;
			float rn=P->Rand[C->Idx[id]%P->ng];
			if (Cv.FSUM[lev]>1.e-20) {
				float sum=0.0;
				for (k=0; k<ncv; k++) {
					if (P->ldirect>0) {
						sum+=Cv.FRAC[lev*NA+k]/Cv.MASS[lev];
					}
					else {
						sum+=Cv.FRAC[k*NA+lev]/Cv.MASS[lev];
					}
					if (sum>=rn) {
						zt=Cv.ZIJ[k]+rn*(Cv.ZIJ[k+1]-Cv.ZIJ[k]);
						relocated=true;
						break;
					}
				}
			}
			if (!relocated) {
				float dz=Cv.ZIJ[lev+1]-Cv.ZIJ[lev];
				float fb=fabs((zt-Cv.ZIJ[lev])/dz);
				float fa=fabs((Cv.ZIJ[lev+1]-zt)/dz);
				zt+=rn*P->lsynctime*(fa*Cv.SUB[lev]+fa*Cv.SUB[lev+1]);
			}
			if (zt<0.0) zt=-zt;
			if (zt>ztop) zt=ztop-fmod(zt,ztop);
			C->Z[id]=zt;
		}
	}
}
/*----------------------------------------------------------------------------*/
