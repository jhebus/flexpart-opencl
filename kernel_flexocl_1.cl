/**
 * The ONLY opencl file for the flexocl largrangian code.
 * 
 * First an appology. The number of arguments in this code make me sad, so i can
 * only image what you are thinking.
 */

#define INTERPOLATION_KERNEL            ( 0 )       
#define INTERPOLATION_KERNEL_WITH_FLUX  ( 1 )       
#define DISPERSION_KERNEL               ( 2 )
#define TRAJECTORY_KERNEL               ( 3 )
#define CONVECTION_KERNEL               ( 4 )

//#define RAND_ARRAY_SIZE         ( 20 )

//These are for the convection kernel
#define dmax(a,b) ((a)>(b)?(a):(b))
#define dmin(a,b) ((a)<(b)?(a):(b))
//#define max(a,b) ((a)>(b)?(a):(b))
//#define min(a,b) ((a)<(b)?(a):(b))
#define dabs(a) (fabs((a)))

float pow_dd(float *a, float *b) { return pow(*a, *b); }
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
//Duplicated from flexocl.h
typedef struct {
    int
        //! Hybrid grid flag
        hybrid,
        //! Global grid indicator
        gl,
        //! Number of vertical levels
        nl,
        //! Number of latitude grids (data columns)
        nc,
        //! Number of longitude grid (data rows)
        nr,
        //! nxy=nr*nc;
        nxy,
        //! number of seconds between field datasets
        ns;
    float
        //! Start longitude [deg]
        x0,
        //! End longitude [deg]
        x1,
        //! Longitude grid [deg]
        dx,
        //! Start latitude [deg]
        y0,
        //! End latitude [deg]
        y1,
        //! Latitude grid [deg]
        dy;
    float
        //! Pressure levels [Pa]
        P[256],
        //! half pressure level parameters [P=A+B*Ps]
        A[256], B[256];
} Grid;
/*---------------------------------------------------------------------------------------------*/
//Duplicated from flexocl.h
typedef struct {
	__global float
		//! U-wind [m/s] (3D)
		*U,
		//! V-wind [m/s] (3D)
		*V,
		//! W-wind [m/s] (3D, converted from Ps/s)
		*W,
		//! Geopotential height [m] (3D)
		*Z,
		//! Air density [kg/m3]  (3D)
		*Rho,
		//! Air density gradient [kg/m4] (3D)
		*DRho,
		//! Air temperature [K] (3D)
		*T,
		//! Relative humidity [%] (3D)
		*RH;
	__global float
		//! Planetary boundary layer [m above surface] (2D)
		*PBL,
		//! Thermal tropopause [m above surface] (2D)
		*Tropo,
		//! Friction velocity [m/s] (2D)
		*Ustar,
		//! Convective velocity scale [m/s] (2D)
		*Wstar,
		//! Monin-Obukhov length [m above surface] (2D)
		*Oli,
		//! Surface pressure [Pa] (2D)
		*Ps;
} Field;
/*---------------------------------------------------------------------------------------------*/
//Duplicated from flexocl.h
typedef struct {
     __global float 
        //! Uniform random number array
        *Rand,
        //! Guassian random number array
        *Gauss;
    float
        //! Turbulent diffusivity for horizontal components in the troposphere [m2/s]
        d_trop,
        //! Turbulent diffusivity for vertical component in the stratosphere [m2/s]
        d_strat,
        //! Factor by which standard deviations of winds at grid points surrounding
        //! particle positions are scaled to yield the scales for the mesoscale wind velocity fluctuations  
        turbmesoscale;
    int
        //! Mode flag: 1 for forward and -1 for backward simulation
        ldirect,
        //! Synchronisation time of all particles [s]
        lsynctime,
        //! Maximum time step for integration [s]
        ldt,
        //! Fine loops for integration of turbulent wind (must >1)
        ifine,
        //! Time step control factor
        ctl,
        //! Moist convection flag: 1 for yes or 0 no.
        lconv,
        //! Flux interpolation flag: 1 for yes or 0 no.
        lflx,
        //! Size of Gauss random data array
        ng;
    int
        //! Output interval [s]
        outsec,
        //! Trajectory length [s]
        lensec,
        //! NVIDIA device flag: 1 for yes or 0 for no
        openCL,
        //! Number of threads for using OpenMP
        openmp,
        //! Performance timing flag: 1 for yes or 0 for no.
        timing,
        // Dispersion flag: 1 for yes or 0 for no.
        dispersion; 
} Parameter;

typedef struct {
    float
        //! Turbulent diffusivity for horizontal components in the troposphere [m2/s]
        d_trop,
        //! Turbulent diffusivity for vertical component in the stratosphere [m2/s]
        d_strat,
        //! Factor by which standard deviations of winds at grid points surrounding
        //! particle positions are scaled to yield the scales for the mesoscale wind velocity fluctuations  
        turbmesoscale;
        int
        //! Mode flag: 1 for forward and -1 for backward simulation
        ldirect,
        //! Synchronisation time of all particles [s]
        lsynctime,
        //! Maximum time step for integration [s]
        ldt,
        //! Fine loops for integration of turbulent wind (must >1)
        ifine,
        //! Time step control factor
        ctl,
        //! Moist convection flag: 1 for yes or 0 no.
        lconv,
        //! Flux interpolation flag: 1 for yes or 0 no.
        lflx,
        //! Size of Gauss random data array
        ng;
    int
        //! Output interval [s]
        outsec,
        //! Trajectory length [s]
        lensec,
        //! NVIDIA device flag: 1 for yes or 0 for no
        openCL,
        //! Number of threads for using OpenMP
        openmp,
        //! Performance timing flag: 1 for yes or 0 for no.
        timing,
        // Dispersion flag: 1 for yes or 0 for no.
        dispersion; 
} DeviceParameter;
/*----------------------------------------------------------------------------*/
typedef struct {
	int     //! Number of particles
		np;	
		//! Random numner index
	__global int
                *Idx,
		//! Grid index
		*Gdx,
		//! Particle index
		*Pdx;
	__global float
		//! Longitude [deg]
		*X,
		//! Latitude [deg]
		*Y,
		//! Altitude [m]
		*Z,
		//! Reflection flag
		*Fg,
		//! Turbulent u-wind of the previous step [m/s]
		*Up,
		//! Turbulent v-wind of the previous step [m/s]
		*Vp,
		//! Turbulent w-wind of the previous step [m/s]
		*Wp,
		//! Meso-scale turbulent u-wind of the previous step [m/s]
		*Um,
		//! Meso-scale turbulent v-wind of the previous step [m/s]
		*Vm,
		//! Meso-scale turbulent w-wind of the previous step [m/s]
		*Wm,
		//! Surface height [m]
		*Zs,
		//! Boundary layer height [m above surface]
		*Zb;
} Class;
/*----------------------------------------------------------------------------*/
 /*Grid indexes
 <pre>
    f4               f3
 (i2,j1)-----------(i2,j2)
    |                 |
    |                 |
    |                 |
    |                 |
 (i1,j1)-----------(i1,j2)
   f1                f2
 </pre>
 */
typedef struct {
	int
		i1,
		//! Latitude grid indexes
		i2,
		j1,
		//! Longitude grid indexes
		j2;
	int
		k1, k2, k3,
		//! Vertical grid indexes
		k4;
	float
		f1, f2, f3,
		//! Horizontal interpolation factors
		f4;
	float
		fa1, fa2, fa3, fa4,
		fb1, fb2, fb3,
		//! Vertical interpolation factors
		fb4;
} Factor;
/*----------------------------------------------------------------------------*/
typedef struct {
	float
		sigu, sigv, sigw, dsigwdz, dsigw2dz, tlu, tlv, tlw;
} Turb;
/*----------------------------------------------------------------------------*/
typedef struct {
	float
		x, y;  //latitude/longitude data
} XY;
/*----------------------------------------------------------------------------*/
#define NA 70		// this must equal NA in convect43c.f
#define NTHR 16

/**
 Structures for moist convection
 */
typedef struct {
    float
        FSUM[NA],
        ZIJ[NA],
        MASS[NA],
        SUB[NA],
        FMASS[NA*NA],
        FRAC[NA*NA],
        MENT[NA*NA],
        QENT[NA*NA],
        ELIJ[NA*NA],
        SIJ[NA*NA];
} Convect;

/**
 Structures for moist convection
 */
typedef struct {
    Convect cpa[NTHR];
} ConvectArray;
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/********************* FUNCTIONS PROTOTYPES ***********************************/
/******************************************************************************/
Factor  index_f( __global Grid *g,  __global float *field_U,
                                __global float *field_V,
                                __global float *field_W,
                                __global float *field_Z,
                                __global float *field_Rho,
                                __global float *field_DRho,
                                __global float *field_T,
                                __global float *field_RH,
                                __global float *field_PBL,
                                __global float *field_Tropo,
                                __global float *field_Ustar,
                                __global float *field_Wstar,
                                __global float *field_Oli,
                                __global float *field_Ps, float x, float y, float z);

Factor  zindex_f( __global Grid *g,  __global float *field_U,
                                __global float *field_V,
                                __global float *field_W,
                                __global float *field_Z,
                                __global float *field_Rho,
                                __global float *field_DRho,
                                __global float *field_T,
                                __global float *field_RH,
                                __global float *field_PBL,
                                __global float *field_Tropo,
                                __global float *field_Ustar,
                                __global float *field_Wstar,
                                __global float *field_Oli,
                                __global float *field_Ps, float z, int i, int j);

/*-------------------------*/
float   interpo2d_f( Factor *f,  __global Grid *g,  __global float* v);
float   interpo3d_f( Factor *f,  __global Grid *g,  __global float* v, int k);

Turb    hanna_f(float z, float h, float ol, float ust, float wst);

XY      xy2ll_f( __global Grid *g, float lon, float lat, float dx, float dy, float zt);

float   hq_f(float P/*[Pa]*/, float T/*[K]*/, float Hum/*[%]*/);

//This needs to be looked at
int     convect_(int *nd, int *nl, float *delt, int *iflag, float *precip, float *wd, float *tprime, float *qprime, float *cbmf, float *ft, float *fq, float *sub, float *fmass, float *tconv, float *qconv, float *qsconv, float *pconv_hpa__, float *phconv_hpa__, int *	nconvtop);
int     tlift_(float *, int *, int *, float *, float *, float *, int *, int *, int *, float *, float *, float *, float *);

int     convmatix(Convect *Cv, const Field *F, int nij, __global Grid *G, __global Parameter *P, __global float *CBMF);
float   field3d_f(Factor *f, __global Grid *g, __global float* v);
float   reflect_f(float z, float zmax);
float   std3d_f(Factor f, __global Grid *g, __global float* v);
float   wvp_f(float T/*[K]*/, float Hum/*[%]*/);



void    dispersion_function(int index,                                
                                int class_np,
                                __global int *class_Idx,
                                __global int *class_Gdx,
                                __global int *class_Pdx,
                                __global float *class_x,
                                __global float *class_y,
                                __global float *class_z,
                                __global float *class_Fg,
                                __global float *class_Up,
                                __global float *class_Vp,
                                __global float *class_Wp,
                                __global float *class_Um,
                                __global float *class_Vm,
                                __global float *class_Wm,
                                __global float *class_Zs,
                                __global float *class_Zb,
        
                                __global Grid *G,
                                int param_ctl,
                                float param_d_strat,
                                float param_d_trop,
                                int param_dispersion,
                                int param_ifine,
                                int param_lconv,
                                int param_ldirect,
                                int param_ldt,
                                int param_lensec,
                                int param_lflx,
                                int param_lsynctime,
                                int param_ng,
                                int param_openCL,
                                int param_openmp,
                                int param_outsec,
                                int param_timing,
                                int param_turbmesoscale,
                                __global float *param_gauss,
        
                                __global float *field_U,
                                __global float *field_V,
                                __global float *field_W,
                                __global float *field_Z,
                                __global float *field_Rho,
                                __global float *field_DRho,
                                __global float *field_T,
                                __global float *field_RH,
                                __global float *field_PBL,
                                __global float *field_Tropo,
                                __global float *field_Ustar,
                                __global float *field_Wstar,
                                __global float *field_Oli,
                                __global float *field_Ps,
                                __global int* int_probe, 
                                __global float* float_probe);




void    trajectory_function(int idx,   __global Class *C, __global Grid *G, __global Parameter *P,  Field *F);
void    convection_function(int index, __global Class *C, __global Grid *G, __global Parameter *P,  Field *F, __global int *Sdx, __global int *Pdx, __global float *CBMF);




/******************************************************************************/
/************************ THE KERNEL ******************************************/
/******************************************************************************/
__kernel void super_kernel( 
                                //which kernel to run 1
                                int kernel_choice,  
                                __global float  *float_probe,
                                __global int    *int_probe,
                                      
                                //for interpolation 84
                                __global float *interp_U,
                                __global float *interp_U_f1,
                                __global float *interp_U_f2,
                                float interp_U_d1,
                                float interp_U_d2,
                                int interp_U_num,
                                __global float *interp_V,
                                __global float *interp_V_f1,
                                __global float *interp_V_f2,
                                float interp_V_d1,
                                float interp_V_d2,
                                int interp_V_num,
                                __global float *interp_W,
                                __global float *interp_W_f1,
                                __global float *interp_W_f2,
                                float interp_W_d1,
                                float interp_W_d2,
                                int interp_W_num,
                                __global float *interp_Z,
                                __global float *interp_Z_f1,
                                __global float *interp_Z_f2,
                                float interp_Z_d1,
                                float interp_Z_d2,
                                int interp_Z_num,
                                __global float *interp_T,
                                __global float *interp_T_f1,
                                __global float *interp_T_f2,
                                float interp_T_d1,
                                float interp_T_d2,
                                int interp_T_num,
                                __global float *interp_RH,
                                __global float *interp_RH_f1,
                                __global float *interp_RH_f2,
                                float interp_RH_d1,
                                float interp_RH_d2,
                                int interp_RH_num,
                                __global float *interp_RHO,
                                __global float *interp_RHO_f1,
                                __global float *interp_RHO_f2,
                                float interp_RHO_d1,
                                float interp_RHO_d2,
                                int interp_RHO_num,
                                __global float *interp_DRHO,
                                __global float *interp_DRHO_f1,
                                __global float *interp_DRHO_f2,
                                float interp_DRHO_d1,
                                float interp_DRHO_d2,
                                int interp_DRHO_num,
                                __global float *interp_PS,
                                __global float *interp_PS_f1,
                                __global float *interp_PS_f2,
                                float interp_PS_d1,
                                float interp_PS_d2,
                                int interp_PS_num,
                                __global float *interp_PBL,
                                __global float *interp_PBL_f1,
                                __global float *interp_PBL_f2,
                                float interp_PBL_d1,
                                float interp_PBL_d2,
                                int interp_PBL_num,
                                __global float *interp_TROPO,
                                __global float *interp_TROPO_f1,
                                __global float *interp_TROPO_f2,
                                float interp_TROPO_d1,
                                float interp_TROPO_d2,
                                int interp_TROPO_num,
                                __global float *interp_USTAR,
                                __global float *interp_USTAR_f1,
                                __global float *interp_USTAR_f2,
                                float interp_USTAR_d1,
                                float interp_USTAR_d2,
                                int interp_USTAR_num,
                                __global float *interp_WSTAR,
                                __global float *interp_WSTAR_f1,
                                __global float *interp_WSTAR_f2,
                                float interp_WSTAR_d1,
                                float interp_WSTAR_d2,
                                int interp_WSTAR_num,
                                __global float *interp_OLI,
                                __global float *interp_OLI_f1,
                                __global float *interp_OLI_f2,
                                float interp_OLI_d1,
                                float interp_OLI_d2,
                                int interp_OLI_num,
                                
                                //the grid 1
                                __global Grid *g,
        
                                //the parameters 3
                                //__global DeviceParameter *param_statics,
                                __global float *param_gauss,
                                __global float *param_rand,
                                int param_ctl,
                                float param_d_strat,
                                float param_d_trop,
                                int param_dispersion,
                                int param_ifine,
                                int param_lconv,
                                int param_ldirect,
                                int param_ldt,
                                int param_lensec,
                                int param_lflx,
                                int param_lsynctime,
                                int param_ng,
                                int param_openCL,
                                int param_openmp,
                                int param_outsec,
                                int param_timing,
                                int param_turbmesoscale,
        
                                //Class Struct 16
                                int class_np,
                                __global int *class_Idx,
                                __global int *class_Gdx,
                                __global int *class_Pdx,
                                __global float *class_x,
                                __global float *class_y,
                                __global float *class_z,
                                __global float *class_Fg,
                                __global float *class_Up,
                                __global float *class_Vp,
                                __global float *class_Wp,
                                __global float *class_Um,
                                __global float *class_Vm,
                                __global float *class_Wm,
                                __global float *class_Zs,
                                __global float *class_Zb,
        
                                //Field struct 14
                                __global float *field_U,
                                __global float *field_V,
                                __global float *field_W,
                                __global float *field_Z,
                                __global float *field_Rho,
                                __global float *field_DRho,
                                __global float *field_T,
                                __global float *field_RH,
                                __global float *field_PBL,
                                __global float *field_Tropo,
                                __global float *field_Ustar,
                                __global float *field_Wstar,
                                __global float *field_Oli,
                                __global float *field_Ps,

                                //For convection 3
                                __global int *conv_pdx,
                                __global int *conv_sdx,
                                __global float *conv_cbmf                                
        ){
    int i = get_global_id(0);
     
        
        
    //int_probe[i] = 74;
    //int_probe[0] = f->rho[0];

    
    //float_probe[0] = field_Rho[0];
    
    switch(kernel_choice){
        case INTERPOLATION_KERNEL:
            if(i < interp_U_num){
                interp_U[i]     = (interp_U_d1 * interp_U_f1[i]) + (interp_U_d2 * interp_U_f2[i]);
                interp_V[i]     = (interp_V_d1 * interp_V_f1[i]) + (interp_V_d2 * interp_V_f2[i]);
                interp_W[i]     = (interp_W_d1 * interp_W_f1[i]) + (interp_W_d2 * interp_W_f2[i]);
                interp_Z[i]     = (interp_Z_d1 * interp_Z_f1[i]) + (interp_Z_d2 * interp_Z_f2[i]);
                interp_T[i]     = (interp_T_d1 * interp_T_f1[i]) + (interp_T_d2 * interp_T_f2[i]);
                interp_RH[i]    = (interp_RH_d1 * interp_RH_f1[i]) + (interp_RH_d2 * interp_RH_f2[i]);
                interp_RHO[i]   = (interp_RHO_d1 * interp_RHO_f1[i]) + (interp_RHO_d2 * interp_RHO_f2[i]);
                interp_DRHO[i]  = (interp_DRHO_d1 * interp_DRHO_f1[i]) + (interp_DRHO_d2 * interp_DRHO_f2[i]);
            }
            //these arrays are smaller than the previous arrays (but the same size)
            if( i < interp_PS_num){
                interp_PS[i]    = (interp_PS_d1 * interp_PS_f1[i]) + (interp_PS_d2 * interp_PS_f2[i]);
                interp_PBL[i]   = (interp_PBL_d1 * interp_PBL_f1[i]) + (interp_PBL_d2 * interp_PBL_f2[i]);
                interp_TROPO[i] = (interp_TROPO_d1 * interp_TROPO_f1[i]) + (interp_TROPO_d2 * interp_TROPO_f2[i]);                
            }
            break;
        case INTERPOLATION_KERNEL_WITH_FLUX:
            if(i < interp_U_num){
                interp_U[i]     = (interp_U_d1 * interp_U_f1[i]) + (interp_U_d2 * interp_U_f2[i]);
                interp_V[i]     = (interp_V_d1 * interp_V_f1[i]) + (interp_V_d2 * interp_V_f2[i]);
                interp_W[i]     = (interp_W_d1 * interp_W_f1[i]) + (interp_W_d2 * interp_W_f2[i]);
                interp_Z[i]     = (interp_Z_d1 * interp_Z_f1[i]) + (interp_Z_d2 * interp_Z_f2[i]);
                interp_T[i]     = (interp_T_d1 * interp_T_f1[i]) + (interp_T_d2 * interp_T_f2[i]);
                interp_RH[i]    = (interp_RH_d1 * interp_RH_f1[i]) + (interp_RH_d2 * interp_RH_f2[i]);
                interp_RHO[i]   = (interp_RHO_d1 * interp_RHO_f1[i]) + (interp_RHO_d2 * interp_RHO_f2[i]);
                interp_DRHO[i]  = (interp_DRHO_d1 * interp_DRHO_f1[i]) + (interp_DRHO_d2 * interp_DRHO_f2[i]);
            }
            //these arrays are smaller than the previous arrays (but the same size)
            if( i < interp_PS_num){
                interp_PS[i]    = (interp_PS_d1 * interp_PS_f1[i]) + (interp_PS_d2 * interp_PS_f2[i]);
                interp_PBL[i]   = (interp_PBL_d1 * interp_PBL_f1[i]) + (interp_PBL_d2 * interp_PBL_f2[i]);
                interp_TROPO[i] = (interp_TROPO_d1 * interp_TROPO_f1[i]) + (interp_TROPO_d2 * interp_TROPO_f2[i]);                

                //Flux
                interp_USTAR[i] = (interp_USTAR_d1 * interp_USTAR_f1[i]) + (interp_USTAR_d2 * interp_USTAR_f2[i]);
                interp_WSTAR[i] = (interp_WSTAR_d1 * interp_WSTAR_f1[i]) + (interp_WSTAR_d2 * interp_WSTAR_f2[i]);
                interp_OLI[i]   = (interp_OLI_d1 * interp_OLI_f1[i]) + (interp_OLI_d2 * interp_OLI_f2[i]);
            }
            break;
        case DISPERSION_KERNEL :
            if( i < class_np){
                
                dispersion_function(        i, 
                                            class_np,
                                            class_Idx,
                                            class_Gdx,
                                            class_Pdx,
                                            class_x,
                                            class_y,
                                            class_z,
                                            class_Fg,
                                            class_Up,
                                            class_Vp,
                                            class_Wp,
                                            class_Um,
                                            class_Vm,
                                            class_Wm,
                                            class_Zs,
                                            class_Zb,
                        
                                            g, 
                                            param_ctl,
                                            param_d_strat,
                                            param_d_trop,
                                            param_dispersion,
                                            param_ifine,
                                            param_lconv,
                                            param_ldirect,
                                            param_ldt,
                                            param_lensec,
                                            param_lflx,
                                            param_lsynctime,
                                            param_ng,
                                            param_openCL,
                                            param_openmp,
                                            param_outsec,
                                            param_timing,
                                            param_turbmesoscale,
                                            param_gauss,
                        
                                            field_U,
                                            field_V,
                                            field_W,
                                            field_Z,
                                            field_Rho,
                                            field_DRho,
                                            field_T,
                                            field_RH,
                                            field_PBL,
                                            field_Tropo,
                                            field_Ustar,
                                            field_Wstar,
                                            field_Oli,
                                            field_Ps,
                                                int_probe, float_probe);
             
            }
            break;
        case TRAJECTORY_KERNEL :
            //trajectory_function(i, c, g, p, f);
            break;
        case CONVECTION_KERNEL :
            //convection_function(i, c, g, p, f, conv_sdx, conv_pdx, conv_cbmf);
            break;
    }
}
/******************************************************************************/
/********************* FUNCTIONS DEFINTIONS ***********************************/
/******************************************************************************/
/*----------------------------------------------------------------------------*/
//this is also used for dispersion, so can possibly be optimised
void    dispersion_function(int idx,                                
                                int class_np,
                                __global int *class_Idx,
                                __global int *class_Gdx,
                                __global int *class_Pdx,
                                __global float *class_X,
                                __global float *class_Y,
                                __global float *class_Z,
                                __global float *class_Fg,
                                __global float *class_Up,
                                __global float *class_Vp,
                                __global float *class_Wp,
                                __global float *class_Um,
                                __global float *class_Vm,
                                __global float *class_Wm,
                                __global float *class_Zs,
                                __global float *class_Zb,
        
                                __global Grid *G,
                                int param_ctl,
                                float param_d_strat,
                                float param_d_trop,
                                int param_dispersion,
                                int param_ifine,
                                int param_lconv,
                                int param_ldirect,
                                int param_ldt,
                                int param_lensec,
                                int param_lflx,
                                int param_lsynctime,
                                int param_ng,
                                int param_openCL,
                                int param_openmp,
                                int param_outsec,
                                int param_timing,
                                int param_turbmesoscale,
                                __global float *param_Gauss,
        
                                __global float *field_U,
                                __global float *field_V,
                                __global float *field_W,
                                __global float *field_Z,
                                __global float *field_Rho,
                                __global float *field_DRho,
                                __global float *field_T,
                                __global float *field_RH,
                                __global float *field_PBL,
                                __global float *field_Tropo,
                                __global float *field_Ustar,
                                __global float *field_Wstar,
                                __global float *field_Oli,
                                __global float *field_Ps,
                                __global int* int_probe, 
                                __global float* float_probe){
        
    
    
        int k   = class_Idx[idx] % param_ng;
	if (k<0) k=0;

// particle position
	float x0 = class_X[idx];
	float y0 = class_Y[idx];
	float z0 = class_Z[idx];
	float xt = x0;
	float yt = y0;
	float zt = z0;
        
// turbulent wind components
	float up = class_Up[idx];          //along wind ??
	float vp = class_Vp[idx];          //cross wind??
	float wp = class_Wp[idx];          //vertical comp of turbulance ???

// time steps
	float dt  = param_ldt;             //Max time step for integration
	float dtf = dt/param_ifine;        //Number of fine itterations allowed per time step
	int   sdt = param_ldt;

// interpolation factors
	Factor fc = index_f(G, 
                field_U,
                field_V,
                field_W,
                field_Z,
                field_Rho,
                field_DRho,
                field_T,
                field_RH,
                field_PBL,
                field_Tropo,
                field_Ustar,
                field_Wstar,
                field_Oli,
                field_Ps, 
                xt, yt, zt);
        //should/can this be check prior to deployment???
	if (fc.i1 < 0 || fc.j1 < 0){
            return;
        }

	int     ldt;
	float   u, v, w;
	float   cosphi, sinphi;
	float   r, zsfc, zpbl, ztop, trop;
	float   dx  = 0.0f,
                dy  = 0.0f,
                d_along_wind = 0.0f,                    //delta along wind
                d_cross_wind = 0.0f;                    //delta cross wind
	XY      xy;

//for the specified time
	for (ldt=0; ldt < param_lsynctime; ldt += sdt) {
// boundaries
		zsfc = interpo3d_f(&fc, G, field_Z, 0);
		ztop = interpo3d_f(&fc, G, field_Z, G->nl-1) - zsfc;
		zpbl = interpo2d_f(&fc, G, field_PBL);
		trop = interpo2d_f(&fc, G, field_Tropo);

                
// winds
		u = field3d_f(&fc, G, field_U);
		v = field3d_f(&fc, G, field_V);
		w = field3d_f(&fc, G, field_W);
		
		if (param_ctl > 0) {
// re-estimate time step for integration
			sdt     = (int) (zpbl/(1.e-10+param_ctl*fabs(w)));
                        
			if (sdt<1)                      sdt = 1;
			if (sdt>param_ldt)                 sdt = param_ldt;
			if (sdt>param_lsynctime-ldt)       sdt = param_lsynctime - ldt;
                        
			dt      = sdt;
			dtf     = sdt/param_ifine;
		}

		dx += u * dt * param_ldirect;
		dy += v * dt * param_ldirect;
		zt += w * dt * param_ldirect;

		if (zt >= zpbl) {
// z goes out of PBL, complete the current interval above PBL.
			ldt += sdt;
			break;
		}

		if (zt < 0.0f) zt = -zt;

// turbulent components
		float oli                       = interpo2d_f(&fc, G, field_Oli);
		float friction_velocity         = interpo2d_f(&fc, G, field_Ustar);
		float convection_velocity       = interpo2d_f(&fc, G, field_Wstar);
		float rhoaux                    = field3d_f(&fc, G, field_DRho) / (1.e-10f + field3d_f(&fc, G, field_Rho));

		Turb tb = hanna_f(zt, zpbl, oli, friction_velocity, convection_velocity);

// Langevin horizontal wind component
		r = dt/tb.tlu;
                int ndx_gauss; 
		if (r < 0.5f) {
                    ndx_gauss = ++k % param_ng;
                    up = (1.0f-r) * up + param_Gauss[ndx_gauss] * tb.sigu * sqrt(2.0f * r);
		}
		else {
                    r  = exp(-r);
                    up = r * up + param_Gauss[++k % param_ng] * tb.sigu * sqrt(1.0f-r * r);
		}

		r = dt / tb.tlv;
		if (r < 0.5f) {
			vp = (1.0f-r) * vp + param_Gauss[++k % param_ng] * tb.sigv * sqrt(2.0f * r);
		}
		else {
			r  = exp(-r);
			vp = r * vp + param_Gauss[++k % param_ng] * tb.sigv * sqrt(1.0f-r * r);
		}

		d_along_wind += up * dt;
		d_cross_wind += vp * dt;

// Langevin vertical wind component
		float cbt = class_Fg[idx];
		int ifine;

                //can we skip the first loop?
		for (ifine=0; ifine < param_ifine; ifine++) {

			if (ifine > 0) 
                            tb = hanna_f(zt, zpbl, oli, friction_velocity, convection_velocity);

			r = dtf/tb.tlw;
			if (r < 0.5f) {
				wp = cbt * ((1.0f-r) * wp + param_Gauss[++k % param_ng] * sqrt(2.f * r) + dtf * (tb.dsigwdz + rhoaux * tb.sigw));
			}
			else {
				r = exp(-r);
				wp = cbt * (r * wp + param_Gauss[++k % param_ng] * sqrt(1.f - r * r ) + tb.tlw * (1.f - r) * (tb.dsigwdz + rhoaux * tb.sigw));
			}

			zt += wp * tb.sigw * dtf;

			if (zt<0.f || zt>zpbl) {
				cbt=-1.0f;
			}
			else {
				cbt=1.0f;
			}

			zt = reflect_f(zt,zpbl);
		}

                //set the reflection flag?
		class_Fg[idx] = cbt;

		fc=index_f(G,field_U,
                            field_V,
                            field_W,
                            field_Z,
                            field_Rho,
                            field_DRho,
                            field_T,
                            field_RH,
                            field_PBL,
                            field_Tropo,
                            field_Ustar,
                            field_Wstar,
                            field_Oli,
                            field_Ps,xt,yt,zt);

                 //should this be check prior to deployment???
		if (fc.i1<0 || fc.j1<0) {
			class_X[idx]=x0;
			class_Y[idx]=y0;
			class_Z[idx]=z0;
                        
			return;
		}
                
	}


// save random disturbances

	class_Up[idx]=up;
	class_Vp[idx]=vp;
	class_Wp[idx]=wp;

// convert along/cross wind dispplacement to dx and dy

	r       = max(1.e-5f, sqrt(dx*dx+dy*dy));
	cosphi  = dx/r;
	sinphi  = dy/r;

	dx += cosphi * d_along_wind - sinphi * d_cross_wind;
	dy += sinphi * d_along_wind + cosphi * d_cross_wind;

// advance to next sync time for particle outside mixing layer

	if (ldt < param_lsynctime) {
		dt=param_lsynctime-ldt;

		if (zt<trop) {
			r       = sqrt(2.f *param_d_trop/dt);
			up      = param_Gauss[++k % param_ng] * r;
			vp      = param_Gauss[++k % param_ng] * r;
//			wp=0.0f;
			// different from flexpart: vertical perturbation even in troposphere.
			r       = sqrt(2. * param_d_trop * 0.01/dt);
			wp      = param_Gauss[++k % param_ng] * r;
		}
		else if (zt < trop + 1000.0f) {
			float weight    = (zt-trop)/1000.0f;
			r               = sqrt(2.f * param_d_trop * (1.-weight) /dt);
			up              = param_Gauss[++k % param_ng] * r;
			vp              = param_Gauss[++k % param_ng] * r;
//			r=sqrt(2.*param_d_strat*weight/dt);
			// different from flexpart.
			r               = sqrt(2.f * param_d_strat*weight/dt)+sqrt(2.*param_d_trop*0.01*(1.-weight)/dt);
			wp              = param_Gauss[++k % param_ng] * r;
		}
		else {
			up      = 0.0f;
			vp      = 0.0f;
			r       = sqrt(2.*param_d_strat/dt);
			wp      = param_Gauss[++k % param_ng] * r;
		}

		dx += (u+up) * dt * param_ldirect;
		dy += (v+vp) * dt * param_ldirect;
		zt += (w+wp) * dt * param_ldirect;

	}
    
           
// add mesoscale random disturbances

	r               = exp(-2.f*param_lsynctime/G->ns);
	float rs        = sqrt(1.-r*r);

	float sig       = std3d_f(fc,G,field_U); 
	class_Um[idx]   = r * class_Um[idx] + rs * param_Gauss[++k % param_ng] * sig * param_turbmesoscale;

	sig             = std3d_f(fc,G,field_V);
	class_Vm[idx]   = r * class_Vm[idx] + rs * param_Gauss[++k % param_ng] * sig * param_turbmesoscale;

	sig             = std3d_f(fc,G,field_W);
	class_Wm[idx]   = r * class_Wm[idx] + rs * param_Gauss[++k % param_ng] * sig * param_turbmesoscale;
	
	dx             += class_Um[idx] * param_lsynctime * param_ldirect;
	dy             += class_Vm[idx] * param_lsynctime * param_ldirect;
	zt             += class_Wm[idx] * param_lsynctime * param_ldirect;

 
	zt              = reflect_f(zt,ztop);

	xy              = xy2ll_f(G, xt, yt, dx, dy, zt + zsfc);
	xt              = xy.x;
	yt              = xy.y;

// save coordinates
   
	class_X[idx]   = xt;
	class_Y[idx]   = yt;
	class_Z[idx]   = zt;
 	class_Zs[idx]  = zsfc;
	class_Zb[idx]  = zpbl;
	class_Idx[idx] = k;
                       
// update grid index
   
	if (param_lconv != 0) {
                int_probe[idx] = idx;
                
		int i = (int) ((yt + 0.5 * G->dy - G->y0) / G->dy);
		int j = (int) ((xt + 0.5 * G->dx - G->x0) / G->dx);
                
#if defined(__FLEX__) || 1
		if (i<0 || j<0){
                   // class_Gdx[idx] = 64;
                    return;//TODO SHOULD CHECK BEFORE KERENL LAUNH errmsg("Bad grid index!");
                } 
#endif
		if (i>=G->nr) i=G->nr-1;
		if (j>=G->nc) j=G->nc-1;
                
		class_Gdx[idx] = i * G->nc + j;
                
	}
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/** Advances one synchronize time step for trjaectory calculation using mean wind fields.
 @param C Class object.
 @param idx Particle index.
 @param G Grid object.
 @param P Parameter object.
 @param F Field obejct.
 */
/*
void trajectory_function(int idx, __global Class *C, __global Grid *G,  __global Parameter *P,  Field *F)
{
    float dt=P->ldt;
    int sdt=P->ldt;

    float x0=C->X[idx];
    float y0=C->Y[idx];
    float z0=C->Z[idx];

    for (int ldt=0; ldt<P->lsynctime; ldt+=sdt) {
// first guess, save winds.
        float xt=C->X[idx];
        float yt=C->Y[idx];
        float zt=C->Z[idx];
        
        Factor fc=index_f(G,F,xt,yt,zt);

        //can i ditch this???
        if (fc.i1<0 || fc.j1<0) {
            C->X[idx]=x0;
            C->Y[idx]=y0;
            C->Z[idx]=z0;
            return;
        }

        float zs=interpo3d_f(&fc,G,F->Z,0);
        float zb=interpo2d_f(&fc,G,F->PBL);
        float ztop=interpo3d_f(&fc,G,F->Z,G->nl-1)-zs;

        float u=field3d_f(&fc,G,F->U);
        float v=field3d_f(&fc,G,F->V);
        float w=field3d_f(&fc,G,F->W);

        if (P->ctl>0) {
// re-estimate time step for integration
            sdt = (int) (zb/(1.e-10+P->ctl*fabs(w)));
            if (sdt<1) sdt=1;
            if (sdt>P->ldt) sdt=P->ldt;
            if (sdt>P->lsynctime-ldt) sdt=P->lsynctime-ldt;
            dt=sdt;
        }
        
        XY xy=xy2ll_f(G,xt,yt,dt*u*P->ldirect,dt*v*P->ldirect,zs+zt);
        float ze=reflect_f(zt+dt*w*P->ldirect,ztop);

// final position
        fc=index_f(G,F,xy.x,xy.y,ze);

        //can i dtich this????
        if (fc.i1<0 || fc.j1<0) {
            C->X[idx]=x0;
            C->Y[idx]=y0;
            C->Z[idx]=z0;
            return;
        }

        u=0.5*(u+field3d_f(&fc,G,F->U));
        v=0.5*(v+field3d_f(&fc,G,F->V));
        w=0.5*(w+field3d_f(&fc,G,F->W));

        xy=xy2ll_f(G,xt,yt,dt*u*P->ldirect,dt*v*P->ldirect,zs+zt);
        zt=reflect_f(zt+dt*w*P->ldirect,ztop);

        C->X[idx]=xy.x;
        C->Y[idx]=xy.y;
        C->Z[idx]=zt;
        C->Zs[idx]=interpo3d_f(&fc,G,F->Z,0);
        C->Zb[idx]=interpo2d_f(&fc,G,F->PBL);
    }
}
 * */
/*----------------------------------------------------------------------------*/
void    convection_function(    int is, 
                                __global Class *C, 
                                __global Grid *G, 
                                __global Parameter *P,  
                                Field *F,       
                                __global int *Sdx,
                                __global int *Pdx,
                                __global float *CBMF){
    Convect Cv;

    int n1      = Sdx[is - 1];
    int n2      = Sdx[is];
    int nij     = C->Gdx[n1];
    float ztop  = F->Z[nij + (G->nl - 1) * G->nxy];

    int ncv     = convmatix(&Cv, F, nij, G, P, CBMF);

    //By this point we have used : SDX, 
    if (ncv > 0) {
// convection occurs
            int i, k, id, lev;
            for (i = n1; i < n2; i++) {
                    id          = Pdx[i];
                    float zt    = C->Z[id];
                    
                    if (zt >= Cv.ZIJ[ncv - 1]) continue;
                    
// inside convection level, find zt level.
                    for (lev = 1; lev < ncv; lev++) {
                            if (Cv.ZIJ[lev] > zt) {
                                    lev--;
                                    break;
                            }
                    }
                    
// relocate particle; refer to redist.f of flexpart.
                    bool relocated      = false;
                    float rn            = P->Rand[C->Idx[id] % P->ng];
                    
                    if (Cv.FSUM[lev] > 1.e-20) {
                            float sum = 0.0;
                            for (k=0; k < ncv; k++) {
                                    if (P->ldirect > 0) {
                                            sum += Cv.FRAC[lev * NA + k]/Cv.MASS[lev];
                                    }
                                    else {
                                            sum += Cv.FRAC[k * NA + lev]/Cv.MASS[lev];
                                    }
                                    if (sum >= rn) {
                                            zt          = Cv.ZIJ[k] + rn * (Cv.ZIJ[k + 1] - Cv.ZIJ[k]);
                                            relocated   = true;
                                            break;
                                    }
                            }
                    }
                    if (!relocated) {
                            float dz = Cv.ZIJ[lev + 1] - Cv.ZIJ[lev];
                            float fb = fabs((zt - Cv.ZIJ[lev]) / dz);
                            float fa = fabs((Cv.ZIJ[lev + 1] - zt) / dz);
                            zt      += rn * P->lsynctime * (fa * Cv.SUB[lev] + fa * Cv.SUB[lev + 1]);
                    }
                    
                    if (zt < 0.0) zt = -zt;
                    if (zt > ztop)  zt = ztop - fmod(zt, ztop);
                    C->Z[id] = zt;
            }
    }
}
/*----------------------------------------------------------------------------*/
int convmatix(Convect *Cv, const Field *F, int nij, __global Grid *G, __global Parameter *P, __global float *CBMF)
{
	int i, k, k0, m, ncv, nd, nl, iflag;
	float t[NA], q[NA], qs[NA], p[NA], ph[NA];
	
        //TODO : check before kernel launch
	//if (G->nl>=NA) errmsg("nl>=NA");

	float ps        = F->Ps[nij] - 10.0f;
	float zs        = F->Z[nij];
	float cbmf      = CBMF[nij];
	float delt      = P->lsynctime;

// find the first level above surface

	for(k0 = 1; k0 < G->nl; k0++) {
		if (G->P[k0] < ps) break;
	}
        
        //TODO : check before kernel launch
	//if (k0>=G->nl) errmsg("k0>=G->nl");

// surface level

	ps              = F->Ps[nij];
	t[0]            = F->T[nij];
	q[0]            = hq_f(ps, t[0], F->RH[nij]);
	qs[0]           = hq_f(ps, t[0], 100.0f);
	p[0]            = 0.01 * ps;					// Pa to hPa
	ph[0]           = p[0] + 1.0f;
	Cv->ZIJ[0]      = 0.0;

	for (k=k0; k<G->nl; k++) {
            m               = k * G->nxy + nij;
            i               = k - k0 + 1;
            t[i]            = F->T[m];
            q[i]            = hq_f(G->P[k], t[i], F->RH[m]);
            qs[i]           = hq_f(G->P[k], t[i], 100.0f);
            p[i]            = 0.01 * G->P[k];
            ph[i]           = 0.5 * (p[i] + p[i-1]);
            Cv->ZIJ[i]      = F->Z[m] - zs;
            Cv->MASS[i-1]   = (ph[i-1] - ph[i]) * 100.0/9.81;		// dp=-rho*g*dz -> p in Pa, mass in kg/m2. 
	}

	ph[i+1]         = 0.99 * p[i];
	Cv->MASS[i]     = (ph[i] - ph[i+1]) * 100.0/9.81;
	nd              = i + 1;
	nl              = nd - 1;
	ncv             = 0;

// calculate convective level

	float precip, wd, tprime, qprime;
	float ft[NA], fq[NA]; 

	convect_(       &nd, 
                        &nl,
                        &delt,
                        &iflag,
                        &precip,
                        &wd,
                        &tprime,
                        &qprime,
                        &cbmf,
                        ft,
                        fq,
                        Cv->SUB,
                        Cv->FMASS,
                        t,
                        q,
                        qs,
                        p,
                        ph,
                        &ncv);

	CBMF[nij] = cbmf;

	if (iflag!=1 || ncv<1) return 0;

        //TODO : check before kernel launch
	//if (ncv>nd) errmsg("ncv > nd");

// FRAC(i,j) is mass flux from i to j in FORTRAN, but j to i in C.

        for (i = 0 ; i < (NA * NA) ; i++){
            Cv->FRAC[i] = 0;
        }
	//memset(Cv->FRAC, 0, NA * NA * sizeof(float));

	for (i = 0; i < ncv; i++) {
		Cv->FSUM[i] = 0.0;
		for (k = 0; k < ncv; k++) {
			m = i * NA + k;
// mass flux to mass displacement from i to j
			Cv->FRAC[m]  = delt * Cv->FMASS[k * NA + i];
			Cv->FSUM[i] += Cv->FRAC[m];
		}
		Cv->FRAC[i*NA+i] += Cv->MASS[i] - Cv->FSUM[i];
		Cv->SUB[i]       *= -287.06*t[i] / (p[i] * 100.0);
	}
	return ncv;
}
/*----------------------------------------------------------------------------*/
/** Finds x-y grid indices and calculates interpolation factors (z refer to surface)
 @param g Grid object.
 @param f Field object.
 @param x Longitude.
 @param y Latitude.
 @param z Altitude.
 */

Factor index_f( __global Grid *g, __global float *field_U,
                                __global float *field_V,
                                __global float *field_W,
                                __global float *field_Z,
                                __global float *field_Rho,
                                __global float *field_DRho,
                                __global float *field_T,
                                __global float *field_RH,
                                __global float *field_PBL,
                                __global float *field_Tropo,
                                __global float *field_Ustar,
                                __global float *field_Wstar,
                                __global float *field_Oli,
                                __global float *field_Ps, float x, float y, float z)
{
	Factor fc, f2;

	fc.j1= (int) ((x-g->x0)/g->dx);
	fc.j2=fc.j1+1;
	
	if (fc.j1<0 || fc.j2>=g->nc) {
		// x goes out of range
		if (!g->gl) {
			// for non-global grid, return negative index.
			fc.i1=-1;
			fc.j1=-1;
			return fc;
		}
		// it must be embraced by the two end grids along x-axis.
		fc.j1=g->nc-1;
		fc.j2=0;
	}

	float r=fabs((x-(g->x0+fc.j1*g->dx))/g->dx);

	fc.i1= (int) ((y-g->y0)/g->dy);
	fc.i2=fc.i1+1;

	if (fc.i1<0 || fc.i2>=g->nr) {
		if (!g->gl) {
			fc.i1=-1;
			fc.j1=-1;
			return fc;
		}
#ifdef __FLEX__
		errmsg("i-index problem");
#endif
		fc.i1=0;
		fc.i2=1;
	}

	float s=fabs((y-(g->y0+fc.i1*g->dy))/g->dy);

	fc.f1=(1.0f-r)*(1.0f-s);
	fc.f2=r*(1.0f-s);
	fc.f3=r*s;
	fc.f4=(1.0f-r)*s;

// vertical indexes and factors

	f2=zindex_f(g,field_U,
field_V,
field_W,
field_Z,
field_Rho,
field_DRho,
field_T,
field_RH,
field_PBL,
field_Tropo,
field_Ustar,
field_Wstar,
field_Oli,
field_Ps,z,fc.i1,fc.j1);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k1=f2.k1;
	fc.fa1=f2.fa1;
	fc.fb1=f2.fb1;

	f2=zindex_f(g,field_U,
field_V,
field_W,
field_Z,
field_Rho,
field_DRho,
field_T,
field_RH,
field_PBL,
field_Tropo,
field_Ustar,
field_Wstar,
field_Oli,
field_Ps,z,fc.i1,fc.j2);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k2=f2.k1;
	fc.fa2=f2.fa1;
	fc.fb2=f2.fb1;

	f2=zindex_f(g,field_U,
field_V,
field_W,
field_Z,
field_Rho,
field_DRho,
field_T,
field_RH,
field_PBL,
field_Tropo,
field_Ustar,
field_Wstar,
field_Oli,
field_Ps,z,fc.i2,fc.j2);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k3=f2.k1;
	fc.fa3=f2.fa1;
	fc.fb3=f2.fb1;

	f2=zindex_f(g,field_U,
field_V,
field_W,
field_Z,
field_Rho,
field_DRho,
field_T,
field_RH,
field_PBL,
field_Tropo,
field_Ustar,
field_Wstar,
field_Oli,
field_Ps,z,fc.i2,fc.j1);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k4=f2.k1;
	fc.fa4=f2.fa1;
	fc.fb4=f2.fb1;

	return fc;
}
/*----------------------------------------------------------------------------*/
/** Interpolates 2D field data
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 */

float interpo2d_f( Factor *f,  __global Grid *g, __global float* v)
{
	int n1  = f->i1 * g->nc;
	int n2  = n1 + g->nc;
	float d = f->f1 * v[n1 + f->j1] +
                  f->f2 * v[n1 + f->j2]+
                  f->f3 * v[n2 + f->j2]+
                  f->f4 * v[n2 + f->j1];
        
	return d;
}
/*----------------------------------------------------------------------------*/
/** Interpolates 3D field data on a level.
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 @param k Level index of v.
 */

float interpo3d_f( Factor *f, __global Grid *g, __global float* v, int k)
{
	int n1  = k * g->nxy + f->i1 * g->nc;
	int n2  = n1 + g->nc;
        
	float d = f->f1 * v[n1 + f->j1] +
                  f->f2 * v[n1 + f->j2] +
		  f->f3 * v[n2 + f->j2] +
		  f->f4 * v[n2 + f->j1];
	return d;
}
/*----------------------------------------------------------------------------*/
//! Hanna function. Refer to hanna.f of flexpart6.4-gfs version
/*
 * Parameterisation of wind fluctuations - Section 7.4 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
 * the acronyms are explained in that doc
 */
Turb hanna_f(float z, float h, float L, float ustar, float wstar)
{
	Turb t;

	if ((h / fabs(L)) < 1.0) {
// Neutral conditions
            ustar               = max(ustar, 1.e-4f);
            float corr          = z/ustar;
            
            //eqn 26 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
            t.sigu              = 1.e-2f + 2.0f * ustar * exp(-3.e-4f * corr);
            
            //eqn 27 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
            t.sigw              = 1.3f * ustar * exp(-2.e-4f * corr);
            t.dsigwdz           = -2.e-4f * t.sigw;
            t.sigw              = t.sigw + 1.e-2f;
            t.sigv              = t.sigw;
            
            //eqn 28 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
            t.tlu               = (0.5f * z/t.sigw) / (1.0f + 1.5e-3f * corr);
            t.tlv               = t.tlu;
            t.tlw               = t.tlu;
	}
	else if (L < 0.0f) {
// Unstable conditions
		float zeta      = z/h;
                
                //??????
		t.sigu          = 1.e-2f + ustar * pow(12.0f - 0.5f * h/L, 0.33333f);
		t.sigv          = t.sigu;
                
                //eqn 22 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
		t.sigw          = sqrt(1.2f * wstar * wstar * (1.0f - 0.9f * zeta) * pow(zeta, 0.66666f) + (1.8f - 1.4f * zeta) * ustar * ustar) + 1.e-2f;
		t.dsigwdz       = 0.5f/t.sigw/h * (-1.4f * ustar * ustar + wstar * wstar * (0.8f * pow(max(zeta, 1.e-3f), -.33333f) - 1.8f * pow(zeta, 0.66666f)));
                
                //eqn 21 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
		t.tlu           = 0.15f * h/t.sigu;
                t.tlv           = t.tlu;
                
		if (z < fabs(L)){
                    //eqn 23 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
                    t.tlw   = 0.1f * z/(t.sigw * (0.55f - 0.38f * fabs(z/L)));
                }
		else if (zeta<0.1f){
                    //eqn 24 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
                    t.tlw   = 0.59f * z/t.sigw;
                }
		else{
                    //eqn 25 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
                    t.tlw   = 0.15f * h/t.sigw * (1.0f - exp(-5.0f * zeta)); 
                }
	}
	else {
// Stable conditions
		float zeta=z/h;
                
                //eqn 29 - 33 (http://zardoz.nilu.no/~flexpart/flexpart/flexpart82.pdf)
		t.sigu          = 1.e-2f + 2.0f * ustar * (1.0f - zeta);
		t.sigv          = 1.e-2f + 1.3f * ustar * (1.0f - zeta);
		t.sigw          = t.sigv;
		t.dsigwdz       = -1.3f * ustar/h;
		t.tlu           = 0.15f * h/t.sigu * sqrt(zeta);
		t.tlv           = 0.467f * t.tlu;               //??
		t.tlw           = 0.1f * h/t.sigw * pow(zeta, 0.8f);
	}

	t.tlu = max(10.0f, t.tlu);
	t.tlv = max(10.0f, t.tlv);
	t.tlw = max(30.0f, t.tlw);
	
	if (t.dsigwdz == 0.0f) t.dsigwdz = 1.e-10f;

        //CAN'T RETURN T!
	return t;
}
/*----------------------------------------------------------------------------*/
/** Converts distance change in m to degree.
 @param g Grid object.
 @param lon Longitude [deg]
 @param lat Latitude [deg]
 @param dx Est-west displacement [m]
 @oaram dy North-south displacement [m]
 @param zt Altitude [m]
 */

XY xy2ll_f( __global Grid *g, float lon, float lat, float dx, float dy, float zt)
{
	 float R2D=57.2957795f;		// radius to degree 180/pi;
	 float D2R=0.0174532925f;		// radius to degree pi/180;

	float a=lon*D2R,
		  b=lat*D2R,
		  re=6378137.0f+zt,				// earth radius (m)
		  r=re*cos(b),
		  x, y;

	if (lat>75.0f) {
		// point near north pole
		x=r*cos(a)-dx*sin(a)-dy*cos(a);
		y=r*sin(a)+dx*cos(a)-dy*sin(a);
		lon=R2D*atan2(y,x);
		lat=R2D*acos(sqrt(x*x+y*y)/re);
	} else if (lat<-75.0f) {
		// point near south pole
		x=r*cos(a)-dx*sin(a)+dy*cos(a);
		y=r*sin(a)+dx*cos(a)+dy*sin(a);
		lon=R2D*atan2(y,x);
		lat=-R2D*acos(sqrt(x*x+y*y)/re);
	} else {
		lon+=R2D*dx/r;
		lat+=R2D*dy/re;
	}

	if (lat>90.0f) {
		lat=180.0f-lat;
		lon+=180.0f;
	}
	else if (lat<-90.0f) {
		lat=-(180.0f+lat);
		lon+=180.0f;
	}

	if (g->gl) {
		if (lon<g->x0) {
			lon+=360.0f;
		}
		else if (lon>g->x1+g->dx) {
			lon-=360.0f;
		}
	}

	XY xy={lon,lat};

	return xy;
}
/*----------------------------------------------------------------------------*/
/** Calculate vertical interpolation factors.
 @param g Grid object.
 @param f Field object.
 @param z Altitude.
 @param i Field row index.
 @param j Field column index.
 */

Factor zindex_f( __global Grid *g, __global float *field_U,
                                __global float *field_V,
                                __global float *field_W,
                                __global float *field_Z,
                                __global float *field_Rho,
                                __global float *field_DRho,
                                __global float *field_T,
                                __global float *field_RH,
                                __global float *field_PBL,
                                __global float *field_Tropo,
                                __global float *field_Ustar,
                                __global float *field_Wstar,
                                __global float *field_Oli,
                                __global float *field_Ps, float z, int i, int j)
{
	// todo: implementation for hybrid or sigma vertical grid.

	int k, n, nxy;
	float z1, z2, dz;
	Factor fc;

	if (z<0.0f) {
#ifdef __FLEX__
	errmsg("z<0 problem");
#else
		z=-z;
#endif
	}

	n       = i * g->nc + j;
	nxy     = g->nxy;
	z1      = field_Z[n];
	z       += z1 + 1.0f;
	
	for (k=1 ; k < g->nl ; k++) {
		z2 = field_Z[n + k * nxy];
		if (z2 > z) {
			dz      = z2-z1;
			fc.k1   = k-1;
			fc.fa1  = fabs((z2-z)/dz);
			fc.fb1  = fabs((z1-z)/dz);
			return fc;
		}
		z1 = z2;
	}

	if (g->gl || z-z2<100.0f) {
		fc.k1=g->nl-2;
		fc.fa1=0.0;
		fc.fb1=1.0;
	}
	else {
		fc.k1=-1;
		fc.fa1=1.0;
		fc.fb1=0.0;
	}

	return fc;
}
/*----------------------------------------------------------------------------*/
/** Interpolate 3D field data
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 */

float field3d_f(Factor *f, __global Grid *g, __global float* v)
{
	int n1, n2, nxy;
	float v1, v2, v3, v4;

	nxy=g->nxy;
	
	n1=f->k1*nxy+f->i1*g->nc+f->j1;
	n2=n1+nxy;
	v1=f->fa1*v[n1]+f->fb1*v[n2];
	
	n1=f->k2*nxy+f->i1*g->nc+f->j2;
	n2=n1+nxy;
	v2=f->fa2*v[n1]+f->fb2*v[n2];

	n1=f->k3*nxy+f->i2*g->nc+f->j2;
	n2=n1+nxy;
	v3=f->fa3*v[n1]+f->fb3*v[n2];

	n1=f->k4*nxy+f->i2*g->nc+f->j1;
	n2=n1+nxy;
	v4=f->fa4*v[n1]+f->fb4*v[n2];

	return f->f1*v1+f->f2*v2+f->f3*v3+f->f4*v4;
}
/*----------------------------------------------------------------------------*/
/** Reflect excessive altitude displacement on boundaries (z refer to the surface)
 * 
 * fmod is not compiling with Apple amd, hence the shamefull code
 @param z Altitude.
 @param zmax Altitude maximum.
 */

float reflect_f(float z, float zmax)
{
	if (z < 0.0f) z = -z;
        
	if (z > zmax) {
		//z = fmod(z, zmax);
                while( (z - (z/zmax)) > 0){
                    z = z - (z/zmax);
                }
		z = zmax - z;
	}
	return z;
}
/*----------------------------------------------------------------------------*/
/** Calculates standard deviation for a grid box.
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to 3D field data.
 */

float std3d_f(Factor f, __global Grid *g, __global float* v)
{
	int n1, n2, nxy;
	float vm, v1, v2, v3, v4, v5, v6, v7, v8;

	nxy = g->nxy;

	n1 = f.k1 * nxy + f.i1 * g->nc + f.j1;
	n2 = n1 + nxy;
	v1 = v[n1];
	v2 = v[n2];

	n1 = f.k2 * nxy + f.i1 * g->nc + f.j2;
	n2 = n1 + nxy;
	v3 = v[n1];
	v4 = v[n2];

	n1 = f.k3 * nxy + f.i2 * g->nc + f.j2;
	n2 = n1 + nxy;
	v5 = v[n1];
	v6 = v[n2];

	n1 = f.k4 * nxy + f.i2 * g->nc + f.j1;
	n2 = n1 + nxy;
	v7 = v[n1];
	v8 = v[n2];

	vm = (v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8)/8.0f;

	v1 -= vm;
	v2 -= vm;
	v3 -= vm;
	v4 -= vm;
	v5 -= vm;
	v6 -= vm;
	v7 -= vm;
	v8 -= vm;

	return 0.0;//sqrt((v1*v1 + v2*v2 + v3*v3 + v4*v4 + v5*v5 + v6*v6 + v7*v7 + v8*v8)/8.0f);
}
/*----------------------------------------------------------------------------*/
/** Calculates specific humidity
 @param P Air pressure [Pa]
 @param T Air temperature [K]
 @param Hum Relative humidity [%]
 <br>
  Ref: http://en.wikipedia.org/wiki/Humidity
*/
float hq_f(float P/*[Pa]*/, float T/*[K]*/, float Hum/*[%]*/)
{
	float pw=wvp_f(T,Hum);
	// mixing ratio
	float mr=0.62197*pw/(P-pw);
	return mr/(1.0+mr);
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/** Calculates water vapor pressure [Pa]
 @param T Air temperature [K]
 @param Hum Relative humidity [%]
 <br>
 Ref:  http://en.wikipedia.org/wiki/Density_of_air
*/

float wvp_f(float T/*[K]*/, float Hum/*[%]*/)
{
	// saturated water vapor pressure [Pa]
	float pw=610.78*pow(10.0,(7.5*T-2048.625)/(T-35.85));
	// water vapor pressure
	return (float) (pw*Hum/100.0);
}
/* --------------------------------------------------------------------------- */
int tlift_(float *gz, int *icb, int *nk, float *tvp, float *tpk, float *clw, int *nd, int *nl, int *kk, float *qconv, float *tconv, float *qsconv, float *pconv_hpa__)
{
    /* System generated locals */
    int i__1;
    float r__1, r__2;

    /* Local variables */
    int i__, j;
    float s, es, qg, rg, tc, tg, ah0, ahg;
    int nsb;
    float alv, cpp;
    int nst;
    float denom, cpinv;


/* -cv */
/*        include 'includepar' */
/*        include 'includeconv' */
/* -cv */
/* ====>Begin Module TLIFT      File convect.f      Undeclared variables */

/*     Argument variables */


/*     Local variables */



/* ====>End Module   TLIFT      File convect.f */

/*   ***   ASSIGN VALUES OF THERMODYNAMIC CONSTANTS     *** */



/*   ***  CALCULATE CERTAIN PARCEL QUANTITIES, INCLUDING STATIC ENERGY   *** */

    /* Parameter adjustments */
    --pconv_hpa__;
    --qsconv;
    --tconv;
    --qconv;
    --clw;
    --tpk;
    --tvp;
    --gz;

    /* Function Body */
    ah0   = ((1.f - qconv[*nk]) * 1005.7f + qconv[*nk] * 2500.f) * tconv[*nk] + qconv[*nk] * (2.501e6f - (tconv[*nk] - 273.15f) * 630.f) + gz[*nk];
    cpp   = (1.f - qconv[*nk]) * 1005.7f + qconv[*nk] * 1870.f;
    cpinv = 1.f / cpp;

    if (*kk == 1) {

/*   ***   CALCULATE LIFTED PARCEL QUANTITIES BELOW CLOUD BASE   *** */

	i__1 = *icb - 1;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    clw[i__] = 0.f;
/* L50: */
	}
	i__1 = *icb - 1;
	for (i__ = *nk; i__ <= i__1; ++i__) {
	    tpk[i__] = tconv[*nk] - (gz[i__] - gz[*nk]) * cpinv;
	    tvp[i__] = tpk[i__] * (qconv[*nk] * 1.6077898550724636f + 1.f);
/* L100: */
	}
    }

/*    ***  FIND LIFTED PARCEL QUANTITIES ABOVE CLOUD BASE    *** */

    nst = *icb;
    nsb = *icb;
    if (*kk == 2) {
	nst = *nl;
	nsb = *icb + 1;
    }
    i__1 = nst;
    for (i__ = nsb; i__ <= i__1; ++i__) {
	tg = tconv[i__];
	qg = qsconv[i__];
	alv = 2.501e6f - (tconv[i__] - 273.15f) * 630.f;
	for (j = 1; j <= 2; ++j) {
	    s = alv * alv * qg / (tconv[i__] * 461.5f * tconv[i__]) + 1005.7f;
	    s = 1.f / s;
	    ahg = tg * 1005.7f + qconv[*nk] * 1494.3f * tconv[i__] + alv * qg 
		    + gz[i__];
	    tg += s * (ah0 - ahg);
	    tg = dmax(tg,35.f);
	    tc = tg - 273.15f;
	    denom = tc + 243.5f;
	    if (tc >= 0.f) {
		es = exp(tc * 17.67f / denom) * 6.112f;
	    } else {
		es = exp(23.33086f - 6111.72784f / tg + log(tg) * .15215f);
	    }
	    qg = es * .62197183098591557f / (pconv_hpa__[i__] - es * 
		    .37802816901408443f);
/* L200: */
	}
	alv = 2.501e6f - (tconv[i__] - 273.15f) * 630.f;
	tpk[i__] = (ah0 - qconv[*nk] * 1494.3f * tconv[i__] - gz[i__] - alv * 
		qg) / 1005.7f;
	clw[i__] = qconv[*nk] - qg;
/* Computing MAX */
	r__1 = 0.f, r__2 = clw[i__];
	clw[i__] = dmax(r__1,r__2);
	rg = qg / (1.f - qconv[*nk]);
	tvp[i__] = tpk[i__] * (rg * 1.6077898550724636f + 1.f);
/* L300: */
    }
    return 0;
} 
/*----------------------------------------------------------------------------*/
int convect_(   int *nd, 
                int *nl, 
                float *delt, 
                int *iflag, 
                float *precip,
                float *wd,
                float *tprime,
                float *qprime,
                float *cbmf,
                float *ft,
                float *fq,
                float *sub,
                float *fmass,
                float *tconv,
                float *qconv,
                float *qsconv,
                float *pconv_hpa__,
                float *phconv_hpa__,
                int *nconvtop){
    
    /* System generated locals */
    int i__1, i__2, i__3;
    float r__1, r__2;
    float d__1, d__2;

    /* Local variables */
    
    int         i__, j, k;
    int         icb;
    int         inb;
    int         nk;
    int         jtt;
    int         inb1;
    int         nent[70];
    int         ihmin;
    
    float       by, rh, mp[70], th[70], qp[70], lv[70], hp[70], tp[70], gz[70], 
                hm[70], tv[70], wt[70], bf2, qp1, fac;
    float       m[70], b6, c6, ad, am, ep[70];
    float       h__[70];
    float       dei, chi, dbo, tca;
    float       alt, cpn[70], sij[4900]	/* was [70][70] */, rat, clw[70], byp,
                fup[70], qti, qsm;
    float       tvp[70], tvx, tvy;
    float       amp1, afac, cape, frac, dhdp, delm, elij[4900]	/* was [70][70] */, 
                delp, dtma, asij, rdcp, plcl, awat, smid, cwat, evap[70], anum, 
                sigp[70];
    
    float       bsum, sigt, ents, smin, ment[4900]	/* was [70][70] */, qent[4900]
	    	/* was [70][70] */, lvcp[70], qstm, delt0, coeff, capem, 
                ahmin, ahmax, delti, altem, denom;
    float       damps, dtpbl, fqold, ftold, epmax, dtmin, revap, cpinv, dpinv, sjmin,
                sjmax, fdown[70], scrit, water[70];
    float       stemp, defrac, dbosum, cbmfold, elacrit, tvaplcl, wdtrain, tvpplcl;


    //rather than have these as static indicies, we will have them here! TODO: is there a better way to have statics?
    /* Table of constant values */
    int c__1 = 1;
    int c__2 = 2;
    
/* -cv *************************************************************************** */
/* -cv C. Forster, November 2003 - May 2004: */
/* -cv */
/* -cv The subroutine has been downloaded from Kerry Emanuel's homepage, */
/* -cv where further infos on the convection scheme can be found */
/* -cv http://www-paoc.mit.edu/~emanuel/home.html */
/* -cv */
/* -cv The following changes have been made to integrate this subroutine */
/* -cv into FLEXPART */
/* -cv */
/* -cv Putting most of the variables in a new common block */
/* -cv renaming eps to eps0 because there is some eps already in includepar */
/* -cv */
/* -cv removing the arrays U,V,TRA and related arrays */
/* -cv */
/* -cv renaming the original arrays T,Q,QS,P,PH to */
/* -cv TCONV,QCONV,QSCONV,PCONV_HPA,PHCONV_HPA */
/* -cv */
/* -cv Initialization of variables has been put into parameter statements instead */
/* -cv of assignment of values at each call, in order to save computation time. */
/* *************************************************************************** */

/* ----------------------------------------------------------------------------- */
/*    *** On input:      *** */

/*     T:   Array of absolute temperature (K) of dimension ND, with first */
/*           index corresponding to lowest model level. Note that this array */
/*           will be altered by the subroutine if dry convective adjustment */
/*           occurs and if IPBL is not equal to 0. */

/*     Q:   Array of specific humidity (gm/gm) of dimension ND, with first */
/*            index corresponding to lowest model level. Must be defined */
/*            at same grid levels as T. Note that this array will be altered */
/*            if dry convective adjustment occurs and if IPBL is not equal to 0. */

/*     QS:  Array of saturation specific humidity of dimension ND, with first */
/*            index corresponding to lowest model level. Must be defined */
/*            at same grid levels as T. Note that this array will be altered */
/*            if dry convective adjustment occurs and if IPBL is not equal to 0. */

/*     U:   Array of zonal wind velocity (m/s) of dimension ND, witth first */
/*            index corresponding with the lowest model level. Defined at */
/*            same levels as T. Note that this array will be altered if */
/*            dry convective adjustment occurs and if IPBL is not equal to 0. */

/*     V:   Same as U but for meridional velocity. */

/*     TRA: Array of passive tracer mixing ratio, of dimensions (ND,NTRA), */
/*            where NTRA is the number of different tracers. If no */
/*            convective tracer transport is needed, define a dummy */
/*            input array of dimension (ND,1). Tracers are defined at */
/*            same vertical levels as T. Note that this array will be altered */
/*            if dry convective adjustment occurs and if IPBL is not equal to 0. */

/*     P:   Array of pressure (mb) of dimension ND, with first */
/*            index corresponding to lowest model level. Must be defined */
/*            at same grid levels as T. */

/*     PH:  Array of pressure (mb) of dimension ND+1, with first index */
/*            corresponding to lowest level. These pressures are defined at */
/*            levels intermediate between those of P, T, Q and QS. The first */
/*            value of PH should be greater than (i.e. at a lower level than) */
/*            the first value of the array P. */

/*     ND:  The dimension of the arrays T,Q,QS,P,PH,FT and FQ */

/*     NL:  The maximum number of levels to which convection can */
/*            penetrate, plus 1. */
/*            NL MUST be less than or equal to ND-1. */

/*     NTRA:The number of different tracers. If no tracer transport */
/*            is needed, set this equal to 1. (On most compilers, setting */
/*            NTRA to 0 will bypass tracer calculation, saving some CPU.) */

/*     DELT: The model time step (sec) between calls to CONVECT */

/* ---------------------------------------------------------------------------- */
/*    ***   On Output:         *** */

/*     IFLAG: An output integer whose value denotes the following: */

/*                VALUE                        INTERPRETATION */
/*                -----                        -------------- */
/*                  0               No moist convection; atmosphere is not */
/*                                  unstable, or surface temperature is less */
/*                                  than 250 K or surface specific humidity */
/*                                  is non-positive. */

/*                  1               Moist convection occurs. */

/*                  2               No moist convection: lifted condensation */
/*                                  level is above the 200 mb level. */

/*                  3               No moist convection: cloud base is higher */
/*                                  then the level NL-1. */

/*                  4               Moist convection occurs, but a CFL condition */
/*                                  on the subsidence warming is violated. This */
/*                                  does not cause the scheme to terminate. */

/*     FT:   Array of temperature tendency (K/s) of dimension ND, defined at same */
/*             grid levels as T, Q, QS and P. */

/*     FQ:   Array of specific humidity tendencies ((gm/gm)/s) of dimension ND, */
/*             defined at same grid levels as T, Q, QS and P. */

/*     FU:   Array of forcing of zonal velocity (m/s^2) of dimension ND, */
/*             defined at same grid levels as T. */

/*     FV:   Same as FU, but for forcing of meridional velocity. */

/*     FTRA: Array of forcing of tracer content, in tracer mixing ratio per */
/*             second, defined at same levels as T. Dimensioned (ND,NTRA). */

/*     PRECIP: Scalar convective precipitation rate (mm/day). */

/*     WD:    A convective downdraft velocity scale. For use in surface */
/*             flux parameterizations. See convect.ps file for details. */

/*     TPRIME: A convective downdraft temperature perturbation scale (K). */
/*              For use in surface flux parameterizations. See convect.ps */
/*              file for details. */

/*     QPRIME: A convective downdraft specific humidity */
/*              perturbation scale (gm/gm). */
/*              For use in surface flux parameterizations. See convect.ps */
/*              file for details. */

/*     CBMF:   The cloud base mass flux ((kg/m**2)/s). THIS SCALAR VALUE MUST */
/*              BE STORED BY THE CALLING PROGRAM AND RETURNED TO CONVECT AT */
/*              ITS NEXT CALL. That is, the value of CBMF must be "remembered" */
/*              by the calling program between calls to CONVECT. */

/* ------------------------------------------------------------------------------ */

/*    ***  THE PARAMETER NA SHOULD IN GENERAL BE GREATER THAN   *** */
/*    ***                OR EQUAL TO  ND + 1                    *** */


/*      include 'includepar' */
/*      include 'includeconv' */

/* -cv====>Begin Module CONVECT    File convect.f      Undeclared variables */

/*     Argument variables */



/*     Local variables */


/*     integer jc,jn */
/*     real alvnew,a2,ahm,alv,rm,sum,qnew,dphinv,tc,thbar,tnew,x */

/* -cv====>End Module   CONVECT    File convect.f */
/*     REAL TOLD(NA) */

/* ----------------------------------------------------------------------- */

/*   ***                     Specify Switches                         *** */

/*   ***   IPBL: Set to zero to bypass dry adiabatic adjustment       *** */
/*   ***    Any other value results in dry adiabatic adjustment       *** */
/*   ***     (Zero value recommended for use in models with           *** */
/*   ***                   boundary layer schemes)                    *** */

/*   ***   MINORIG: Lowest level from which convection may originate  *** */
/*   ***     (Should be first model level at which T is defined       *** */
/*   ***      for models using bulk PBL schemes; otherwise, it should *** */
/*   ***      be the first model level at which T is defined above    *** */
/*   ***                      the surface layer)                      *** */


/* ------------------------------------------------------------------------------ */

/*   ***                    SPECIFY PARAMETERS                        *** */

/*   *** ELCRIT IS THE AUTOCONVERSION THERSHOLD WATER CONTENT (gm/gm) *** */
/*   ***  TLCRIT IS CRITICAL TEMPERATURE BELOW WHICH THE AUTO-        *** */
/*   ***       CONVERSION THRESHOLD IS ASSUMED TO BE ZERO             *** */
/*   ***     (THE AUTOCONVERSION THRESHOLD VARIES LINEARLY            *** */
/*   ***               BETWEEN 0 C AND TLCRIT)                        *** */
/*   ***   ENTP IS THE COEFFICIENT OF MIXING IN THE ENTRAINMENT       *** */
/*   ***                       FORMULATION                            *** */
/*   ***  SIGD IS THE FRACTIONAL AREA COVERED BY UNSATURATED DNDRAFT  *** */
/*   ***  SIGS IS THE FRACTION OF PRECIPITATION FALLING OUTSIDE       *** */
/*   ***                        OF CLOUD                              *** */
/*   ***        OMTRAIN IS THE ASSUMED FALL SPEED (P/s) OF RAIN       *** */
/*   ***     OMTSNOW IS THE ASSUMED FALL SPEED (P/s) OF SNOW          *** */
/*   ***  COEFFR IS A COEFFICIENT GOVERNING THE RATE OF EVAPORATION   *** */
/*   ***                          OF RAIN                             *** */
/*   ***  COEFFS IS A COEFFICIENT GOVERNING THE RATE OF EVAPORATION   *** */
/*   ***                          OF SNOW                             *** */
/*   ***     CU IS THE COEFFICIENT GOVERNING CONVECTIVE MOMENTUM      *** */
/*   ***                         TRANSPORT                            *** */
/*   ***    DTMAX IS THE MAXIMUM NEGATIVE TEMPERATURE PERTURBATION    *** */
/*   ***        A LIFTED PARCEL IS ALLOWED TO HAVE BELOW ITS LFC      *** */
/*   ***    ALPHA AND DAMP ARE PARAMETERS THAT CONTROL THE RATE OF    *** */
/*   ***                 APPROACH TO QUASI-EQUILIBRIUM                *** */
/*   ***   (THEIR STANDARD VALUES ARE  0.20 AND 0.1, RESPECTIVELY)    *** */
/*   ***                   (DAMP MUST BE LESS THAN 1)                 *** */

/* original 0.2 */

/*   ***        ASSIGN VALUES OF THERMODYNAMIC CONSTANTS,        *** */
/*   ***            GRAVITY, AND LIQUID WATER DENSITY.           *** */
/*   ***             THESE SHOULD BE CONSISTENT WITH             *** */
/*   ***              THOSE USED IN CALLING PROGRAM              *** */
/*   ***     NOTE: THESE ARE ALSO SPECIFIED IN SUBROUTINE TLIFT  *** */


/* EPSILON IS A SMALL NUMBER USED TO EXCLUDE MASS FLUXES OF ZERO */

    /* Parameter adjustments */
    --phconv_hpa__;
    --pconv_hpa__;
    --qsconv;
    --qconv;
    --tconv;
    --sub;
    --fq;
    --ft;
    fmass -= 71;

    /* Function Body */
    delti = 1.f / *delt;

/*           ***  INITIALIZE OUTPUT ARRAYS AND PARAMETERS  *** */

    i__1 = *nl + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ft[i__] = 0.f;
	fq[i__] = 0.f;
	fdown[i__ - 1] = 0.f;
	sub[i__] = 0.f;
	fup[i__ - 1] = 0.f;
	m[i__ - 1] = 0.f;
	mp[i__ - 1] = 0.f;
	i__2 = *nl + 1;
	for (j = 1; j <= i__2; ++j) {
	    fmass[i__ + j * 70] = 0.f;
	    ment[i__ + j * 70 - 71] = 0.f;
/* L5: */
	}
    }
    i__2 = *nl + 1;
    for (i__ = 1; i__ <= i__2; ++i__) {
	rdcp = ((1.f - qconv[i__]) * 287.04f + qconv[i__] * 461.5f) / ((1.f - 
		qconv[i__]) * 1005.7f + qconv[i__] * 1870.f);
	d__1 = (float) (1e3f / pconv_hpa__[i__]);
	d__2 = (float) rdcp;
	th[i__ - 1] = tconv[i__] * pow_dd(&d__1, &d__2);
/* L7: */
    }
    *precip = 0.f;
    *wd = 0.f;
    *tprime = 0.f;
    *qprime = 0.f;
    *iflag = 0;

/*       IF(IPBL.NE.0)THEN */

/*     ***            PERFORM DRY ADIABATIC ADJUSTMENT            *** */

/*       JC=0 */
/*       DO 30 I=NL-1,1,-1 */
/*        JN=0 */
/*         SUM=TH(I)*(1.+QCONV(I)*EPSI-QCONV(I)) */
/*        DO 10 J=I+1,NL */
/*         SUM=SUM+TH(J)*(1.+QCONV(J)*EPSI-QCONV(J)) */
/*         THBAR=SUM/FLOAT(J+1-I) */
/*         IF((TH(J)*(1.+QCONV(J)*EPSI-QCONV(J))).LT.THBAR)JN=J */
/*  10    CONTINUE */
/*        IF(I.EQ.1)JN=MAX(JN,2) */
/*        IF(JN.EQ.0)GOTO 30 */
/*  12    CONTINUE */
/*        AHM=0.0 */
/*        RM=0.0 */
/*        DO 15 J=I,JN */
/*         AHM=AHM+(CPD*(1.-QCONV(J))+QCONV(J)*CPV)*TCONV(J)* */
/*    +   (PHCONV_HPA(J)-PHCONV_HPA(J+1)) */
/*         RM=RM+QCONV(J)*(PHCONV_HPA(J)-PHCONV_HPA(J+1)) */
/*  15    CONTINUE */
/*        DPHINV=1./(PHCONV_HPA(I)-PHCONV_HPA(JN+1)) */
/*        RM=RM*DPHINV */
/*        A2=0.0 */
/*        DO 20 J=I,JN */
/*         QCONV(J)=RM */
/*         RDCP=(RD*(1.-QCONV(J))+QCONV(J)*RV)/ */
/*    1     (CPD*(1.-QCONV(J))+QCONV(J)*CPV) */
/*         X=(0.001*PCONV_HPA(J))**RDCP */
/*         TOLD(J)=TCONV(J) */
/*         TCONV(J)=X */
/*         A2=A2+(CPD*(1.-QCONV(J))+QCONV(J)*CPV)*X* */
/*    1    (PHCONV_HPA(J)-PHCONV_HPA(J+1)) */
/*  20    CONTINUE */
/*        DO 25 J=I,JN */
/*         TH(J)=AHM/A2 */
/*         TCONV(J)=TCONV(J)*TH(J) */
/*         TC=TOLD(J)-273.15 */
/*         ALV=LV0-CPVMCL*TC */
/*         QSCONV(J)=QSCONV(J)+QSCONV(J)*(1.+QSCONV(J)*(EPSI-1.))*ALV* */
/*    1    (TCONV(J)- TOLD(J))/(RV*TOLD(J)*TOLD(J)) */
/*      if (qslev(j) .lt. 0.) then */
/*        write(*,*) 'qslev.lt.0 ',j,qslev */
/*      endif */
/*  25    CONTINUE */
/*        IF((TH(JN+1)*(1.+QCONV(JN+1)*EPSI-QCONV(JN+1))).LT. */
/*    1    (TH(JN)*(1.+QCONV(JN)*EPSI-QCONV(JN))))THEN */
/*         JN=JN+1 */
/*         GOTO 12 */
/*        END IF */
/*        IF(I.EQ.1)JC=JN */
/*  30   CONTINUE */

/*   ***   Remove any supersaturation that results from adjustment *** */

/*     IF(JC.GT.1)THEN */
/*      DO 38 J=1,JC */
/*         IF(QSCONV(J).LT.QCONV(J))THEN */
/*          ALV=LV0-CPVMCL*(TCONV(J)-273.15) */
/*          TNEW=TCONV(J)+ALV*(QCONV(J)-QSCONV(J))/(CPD*(1.-QCONV(J))+ */
/*    1      CL*QCONV(J)+QSCONV(J)*(CPV-CL+ALV*ALV/(RV*TCONV(J)*TCONV(J)))) */
/*          ALVNEW=LV0-CPVMCL*(TNEW-273.15) */
/*          QNEW=(ALV*QCONV(J)-(TNEW-TCONV(J))*(CPD*(1.-QCONV(J)) */
/*    1     +CL*QCONV(J)))/ALVNEW */
/*          PRECIP=PRECIP+24.*3600.*1.0E5*(PHCONV_HPA(J)-PHCONV_HPA(J+1))* */
/*    1      (QCONV(J)-QNEW)/(G*DELT*ROWL) */
/*          TCONV(J)=TNEW */
/*          QCONV(J)=QNEW */
/*          QSCONV(J)=QNEW */
/*         END IF */
/*  38  CONTINUE */
/*     END IF */

/*     END IF */

/*  *** CALCULATE ARRAYS OF GEOPOTENTIAL, HEAT CAPACITY AND STATIC ENERGY */

    gz[0] = 0.f;
    cpn[0] = (1.f - qconv[1]) * 1005.7f + qconv[1] * 1870.f;
    h__[0] = tconv[1] * cpn[0];
    lv[0] = 2.501e6f - (tconv[1] - 273.15f) * 630.f;
    hm[0] = lv[0] * qconv[1];
    tv[0] = tconv[1] * (qconv[1] * 1.6077898550724636f + 1.f - qconv[1]);
    ahmin = 1e12f;
    ihmin = *nl;
    i__2 = *nl + 1;
    for (i__ = 2; i__ <= i__2; ++i__) {
	tvx = tconv[i__] * (qconv[i__] * 1.6077898550724636f + 1.f - qconv[
		i__]);
	tvy = tconv[i__ - 1] * (qconv[i__ - 1] * 1.6077898550724636f + 1.f - 
		qconv[i__ - 1]);
	gz[i__ - 1] = gz[i__ - 2] + (tvx + tvy) * 143.52000000000001f * (
		pconv_hpa__[i__ - 1] - pconv_hpa__[i__]) / phconv_hpa__[i__];
	cpn[i__ - 1] = (1.f - qconv[i__]) * 1005.7f + qconv[i__] * 1870.f;
	h__[i__ - 1] = tconv[i__] * cpn[i__ - 1] + gz[i__ - 1];
	lv[i__ - 1] = 2.501e6f - (tconv[i__] - 273.15f) * 630.f;
	hm[i__ - 1] = ((1.f - qconv[i__]) * 1005.7f + qconv[i__] * 2500.f) * (
		tconv[i__] - tconv[1]) + lv[i__ - 1] * qconv[i__] + gz[i__ - 
		1];
	tv[i__ - 1] = tconv[i__] * (qconv[i__] * 1.6077898550724636f + 1.f - 
		qconv[i__]);

/*  ***  Find level of minimum moist static energy    *** */

	if (i__ >= 1 && hm[i__ - 1] < ahmin && hm[i__ - 1] < hm[i__ - 2]) {
	    ahmin = hm[i__ - 1];
	    ihmin = i__;
	}
/* L40: */
    }
/* Computing MIN */
    i__2 = ihmin, i__1 = *nl - 1;
    ihmin = min(i__2,i__1);

/*  ***     Find that model level below the level of minimum moist       *** */
/*  ***  static energy that has the maximum value of moist static energy *** */

    ahmax = 0.f;
    i__2 = ihmin;
    for (i__ = 1; i__ <= i__2; ++i__) {
	if (hm[i__ - 1] > ahmax) {
	    nk = i__;
	    ahmax = hm[i__ - 1];
	}
/* L42: */
    }

/*  ***  CHECK WHETHER PARCEL LEVEL TEMPERATURE AND SPECIFIC HUMIDITY   *** */
/*  ***                          ARE REASONABLE                         *** */
/*  ***      Skip convection if HM increases monotonically upward       *** */

    if (tconv[nk] < 250.f || qconv[nk] <= 0.f || ihmin == *nl - 1) {
	*iflag = 0;
	*cbmf = 0.f;
	return 0;
    }

/*   ***  CALCULATE LIFTED CONDENSATION LEVEL OF AIR AT PARCEL ORIGIN LEVEL *** */
/*   ***       (WITHIN 0.2% OF FORMULA OF BOLTON, MON. WEA. REV.,1980)      *** */

    rh = qconv[nk] / qsconv[nk];
    chi = tconv[nk] / (1669.f - rh * 122.f - tconv[nk]);
    d__1 = (float) rh;
    d__2 = (float) chi;
    plcl = pconv_hpa__[nk] * pow_dd(&d__1, &d__2);
    if (plcl < 200.f || plcl >= 2e3f) {
	*iflag = 2;
	*cbmf = 0.f;
	return 0;
    }

/*   ***  CALCULATE FIRST LEVEL ABOVE LCL (=ICB)  *** */

    icb = *nl - 1;
    i__2 = *nl;
    for (i__ = nk + 1; i__ <= i__2; ++i__) {
	if (pconv_hpa__[i__] < plcl) {
	    icb = min(icb,i__);
	}
/* L50: */
    }
    if (icb >= *nl - 1) {
	*iflag = 3;
	*cbmf = 0.f;
	return 0;
    }

/*   *** FIND TEMPERATURE UP THROUGH ICB AND TEST FOR INSTABILITY           *** */

/*   *** SUBROUTINE TLIFT CALCULATES PART OF THE LIFTED PARCEL VIRTUAL      *** */
/*   ***  TEMPERATURE, THE ACTUAL TEMPERATURE AND THE ADIABATIC             *** */
/*   ***                   LIQUID WATER CONTENT                             *** */

    tlift_(gz, &icb, &nk, tvp, tp, clw, nd, nl, &c__1, &qconv[1], &tconv[1], &qsconv[1], &pconv_hpa__[1]);
    i__2 = icb;
    for (i__ = nk; i__ <= i__2; ++i__) {
	tvp[i__ - 1] -= tp[i__ - 1] * qconv[nk];
/* L54: */
    }

/*   ***  If there was no convection at last time step and parcel    *** */
/*   ***       is stable at ICB then skip rest of calculation        *** */

    if (*cbmf == 0.f && tvp[icb - 1] <= tv[icb - 1] - .9f) {
	*iflag = 0;
	return 0;
    }

/*   ***  IF THIS POINT IS REACHED, MOIST CONVECTIVE ADJUSTMENT IS NECESSARY *** */

    if (*iflag != 4) {
	*iflag = 1;
    }

/*   ***  FIND THE REST OF THE LIFTED PARCEL TEMPERATURES          *** */

    tlift_(gz, &icb, &nk, tvp, tp, clw, nd, nl, &c__2, &qconv[1], &tconv[1], &
	    qsconv[1], &pconv_hpa__[1]);

/*   ***  SET THE PRECIPITATION EFFICIENCIES AND THE FRACTION OF   *** */
/*   ***          PRECIPITATION FALLING OUTSIDE OF CLOUD           *** */
/*   ***      THESE MAY BE FUNCTIONS OF TP(I), PCONV_HPA(I) AND CLW(I)     *** */

    i__2 = nk;
    for (i__ = 1; i__ <= i__2; ++i__) {
	ep[i__ - 1] = 0.f;
	sigp[i__ - 1] = .12f;
/* L57: */
    }
    i__2 = *nl;
    for (i__ = nk + 1; i__ <= i__2; ++i__) {
	tca = tp[i__ - 1] - 273.15f;
	if (tca >= 0.f) {
	    elacrit = .0011f;
	} else {
	    elacrit = (1.f - tca / -55.f) * .0011f;
	}
	elacrit = dmax(elacrit,0.f);
	epmax = .999f;
/* Computing MAX */
	r__1 = clw[i__ - 1];
	ep[i__ - 1] = epmax * (1.f - elacrit / dmax(r__1,1e-8f));
/* Computing MAX */
	r__1 = ep[i__ - 1];
	ep[i__ - 1] = dmax(r__1,0.f);
/* Computing MIN */
	r__1 = ep[i__ - 1];
	ep[i__ - 1] = dmin(r__1,epmax);
	sigp[i__ - 1] = .12f;
/* L60: */
    }

/*   ***       CALCULATE VIRTUAL TEMPERATURE AND LIFTED PARCEL     *** */
/*   ***                    VIRTUAL TEMPERATURE                    *** */

    i__2 = *nl;
    for (i__ = icb + 1; i__ <= i__2; ++i__) {
	tvp[i__ - 1] -= tp[i__ - 1] * qconv[nk];
/* L64: */
    }
    tvp[*nl] = tvp[*nl - 1] - (gz[*nl] - gz[*nl - 1]) / 1005.7f;

/*   ***        NOW INITIALIZE VARIOUS ARRAYS USED IN THE COMPUTATIONS       *** */

    i__2 = *nl + 1;
    for (i__ = 1; i__ <= i__2; ++i__) {
	hp[i__ - 1] = h__[i__ - 1];
	nent[i__ - 1] = 0;
	water[i__ - 1] = 0.f;
	evap[i__ - 1] = 0.f;
	wt[i__ - 1] = 5.5f;
	lvcp[i__ - 1] = lv[i__ - 1] / cpn[i__ - 1];
	i__1 = *nl + 1;
	for (j = 1; j <= i__1; ++j) {
	    qent[i__ + j * 70 - 71] = qconv[j];
	    elij[i__ + j * 70 - 71] = 0.f;
	    sij[i__ + j * 70 - 71] = 0.f;
/* L70: */
	}
    }
    qp[0] = qconv[1];
    i__1 = *nl + 1;
    for (i__ = 2; i__ <= i__1; ++i__) {
	qp[i__ - 1] = qconv[i__ - 1];
/* L72: */
    }

/*  ***  FIND THE FIRST MODEL LEVEL (INB1) ABOVE THE PARCEL'S      *** */
/*  ***          HIGHEST LEVEL OF NEUTRAL BUOYANCY                 *** */
/*  ***     AND THE HIGHEST LEVEL OF POSITIVE CAPE (INB)           *** */

    cape = 0.f;
    capem = 0.f;
    inb = icb + 1;
    inb1 = inb;
    byp = 0.f;
    i__1 = *nl - 1;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	by = (tvp[i__ - 1] - tv[i__ - 1]) * (phconv_hpa__[i__] - phconv_hpa__[
		i__ + 1]) / pconv_hpa__[i__];
	cape += by;
	if (by >= 0.f) {
	    inb1 = i__ + 1;
	}
	if (cape > 0.f) {
	    inb = i__ + 1;
	    byp = (tvp[i__] - tv[i__]) * (phconv_hpa__[i__ + 1] - 
		    phconv_hpa__[i__ + 2]) / pconv_hpa__[i__ + 1];
	    capem = cape;
	}
/* L82: */
    }
    inb = max(inb,inb1);
    cape = capem + byp;
    defrac = capem - cape;
    defrac = dmax(defrac,.001f);
    frac = -cape / defrac;
    frac = dmin(frac,1.f);
    frac = dmax(frac,0.f);

/*   ***   CALCULATE LIQUID WATER STATIC ENERGY OF LIFTED PARCEL   *** */

    i__1 = inb;
    for (i__ = icb; i__ <= i__1; ++i__) {
	hp[i__ - 1] = h__[nk - 1] + (lv[i__ - 1] + tconv[i__] * 
		-864.29999999999995f) * ep[i__ - 1] * clw[i__ - 1];
/* L95: */
    }

/*   ***  CALCULATE CLOUD BASE MASS FLUX AND RATES OF MIXING, M(I),  *** */
/*   ***                   AT EACH MODEL LEVEL                       *** */

    dbosum = 0.f;

/*   ***     INTERPOLATE DIFFERENCE BETWEEN LIFTED PARCEL AND      *** */
/*   ***  ENVIRONMENTAL TEMPERATURES TO LIFTED CONDENSATION LEVEL  *** */

    tvpplcl = tvp[icb - 2] - tvp[icb - 2] * 287.04f * (pconv_hpa__[icb - 1] - 
	    plcl) / (cpn[icb - 2] * pconv_hpa__[icb - 1]);
    tvaplcl = tv[icb - 1] + (tvp[icb - 1] - tvp[icb]) * (plcl - pconv_hpa__[
	    icb]) / (pconv_hpa__[icb] - pconv_hpa__[icb + 1]);
    dtpbl = 0.f;
    i__1 = icb - 1;
    for (i__ = nk; i__ <= i__1; ++i__) {
	dtpbl += (tvp[i__ - 1] - tv[i__ - 1]) * (phconv_hpa__[i__] - 
		phconv_hpa__[i__ + 1]);
/* L96: */
    }
    dtpbl /= phconv_hpa__[nk] - phconv_hpa__[icb];
    dtmin = tvpplcl - tvaplcl + .9f + dtpbl;
    dtma = dtmin;

/*   ***  ADJUST CLOUD BASE MASS FLUX   *** */

    cbmfold = *cbmf;
/* *** C. Forster: adjustment of CBMF is not allowed to depend on FLEXPART timestep */
    delt0 = *delt / 3.f;
    damps = *delt * .1f / delt0;
    *cbmf = (1.f - damps) * *cbmf + dtma * .0025000000000000005f;
    *cbmf = dmax(*cbmf,0.f);

/*   *** If cloud base mass flux is zero, skip rest of calculation  *** */

    if (*cbmf == 0.f && cbmfold == 0.f) {
	return 0;
    }

/*   ***   CALCULATE RATES OF MIXING,  M(I)   *** */

    m[icb - 1] = 0.f;
    i__1 = inb;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	k = min(i__,inb1);
	dbo = (r__1 = tv[k - 1] - tvp[k - 1], dabs(r__1)) + (phconv_hpa__[k] 
		- phconv_hpa__[k + 1]) * .029999999999999999f;
	dbosum += dbo;
	m[i__ - 1] = *cbmf * dbo;
/* L103: */
    }
    i__1 = inb;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	m[i__ - 1] /= dbosum;
/* L110: */
    }

/*   ***  CALCULATE ENTRAINED AIR MASS FLUX (MENT), TOTAL WATER MIXING  *** */
/*   ***     RATIO (QENT), TOTAL CONDENSED WATER (ELIJ), AND MIXING     *** */
/*   ***                        FRACTION (SIJ)                          *** */

    i__1 = inb;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	qti = qconv[nk] - ep[i__ - 1] * clw[i__ - 1];
	i__2 = inb;
	for (j = icb; j <= i__2; ++j) {
	    bf2 = lv[j - 1] * lv[j - 1] * qsconv[j] / (tconv[j] * 461.5f * 
		    tconv[j] * 1005.7f) + 1.f;
	    anum = h__[j - 1] - hp[i__ - 1] + tconv[j] * 864.29999999999995f *
		     (qti - qconv[j]);
	    denom = h__[i__ - 1] - hp[i__ - 1] + (qconv[i__] - qti) * 
		    -864.29999999999995f * tconv[j];
	    dei = denom;
	    if (dabs(dei) < .01f) {
		dei = .01f;
	    }
	    sij[i__ + j * 70 - 71] = anum / dei;
	    sij[i__ + i__ * 70 - 71] = 1.f;
	    altem = sij[i__ + j * 70 - 71] * qconv[i__] + (1.f - sij[i__ + j *
		     70 - 71]) * qti - qsconv[j];
	    altem /= bf2;
	    cwat = clw[j - 1] * (1.f - ep[j - 1]);
	    stemp = sij[i__ + j * 70 - 71];
	    if ((stemp < 0.f || stemp > 1.f || altem > cwat) && j > i__) {
		anum -= lv[j - 1] * (qti - qsconv[j] - cwat * bf2);
		denom += lv[j - 1] * (qconv[i__] - qti);
		if (dabs(denom) < .01f) {
		    denom = .01f;
		}
		sij[i__ + j * 70 - 71] = anum / denom;
		altem = sij[i__ + j * 70 - 71] * qconv[i__] + (1.f - sij[i__ 
			+ j * 70 - 71]) * qti - qsconv[j];
		altem -= (bf2 - 1.f) * cwat;
	    }
	    if (sij[i__ + j * 70 - 71] > 0.f && sij[i__ + j * 70 - 71] < .9f) 
		    {
		qent[i__ + j * 70 - 71] = sij[i__ + j * 70 - 71] * qconv[i__] 
			+ (1.f - sij[i__ + j * 70 - 71]) * qti;
		elij[i__ + j * 70 - 71] = altem;
/* Computing MAX */
		r__1 = 0.f, r__2 = elij[i__ + j * 70 - 71];
		elij[i__ + j * 70 - 71] = dmax(r__1,r__2);
		ment[i__ + j * 70 - 71] = m[i__ - 1] / (1.f - sij[i__ + j * 
			70 - 71]);
		++nent[i__ - 1];
	    }
/* Computing MAX */
	    r__1 = 0.f, r__2 = sij[i__ + j * 70 - 71];
	    sij[i__ + j * 70 - 71] = dmax(r__1,r__2);
/* Computing MIN */
	    r__1 = 1.f, r__2 = sij[i__ + j * 70 - 71];
	    sij[i__ + j * 70 - 71] = dmin(r__1,r__2);
/* L160: */
	}

/*   ***   IF NO AIR CAN ENTRAIN AT LEVEL I ASSUME THAT UPDRAFT DETRAINS  *** */
/*   ***   AT THAT LEVEL AND CALCULATE DETRAINED AIR FLUX AND PROPERTIES  *** */

	if (nent[i__ - 1] == 0) {
	    ment[i__ + i__ * 70 - 71] = m[i__ - 1];
	    qent[i__ + i__ * 70 - 71] = qconv[nk] - ep[i__ - 1] * clw[i__ - 1]
		    ;
	    elij[i__ + i__ * 70 - 71] = clw[i__ - 1];
	    sij[i__ + i__ * 70 - 71] = 1.f;
	}
/* L170: */
    }
    sij[inb + inb * 70 - 71] = 1.f;

/*   ***  NORMALIZE ENTRAINED AIR MASS FLUXES TO REPRESENT EQUAL  *** */
/*   ***              PROBABILITIES OF MIXING                     *** */

    i__1 = inb;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	if (nent[i__ - 1] != 0) {
	    qp1 = qconv[nk] - ep[i__ - 1] * clw[i__ - 1];
	    anum = h__[i__ - 1] - hp[i__ - 1] - lv[i__ - 1] * (qp1 - qsconv[
		    i__]);
	    denom = h__[i__ - 1] - hp[i__ - 1] + lv[i__ - 1] * (qconv[i__] - 
		    qp1);
	    if (dabs(denom) < .01f) {
		denom = .01f;
	    }
	    scrit = anum / denom;
	    alt = qp1 - qsconv[i__] + scrit * (qconv[i__] - qp1);
	    if (alt < 0.f) {
		scrit = 1.f;
	    }
	    scrit = dmax(scrit,0.f);
	    asij = 0.f;
	    smin = 1.f;
	    i__2 = inb;
	    for (j = icb; j <= i__2; ++j) {
		if (sij[i__ + j * 70 - 71] > 0.f && sij[i__ + j * 70 - 71] < 
			.9f) {
		    if (j > i__) {
/* Computing MIN */
			r__1 = sij[i__ + j * 70 - 71];
			smid = dmin(r__1,scrit);
			sjmax = smid;
			sjmin = smid;
			if (smid < smin && sij[i__ + (j + 1) * 70 - 71] < 
				smid) {
			    smin = smid;
/* Computing MIN */
			    r__1 = sij[i__ + (j + 1) * 70 - 71], r__2 = sij[
				    i__ + j * 70 - 71], r__1 = min(r__1,r__2);
			    sjmax = dmin(r__1,scrit);
/* Computing MAX */
			    r__1 = sij[i__ + (j - 1) * 70 - 71], r__2 = sij[
				    i__ + j * 70 - 71];
			    sjmin = dmax(r__1,r__2);
			    sjmin = dmin(sjmin,scrit);
			}
		    } else {
/* Computing MAX */
			r__1 = sij[i__ + (j + 1) * 70 - 71];
			sjmax = dmax(r__1,scrit);
/* Computing MAX */
			r__1 = sij[i__ + j * 70 - 71];
			smid = dmax(r__1,scrit);
			sjmin = 0.f;
			if (j > 1) {
			    sjmin = sij[i__ + (j - 1) * 70 - 71];
			}
			sjmin = dmax(sjmin,scrit);
		    }
		    delp = (r__1 = sjmax - smid, dabs(r__1));
		    delm = (r__1 = sjmin - smid, dabs(r__1));
		    asij += (delp + delm) * (phconv_hpa__[j] - phconv_hpa__[j 
			    + 1]);
		    ment[i__ + j * 70 - 71] = ment[i__ + j * 70 - 71] * (delp 
			    + delm) * (phconv_hpa__[j] - phconv_hpa__[j + 1]);
		}
/* L175: */
	    }
	    asij = dmax(1e-21f,asij);
	    asij = 1.f / asij;
	    i__2 = inb;
	    for (j = icb; j <= i__2; ++j) {
		ment[i__ + j * 70 - 71] *= asij;
/* L180: */
	    }
	    bsum = 0.f;
	    i__2 = inb;
	    for (j = icb; j <= i__2; ++j) {
		bsum += ment[i__ + j * 70 - 71];
/* L190: */
	    }
	    if (bsum < 1e-18f) {
		nent[i__ - 1] = 0;
		ment[i__ + i__ * 70 - 71] = m[i__ - 1];
		qent[i__ + i__ * 70 - 71] = qconv[nk] - ep[i__ - 1] * clw[i__ 
			- 1];
		elij[i__ + i__ * 70 - 71] = clw[i__ - 1];
		sij[i__ + i__ * 70 - 71] = 1.f;
	    }
	}
/* L200: */
    }

/*   ***  CHECK WHETHER EP(INB)=0, IF SO, SKIP PRECIPITATING    *** */
/*   ***             DOWNDRAFT CALCULATION                      *** */

    if (ep[inb - 1] < 1e-4f) {
	goto L405;
    }

/*   ***  INTEGRATE LIQUID WATER EQUATION TO FIND CONDENSED WATER   *** */
/*   ***                AND CONDENSED WATER FLUX                    *** */

    jtt = 2;

/*    ***                    BEGIN DOWNDRAFT LOOP                    *** */

    for (i__ = inb; i__ >= 1; --i__) {

/*    ***              CALCULATE DETRAINED PRECIPITATION             *** */

	wdtrain = ep[i__ - 1] * 9.81f * m[i__ - 1] * clw[i__ - 1];
	if (i__ > 1) {
	    i__1 = i__ - 1;
	    for (j = 1; j <= i__1; ++j) {
		awat = elij[j + i__ * 70 - 71] - (1.f - ep[i__ - 1]) * clw[
			i__ - 1];
		awat = dmax(0.f,awat);
/* L320: */
		wdtrain += awat * 9.81f * ment[j + i__ * 70 - 71];
	    }
	}

/*    ***    FIND RAIN WATER AND EVAPORATION USING PROVISIONAL   *** */
/*    ***              ESTIMATES OF QP(I)AND QP(I-1)             *** */


/*  ***  Value of terminal velocity and coefficient of evaporation for snow   *** */

	coeff = .8f;
	wt[i__ - 1] = 5.5f;

/*  ***  Value of terminal velocity and coefficient of evaporation for rain   *** */

	if (tconv[i__] > 273.f) {
	    coeff = 1.f;
	    wt[i__ - 1] = 50.f;
	}
	qsm = (qconv[i__] + qp[i__]) * .5f;
	afac = coeff * phconv_hpa__[i__] * (qsconv[i__] - qsm) / (
		phconv_hpa__[i__] * 2e3f * qsconv[i__] + 1e4f);
	afac = dmax(afac,0.f);
	sigt = sigp[i__ - 1];
	sigt = dmax(0.f,sigt);
	sigt = dmin(1.f,sigt);
	b6 = (phconv_hpa__[i__] - phconv_hpa__[i__ + 1]) * 100.f * sigt * 
		afac / wt[i__ - 1];
	c6 = (water[i__] * wt[i__] + wdtrain / .05f) / wt[i__ - 1];
	revap = (-b6 + sqrt(b6 * b6 + c6 * 4.f)) * .5f;
	evap[i__ - 1] = sigt * afac * revap;
	water[i__ - 1] = revap * revap;

/*    ***  CALCULATE PRECIPITATING DOWNDRAFT MASS FLUX UNDER     *** */
/*    ***              HYDROSTATIC APPROXIMATION                 *** */

	if (i__ == 1) {
	    goto L360;
	}
	dhdp = (h__[i__ - 1] - h__[i__ - 2]) / (pconv_hpa__[i__ - 1] - 
		pconv_hpa__[i__]);
	dhdp = dmax(dhdp,10.f);
	mp[i__ - 1] = lv[i__ - 1] * 10.19367991845056f * .05f * evap[i__ - 1] 
		/ dhdp;
/* Computing MAX */
	r__1 = mp[i__ - 1];
	mp[i__ - 1] = dmax(r__1,0.f);

/*   ***   ADD SMALL AMOUNT OF INERTIA TO DOWNDRAFT              *** */

	fac = 20.f / (phconv_hpa__[i__ - 1] - phconv_hpa__[i__]);
	mp[i__ - 1] = (fac * mp[i__] + mp[i__ - 1]) / (fac + 1.f);

/*    ***      FORCE MP TO DECREASE LINEARLY TO ZERO                 *** */
/*    ***      BETWEEN ABOUT 950 MB AND THE SURFACE                  *** */

	if (pconv_hpa__[i__] > pconv_hpa__[1] * .949f) {
	    jtt = max(jtt,i__);
	    mp[i__ - 1] = mp[jtt - 1] * (pconv_hpa__[1] - pconv_hpa__[i__]) / 
		    (pconv_hpa__[1] - pconv_hpa__[jtt]);
	}
L360:

/*    ***       FIND MIXING RATIO OF PRECIPITATING DOWNDRAFT     *** */

	if (i__ == inb) {
	    goto L400;
	}
	if (i__ == 1) {
	    qstm = qsconv[1];
	} else {
	    qstm = qsconv[i__ - 1];
	}
	if (mp[i__ - 1] > mp[i__]) {
	    rat = mp[i__] / mp[i__ - 1];
	    qp[i__ - 1] = qp[i__] * rat + qconv[i__] * (1.f - rat) + (
		    phconv_hpa__[i__] - phconv_hpa__[i__ + 1]) * 
		    .509683995922528f * (evap[i__ - 1] / mp[i__ - 1]);
	} else {
	    if (mp[i__] > 0.f) {
		qp[i__ - 1] = (gz[i__] - gz[i__ - 1] + qp[i__] * (lv[i__] + 
			tconv[i__ + 1] * 1494.3f) + (tconv[i__ + 1] - tconv[
			i__]) * 1005.7f) / (lv[i__ - 1] + tconv[i__] * 
			1494.3f);
	    }
	}
/* Computing MIN */
	r__1 = qp[i__ - 1];
	qp[i__ - 1] = dmin(r__1,qstm);
/* Computing MAX */
	r__1 = qp[i__ - 1];
	qp[i__ - 1] = dmax(r__1,0.f);
L400:
	;
    }

/*   ***  CALCULATE SURFACE PRECIPITATION IN MM/DAY     *** */

    *precip += wt[0] * .05f * water[0] * 3600.f * 2.4e4f / 9810.f;

L405:

/*   ***  CALCULATE DOWNDRAFT VELOCITY SCALE AND SURFACE TEMPERATURE AND  *** */
/*   ***                    WATER VAPOR FLUCTUATIONS                      *** */

    *wd = (r__1 = mp[icb - 1], dabs(r__1)) * 10.f * .01f * 287.04f * tconv[
	    icb] / (pconv_hpa__[icb] * .05f);
    *qprime = (qp[0] - qconv[1]) * .5f;
    *tprime = *qprime * 2.501e6f / 1005.7f;

/*   ***  CALCULATE TENDENCIES OF LOWEST LEVEL POTENTIAL TEMPERATURE  *** */
/*   ***                      AND MIXING RATIO                        *** */

    dpinv = .01f / (phconv_hpa__[1] - phconv_hpa__[2]);
    am = 0.f;
    if (nk == 1) {
	i__1 = inb;
	for (k = 2; k <= i__1; ++k) {
/* L410: */
	    am += m[k - 1];
	}
    }
/* save saturated upward mass flux for first level */
    fup[0] = am;
    if (dpinv * 19.620000000000001f * am >= delti) {
	*iflag = 4;
    }
    ft[1] += dpinv * 9.81f * am * (tconv[2] - tconv[1] + (gz[1] - gz[0]) / 
	    cpn[0]);
    ft[1] -= lvcp[0] * .05f * evap[0];
    ft[1] += wt[1] * .05f * 1494.3f * water[1] * (tconv[2] - tconv[1]) * 
	    dpinv / cpn[0];
    fq[1] = fq[1] + mp[1] * 9.81f * (qp[1] - qconv[1]) * dpinv + evap[0] * 
	    .05f;
    fq[1] += am * 9.81f * (qconv[2] - qconv[1]) * dpinv;
    i__1 = inb;
    for (j = 2; j <= i__1; ++j) {
	fq[1] += dpinv * 9.81f * ment[j - 1] * (qent[j - 1] - qconv[1]);
/* L415: */
    }

/*   ***  CALCULATE TENDENCIES OF POTENTIAL TEMPERATURE AND MIXING RATIO  *** */
/*   ***               AT LEVELS ABOVE THE LOWEST LEVEL                   *** */

/*   ***  FIRST FIND THE NET SATURATED UPDRAFT AND DOWNDRAFT MASS FLUXES  *** */
/*   ***                      THROUGH EACH LEVEL                          *** */

    i__1 = inb;
    for (i__ = 2; i__ <= i__1; ++i__) {
	dpinv = .01f / (phconv_hpa__[i__] - phconv_hpa__[i__ + 1]);
	cpinv = 1.f / cpn[i__ - 1];
	amp1 = 0.f;
	ad = 0.f;
	if (i__ >= nk) {
	    i__2 = inb + 1;
	    for (k = i__ + 1; k <= i__2; ++k) {
/* L440: */
		amp1 += m[k - 1];
	    }
	}
	i__2 = i__;
	for (k = 1; k <= i__2; ++k) {
	    i__3 = inb + 1;
	    for (j = i__ + 1; j <= i__3; ++j) {
		amp1 += ment[k + j * 70 - 71];
/* L450: */
	    }
	}
/* save saturated upward mass flux */
	fup[i__ - 1] = amp1;
	if (dpinv * 19.620000000000001f * amp1 >= delti) {
	    *iflag = 4;
	}
	i__3 = i__ - 1;
	for (k = 1; k <= i__3; ++k) {
	    i__2 = inb;
	    for (j = i__; j <= i__2; ++j) {
		ad += ment[j + k * 70 - 71];
/* L470: */
	    }
	}
/* save saturated downward mass flux */
	fdown[i__ - 1] = ad;
	ft[i__] = ft[i__] + dpinv * 9.81f * (amp1 * (tconv[i__ + 1] - tconv[
		i__] + (gz[i__] - gz[i__ - 1]) * cpinv) - ad * (tconv[i__] - 
		tconv[i__ - 1] + (gz[i__ - 1] - gz[i__ - 2]) * cpinv)) - lvcp[
		i__ - 1] * .05f * evap[i__ - 1];
	ft[i__] += dpinv * 9.81f * ment[i__ + i__ * 70 - 71] * (hp[i__ - 1] - 
		h__[i__ - 1] + tconv[i__] * 864.29999999999995f * (qconv[i__] 
		- qent[i__ + i__ * 70 - 71])) * cpinv;
	ft[i__] += wt[i__] * .05f * 1494.3f * water[i__] * (tconv[i__ + 1] - 
		tconv[i__]) * dpinv * cpinv;
	fq[i__] += dpinv * 9.81f * (amp1 * (qconv[i__ + 1] - qconv[i__]) - ad 
		* (qconv[i__] - qconv[i__ - 1]));
	i__2 = i__ - 1;
	for (k = 1; k <= i__2; ++k) {
	    awat = elij[k + i__ * 70 - 71] - (1.f - ep[i__ - 1]) * clw[i__ - 
		    1];
	    awat = dmax(awat,0.f);
	    fq[i__] += dpinv * 9.81f * ment[k + i__ * 70 - 71] * (qent[k + 
		    i__ * 70 - 71] - awat - qconv[i__]);
/* L480: */
	}
	i__2 = inb;
	for (k = i__; k <= i__2; ++k) {
	    fq[i__] += dpinv * 9.81f * ment[k + i__ * 70 - 71] * (qent[k + 
		    i__ * 70 - 71] - qconv[i__]);
/* L490: */
	}
	fq[i__] = fq[i__] + evap[i__ - 1] * .05f + (mp[i__] * (qp[i__] - 
		qconv[i__]) - mp[i__ - 1] * (qp[i__ - 1] - qconv[i__ - 1])) * 
		9.81f * dpinv;
/* L500: */
    }

/*   *** Adjust tendencies at top of convection layer to reflect  *** */
/*   ***       actual position of the level zero CAPE             *** */

    fqold = fq[inb];
    fq[inb] *= 1.f - frac;
    fq[inb - 1] += frac * fqold * ((phconv_hpa__[inb] - phconv_hpa__[inb + 1])
	     / (phconv_hpa__[inb - 1] - phconv_hpa__[inb])) * lv[inb - 1] / 
	    lv[inb - 2];
    ftold = ft[inb];
    ft[inb] *= 1.f - frac;
    ft[inb - 1] += frac * ftold * ((phconv_hpa__[inb] - phconv_hpa__[inb + 1])
	     / (phconv_hpa__[inb - 1] - phconv_hpa__[inb])) * cpn[inb - 1] / 
	    cpn[inb - 2];

/*   ***   Very slightly adjust tendencies to force exact   *** */
/*   ***     enthalpy, momentum and tracer conservation     *** */

    ents = 0.f;
    i__1 = inb;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ents += (cpn[i__ - 1] * ft[i__] + lv[i__ - 1] * fq[i__]) * (
		phconv_hpa__[i__] - phconv_hpa__[i__ + 1]);
/* L680: */
    }
    ents /= phconv_hpa__[1] - phconv_hpa__[inb + 1];
    i__1 = inb;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ft[i__] -= ents / cpn[i__ - 1];
/* L640: */
    }
/* ************************************************ */
/* **** DETERMINE MASS DISPLACEMENT MATRIX */
/* ***** AND COMPENSATING SUBSIDENCE */
/* ************************************************ */
/* mass displacement matrix due to saturated up-and downdrafts */
/* inside the cloud and determine compensating subsidence */
/* FUP(I) (saturated updrafts), FDOWN(I) (saturated downdrafts) are assumed to be */
/* balanced by  compensating subsidence (SUB(I)) */
/* FDOWN(I) and SUB(I) defined positive downwards */
/* NCONVTOP IS THE TOP LEVEL AT WHICH CONVECTIVE MASS FLUXES ARE DIAGNOSED */
/* EPSILON IS A SMALL NUMBER */
    sub[1] = 0.f;
    *nconvtop = 1;
    i__1 = inb + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = inb + 1;
	for (j = 1; j <= i__2; ++j) {
	    if (j == nk) {
		fmass[j + i__ * 70] += m[i__ - 1];
	    }
	    fmass[j + i__ * 70] += ment[j + i__ * 70 - 71];
	    if (fmass[j + i__ * 70] > 1e-20f) {
/* Computing MAX */
		i__3 = max(*nconvtop,i__);
		*nconvtop = max(i__3,j);
	    }
	}
	if (i__ > 1) {
	    sub[i__] = fup[i__ - 2] - fdown[i__ - 1];
	}
    }
    ++(*nconvtop);
    return 0;
}
/*----------------------------------------------------------------------------*/