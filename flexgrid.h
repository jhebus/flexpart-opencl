/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXGRID_H__
#define __FLEXGRID_H__

#include "flexocl.h"
#include "flexarr.h"

#define NAME_LENGTH 16		// length of parameter name

// parameter name index
enum {  P_NAME=0, Lat_NAME, Lon_NAME,
		U_NAME, Us_NAME,
		V_NAME, Vs_NAME,
		W_NAME, Ws_NAME,
		Z_NAME, Zs_NAME,
		T_NAME, Ts_NAME,
		RH_NAME, RHs_NAME,
		Ps_NAME, 
		PBL_NAME, TROP_NAME,
		SHF_NAME, ZMF_NAME, MMF_NAME,
		UST_NAME, WST_NAME, OLI_NAME,
		Rho_NAME,
		A_NAME, B_NAME,
		END_NAME };


/**
 flexGrid holds grid parameters and field varaible names.
 <br>
 The constructor includes the following calls to set the default variable names.
 If variables are named differently in data files, you must set them in the flexGrid correctly.  
 <pre>
 setName(P_NAME,"Pre");        //Pressure, the first grid variable, must be 1D-double.
 setName(Lat_NAME,"Lat");      //Latitude, the second grid variable, must be 1D-double.
 setName(Lon_NAME,"Lon");      //Longitude, the third grid variable, must be 1D-double.
 setName(U_NAME,"U");          //U-wind, must be 3D-float.
 setName(Us_NAME,"U10");       //Surface u-wind, must be 2D-float.
 setName(V_NAME,"V");          //V-wind, must be 3D-float.
 setName(Vs_NAME,"V10");       //Surface v-wind, must be 2D-float.
 setName(W_NAME,"W");          //W-wind, must be 3D-float.
 setName(Ws_NAME,"W9950");     //Near surface w-wind, must be 2D-float.
 setName(Z_NAME,"GPH");        //Geopotential height, must be 3D-float.
 setName(Zs_NAME,"GPHs");      //Surface geopotential height, must be 2D-float.
 setName(T_NAME,"T");          //Air temperature, must be 3D-float.
 setName(Ts_NAME,"T2");        //Surface air temperature, must be 2D-float.
 setName(RH_NAME,"RH");        //Relative humidity, must be 3D-float.
 setName(RHs_NAME,"RH2");      //Surface relative humidity, must be 2D-float.
 setName(Ps_NAME,"Ps");        //Surface pressure, must be 2D-float.
 setName(SHF_NAME,"SHF");      //Sensible heat flux, must be 2D-float.
 setName(ZMF_NAME,"ZMF");      //Zonal momentum flux, must be 2D-float.
 setName(MMF_NAME,"MMF");      //Meridional momentum flux, must be 2D-float.
 setName(PBL_NAME,"PBL");      //Planetary boundary layer height, must be 2D-float.
 setName(TROP_NAME,"TROP");		//Thermal tropopause height, must be 2D-float.
 setName(UST_NAME,"Ustar");		//Friction velocity, must be 2D-float.
 setName(WST_NAME,"Wstar");		//Convective velocity scale, must be 2D-float.
 setName(OLI_NAME,"OLI");		//Monin-Obukhov length, must be 2D-float.
 setName(Rho_NAME,"Rho");       //Air density.
 setName(A_NAME,"A");			//half pressure level parameter as in P=A+B*Ps.
 setName(B_NAME,"B");			//half pressure level parameter as in P=A+B*Ps.
 </pre>
 */

class flexGrid
{
public:
	//! Constructor
	flexGrid();

	//! Destructor
	~flexGrid();

	/** Reads data of grid variables in netCDF file.
	 @param fname File name.
	 @param ns Number of seconds between two field datasets.
	 @param cuda Flag for loading grid data to GPU device.
	 */
	void readData(const char *fname, int ns, bool cuda);

	/** Sets variable name.
	 @param idx Index of variable name array.
	 @param name Name for variable.
	 */
	void setName(int idx, const char *name);

	/** Gets variable name.
	 @param idx Index of variable name array.
	 */
	const char *name(int idx) const { return Names[idx]; }

	//! Clears device memory.
	void clearDevice();
        
        // Updates device pointers
	void updateDevice();
        
        //update the host pointers
        void updateHost();

private:


        

public:
	flexGrid
		//! Nested grid (regional)
		*nested;

	Grid
		//! Grid data in host computer
		host;
        
        //we do have device pointer here as the data is static
        cl_mem 
                //Grid data on device
                device;
private:

	char
		Names[END_NAME][NAME_LENGTH];		// field names
};

#endif
