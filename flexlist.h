/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXLIST_H__
#define __FLEXLIST_H__

#include "flexclass.h"
#include "flexconv.h"
//#include "flexgl.h"
#include <vector>
#include <string>

#define MAX_RELEASE 720
#define MAX_SLEN 64

/**
 flexList manages released particle classes
 */

class flexList
{
public:
	//! Constructor
	flexList();

	//! Destructor
	~flexList();

	/**
	 Releases particles. Refer to the release function of flexClass for details.
	 */
	void release(const char *name, int n, float x0, float y0, float ds, float z0, float dz, float mass, int halflife);

	/**
	 Releases particles. Refer to the rrelgrid function of flexClass for details.
	 */
	void relgrid(const char *name, float x0, float dx, int nx, float y0, float dy, int ny, float z0, float dz, int nz);

	/**
	 Adds release using the release parameters of the previous cal to release(...) or relgrid(...).
	 */
	int add(double ctime, const flexCount &Ct, int idx);

	/**
	 Removes released particle classes whos age is older than sec seconds.
	 */
	void remove(int sec);

	/**
	 Removes all particle classes in the list.
	 */
	void clear();

	/**
	 Sets grid for particle counting to all particle classes.
	 */
	void cgrid(const flexCount &C);

	/**
	 Sets air mass.
	 */
	void mass(const flexGrid &G, const flexField &F);

	/**
	 Starts dispersion modeling for all particle classes.
	 */
	void dispersion(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &C);

	/**
	 Starts trajectory calculation for all particle classes.
	 */
	void trajectory(const flexGrid &G, const flexParam &P, const flexField &F);

        /**
         * Calculate the new mass of the particles, based on radioactive decay
         */
        void radioactive(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &C);
        
	/**
	 Shows particle positions by OpenGL.
	 */
	//void show(flexGL &GL, double ctime, bool cuda);

	/**
	 Saves particle positions of all particle classes.
	 */
	void saveXYZ(const char *path, const Grid &G, const Parameter &P, const Field &F, double ctime);

	/**
	 Saves particle counting results all particle classes.
	 */
	void saveCNT(const char *path, const Parameter &P, double ctime);

	/**
	 Gets the number of releases in the list.
	 */
	size_t count() const { return _name.size(); }

	/**
	 Resets the list.
	 */
	void reset();

	/**
	 Sets a new initial position (to be called by flexGL).
	 */
	void newXY(float x, float y)
	{
		_x0[0]=x;
		_y0[0]=y;
	}

private:
	flexConv *Cv;		// for moist convection

	// linked list for particle classes
	struct node {
		flexClass *ptr;
		node* next;
		node(flexClass *c, node *n) : ptr(c), next(n) { }
	};

	node* _node;

	// variable for keeping release parameters

	int
		_reltype;			// release type

	std::vector<int>
		_n,				// number of particles
		_nx,				// number of x-grids
		_ny,				// number of y-reids
		_nz;				// number of y-reids
	
	std::vector<float>
                _mass,                          //the mass of each particle in a release (assumes homogenous partilces)
                _half_life,                     //the half life of particles in a release (assumes homogenous partilces)
		_x0,				// lontitude [deg]
		_dx,				// lontitude grid
		_y0,				// latitude [deg]
		_dy,				// latitude grid
		_ds,				// distance [m] from (x0,y0)
		_z0,				// altitude [m]
		_dz;				// altitude above z0

	std::vector<std::string>
		_name;				// release names
};

#endif
