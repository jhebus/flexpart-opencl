/* 
 * File:   flexocl.h
 * Author: jhebus
 *
 * Created on 03 July 2013, 21:08
 */

#ifndef FLEXOCL_H
#define	FLEXOCL_H

#ifdef __APPLE__
#include <OpenCL/CL.h>
#else
#include <CL/cl.h>
#endif

//used to include  or exclude the radio active calculations
#define RADIOACTIVE 0

//debugging tool
#define KERNEL_CALCULATIONS 1

/*
 * These are the indices to the kernel "state machine". Basically which branch
 * of the case statement in the kernel is executed.
 */ 
#define INTERPOLATION_KERNEL            ( 0 )       
#define INTERPOLATION_KERNEL_WITH_FLUX  ( 1 )       
#define DISPERSION_KERNEL               ( 2 )
#define TRAJECTORY_KERNEL               ( 3 )
#define CONVECTION_KERNEL               ( 4 )
#define RADIOACTIVE_KERNEL              ( 5 )
#define MASS_KERNEL                     ( 6 )
#define WET_DEPOSITION_KERNEL           ( 7 )
#define DRY_DEPOSITION_MATTER_KERNEL    ( 8 )
#define DRY_DEPOSITION_GAS_KERNEL       ( 9 ) 
#define RELEASE_KERNEL                  ( 10 )

//This value was lifted from felxparam.cpp and hardcoded (shoudl be 2000000)
#define ARRAY_SIZE 20

// Number of arguments per kernel argument type
#define KERNEL_CHOICE_ARGS      ( 1 )
//#define ERROR_ARGS              ( 2 )      // use this if you use the probes in the kernel     
#define ERROR_ARGS              ( 0 )           
#define INTERPOLATION_ARGS      ( 31 )
#define GRID_ARGS               ( 1 )
#define PARAM_ARGS              ( 17 )
#define CLASS_ARGS              ( 17 )
#define FIELD_ARGS              ( 14 )
#define CONVECTION_ARGS         ( 0 )
#define MASS_ARGS               ( 0 ) 
/*----------------------------------------------------------------------------*/
/**
 Structures for grid data to be used by a NVIDIA device and the host computer.
 */
typedef struct {
	int
		//! Hybrid grid flag
		hybrid,
		//! Global grid indicator
		gl,
		//! Number of vertical levels
		nl,
		//! Number of latitude grids (data columns)
		nc,
		//! Number of longitude grid (data rows)
		nr,
		//! nxy=nr*nc;
		nxy,
		//! number of seconds between field datasets
		ns;
	float
		//! Start longitude [deg]
		x0,
		//! End longitude [deg]
		x1,
		//! Longitude grid [deg]
		dx,
		//! Start latitude [deg]
		y0,
		//! End latitude [deg]
		y1,
		//! Latitude grid [deg]
		dy;
	float
		//! Pressure levels [Pa]
		P[256],
		//! half pressure level parameters [P=A+B*Ps]
		A[256], B[256];
} Grid;

/**
 Structures for parameter data to be used by a NVIDIA device and the host computer.
 */
typedef struct {
	float
		//! Turbulent diffusivity for horizontal components in the troposphere [m2/s]
		d_trop,
		//! Turbulent diffusivity for vertical component in the stratosphere [m2/s]
		d_strat,
		//! Factor by which standard deviations of winds at grid points surrounding
		//! particle positions are scaled to yield the scales for the mesoscale wind velocity fluctuations  
		turbmesoscale;
	int
		//! Mode flag: 1 for forward and -1 for backward simulation
		ldirect,
		//! Synchronisation time of all particles [s]
		lsynctime,
		//! Maximum time step for integration [s]
		ldt,
		//! Fine loops for integration of turbulent wind (must >1)
		ifine,
		//! Time step control factor
		ctl,
		//! Moist convection flag: 1 for yes or 0 no.
		lconv,
		//! Flux interpolation flag: 1 for yes or 0 no.
		lflx,
		//! Size of Gauss random data array
		ng;
	float
		//! Uniform random number array
		*Rand,
		//! Guassian random number array
		*Gauss;
	int
		//! Output interval [s]
		outsec,
		//! Trajectory length [s]
		lensec,
		//! NVIDIA device flag: 1 for yes or 0 for no
		openCL,
		//! Number of threads for using OpenMP
		openmp,
		//! Performance timing flag: 1 for yes or 0 for no.
		timing,
		// Dispersion flag: 1 for yes or 0 for no.
		dispersion; 
} Parameter;
/*----------------------------------------------------------------------------*/
//same as parameter but not arrays
typedef struct {
	float
		//! Turbulent diffusivity for horizontal components in the troposphere [m2/s]
		d_trop,
		//! Turbulent diffusivity for vertical component in the stratosphere [m2/s]
		d_strat,
		//! Factor by which standard deviations of winds at grid points surrounding
		//! particle positions are scaled to yield the scales for the mesoscale wind velocity fluctuations  
		turbmesoscale;
	int
		//! Mode flag: 1 for forward and -1 for backward simulation
		ldirect,
		//! Synchronisation time of all particles [s]
		lsynctime,
		//! Maximum time step for integration [s]
		ldt,
		//! Fine loops for integration of turbulent wind (must >1)
		ifine,
		//! Time step control factor
		ctl,
		//! Moist convection flag: 1 for yes or 0 no.
		lconv,
		//! Flux interpolation flag: 1 for yes or 0 no.
		lflx,
		//! Size of Gauss random data array
		ng;
	int
		//! Output interval [s]
		outsec,
		//! Trajectory length [s]
		lensec,
		//! NVIDIA device flag: 1 for yes or 0 for no
		openCL,
		//! Number of threads for using OpenMP
		openmp,
		//! Performance timing flag: 1 for yes or 0 for no.
		timing,
		// Dispersion flag: 1 for yes or 0 for no.
		dispersion; 
} DeviceParameter;
/*----------------------------------------------------------------------------*/
/**
 Structures for field data to be used by a NVIDIA device and the host computer.
 */
typedef struct {
	float
		//! U-wind [m/s] (3D)
		*U,
		//! V-wind [m/s] (3D)
		*V,
		//! W-wind [m/s] (3D, converted from Ps/s)
		*W,
		//! Geopotential height [m] (3D)
		*Z,
		//! Air density [kg/m3]  (3D)
		*Rho,
		//! Air density gradient [kg/m4] (3D)
		*DRho,
		//! Air temperature [K] (3D)
		*T,
		//! Relative humidity [%] (3D)
		*RH;
	float
		//! Planetary boundary layer [m above surface] (2D)
		*PBL,
		//! Thermal tropopause [m above surface] (2D)
		*Tropo,
		//! Friction velocity [m/s] (2D)
		*Ustar,
		//! Convective velocity scale [m/s] (2D)
		*Wstar,
		//! Monin-Obukhov length [m above surface] (2D)
		*Oli,
		//! Surface pressure [Pa] (2D)
		*Ps;
} Field;

typedef struct {
    cl_mem
		//! U-wind [m/s] (3D)
		U,
		//! V-wind [m/s] (3D)
		V,
		//! W-wind [m/s] (3D, converted from Ps/s)
		W,
		//! Geopotential height [m] (3D)
		Z,
		//! Air density [kg/m3]  (3D)
		Rho,
		//! Air density gradient [kg/m4] (3D)
		DRho,
		//! Air temperature [K] (3D)
		T,
		//! Relative humidity [%] (3D)
		RH,	
		//! Planetary boundary layer [m above surface] (2D)
		PBL,
		//! Thermal tropopause [m above surface] (2D)
		Tropo,
		//! Friction velocity [m/s] (2D)
		Ustar,
		//! Convective velocity scale [m/s] (2D)
		Wstar,
		//! Monin-Obukhov length [m above surface] (2D)
		Oli,
		//! Surface pressure [Pa] (2D)
		Ps;
} DeviceField;


/*----------------------------------------------------------------------------*/
/**
 Structures for particle data to be used by a NVIDIA device and the host computer.
 */
typedef struct {
	int
		//! Number of particles
		np,	
		//! Random number index
		*Idx,
		//! Grid index
		*Gdx,
		//! Particle index (this is never used?)
		*Pdx;
	float
                //! Mass of the particle [kg]
                *Mass,
                //  Radioactivity of the particle in %Bq (see note 1, flexclass.cpp)
                *Radioactivity,
		//! Longitude [deg]
		*X,
		//! Latitude [deg]
		*Y,
		//! Altitude [m]
		*Z,
		//! Reflection flag
		*Fg,
		//! Turbulent u-wind of the previous step [m/s]
		*Up,
		//! Turbulent v-wind of the previous step [m/s]
		*Vp,
		//! Turbulent w-wind of the previous step [m/s]
		*Wp,
		//! Meso-scale turbulent u-wind of the previous step [m/s]
		*Um,
		//! Meso-scale turbulent v-wind of the previous step [m/s]
		*Vm,
		//! Meso-scale turbulent w-wind of the previous step [m/s]
		*Wm,
		//! Surface height [m]
		*Zs,
		//! Boundary layer height [m above surface]
		*Zb;
} Class;

typedef struct {
	int
		//! Number of particles
		np;
        cl_mem
		//! Random numner index
		Idx,
		//! Grid index
		Gdx,
		//! Particle index (this is never used?)
		Pdx;
	cl_mem
                 //! Mass of the particle [kg]
                Mass,
                //  Radioactivity of the particle in %Bq (see note 1, flexclass.cpp)
                Radioactivity,
		//! Longitude [deg]
		X,
		//! Latitude [deg]
		Y,
		//! Altitude [m]
		Z,
		//! Reflection flag
		Fg,
		//! Turbulent u-wind of the previous step [m/s]
		Up,
		//! Turbulent v-wind of the previous step [m/s]
		Vp,
		//! Turbulent w-wind of the previous step [m/s]
		Wp,
		//! Meso-scale turbulent u-wind of the previous step [m/s]
		Um,
		//! Meso-scale turbulent v-wind of the previous step [m/s]
		Vm,
		//! Meso-scale turbulent w-wind of the previous step [m/s]
		Wm,
		//! Surface height [m]
		Zs,
		//! Boundary layer height [m above surface]
		Zb;
} DeviceClass;

/**
 Structures for interpolation data to be used by a NVIDIA device and the host computer.

 Grid indexes
 <pre>
    f4               f3
 (i2,j1)-----------(i2,j2)
    |                 |
    |                 |
    |                 |
    |                 |
 (i1,j1)-----------(i1,j2)
   f1                f2
 </pre>
 */
typedef struct {
	int
		i1,
		//! Latitude grid indexes
		i2,
		j1,
		//! Longitude grid indexes
		j2;
	int
		k1, k2, k3,
		//! Vertical grid indexes
		k4;
	float
		f1, f2, f3,
		//! Horizontal interpolation factors
		f4;
	float
		fa1, fa2, fa3, fa4,
		fb1, fb2, fb3,
		//! Vertical interpolation factors
		fb4;
} Factor;

/**
 Structures for turbulent parameters to be used by a NVIDIA device and the host computer.
 */
typedef struct {
	float
		sigu, sigv, sigw, dsigwdz, dsigw2dz, tlu, tlv, tlw;
} Turb;

/**
 Structures for latitude/longitude data to be used by a NVIDIA device and the host computer.
 */
typedef struct {
	float
		x, y;  //latitude/longitude data
} XY;

#define NA 70		// this must equal NA in convect43c.f
#define NTHR 16

/**
 Structures for moist convection to be used by a NVIDIA device and the host computer.
 */
typedef struct {
    float
        FSUM[NA],
        ZIJ[NA],
        MASS[NA],
        SUB[NA],
        FMASS[NA*NA],
        FRAC[NA*NA],
        MENT[NA*NA],
        QENT[NA*NA],
        ELIJ[NA*NA],
        SIJ[NA*NA];
} Convect;

/**
 Structures for moist convection to be used by a NVIDIA device and the host computer.
 */
typedef struct {
    Convect cpa[NTHR];
} ConvectArray;

/*----------------------------------------------------------------------------*/
/**
 * Allocate size bytes of space on the device, and save the pointer to ptr
 * 
 * @param ptr
 * @param size
 * @param hostPtr
 */
void mallocOnDevice(cl_mem *ptr, size_t size, void* hostPtr);

/**
 * Copy size bytes from src on the host to dest on the device. Dest should 
 * already be allocated.
 * 
 * @param dest
 * @param src
 * @param size
 */
void setDeviceData(cl_mem dest, void* src, size_t size);

/**
 * Copy size bytes from src on the device to dest on the host
 * 
 * @param dest
 * @param src
 * @param size
 */
void getDeviceData(void* dest, cl_mem src, size_t size);

/**
 * Set size bytes of memory pointed to by ptr to 0s on the device
 * @param ptr
 * @param size
 */
void clearDeviceMemory(void *ptr, size_t size);

/**
 * Free memory allocated on the device
 * @param ptr
 */
void freeOnDevice(cl_mem ptr);

/**
 * This should be called once, at the start of the simulator to setup the openCL
 * internals.
 * 
 * @param device_choice         the number of the device to use
 * @param platform_choice       the number of the platform to choose
 */
void initOpenCL(int platform_choice, int device_choice, bool list_details);

/**
 * 
 * @return max number of compute units for the chosen device
 */
int getMaxComputeUnits();

/**
 * Release the resources that were allocated by initOpenCL
 */
void tearDownOpenCL();

/**
 * Create an openCL kernel
 * @param kernelFunction
 * @param kernel
 */
//void createKernel(char* kernelFunction, cl_kernel *kernel);

void setKernelArgs(int kernel_choice, int argNum, int dataSize, void* data);


/**
 * Return the cl_kernel variable for the kernel.
 */
cl_kernel getSuperKernel();

/**
 * Run the kernel, and wait till it completes. It is assumed that all kernel 
 * arguments have been set before calling this.
 */
void runKernelBlocking(size_t* workGrp_size, size_t workItem_size);

/**
 * This should copy the data from one device buffer to another. Not sure if this
 * will/should work in openCL
 * 
 * @param dst
 * @param src
 * @param size
 */
void copyDeviceData(cl_mem dst, const void *src, size_t size);
/*----------------------------------------------------------------------------*/
/**
 * Set all the openCL kernel arguments to 0 or NULL before the necessary code 
 * sets the actual values. Removes the need to worry about not having set the 
 * arguments later
 */
void initKernelArgs();
/*----------------------------------------------------------------------------*/    
typedef unsigned long long timestamp_t;

timestamp_t get_timestamp ();
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
 * We still need a class to compile and run kernels. There will only need to be 
 * 1 context and 1 command queue (as there is only 1 device). This class will
 * hold this information and enable other classes to use the device without 
 * having to know anything.
 */
/*
class flexOCL {
public:
    
    //constructor
    flexOCL(int device_choice, int platform_choice);
    
    //destructor
    ~flexOCL();
    
    void runKernel(cl_kernel kernel, size_t workGrp_size, size_t workItem_size);
    
    void createKernel(char* sourceFile, char* path, char* kernelFunction, cl_kernel kernel);
    
    
private:
    
    cl_context          context;
    cl_command_queue    command_queue;
    
    cl_uint             num_platforms;
    cl_uint             num_devices;
    
    cl_platform_id*     clPlatformIDs;
    cl_device_id*       clDeviceIDs;
    
    cl_int              errorNum;
};
 */ 
#endif	/* FLEXOCL_H */

