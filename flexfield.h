/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXFIELD_H__
#define __FLEXFIELD_H__

#include "flexocl.h" 
#include "flexgrid.h"
#include "flexparam.h"
#include "flexarr.h" 

/**
 flexField holds all necessary meteorological data for modelling.
 <br>
 Input data must be in netCDF format. The coordinate variables include pressure,
 latitude, and longitude; and must be of double type. 2D field data must be layout as
 field(latitude,longitude) and 3D field as field(pressure,latitude,longitude).
 Field data must be of float type. Refer to flexGrid for variable names and data types.
 */

class flexField
{
public:
	//! Constructor
	flexField();

	//! Destructor
	~flexField();

	/** Reads field data.
	 @param fname File name.
	 @param time Julian date and time number.
	 @param G flexGrid obejct.
	 @param P flexParam object.
	*/
	void readData(const char *fname, double time, const flexGrid &G, const flexParam &P);

	/** Interpolates field data and save results to itself.
	 @param F1 First field dataset.
	 @param F2 Second field dataset.
	 @param time time between that of F1 and F2.
	 @param G flexGrid object.
	 @param P flexParam object.
	 @param flag First time interpolation flag.
	 */
	void interpolate(flexField &F1, flexField &F2, double time, const flexGrid &G, const flexParam &P, bool flag=false);

	//! Gets the time of this field dataset
	double time() const { return _time; }

	//! Clears device memories
	void clearDevice();
        
        // Updates device pointers
	void updateDevice();

// Update host pointer
        void updateHost();

private:


        
//Executes the interpolation kernel with the specified arguments and wait till 
//completion
        //void runInterpolatonKernel(cl_mem result, cl_mem F1, cl_mem F2, float f1, float f2, int n);
        void runInterpolationKernel(DeviceField *results, DeviceField *F1, DeviceField *F2, double field_mul_1, double field_mul_2, int kernel_choice);
        void runTestKernel();
// Sets 2D variable to 3D variable
	void setSurface(flexArr<float> &V, const flexArr<float> &Vs, const Grid &grid);

// find layers under the surface
	void zindex(const Grid &G);

// density
	void rho(const Grid &G);

// density gradient
	void drhodz(const Grid &G);

// convert Pa/s to m/s
void pa2m(const Grid &G);

// friction velocity
	void ustar(const Grid &G);

// convective velocity scale
	void wstar(const Grid &G);

// Monin-Obukhov length
	void obukhov(const Grid &G);

// planetary boundary layer height
	void pbl(const Grid &G);

// tropopause layer height
	void trop(const Grid &G);

// Profile
	void profile(const Grid &G);

// Pressure
	void pressure(const Grid &G);

// Initializes arrays
	void initField(int nl, int nr, int nc);

// Sets field pointers
	void setPtr(bool cuda);

// pressure on k-level
	double pk(const Grid &G, int k, int i, int j)
	{
		double p=G.P[k];
		if (G.hybrid) p=G.A[k]+G.B[k]*Ps(i,j);
		return p;
	}

public:
	flexField
		//! Nested field (regional)
		*nested;

	Field
		//! Field data in host computer.
		host;
        DeviceField
		//! Field data in GPU device. dont think that i need this
		*dev_struct;

        
        cl_mem d_field_data, d_field_offsets;
        
private:

	const double
		ga,				// gravity acceleration of earth [m/s**2]
		cpa,			// specific heat for dry air
		karman;			// Karman's constant

	flexArr<float>
// 3D field (nl,nr,nc)
		U, V, W,		// winds in x[m/s],y[m/s],z[Pa/s] direction
		T,				// air temperature [K]
		Z,				// geopotential height [m]
		RH,				// relative humidity [%]
		Rho,			// air density [kg/m3]
		DRho,			// drho/dz
// 2D field (nr,nc)
		Ps,				// surface pressure [Pa]
		Zs,				// surface geopotential height [m]
		PBL,			// planetary boundary layer [m above surface]
		Tropo,			// thermal tropopause [m above surface]
		SHF,			// sensible heat fluex [W/m2] (positive means surface to air!)
		ZMF,			// zonal momentum flux [N/m2]
		MMF,			// meridional momentum flux [N/m2]
		Ustar,			// friction velocity [m/s]
		Wstar,			// convective velocity scale [m/s]
		Oli,			// Monin-Obukhov length [m above surface]
		Wrk;			// work space

	flexArr<int>
		Idx;			// surface layer index

	cl_mem lengths;                 // This points to an array, used to store
                                        // lengths of the field arrays copied over
// 3D field in device (float)
        //cl_mem
	//	d_U, d_V, d_W, d_Z, d_Rho, d_DRho, d_T, d_RH,

// 2D field in device (float)
	//	d_PBL, d_Tropo, d_Ustar, d_Wstar, d_Oli, d_Ps;

	double
		_time;			// Julian date and time of dataset
        
                
        bool    // using OpenCL
                openCL; 
        

        
        
};

#endif
