/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexutility.h"
#include "flexocl.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


int cal2jul(int yy, int mm, int dd)
{
	yy -= mm < 3;
	mm += 12 * (mm < 3);
	int a = int(365.25 * (yy + 4716)),
		b = int(30.6001 * (mm + 1));
	return int(a + b + dd - 2416556);
}


void jul2cal(int &yy, int &mm, int &dd, int &hh, int &mi, double jul)
{
	const double sd = 1.0e-11; // this constant may be OS or compiler dependent

	int a = int(jul + 2415032),
		b = int(a + 1524),
		c = int((double(b) - 122.1) / 365.25),
		d = int(365.25 * c),
		e = int(double(b - d) / 30.6001),
		f = int(30.6001 * e);

	dd = b - d - f;
	mm = e - 1 - 12 * (e > 13);
	yy = c - 4715 - (mm > 2);

	jul -= floor(jul);

	if (jul == 0.0) {
		hh = 0;
		mi = 0;
	}
	else {
		jul += sd;
		jul *= 24;
		hh = int(jul);
		jul = (jul - hh) * 60.0;
		mi = int(jul);
	}
}


/********************************************************************
 * Ref: Numerical Recipe in C
 ********************************************************************/

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

float ran1(long *idum)
{
	int j;
	long k;
	static long iy=0;
	static long iv[NTAB];
	float temp;

	if (*idum <= 0 || !iy) {
		if (-(*idum) < 1) 
                    *idum=1;
		else 
                    *idum = -(*idum);
		for (j=NTAB+7;j>=0;j--) {
			k=(*idum)/IQ;
			*idum=IA*(*idum-k*IQ)-IR*k;
			if (*idum < 0) *idum += IM;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ;
	*idum=IA*(*idum-k*IQ)-IR*k;
	if (*idum < 0) *idum += IM;
	j=iy/NDIV;
	iy=iv[j];
	iv[j] = *idum;
	if ((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}

float ran2(long &idum, long &iy, long *iv, int nv)
{
	int j;
	long k;
	float temp;

	if (nv < NTAB) errmsg("nv < NTAB in ran2");

	if (idum <= 0 || !iy) {
		if (-idum < 1) 
                    idum=1;
		else 
                    idum = -idum;
                
		for (j=NTAB+7;j>=0;j--) {
			k=idum/IQ;
			idum=IA*(idum-k*IQ)-IR*k;
			if (idum < 0) idum += IM;
			if (j < NTAB) iv[j] = idum;
		}
		iy=iv[0];
	}
	k=idum/IQ;
	idum=IA*(idum-k*IQ)-IR*k;
	if (idum < 0) idum += IM;
	j=iy/NDIV;
	iy=iv[j];
	iv[j] = idum;
	if ((temp=AM*iy) > RNMX) 
            return RNMX;
	else 
            return temp;
}

#undef IA
#undef IM
#undef AM
#undef IQ
#undef IR
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX

float gasdev1(long *idum)
{
	static int iset=0;
	static float gset;
	float fac,rsq,v1,v2;

	if (*idum < 0) iset=0;
	if  (iset == 0) {
		do {
			v1=2.0*ran1(idum)-1.0;
			v2=2.0*ran1(idum)-1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		gset=v1*fac;
		iset=1;
		return v2*fac;
	} else {
		iset=0;
		return gset;
	}
}

void gasdev2(long &idum, long &iy, long *iv, int nv, float &gset1, float &gset2)
{
	float fac,rsq,v1,v2;

	do {
		v1=2.0*ran2(idum,iy,iv,nv)-1.0;
		v2=2.0*ran2(idum,iy,iv,nv)-1.0;
		rsq=v1*v1+v2*v2;
	}
	while (rsq >= 1.0 || rsq == 0.0);

	fac=sqrt(-2.0*log(rsq)/rsq);
	gset1=v1*fac;
	gset2=v2*fac;
}


void errmsg(const char *msg1, const char *msg2, const char *msg3)
{
	static char msg[1024];
	if (msg3) {
		sprintf(msg,"%s%s%s!\n",msg1,msg2,msg3);
	}
	else if (msg2) {
		sprintf(msg,"%s%s!\n",msg1,msg2);
	}
	else {
		sprintf(msg,"%s!\n",msg1);
	}
	throw msg;
}

#ifdef _DEBUG
void print_variable(const char *vname, float *ptr, int np, int l)
{
	float vmin=1.e15;
	float vmax=-1.e15;
	for (int i=0; i<np; i++) {
		if (ptr[i]<vmin) vmin=ptr[i];
		if (ptr[i]>vmax) vmax=ptr[i];
	}
	printf("%s,%02d,%.5f,%.5f\n",vname,l,vmin,vmax);
}
#endif

void clamp_variable(float *ptr, int np, float vmin, float vmax)
{
	for (int i=0; i<np; i++) {
		if (ptr[i]<vmin) ptr[i]=vmin;
		if (ptr[i]>vmax) ptr[i]=vmax;
	}
}
/*----------------------------------------------------------------------------*/
const char* decode_error(cl_int error)
{
    static const char* errorString[] = {
        "CL_SUCCESS",
        "CL_DEVICE_NOT_FOUND",
        "CL_DEVICE_NOT_AVAILABLE",
        "CL_COMPILER_NOT_AVAILABLE",
        "CL_MEM_OBJECT_ALLOCATION_FAILURE",
        "CL_OUT_OF_RESOURCES",
        "CL_OUT_OF_HOST_MEMORY",
        "CL_PROFILING_INFO_NOT_AVAILABLE",
        "CL_MEM_COPY_OVERLAP",
        "CL_IMAGE_FORMAT_MISMATCH",
        "CL_IMAGE_FORMAT_NOT_SUPPORTED",
        "CL_BUILD_PROGRAM_FAILURE",
        "CL_MAP_FAILURE",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "CL_INVALID_VALUE",
        "CL_INVALID_DEVICE_TYPE",
        "CL_INVALID_PLATFORM",
        "CL_INVALID_DEVICE",
        "CL_INVALID_CONTEXT",
        "CL_INVALID_QUEUE_PROPERTIES",
        "CL_INVALID_COMMAND_QUEUE",
        "CL_INVALID_HOST_PTR",
        "CL_INVALID_MEM_OBJECT",
        "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
        "CL_INVALID_IMAGE_SIZE",
        "CL_INVALID_SAMPLER",
        "CL_INVALID_BINARY",
        "CL_INVALID_BUILD_OPTIONS",
        "CL_INVALID_PROGRAM",
        "CL_INVALID_PROGRAM_EXECUTABLE",
        "CL_INVALID_KERNEL_NAME",
        "CL_INVALID_KERNEL_DEFINITION",
        "CL_INVALID_KERNEL",
        "CL_INVALID_ARG_INDEX",
        "CL_INVALID_ARG_VALUE",
        "CL_INVALID_ARG_SIZE",
        "CL_INVALID_KERNEL_ARGS",
        "CL_INVALID_WORK_DIMENSION",
        "CL_INVALID_WORK_GROUP_SIZE",
        "CL_INVALID_WORK_ITEM_SIZE",
        "CL_INVALID_GLOBAL_OFFSET",
        "CL_INVALID_EVENT_WAIT_LIST",
        "CL_INVALID_EVENT",
        "CL_INVALID_OPERATION",
        "CL_INVALID_GL_OBJECT",
        "CL_INVALID_BUFFER_SIZE",
        "CL_INVALID_MIP_LEVEL",
        "CL_INVALID_GLOBAL_WORK_SIZE",
    };

    const int errorCount = sizeof(errorString) / sizeof(errorString[0]);

    const int index = -error;

    if(error == 1){
        return "COMPILATION_FAILS";
    }
    printf("RAW ERROR : %d\n", error);
    return (index >= 0 && index < errorCount) ? errorString[index] : "";

}
/*----------------------------------------------------------------------------*/
size_t shrRoundUp( int group_size, int global_size ){
        int r = global_size % group_size;
        if( r == 0 )
            return global_size;
        return global_size + group_size - r;
}
/*----------------------------------------------------------------------------*/
char *file_contents(const char *filename, size_t *length)
{
    FILE *f = fopen(filename, "r");
    void *buffer;

    if (!f) {
        fprintf(stderr, "Unable to open %s for reading\n", filename);
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    *length = ftell(f);
    fseek(f, 0, SEEK_SET);

    buffer = malloc(*length+1);
    *length = fread(buffer, 1, *length, f);
    fclose(f);
    ((char*)buffer)[*length] = '\0';

    return (char*)buffer;
}