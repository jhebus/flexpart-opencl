/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXCOUNT_H__
#define __FLEXCOUNT_H__

#include "flexocl.h"
#include "flexarr.h"
#include "flexgrid.h"
#include "flexfield.h"


/**
 flexCount takes care of counting particles and calculating residence time in grids.
*/

class flexCount
{
public:
    //! Constructor
	flexCount();

    //! Destructor
	~flexCount();

    /** Sets particle counting grid.
     @param x0 Initial longitude [degree].
     @param nx Number of grids along the longitude.
     @param y0 Initial latitude [degree].
     @param ny Number of grids along the latitude.
     @param deg Grid size [degree].
     @param zm Initial altitude [m]
     */
	void set(float x0, int nx, float y0, int ny, float deg, float zm);

    /** Sets particle counting grid.
     @param G flexGrid object.
     @param F flexField object.
     */
	void set(const flexCount &Ct);

    /** Sets air mass.
     @param G flexGrid object.
     @param F flexField object.
     */
	void mass(const flexGrid &G, const flexField &F);

    /** Counts particle and calculates residence time [s].
     @param Cs fleClass object.
     @param Ct flexCount object.
     @param sync Synchronization time interval [s].
     */
	void count(const Class &Cs, flexCount &Ct, int sync);

    /** Saves particle counting results.
     @param fname File name.
	 @param P Parameter object.
     @param np Number of particles.
	 @param age Particle class age.
	 @param ctime Current time.
	 @param len Time record length.
     */
	void save(const char *fname, const Parameter &P, int age, double ctime, int len=0);

    //! Clears particle count.
	void clear() { return C.clear(); }

    //! Gets the initial grid longitude.
	float x0() const { return X(0); }

    //! Gets the initial grid latitude.
	float y0() const { return Y(0); }

    //! Gets the initial grid altitude.
	float zm() const { return Z(1); }

    //! Gets the grid size.
	float deg() const { return _deg; }

    //! Gets the number of grid along x.
	int nx() const { return X.nd(); }

	//! Gets the number of grid along y.
	int ny() const { return Y.nd(); }

	//! Gets the number of grid along z.
	int nz() const { return Z.nd(); }

	//! Gets the total number of grids.
	int nd() const { return C.nd(); }

private:

    /**
     * This function, as well as saving the particle count, now also saves the 
     * radiation data in Radiation
     * 
     * @param i
     * @param j
     * @param cnt
     * @param z
     * @param zb
     * @param rad
     */
    void cnt_f(int i, int j, double cnt, double z, double zb, double rad);

    void add_rt(float rt, int idx) { R(idx)+=rt; }

    void add_radiation(float rad, int idx) {Radiation(idx) += rad;}

    void add_cn(float cn, int idx) { C(idx)+=cn; }

    void updateDevice();

    void updateHost();

    void runMassKernel(cl_mem G, DeviceField *F, int itterations);

    void clearDevice();

private:
	flexArr<float>
		X, Y, Z,        		// grid coordinate
		A,				// grid area [m]
		M,				// air mass [kg/m2]
		C,				// particle count
		R,				// Residence time [s]
                Radiation;                      // Radioactive particles [Bq as % of mass]

	float
		_deg;

	bool
		_global;		// grid flag
        
        cl_mem d_X, d_Y, d_Z, d_M;
};

#endif
