/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXPARAM_H__
#define __FLEXPARAM_H__

#include "flexocl.h"
#include "flexarr.h"

/**
 flexParam contains parameters for modelling.
 */

class flexParam
{
public:
	//! Constructor
	flexParam();

	//! Destructor
	~flexParam();

	//! Update device data.
	void update();

	//! Clears device pointers
	void clearDevice();

public:
	Parameter
		//! Parameters in host computer.
		host;
        
        //float
        cl_mem
               // d_params,               //the parameters on the device (without arrays)
		d_Rand,
		d_Gauss;		// Guassian random numbers on the device
        
        
private:

// Updates device pointers
	void updateDevice();
        
//copy the device data back to the host
        void updateHost();

private:

	flexArr<float>
		Rand,			// random numbers between 0 to 1
		Gauss;			// Guassian random numbers on the host
        
        

};

#endif
