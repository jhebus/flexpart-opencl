/* convect43c.f -- translated by f2c (version 20060506).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "convect43c.h"

doublereal pow_dd(doublereal *a, doublereal *b) { return pow(*a, *b); }


/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;

/* ************************************************************************** */
/* ****                       SUBROUTINE CONVECT                        ***** */
/* ****                          VERSION 4.3c                           ***** */
/* ****                          20 May, 2002                           ***** */
/* ****                          Kerry Emanuel                          ***** */
/* ************************************************************************** */

/* Subroutine */ int convect_(integer *nd, integer *nl, real *delt, integer *
	iflag, real *precip, real *wd, real *tprime, real *qprime, real *cbmf,
	 real *ft, real *fq, real *sub, real *fmass, real *tconv, real *qconv,
	 real *qsconv, real *pconv_hpa__, real *phconv_hpa__, integer *
	nconvtop)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    real r__1, r__2;
    doublereal d__1, d__2;

    /* Local variables */
    real h__[70];
    integer i__, j, k;
    real m[70], b6, c6, ad, am, ep[70];
    integer nk;
    real by, rh, mp[70], th[70], qp[70], lv[70], hp[70], tp[70], gz[70], hm[
	    70], tv[70], wt[70], bf2, qp1, fac;
    integer icb;
    real dei, chi, dbo, tca;
    integer inb;
    real alt, cpn[70], sij[4900]	/* was [70][70] */, rat, clw[70], byp,
	     fup[70], qti, qsm;
    integer jtt;
    real tvp[70], tvx, tvy;
    integer inb1;
    real amp1, afac, cape, frac, dhdp, delm, elij[4900]	/* was [70][70] */, 
	    delp, dtma, asij, rdcp, plcl, awat, smid, cwat, evap[70], anum, 
	    sigp[70];
    integer nent[70];
    real bsum, sigt, ents, smin, ment[4900]	/* was [70][70] */, qent[4900]
	    	/* was [70][70] */, lvcp[70], qstm, delt0, coeff, capem, 
	    ahmin, ahmax, delti, altem, denom;
    integer ihmin;
    real damps, dtpbl, fqold, ftold, epmax, dtmin, revap, cpinv, dpinv, sjmin,
	     sjmax, fdown[70], scrit, water[70];
    extern /* Subroutine */ int tlift_(real *, integer *, integer *, real *, 
	    real *, real *, integer *, integer *, integer *, real *, real *, 
	    real *, real *);
    real stemp, defrac, dbosum, cbmfold, elacrit, tvaplcl, wdtrain, tvpplcl;


/* -cv *************************************************************************** */
/* -cv C. Forster, November 2003 - May 2004: */
/* -cv */
/* -cv The subroutine has been downloaded from Kerry Emanuel's homepage, */
/* -cv where further infos on the convection scheme can be found */
/* -cv http://www-paoc.mit.edu/~emanuel/home.html */
/* -cv */
/* -cv The following changes have been made to integrate this subroutine */
/* -cv into FLEXPART */
/* -cv */
/* -cv Putting most of the variables in a new common block */
/* -cv renaming eps to eps0 because there is some eps already in includepar */
/* -cv */
/* -cv removing the arrays U,V,TRA and related arrays */
/* -cv */
/* -cv renaming the original arrays T,Q,QS,P,PH to */
/* -cv TCONV,QCONV,QSCONV,PCONV_HPA,PHCONV_HPA */
/* -cv */
/* -cv Initialization of variables has been put into parameter statements instead */
/* -cv of assignment of values at each call, in order to save computation time. */
/* *************************************************************************** */

/* ----------------------------------------------------------------------------- */
/*    *** On input:      *** */

/*     T:   Array of absolute temperature (K) of dimension ND, with first */
/*           index corresponding to lowest model level. Note that this array */
/*           will be altered by the subroutine if dry convective adjustment */
/*           occurs and if IPBL is not equal to 0. */

/*     Q:   Array of specific humidity (gm/gm) of dimension ND, with first */
/*            index corresponding to lowest model level. Must be defined */
/*            at same grid levels as T. Note that this array will be altered */
/*            if dry convective adjustment occurs and if IPBL is not equal to 0. */

/*     QS:  Array of saturation specific humidity of dimension ND, with first */
/*            index corresponding to lowest model level. Must be defined */
/*            at same grid levels as T. Note that this array will be altered */
/*            if dry convective adjustment occurs and if IPBL is not equal to 0. */

/*     U:   Array of zonal wind velocity (m/s) of dimension ND, witth first */
/*            index corresponding with the lowest model level. Defined at */
/*            same levels as T. Note that this array will be altered if */
/*            dry convective adjustment occurs and if IPBL is not equal to 0. */

/*     V:   Same as U but for meridional velocity. */

/*     TRA: Array of passive tracer mixing ratio, of dimensions (ND,NTRA), */
/*            where NTRA is the number of different tracers. If no */
/*            convective tracer transport is needed, define a dummy */
/*            input array of dimension (ND,1). Tracers are defined at */
/*            same vertical levels as T. Note that this array will be altered */
/*            if dry convective adjustment occurs and if IPBL is not equal to 0. */

/*     P:   Array of pressure (mb) of dimension ND, with first */
/*            index corresponding to lowest model level. Must be defined */
/*            at same grid levels as T. */

/*     PH:  Array of pressure (mb) of dimension ND+1, with first index */
/*            corresponding to lowest level. These pressures are defined at */
/*            levels intermediate between those of P, T, Q and QS. The first */
/*            value of PH should be greater than (i.e. at a lower level than) */
/*            the first value of the array P. */

/*     ND:  The dimension of the arrays T,Q,QS,P,PH,FT and FQ */

/*     NL:  The maximum number of levels to which convection can */
/*            penetrate, plus 1. */
/*            NL MUST be less than or equal to ND-1. */

/*     NTRA:The number of different tracers. If no tracer transport */
/*            is needed, set this equal to 1. (On most compilers, setting */
/*            NTRA to 0 will bypass tracer calculation, saving some CPU.) */

/*     DELT: The model time step (sec) between calls to CONVECT */

/* ---------------------------------------------------------------------------- */
/*    ***   On Output:         *** */

/*     IFLAG: An output integer whose value denotes the following: */

/*                VALUE                        INTERPRETATION */
/*                -----                        -------------- */
/*                  0               No moist convection; atmosphere is not */
/*                                  unstable, or surface temperature is less */
/*                                  than 250 K or surface specific humidity */
/*                                  is non-positive. */

/*                  1               Moist convection occurs. */

/*                  2               No moist convection: lifted condensation */
/*                                  level is above the 200 mb level. */

/*                  3               No moist convection: cloud base is higher */
/*                                  then the level NL-1. */

/*                  4               Moist convection occurs, but a CFL condition */
/*                                  on the subsidence warming is violated. This */
/*                                  does not cause the scheme to terminate. */

/*     FT:   Array of temperature tendency (K/s) of dimension ND, defined at same */
/*             grid levels as T, Q, QS and P. */

/*     FQ:   Array of specific humidity tendencies ((gm/gm)/s) of dimension ND, */
/*             defined at same grid levels as T, Q, QS and P. */

/*     FU:   Array of forcing of zonal velocity (m/s^2) of dimension ND, */
/*             defined at same grid levels as T. */

/*     FV:   Same as FU, but for forcing of meridional velocity. */

/*     FTRA: Array of forcing of tracer content, in tracer mixing ratio per */
/*             second, defined at same levels as T. Dimensioned (ND,NTRA). */

/*     PRECIP: Scalar convective precipitation rate (mm/day). */

/*     WD:    A convective downdraft velocity scale. For use in surface */
/*             flux parameterizations. See convect.ps file for details. */

/*     TPRIME: A convective downdraft temperature perturbation scale (K). */
/*              For use in surface flux parameterizations. See convect.ps */
/*              file for details. */

/*     QPRIME: A convective downdraft specific humidity */
/*              perturbation scale (gm/gm). */
/*              For use in surface flux parameterizations. See convect.ps */
/*              file for details. */

/*     CBMF:   The cloud base mass flux ((kg/m**2)/s). THIS SCALAR VALUE MUST */
/*              BE STORED BY THE CALLING PROGRAM AND RETURNED TO CONVECT AT */
/*              ITS NEXT CALL. That is, the value of CBMF must be "remembered" */
/*              by the calling program between calls to CONVECT. */

/* ------------------------------------------------------------------------------ */

/*    ***  THE PARAMETER NA SHOULD IN GENERAL BE GREATER THAN   *** */
/*    ***                OR EQUAL TO  ND + 1                    *** */


/*      include 'includepar' */
/*      include 'includeconv' */

/* -cv====>Begin Module CONVECT    File convect.f      Undeclared variables */

/*     Argument variables */



/*     Local variables */


/*     integer jc,jn */
/*     real alvnew,a2,ahm,alv,rm,sum,qnew,dphinv,tc,thbar,tnew,x */

/* -cv====>End Module   CONVECT    File convect.f */
/*     REAL TOLD(NA) */

/* ----------------------------------------------------------------------- */

/*   ***                     Specify Switches                         *** */

/*   ***   IPBL: Set to zero to bypass dry adiabatic adjustment       *** */
/*   ***    Any other value results in dry adiabatic adjustment       *** */
/*   ***     (Zero value recommended for use in models with           *** */
/*   ***                   boundary layer schemes)                    *** */

/*   ***   MINORIG: Lowest level from which convection may originate  *** */
/*   ***     (Should be first model level at which T is defined       *** */
/*   ***      for models using bulk PBL schemes; otherwise, it should *** */
/*   ***      be the first model level at which T is defined above    *** */
/*   ***                      the surface layer)                      *** */


/* ------------------------------------------------------------------------------ */

/*   ***                    SPECIFY PARAMETERS                        *** */

/*   *** ELCRIT IS THE AUTOCONVERSION THERSHOLD WATER CONTENT (gm/gm) *** */
/*   ***  TLCRIT IS CRITICAL TEMPERATURE BELOW WHICH THE AUTO-        *** */
/*   ***       CONVERSION THRESHOLD IS ASSUMED TO BE ZERO             *** */
/*   ***     (THE AUTOCONVERSION THRESHOLD VARIES LINEARLY            *** */
/*   ***               BETWEEN 0 C AND TLCRIT)                        *** */
/*   ***   ENTP IS THE COEFFICIENT OF MIXING IN THE ENTRAINMENT       *** */
/*   ***                       FORMULATION                            *** */
/*   ***  SIGD IS THE FRACTIONAL AREA COVERED BY UNSATURATED DNDRAFT  *** */
/*   ***  SIGS IS THE FRACTION OF PRECIPITATION FALLING OUTSIDE       *** */
/*   ***                        OF CLOUD                              *** */
/*   ***        OMTRAIN IS THE ASSUMED FALL SPEED (P/s) OF RAIN       *** */
/*   ***     OMTSNOW IS THE ASSUMED FALL SPEED (P/s) OF SNOW          *** */
/*   ***  COEFFR IS A COEFFICIENT GOVERNING THE RATE OF EVAPORATION   *** */
/*   ***                          OF RAIN                             *** */
/*   ***  COEFFS IS A COEFFICIENT GOVERNING THE RATE OF EVAPORATION   *** */
/*   ***                          OF SNOW                             *** */
/*   ***     CU IS THE COEFFICIENT GOVERNING CONVECTIVE MOMENTUM      *** */
/*   ***                         TRANSPORT                            *** */
/*   ***    DTMAX IS THE MAXIMUM NEGATIVE TEMPERATURE PERTURBATION    *** */
/*   ***        A LIFTED PARCEL IS ALLOWED TO HAVE BELOW ITS LFC      *** */
/*   ***    ALPHA AND DAMP ARE PARAMETERS THAT CONTROL THE RATE OF    *** */
/*   ***                 APPROACH TO QUASI-EQUILIBRIUM                *** */
/*   ***   (THEIR STANDARD VALUES ARE  0.20 AND 0.1, RESPECTIVELY)    *** */
/*   ***                   (DAMP MUST BE LESS THAN 1)                 *** */

/* original 0.2 */

/*   ***        ASSIGN VALUES OF THERMODYNAMIC CONSTANTS,        *** */
/*   ***            GRAVITY, AND LIQUID WATER DENSITY.           *** */
/*   ***             THESE SHOULD BE CONSISTENT WITH             *** */
/*   ***              THOSE USED IN CALLING PROGRAM              *** */
/*   ***     NOTE: THESE ARE ALSO SPECIFIED IN SUBROUTINE TLIFT  *** */


/* EPSILON IS A SMALL NUMBER USED TO EXCLUDE MASS FLUXES OF ZERO */

    /* Parameter adjustments */
    --phconv_hpa__;
    --pconv_hpa__;
    --qsconv;
    --qconv;
    --tconv;
    --sub;
    --fq;
    --ft;
    fmass -= 71;

    /* Function Body */
    delti = 1.f / *delt;

/*           ***  INITIALIZE OUTPUT ARRAYS AND PARAMETERS  *** */

    i__1 = *nl + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ft[i__] = 0.f;
	fq[i__] = 0.f;
	fdown[i__ - 1] = 0.f;
	sub[i__] = 0.f;
	fup[i__ - 1] = 0.f;
	m[i__ - 1] = 0.f;
	mp[i__ - 1] = 0.f;
	i__2 = *nl + 1;
	for (j = 1; j <= i__2; ++j) {
	    fmass[i__ + j * 70] = 0.f;
	    ment[i__ + j * 70 - 71] = 0.f;
/* L5: */
	}
    }
    i__2 = *nl + 1;
    for (i__ = 1; i__ <= i__2; ++i__) {
	rdcp = ((1.f - qconv[i__]) * 287.04f + qconv[i__] * 461.5f) / ((1.f - 
		qconv[i__]) * 1005.7f + qconv[i__] * 1870.f);
	d__1 = (doublereal) (1e3f / pconv_hpa__[i__]);
	d__2 = (doublereal) rdcp;
	th[i__ - 1] = tconv[i__] * pow_dd(&d__1, &d__2);
/* L7: */
    }
    *precip = 0.f;
    *wd = 0.f;
    *tprime = 0.f;
    *qprime = 0.f;
    *iflag = 0;

/*       IF(IPBL.NE.0)THEN */

/*     ***            PERFORM DRY ADIABATIC ADJUSTMENT            *** */

/*       JC=0 */
/*       DO 30 I=NL-1,1,-1 */
/*        JN=0 */
/*         SUM=TH(I)*(1.+QCONV(I)*EPSI-QCONV(I)) */
/*        DO 10 J=I+1,NL */
/*         SUM=SUM+TH(J)*(1.+QCONV(J)*EPSI-QCONV(J)) */
/*         THBAR=SUM/FLOAT(J+1-I) */
/*         IF((TH(J)*(1.+QCONV(J)*EPSI-QCONV(J))).LT.THBAR)JN=J */
/*  10    CONTINUE */
/*        IF(I.EQ.1)JN=MAX(JN,2) */
/*        IF(JN.EQ.0)GOTO 30 */
/*  12    CONTINUE */
/*        AHM=0.0 */
/*        RM=0.0 */
/*        DO 15 J=I,JN */
/*         AHM=AHM+(CPD*(1.-QCONV(J))+QCONV(J)*CPV)*TCONV(J)* */
/*    +   (PHCONV_HPA(J)-PHCONV_HPA(J+1)) */
/*         RM=RM+QCONV(J)*(PHCONV_HPA(J)-PHCONV_HPA(J+1)) */
/*  15    CONTINUE */
/*        DPHINV=1./(PHCONV_HPA(I)-PHCONV_HPA(JN+1)) */
/*        RM=RM*DPHINV */
/*        A2=0.0 */
/*        DO 20 J=I,JN */
/*         QCONV(J)=RM */
/*         RDCP=(RD*(1.-QCONV(J))+QCONV(J)*RV)/ */
/*    1     (CPD*(1.-QCONV(J))+QCONV(J)*CPV) */
/*         X=(0.001*PCONV_HPA(J))**RDCP */
/*         TOLD(J)=TCONV(J) */
/*         TCONV(J)=X */
/*         A2=A2+(CPD*(1.-QCONV(J))+QCONV(J)*CPV)*X* */
/*    1    (PHCONV_HPA(J)-PHCONV_HPA(J+1)) */
/*  20    CONTINUE */
/*        DO 25 J=I,JN */
/*         TH(J)=AHM/A2 */
/*         TCONV(J)=TCONV(J)*TH(J) */
/*         TC=TOLD(J)-273.15 */
/*         ALV=LV0-CPVMCL*TC */
/*         QSCONV(J)=QSCONV(J)+QSCONV(J)*(1.+QSCONV(J)*(EPSI-1.))*ALV* */
/*    1    (TCONV(J)- TOLD(J))/(RV*TOLD(J)*TOLD(J)) */
/*      if (qslev(j) .lt. 0.) then */
/*        write(*,*) 'qslev.lt.0 ',j,qslev */
/*      endif */
/*  25    CONTINUE */
/*        IF((TH(JN+1)*(1.+QCONV(JN+1)*EPSI-QCONV(JN+1))).LT. */
/*    1    (TH(JN)*(1.+QCONV(JN)*EPSI-QCONV(JN))))THEN */
/*         JN=JN+1 */
/*         GOTO 12 */
/*        END IF */
/*        IF(I.EQ.1)JC=JN */
/*  30   CONTINUE */

/*   ***   Remove any supersaturation that results from adjustment *** */

/*     IF(JC.GT.1)THEN */
/*      DO 38 J=1,JC */
/*         IF(QSCONV(J).LT.QCONV(J))THEN */
/*          ALV=LV0-CPVMCL*(TCONV(J)-273.15) */
/*          TNEW=TCONV(J)+ALV*(QCONV(J)-QSCONV(J))/(CPD*(1.-QCONV(J))+ */
/*    1      CL*QCONV(J)+QSCONV(J)*(CPV-CL+ALV*ALV/(RV*TCONV(J)*TCONV(J)))) */
/*          ALVNEW=LV0-CPVMCL*(TNEW-273.15) */
/*          QNEW=(ALV*QCONV(J)-(TNEW-TCONV(J))*(CPD*(1.-QCONV(J)) */
/*    1     +CL*QCONV(J)))/ALVNEW */
/*          PRECIP=PRECIP+24.*3600.*1.0E5*(PHCONV_HPA(J)-PHCONV_HPA(J+1))* */
/*    1      (QCONV(J)-QNEW)/(G*DELT*ROWL) */
/*          TCONV(J)=TNEW */
/*          QCONV(J)=QNEW */
/*          QSCONV(J)=QNEW */
/*         END IF */
/*  38  CONTINUE */
/*     END IF */

/*     END IF */

/*  *** CALCULATE ARRAYS OF GEOPOTENTIAL, HEAT CAPACITY AND STATIC ENERGY */

    gz[0] = 0.f;
    cpn[0] = (1.f - qconv[1]) * 1005.7f + qconv[1] * 1870.f;
    h__[0] = tconv[1] * cpn[0];
    lv[0] = 2.501e6f - (tconv[1] - 273.15f) * 630.f;
    hm[0] = lv[0] * qconv[1];
    tv[0] = tconv[1] * (qconv[1] * 1.6077898550724636f + 1.f - qconv[1]);
    ahmin = 1e12f;
    ihmin = *nl;
    i__2 = *nl + 1;
    for (i__ = 2; i__ <= i__2; ++i__) {
	tvx = tconv[i__] * (qconv[i__] * 1.6077898550724636f + 1.f - qconv[
		i__]);
	tvy = tconv[i__ - 1] * (qconv[i__ - 1] * 1.6077898550724636f + 1.f - 
		qconv[i__ - 1]);
	gz[i__ - 1] = gz[i__ - 2] + (tvx + tvy) * 143.52000000000001f * (
		pconv_hpa__[i__ - 1] - pconv_hpa__[i__]) / phconv_hpa__[i__];
	cpn[i__ - 1] = (1.f - qconv[i__]) * 1005.7f + qconv[i__] * 1870.f;
	h__[i__ - 1] = tconv[i__] * cpn[i__ - 1] + gz[i__ - 1];
	lv[i__ - 1] = 2.501e6f - (tconv[i__] - 273.15f) * 630.f;
	hm[i__ - 1] = ((1.f - qconv[i__]) * 1005.7f + qconv[i__] * 2500.f) * (
		tconv[i__] - tconv[1]) + lv[i__ - 1] * qconv[i__] + gz[i__ - 
		1];
	tv[i__ - 1] = tconv[i__] * (qconv[i__] * 1.6077898550724636f + 1.f - 
		qconv[i__]);

/*  ***  Find level of minimum moist static energy    *** */

	if (i__ >= 1 && hm[i__ - 1] < ahmin && hm[i__ - 1] < hm[i__ - 2]) {
	    ahmin = hm[i__ - 1];
	    ihmin = i__;
	}
/* L40: */
    }
/* Computing MIN */
    i__2 = ihmin, i__1 = *nl - 1;
    ihmin = min(i__2,i__1);

/*  ***     Find that model level below the level of minimum moist       *** */
/*  ***  static energy that has the maximum value of moist static energy *** */

    ahmax = 0.f;
    i__2 = ihmin;
    for (i__ = 1; i__ <= i__2; ++i__) {
	if (hm[i__ - 1] > ahmax) {
	    nk = i__;
	    ahmax = hm[i__ - 1];
	}
/* L42: */
    }

/*  ***  CHECK WHETHER PARCEL LEVEL TEMPERATURE AND SPECIFIC HUMIDITY   *** */
/*  ***                          ARE REASONABLE                         *** */
/*  ***      Skip convection if HM increases monotonically upward       *** */

    if (tconv[nk] < 250.f || qconv[nk] <= 0.f || ihmin == *nl - 1) {
	*iflag = 0;
	*cbmf = 0.f;
	return 0;
    }

/*   ***  CALCULATE LIFTED CONDENSATION LEVEL OF AIR AT PARCEL ORIGIN LEVEL *** */
/*   ***       (WITHIN 0.2% OF FORMULA OF BOLTON, MON. WEA. REV.,1980)      *** */

    rh = qconv[nk] / qsconv[nk];
    chi = tconv[nk] / (1669.f - rh * 122.f - tconv[nk]);
    d__1 = (doublereal) rh;
    d__2 = (doublereal) chi;
    plcl = pconv_hpa__[nk] * pow_dd(&d__1, &d__2);
    if (plcl < 200.f || plcl >= 2e3f) {
	*iflag = 2;
	*cbmf = 0.f;
	return 0;
    }

/*   ***  CALCULATE FIRST LEVEL ABOVE LCL (=ICB)  *** */

    icb = *nl - 1;
    i__2 = *nl;
    for (i__ = nk + 1; i__ <= i__2; ++i__) {
	if (pconv_hpa__[i__] < plcl) {
	    icb = min(icb,i__);
	}
/* L50: */
    }
    if (icb >= *nl - 1) {
	*iflag = 3;
	*cbmf = 0.f;
	return 0;
    }

/*   *** FIND TEMPERATURE UP THROUGH ICB AND TEST FOR INSTABILITY           *** */

/*   *** SUBROUTINE TLIFT CALCULATES PART OF THE LIFTED PARCEL VIRTUAL      *** */
/*   ***  TEMPERATURE, THE ACTUAL TEMPERATURE AND THE ADIABATIC             *** */
/*   ***                   LIQUID WATER CONTENT                             *** */

    tlift_(gz, &icb, &nk, tvp, tp, clw, nd, nl, &c__1, &qconv[1], &tconv[1], &
	    qsconv[1], &pconv_hpa__[1]);
    i__2 = icb;
    for (i__ = nk; i__ <= i__2; ++i__) {
	tvp[i__ - 1] -= tp[i__ - 1] * qconv[nk];
/* L54: */
    }

/*   ***  If there was no convection at last time step and parcel    *** */
/*   ***       is stable at ICB then skip rest of calculation        *** */

    if (*cbmf == 0.f && tvp[icb - 1] <= tv[icb - 1] - .9f) {
	*iflag = 0;
	return 0;
    }

/*   ***  IF THIS POINT IS REACHED, MOIST CONVECTIVE ADJUSTMENT IS NECESSARY *** */

    if (*iflag != 4) {
	*iflag = 1;
    }

/*   ***  FIND THE REST OF THE LIFTED PARCEL TEMPERATURES          *** */

    tlift_(gz, &icb, &nk, tvp, tp, clw, nd, nl, &c__2, &qconv[1], &tconv[1], &
	    qsconv[1], &pconv_hpa__[1]);

/*   ***  SET THE PRECIPITATION EFFICIENCIES AND THE FRACTION OF   *** */
/*   ***          PRECIPITATION FALLING OUTSIDE OF CLOUD           *** */
/*   ***      THESE MAY BE FUNCTIONS OF TP(I), PCONV_HPA(I) AND CLW(I)     *** */

    i__2 = nk;
    for (i__ = 1; i__ <= i__2; ++i__) {
	ep[i__ - 1] = 0.f;
	sigp[i__ - 1] = .12f;
/* L57: */
    }
    i__2 = *nl;
    for (i__ = nk + 1; i__ <= i__2; ++i__) {
	tca = tp[i__ - 1] - 273.15f;
	if (tca >= 0.f) {
	    elacrit = .0011f;
	} else {
	    elacrit = (1.f - tca / -55.f) * .0011f;
	}
	elacrit = dmax(elacrit,0.f);
	epmax = .999f;
/* Computing MAX */
	r__1 = clw[i__ - 1];
	ep[i__ - 1] = epmax * (1.f - elacrit / dmax(r__1,1e-8f));
/* Computing MAX */
	r__1 = ep[i__ - 1];
	ep[i__ - 1] = dmax(r__1,0.f);
/* Computing MIN */
	r__1 = ep[i__ - 1];
	ep[i__ - 1] = dmin(r__1,epmax);
	sigp[i__ - 1] = .12f;
/* L60: */
    }

/*   ***       CALCULATE VIRTUAL TEMPERATURE AND LIFTED PARCEL     *** */
/*   ***                    VIRTUAL TEMPERATURE                    *** */

    i__2 = *nl;
    for (i__ = icb + 1; i__ <= i__2; ++i__) {
	tvp[i__ - 1] -= tp[i__ - 1] * qconv[nk];
/* L64: */
    }
    tvp[*nl] = tvp[*nl - 1] - (gz[*nl] - gz[*nl - 1]) / 1005.7f;

/*   ***        NOW INITIALIZE VARIOUS ARRAYS USED IN THE COMPUTATIONS       *** */

    i__2 = *nl + 1;
    for (i__ = 1; i__ <= i__2; ++i__) {
	hp[i__ - 1] = h__[i__ - 1];
	nent[i__ - 1] = 0;
	water[i__ - 1] = 0.f;
	evap[i__ - 1] = 0.f;
	wt[i__ - 1] = 5.5f;
	lvcp[i__ - 1] = lv[i__ - 1] / cpn[i__ - 1];
	i__1 = *nl + 1;
	for (j = 1; j <= i__1; ++j) {
	    qent[i__ + j * 70 - 71] = qconv[j];
	    elij[i__ + j * 70 - 71] = 0.f;
	    sij[i__ + j * 70 - 71] = 0.f;
/* L70: */
	}
    }
    qp[0] = qconv[1];
    i__1 = *nl + 1;
    for (i__ = 2; i__ <= i__1; ++i__) {
	qp[i__ - 1] = qconv[i__ - 1];
/* L72: */
    }

/*  ***  FIND THE FIRST MODEL LEVEL (INB1) ABOVE THE PARCEL'S      *** */
/*  ***          HIGHEST LEVEL OF NEUTRAL BUOYANCY                 *** */
/*  ***     AND THE HIGHEST LEVEL OF POSITIVE CAPE (INB)           *** */

    cape = 0.f;
    capem = 0.f;
    inb = icb + 1;
    inb1 = inb;
    byp = 0.f;
    i__1 = *nl - 1;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	by = (tvp[i__ - 1] - tv[i__ - 1]) * (phconv_hpa__[i__] - phconv_hpa__[
		i__ + 1]) / pconv_hpa__[i__];
	cape += by;
	if (by >= 0.f) {
	    inb1 = i__ + 1;
	}
	if (cape > 0.f) {
	    inb = i__ + 1;
	    byp = (tvp[i__] - tv[i__]) * (phconv_hpa__[i__ + 1] - 
		    phconv_hpa__[i__ + 2]) / pconv_hpa__[i__ + 1];
	    capem = cape;
	}
/* L82: */
    }
    inb = max(inb,inb1);
    cape = capem + byp;
    defrac = capem - cape;
    defrac = dmax(defrac,.001f);
    frac = -cape / defrac;
    frac = dmin(frac,1.f);
    frac = dmax(frac,0.f);

/*   ***   CALCULATE LIQUID WATER STATIC ENERGY OF LIFTED PARCEL   *** */

    i__1 = inb;
    for (i__ = icb; i__ <= i__1; ++i__) {
	hp[i__ - 1] = h__[nk - 1] + (lv[i__ - 1] + tconv[i__] * 
		-864.29999999999995f) * ep[i__ - 1] * clw[i__ - 1];
/* L95: */
    }

/*   ***  CALCULATE CLOUD BASE MASS FLUX AND RATES OF MIXING, M(I),  *** */
/*   ***                   AT EACH MODEL LEVEL                       *** */

    dbosum = 0.f;

/*   ***     INTERPOLATE DIFFERENCE BETWEEN LIFTED PARCEL AND      *** */
/*   ***  ENVIRONMENTAL TEMPERATURES TO LIFTED CONDENSATION LEVEL  *** */

    tvpplcl = tvp[icb - 2] - tvp[icb - 2] * 287.04f * (pconv_hpa__[icb - 1] - 
	    plcl) / (cpn[icb - 2] * pconv_hpa__[icb - 1]);
    tvaplcl = tv[icb - 1] + (tvp[icb - 1] - tvp[icb]) * (plcl - pconv_hpa__[
	    icb]) / (pconv_hpa__[icb] - pconv_hpa__[icb + 1]);
    dtpbl = 0.f;
    i__1 = icb - 1;
    for (i__ = nk; i__ <= i__1; ++i__) {
	dtpbl += (tvp[i__ - 1] - tv[i__ - 1]) * (phconv_hpa__[i__] - 
		phconv_hpa__[i__ + 1]);
/* L96: */
    }
    dtpbl /= phconv_hpa__[nk] - phconv_hpa__[icb];
    dtmin = tvpplcl - tvaplcl + .9f + dtpbl;
    dtma = dtmin;

/*   ***  ADJUST CLOUD BASE MASS FLUX   *** */

    cbmfold = *cbmf;
/* *** C. Forster: adjustment of CBMF is not allowed to depend on FLEXPART timestep */
    delt0 = *delt / 3.f;
    damps = *delt * .1f / delt0;
    *cbmf = (1.f - damps) * *cbmf + dtma * .0025000000000000005f;
    *cbmf = dmax(*cbmf,0.f);

/*   *** If cloud base mass flux is zero, skip rest of calculation  *** */

    if (*cbmf == 0.f && cbmfold == 0.f) {
	return 0;
    }

/*   ***   CALCULATE RATES OF MIXING,  M(I)   *** */

    m[icb - 1] = 0.f;
    i__1 = inb;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	k = min(i__,inb1);
	dbo = (r__1 = tv[k - 1] - tvp[k - 1], dabs(r__1)) + (phconv_hpa__[k] 
		- phconv_hpa__[k + 1]) * .029999999999999999f;
	dbosum += dbo;
	m[i__ - 1] = *cbmf * dbo;
/* L103: */
    }
    i__1 = inb;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	m[i__ - 1] /= dbosum;
/* L110: */
    }

/*   ***  CALCULATE ENTRAINED AIR MASS FLUX (MENT), TOTAL WATER MIXING  *** */
/*   ***     RATIO (QENT), TOTAL CONDENSED WATER (ELIJ), AND MIXING     *** */
/*   ***                        FRACTION (SIJ)                          *** */

    i__1 = inb;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	qti = qconv[nk] - ep[i__ - 1] * clw[i__ - 1];
	i__2 = inb;
	for (j = icb; j <= i__2; ++j) {
	    bf2 = lv[j - 1] * lv[j - 1] * qsconv[j] / (tconv[j] * 461.5f * 
		    tconv[j] * 1005.7f) + 1.f;
	    anum = h__[j - 1] - hp[i__ - 1] + tconv[j] * 864.29999999999995f *
		     (qti - qconv[j]);
	    denom = h__[i__ - 1] - hp[i__ - 1] + (qconv[i__] - qti) * 
		    -864.29999999999995f * tconv[j];
	    dei = denom;
	    if (dabs(dei) < .01f) {
		dei = .01f;
	    }
	    sij[i__ + j * 70 - 71] = anum / dei;
	    sij[i__ + i__ * 70 - 71] = 1.f;
	    altem = sij[i__ + j * 70 - 71] * qconv[i__] + (1.f - sij[i__ + j *
		     70 - 71]) * qti - qsconv[j];
	    altem /= bf2;
	    cwat = clw[j - 1] * (1.f - ep[j - 1]);
	    stemp = sij[i__ + j * 70 - 71];
	    if ((stemp < 0.f || stemp > 1.f || altem > cwat) && j > i__) {
		anum -= lv[j - 1] * (qti - qsconv[j] - cwat * bf2);
		denom += lv[j - 1] * (qconv[i__] - qti);
		if (dabs(denom) < .01f) {
		    denom = .01f;
		}
		sij[i__ + j * 70 - 71] = anum / denom;
		altem = sij[i__ + j * 70 - 71] * qconv[i__] + (1.f - sij[i__ 
			+ j * 70 - 71]) * qti - qsconv[j];
		altem -= (bf2 - 1.f) * cwat;
	    }
	    if (sij[i__ + j * 70 - 71] > 0.f && sij[i__ + j * 70 - 71] < .9f) 
		    {
		qent[i__ + j * 70 - 71] = sij[i__ + j * 70 - 71] * qconv[i__] 
			+ (1.f - sij[i__ + j * 70 - 71]) * qti;
		elij[i__ + j * 70 - 71] = altem;
/* Computing MAX */
		r__1 = 0.f, r__2 = elij[i__ + j * 70 - 71];
		elij[i__ + j * 70 - 71] = dmax(r__1,r__2);
		ment[i__ + j * 70 - 71] = m[i__ - 1] / (1.f - sij[i__ + j * 
			70 - 71]);
		++nent[i__ - 1];
	    }
/* Computing MAX */
	    r__1 = 0.f, r__2 = sij[i__ + j * 70 - 71];
	    sij[i__ + j * 70 - 71] = dmax(r__1,r__2);
/* Computing MIN */
	    r__1 = 1.f, r__2 = sij[i__ + j * 70 - 71];
	    sij[i__ + j * 70 - 71] = dmin(r__1,r__2);
/* L160: */
	}

/*   ***   IF NO AIR CAN ENTRAIN AT LEVEL I ASSUME THAT UPDRAFT DETRAINS  *** */
/*   ***   AT THAT LEVEL AND CALCULATE DETRAINED AIR FLUX AND PROPERTIES  *** */

	if (nent[i__ - 1] == 0) {
	    ment[i__ + i__ * 70 - 71] = m[i__ - 1];
	    qent[i__ + i__ * 70 - 71] = qconv[nk] - ep[i__ - 1] * clw[i__ - 1]
		    ;
	    elij[i__ + i__ * 70 - 71] = clw[i__ - 1];
	    sij[i__ + i__ * 70 - 71] = 1.f;
	}
/* L170: */
    }
    sij[inb + inb * 70 - 71] = 1.f;

/*   ***  NORMALIZE ENTRAINED AIR MASS FLUXES TO REPRESENT EQUAL  *** */
/*   ***              PROBABILITIES OF MIXING                     *** */

    i__1 = inb;
    for (i__ = icb + 1; i__ <= i__1; ++i__) {
	if (nent[i__ - 1] != 0) {
	    qp1 = qconv[nk] - ep[i__ - 1] * clw[i__ - 1];
	    anum = h__[i__ - 1] - hp[i__ - 1] - lv[i__ - 1] * (qp1 - qsconv[
		    i__]);
	    denom = h__[i__ - 1] - hp[i__ - 1] + lv[i__ - 1] * (qconv[i__] - 
		    qp1);
	    if (dabs(denom) < .01f) {
		denom = .01f;
	    }
	    scrit = anum / denom;
	    alt = qp1 - qsconv[i__] + scrit * (qconv[i__] - qp1);
	    if (alt < 0.f) {
		scrit = 1.f;
	    }
	    scrit = dmax(scrit,0.f);
	    asij = 0.f;
	    smin = 1.f;
	    i__2 = inb;
	    for (j = icb; j <= i__2; ++j) {
		if (sij[i__ + j * 70 - 71] > 0.f && sij[i__ + j * 70 - 71] < 
			.9f) {
		    if (j > i__) {
/* Computing MIN */
			r__1 = sij[i__ + j * 70 - 71];
			smid = dmin(r__1,scrit);
			sjmax = smid;
			sjmin = smid;
			if (smid < smin && sij[i__ + (j + 1) * 70 - 71] < 
				smid) {
			    smin = smid;
/* Computing MIN */
			    r__1 = sij[i__ + (j + 1) * 70 - 71], r__2 = sij[
				    i__ + j * 70 - 71], r__1 = min(r__1,r__2);
			    sjmax = dmin(r__1,scrit);
/* Computing MAX */
			    r__1 = sij[i__ + (j - 1) * 70 - 71], r__2 = sij[
				    i__ + j * 70 - 71];
			    sjmin = dmax(r__1,r__2);
			    sjmin = dmin(sjmin,scrit);
			}
		    } else {
/* Computing MAX */
			r__1 = sij[i__ + (j + 1) * 70 - 71];
			sjmax = dmax(r__1,scrit);
/* Computing MAX */
			r__1 = sij[i__ + j * 70 - 71];
			smid = dmax(r__1,scrit);
			sjmin = 0.f;
			if (j > 1) {
			    sjmin = sij[i__ + (j - 1) * 70 - 71];
			}
			sjmin = dmax(sjmin,scrit);
		    }
		    delp = (r__1 = sjmax - smid, dabs(r__1));
		    delm = (r__1 = sjmin - smid, dabs(r__1));
		    asij += (delp + delm) * (phconv_hpa__[j] - phconv_hpa__[j 
			    + 1]);
		    ment[i__ + j * 70 - 71] = ment[i__ + j * 70 - 71] * (delp 
			    + delm) * (phconv_hpa__[j] - phconv_hpa__[j + 1]);
		}
/* L175: */
	    }
	    asij = dmax(1e-21f,asij);
	    asij = 1.f / asij;
	    i__2 = inb;
	    for (j = icb; j <= i__2; ++j) {
		ment[i__ + j * 70 - 71] *= asij;
/* L180: */
	    }
	    bsum = 0.f;
	    i__2 = inb;
	    for (j = icb; j <= i__2; ++j) {
		bsum += ment[i__ + j * 70 - 71];
/* L190: */
	    }
	    if (bsum < 1e-18f) {
		nent[i__ - 1] = 0;
		ment[i__ + i__ * 70 - 71] = m[i__ - 1];
		qent[i__ + i__ * 70 - 71] = qconv[nk] - ep[i__ - 1] * clw[i__ 
			- 1];
		elij[i__ + i__ * 70 - 71] = clw[i__ - 1];
		sij[i__ + i__ * 70 - 71] = 1.f;
	    }
	}
/* L200: */
    }

/*   ***  CHECK WHETHER EP(INB)=0, IF SO, SKIP PRECIPITATING    *** */
/*   ***             DOWNDRAFT CALCULATION                      *** */

    if (ep[inb - 1] < 1e-4f) {
	goto L405;
    }

/*   ***  INTEGRATE LIQUID WATER EQUATION TO FIND CONDENSED WATER   *** */
/*   ***                AND CONDENSED WATER FLUX                    *** */

    jtt = 2;

/*    ***                    BEGIN DOWNDRAFT LOOP                    *** */

    for (i__ = inb; i__ >= 1; --i__) {

/*    ***              CALCULATE DETRAINED PRECIPITATION             *** */

	wdtrain = ep[i__ - 1] * 9.81f * m[i__ - 1] * clw[i__ - 1];
	if (i__ > 1) {
	    i__1 = i__ - 1;
	    for (j = 1; j <= i__1; ++j) {
		awat = elij[j + i__ * 70 - 71] - (1.f - ep[i__ - 1]) * clw[
			i__ - 1];
		awat = dmax(0.f,awat);
/* L320: */
		wdtrain += awat * 9.81f * ment[j + i__ * 70 - 71];
	    }
	}

/*    ***    FIND RAIN WATER AND EVAPORATION USING PROVISIONAL   *** */
/*    ***              ESTIMATES OF QP(I)AND QP(I-1)             *** */


/*  ***  Value of terminal velocity and coefficient of evaporation for snow   *** */

	coeff = .8f;
	wt[i__ - 1] = 5.5f;

/*  ***  Value of terminal velocity and coefficient of evaporation for rain   *** */

	if (tconv[i__] > 273.f) {
	    coeff = 1.f;
	    wt[i__ - 1] = 50.f;
	}
	qsm = (qconv[i__] + qp[i__]) * .5f;
	afac = coeff * phconv_hpa__[i__] * (qsconv[i__] - qsm) / (
		phconv_hpa__[i__] * 2e3f * qsconv[i__] + 1e4f);
	afac = dmax(afac,0.f);
	sigt = sigp[i__ - 1];
	sigt = dmax(0.f,sigt);
	sigt = dmin(1.f,sigt);
	b6 = (phconv_hpa__[i__] - phconv_hpa__[i__ + 1]) * 100.f * sigt * 
		afac / wt[i__ - 1];
	c6 = (water[i__] * wt[i__] + wdtrain / .05f) / wt[i__ - 1];
	revap = (-b6 + sqrt(b6 * b6 + c6 * 4.f)) * .5f;
	evap[i__ - 1] = sigt * afac * revap;
	water[i__ - 1] = revap * revap;

/*    ***  CALCULATE PRECIPITATING DOWNDRAFT MASS FLUX UNDER     *** */
/*    ***              HYDROSTATIC APPROXIMATION                 *** */

	if (i__ == 1) {
	    goto L360;
	}
	dhdp = (h__[i__ - 1] - h__[i__ - 2]) / (pconv_hpa__[i__ - 1] - 
		pconv_hpa__[i__]);
	dhdp = dmax(dhdp,10.f);
	mp[i__ - 1] = lv[i__ - 1] * 10.19367991845056f * .05f * evap[i__ - 1] 
		/ dhdp;
/* Computing MAX */
	r__1 = mp[i__ - 1];
	mp[i__ - 1] = dmax(r__1,0.f);

/*   ***   ADD SMALL AMOUNT OF INERTIA TO DOWNDRAFT              *** */

	fac = 20.f / (phconv_hpa__[i__ - 1] - phconv_hpa__[i__]);
	mp[i__ - 1] = (fac * mp[i__] + mp[i__ - 1]) / (fac + 1.f);

/*    ***      FORCE MP TO DECREASE LINEARLY TO ZERO                 *** */
/*    ***      BETWEEN ABOUT 950 MB AND THE SURFACE                  *** */

	if (pconv_hpa__[i__] > pconv_hpa__[1] * .949f) {
	    jtt = max(jtt,i__);
	    mp[i__ - 1] = mp[jtt - 1] * (pconv_hpa__[1] - pconv_hpa__[i__]) / 
		    (pconv_hpa__[1] - pconv_hpa__[jtt]);
	}
L360:

/*    ***       FIND MIXING RATIO OF PRECIPITATING DOWNDRAFT     *** */

	if (i__ == inb) {
	    goto L400;
	}
	if (i__ == 1) {
	    qstm = qsconv[1];
	} else {
	    qstm = qsconv[i__ - 1];
	}
	if (mp[i__ - 1] > mp[i__]) {
	    rat = mp[i__] / mp[i__ - 1];
	    qp[i__ - 1] = qp[i__] * rat + qconv[i__] * (1.f - rat) + (
		    phconv_hpa__[i__] - phconv_hpa__[i__ + 1]) * 
		    .509683995922528f * (evap[i__ - 1] / mp[i__ - 1]);
	} else {
	    if (mp[i__] > 0.f) {
		qp[i__ - 1] = (gz[i__] - gz[i__ - 1] + qp[i__] * (lv[i__] + 
			tconv[i__ + 1] * 1494.3f) + (tconv[i__ + 1] - tconv[
			i__]) * 1005.7f) / (lv[i__ - 1] + tconv[i__] * 
			1494.3f);
	    }
	}
/* Computing MIN */
	r__1 = qp[i__ - 1];
	qp[i__ - 1] = dmin(r__1,qstm);
/* Computing MAX */
	r__1 = qp[i__ - 1];
	qp[i__ - 1] = dmax(r__1,0.f);
L400:
	;
    }

/*   ***  CALCULATE SURFACE PRECIPITATION IN MM/DAY     *** */

    *precip += wt[0] * .05f * water[0] * 3600.f * 2.4e4f / 9810.f;

L405:

/*   ***  CALCULATE DOWNDRAFT VELOCITY SCALE AND SURFACE TEMPERATURE AND  *** */
/*   ***                    WATER VAPOR FLUCTUATIONS                      *** */

    *wd = (r__1 = mp[icb - 1], dabs(r__1)) * 10.f * .01f * 287.04f * tconv[
	    icb] / (pconv_hpa__[icb] * .05f);
    *qprime = (qp[0] - qconv[1]) * .5f;
    *tprime = *qprime * 2.501e6f / 1005.7f;

/*   ***  CALCULATE TENDENCIES OF LOWEST LEVEL POTENTIAL TEMPERATURE  *** */
/*   ***                      AND MIXING RATIO                        *** */

    dpinv = .01f / (phconv_hpa__[1] - phconv_hpa__[2]);
    am = 0.f;
    if (nk == 1) {
	i__1 = inb;
	for (k = 2; k <= i__1; ++k) {
/* L410: */
	    am += m[k - 1];
	}
    }
/* save saturated upward mass flux for first level */
    fup[0] = am;
    if (dpinv * 19.620000000000001f * am >= delti) {
	*iflag = 4;
    }
    ft[1] += dpinv * 9.81f * am * (tconv[2] - tconv[1] + (gz[1] - gz[0]) / 
	    cpn[0]);
    ft[1] -= lvcp[0] * .05f * evap[0];
    ft[1] += wt[1] * .05f * 1494.3f * water[1] * (tconv[2] - tconv[1]) * 
	    dpinv / cpn[0];
    fq[1] = fq[1] + mp[1] * 9.81f * (qp[1] - qconv[1]) * dpinv + evap[0] * 
	    .05f;
    fq[1] += am * 9.81f * (qconv[2] - qconv[1]) * dpinv;
    i__1 = inb;
    for (j = 2; j <= i__1; ++j) {
	fq[1] += dpinv * 9.81f * ment[j - 1] * (qent[j - 1] - qconv[1]);
/* L415: */
    }

/*   ***  CALCULATE TENDENCIES OF POTENTIAL TEMPERATURE AND MIXING RATIO  *** */
/*   ***               AT LEVELS ABOVE THE LOWEST LEVEL                   *** */

/*   ***  FIRST FIND THE NET SATURATED UPDRAFT AND DOWNDRAFT MASS FLUXES  *** */
/*   ***                      THROUGH EACH LEVEL                          *** */

    i__1 = inb;
    for (i__ = 2; i__ <= i__1; ++i__) {
	dpinv = .01f / (phconv_hpa__[i__] - phconv_hpa__[i__ + 1]);
	cpinv = 1.f / cpn[i__ - 1];
	amp1 = 0.f;
	ad = 0.f;
	if (i__ >= nk) {
	    i__2 = inb + 1;
	    for (k = i__ + 1; k <= i__2; ++k) {
/* L440: */
		amp1 += m[k - 1];
	    }
	}
	i__2 = i__;
	for (k = 1; k <= i__2; ++k) {
	    i__3 = inb + 1;
	    for (j = i__ + 1; j <= i__3; ++j) {
		amp1 += ment[k + j * 70 - 71];
/* L450: */
	    }
	}
/* save saturated upward mass flux */
	fup[i__ - 1] = amp1;
	if (dpinv * 19.620000000000001f * amp1 >= delti) {
	    *iflag = 4;
	}
	i__3 = i__ - 1;
	for (k = 1; k <= i__3; ++k) {
	    i__2 = inb;
	    for (j = i__; j <= i__2; ++j) {
		ad += ment[j + k * 70 - 71];
/* L470: */
	    }
	}
/* save saturated downward mass flux */
	fdown[i__ - 1] = ad;
	ft[i__] = ft[i__] + dpinv * 9.81f * (amp1 * (tconv[i__ + 1] - tconv[
		i__] + (gz[i__] - gz[i__ - 1]) * cpinv) - ad * (tconv[i__] - 
		tconv[i__ - 1] + (gz[i__ - 1] - gz[i__ - 2]) * cpinv)) - lvcp[
		i__ - 1] * .05f * evap[i__ - 1];
	ft[i__] += dpinv * 9.81f * ment[i__ + i__ * 70 - 71] * (hp[i__ - 1] - 
		h__[i__ - 1] + tconv[i__] * 864.29999999999995f * (qconv[i__] 
		- qent[i__ + i__ * 70 - 71])) * cpinv;
	ft[i__] += wt[i__] * .05f * 1494.3f * water[i__] * (tconv[i__ + 1] - 
		tconv[i__]) * dpinv * cpinv;
	fq[i__] += dpinv * 9.81f * (amp1 * (qconv[i__ + 1] - qconv[i__]) - ad 
		* (qconv[i__] - qconv[i__ - 1]));
	i__2 = i__ - 1;
	for (k = 1; k <= i__2; ++k) {
	    awat = elij[k + i__ * 70 - 71] - (1.f - ep[i__ - 1]) * clw[i__ - 
		    1];
	    awat = dmax(awat,0.f);
	    fq[i__] += dpinv * 9.81f * ment[k + i__ * 70 - 71] * (qent[k + 
		    i__ * 70 - 71] - awat - qconv[i__]);
/* L480: */
	}
	i__2 = inb;
	for (k = i__; k <= i__2; ++k) {
	    fq[i__] += dpinv * 9.81f * ment[k + i__ * 70 - 71] * (qent[k + 
		    i__ * 70 - 71] - qconv[i__]);
/* L490: */
	}
	fq[i__] = fq[i__] + evap[i__ - 1] * .05f + (mp[i__] * (qp[i__] - 
		qconv[i__]) - mp[i__ - 1] * (qp[i__ - 1] - qconv[i__ - 1])) * 
		9.81f * dpinv;
/* L500: */
    }

/*   *** Adjust tendencies at top of convection layer to reflect  *** */
/*   ***       actual position of the level zero CAPE             *** */

    fqold = fq[inb];
    fq[inb] *= 1.f - frac;
    fq[inb - 1] += frac * fqold * ((phconv_hpa__[inb] - phconv_hpa__[inb + 1])
	     / (phconv_hpa__[inb - 1] - phconv_hpa__[inb])) * lv[inb - 1] / 
	    lv[inb - 2];
    ftold = ft[inb];
    ft[inb] *= 1.f - frac;
    ft[inb - 1] += frac * ftold * ((phconv_hpa__[inb] - phconv_hpa__[inb + 1])
	     / (phconv_hpa__[inb - 1] - phconv_hpa__[inb])) * cpn[inb - 1] / 
	    cpn[inb - 2];

/*   ***   Very slightly adjust tendencies to force exact   *** */
/*   ***     enthalpy, momentum and tracer conservation     *** */

    ents = 0.f;
    i__1 = inb;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ents += (cpn[i__ - 1] * ft[i__] + lv[i__ - 1] * fq[i__]) * (
		phconv_hpa__[i__] - phconv_hpa__[i__ + 1]);
/* L680: */
    }
    ents /= phconv_hpa__[1] - phconv_hpa__[inb + 1];
    i__1 = inb;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ft[i__] -= ents / cpn[i__ - 1];
/* L640: */
    }
/* ************************************************ */
/* **** DETERMINE MASS DISPLACEMENT MATRIX */
/* ***** AND COMPENSATING SUBSIDENCE */
/* ************************************************ */
/* mass displacement matrix due to saturated up-and downdrafts */
/* inside the cloud and determine compensating subsidence */
/* FUP(I) (saturated updrafts), FDOWN(I) (saturated downdrafts) are assumed to be */
/* balanced by  compensating subsidence (SUB(I)) */
/* FDOWN(I) and SUB(I) defined positive downwards */
/* NCONVTOP IS THE TOP LEVEL AT WHICH CONVECTIVE MASS FLUXES ARE DIAGNOSED */
/* EPSILON IS A SMALL NUMBER */
    sub[1] = 0.f;
    *nconvtop = 1;
    i__1 = inb + 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = inb + 1;
	for (j = 1; j <= i__2; ++j) {
	    if (j == nk) {
		fmass[j + i__ * 70] += m[i__ - 1];
	    }
	    fmass[j + i__ * 70] += ment[j + i__ * 70 - 71];
	    if (fmass[j + i__ * 70] > 1e-20f) {
/* Computing MAX */
		i__3 = max(*nconvtop,i__);
		*nconvtop = max(i__3,j);
	    }
	}
	if (i__ > 1) {
	    sub[i__] = fup[i__ - 2] - fdown[i__ - 1];
	}
    }
    ++(*nconvtop);
    return 0;

} /* convect_ */


/* --------------------------------------------------------------------------- */

/* Subroutine */ int tlift_(real *gz, integer *icb, integer *nk, real *tvp, 
	real *tpk, real *clw, integer *nd, integer *nl, integer *kk, real *
	qconv, real *tconv, real *qsconv, real *pconv_hpa__)
{
    /* System generated locals */
    integer i__1;
    real r__1, r__2;

    /* Local variables */
    integer i__, j;
    real s, es, qg, rg, tc, tg, ah0, ahg;
    integer nsb;
    real alv, cpp;
    integer nst;
    real denom, cpinv;


/* -cv */
/*        include 'includepar' */
/*        include 'includeconv' */
/* -cv */
/* ====>Begin Module TLIFT      File convect.f      Undeclared variables */

/*     Argument variables */


/*     Local variables */



/* ====>End Module   TLIFT      File convect.f */

/*   ***   ASSIGN VALUES OF THERMODYNAMIC CONSTANTS     *** */



/*   ***  CALCULATE CERTAIN PARCEL QUANTITIES, INCLUDING STATIC ENERGY   *** */

    /* Parameter adjustments */
    --pconv_hpa__;
    --qsconv;
    --tconv;
    --qconv;
    --clw;
    --tpk;
    --tvp;
    --gz;

    /* Function Body */
    ah0 = ((1.f - qconv[*nk]) * 1005.7f + qconv[*nk] * 2500.f) * tconv[*nk] + 
	    qconv[*nk] * (2.501e6f - (tconv[*nk] - 273.15f) * 630.f) + gz[*nk]
	    ;
    cpp = (1.f - qconv[*nk]) * 1005.7f + qconv[*nk] * 1870.f;
    cpinv = 1.f / cpp;

    if (*kk == 1) {

/*   ***   CALCULATE LIFTED PARCEL QUANTITIES BELOW CLOUD BASE   *** */

	i__1 = *icb - 1;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    clw[i__] = 0.f;
/* L50: */
	}
	i__1 = *icb - 1;
	for (i__ = *nk; i__ <= i__1; ++i__) {
	    tpk[i__] = tconv[*nk] - (gz[i__] - gz[*nk]) * cpinv;
	    tvp[i__] = tpk[i__] * (qconv[*nk] * 1.6077898550724636f + 1.f);
/* L100: */
	}
    }

/*    ***  FIND LIFTED PARCEL QUANTITIES ABOVE CLOUD BASE    *** */

    nst = *icb;
    nsb = *icb;
    if (*kk == 2) {
	nst = *nl;
	nsb = *icb + 1;
    }
    i__1 = nst;
    for (i__ = nsb; i__ <= i__1; ++i__) {
	tg = tconv[i__];
	qg = qsconv[i__];
	alv = 2.501e6f - (tconv[i__] - 273.15f) * 630.f;
	for (j = 1; j <= 2; ++j) {
	    s = alv * alv * qg / (tconv[i__] * 461.5f * tconv[i__]) + 1005.7f;
	    s = 1.f / s;
	    ahg = tg * 1005.7f + qconv[*nk] * 1494.3f * tconv[i__] + alv * qg 
		    + gz[i__];
	    tg += s * (ah0 - ahg);
	    tg = dmax(tg,35.f);
	    tc = tg - 273.15f;
	    denom = tc + 243.5f;
	    if (tc >= 0.f) {
		es = exp(tc * 17.67f / denom) * 6.112f;
	    } else {
		es = exp(23.33086f - 6111.72784f / tg + log(tg) * .15215f);
	    }
	    qg = es * .62197183098591557f / (pconv_hpa__[i__] - es * 
		    .37802816901408443f);
/* L200: */
	}
	alv = 2.501e6f - (tconv[i__] - 273.15f) * 630.f;
	tpk[i__] = (ah0 - qconv[*nk] * 1494.3f * tconv[i__] - gz[i__] - alv * 
		qg) / 1005.7f;
	clw[i__] = qconv[*nk] - qg;
/* Computing MAX */
	r__1 = 0.f, r__2 = clw[i__];
	clw[i__] = dmax(r__1,r__2);
	rg = qg / (1.f - qconv[*nk]);
	tvp[i__] = tpk[i__] * (rg * 1.6077898550724636f + 1.f);
/* L300: */
    }
    return 0;
} /* tlift_ */

