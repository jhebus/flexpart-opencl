/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXCONV_H__
#define __FLEXCONV_H__

#include "flexclass.h"
#include "flexgrid.h"
#include "flexparam.h"
#include "flexfield.h"
#include "flexarr.h"


/**
 flexConv implements moist convection. It needs more work and does not work with NVIDIA GPU yet.
 */

class flexConv
{
public:
    //! Constructor
	flexConv();

    //! Destructor
	~flexConv();

    /** Re-distributes particles by moist convection.
     @param C flexClass object.
     @param G flexGrid object.
     @param P flexParam object.
     @param F flexField object.
     */
	void convection(flexClass &C, const flexGrid &G, const flexParam &P, const flexField &F);

private:
	void clearDevice();

        void updateDevice(int np, int nxy, int cuda);
	//void updateDevice(int resize_a, int resize_b);
        
        void updateHost();
        
        void runConvectionKernel(int itterations, DeviceClass *C, cl_mem G, const flexParam *P, DeviceField *F);

	void redistribute(int is, Class *C, const Grid *G, const Parameter *P, const Field *F);

	int convmatix(Convect &Cv, const Field *F, int nij, const Grid *G, const Parameter *P);

private:
    
        
	ConvectArray    convect_array;                  //host version
        cl_mem          dev_convect_array;              //device version

	flexArr<float>
		CBMF;			// convection fluxes from the previous time step 

	flexArr<int>
		Pdx,			// sorted grid indexes of particles
		Sdx;			// segments of sorted grid indexes of particles
						// if n1=Sdx[i-1] and n2=Sdx[i],
						// then particles with indexes of Pdx[k=n1] to Pdx[k<n1] are in the same horizontal grid box.

	//int
        cl_mem
		d_Pdx, d_Sdx;

	//float
        cl_mem
		d_CBMF;
};

#endif
