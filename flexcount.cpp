/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexcount.h"
#include "flexutility.h"
#include "flexocl.h"
#include "flexclass.h"
#include "./netCDF/netcdf.h"
#include "flexutility.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define __device__ static inline
#define __FLEX__

#include "flextraj_kernel.h"

#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

//this is a hack till i understand how to pass scalars in structs
//static Parameter static_p;

static int x_len, y_len, z_len, m_len, m_row, m_column;
static float static_deg;
extern cl_mem float_probe, int_probe;


#include "kernel_meterological_functions.h"
#include "kernel_trajectory_functions.h"


flexCount::flexCount(){
    PRINTF("FLEXCOUNT: create\n");
	_global=false;
        d_X = NULL;
        d_Y = NULL;
        d_Z = NULL;
        d_M = NULL;
}


flexCount::~flexCount(){
    PRINTF("FLEXCOUNT: destroy\n");
}
/*----------------------------------------------------------------------------*/
void flexCount::set(float x0, int nx, float y0, int ny, float deg, float zm)
{
	if (x0<-180 || y0<-90 || x0+nx*deg>360 || y0+ny*deg>90 || zm<100)
		errmsg("Bad parameters for setting particle counting grid");

	_deg=deg;

	X.resize(nx);
	Y.resize(ny);
	A.resize(ny);
        
        clearDevice();

	int k;
	for (k=0; k<nx; k++) X(k)=x0+k*deg;
	for (k=0; k<ny; k++) Y(k)=y0+k*deg;
	for (k=0; k<ny; k++) {
		double r=6371.0*1000;							// earth radius [m]
		double dy=6.283185307*r*deg/360.0;
		r=r*cos((y0+(k+0.5)*deg)*0.017453293);
		double dx=6.283185307*r*deg/360.0;
		A(k)=dx*dy;
	}

	if (deg*nx>359.9f && deg*ny>179.9f) _global=true;

	int nl=2;
	float z=zm;
	while (z<50000.0f) {
		nl++;
		z*=2.0;
	}

	Z.resize(nl);
	Z(0)=0;
	for (k=1; k<nl-1; k++) {
		Z(k)=zm;
		zm*=2.0;
	}
	Z(nl-1)=1.e9;
}
/*----------------------------------------------------------------------------*/
void flexCount::set(const flexCount &Ct)
{
	if (Ct.nx()<=0) return;

	if (&Ct!=this) set(Ct.x0(),Ct.nx(),Ct.y0(),Ct.ny(),Ct.deg(),Ct.zm());

	C.resize(Ct.nz(),Ct.ny(),Ct.nx());
	C.clear();

	M.resize(Ct.nz(),Ct.ny(),Ct.nx());
	M.clear();
        
        clearDevice();

	R.resize(Ct.nz(),Ct.ny(),Ct.nx());
	R.clear();
        
#if RADIOACTIVE
        Radiation.resize(Ct.nz(),Ct.ny(),Ct.nx());
        R.clear();
#endif
}
/*----------------------------------------------------------------------------*/
void flexCount::mass(const flexGrid &G, const flexField &F)
{
    
    if (M.nd()==0) return;
#if 0

    updateDevice();

    runMassKernel(G.device, F.dev_struct, Y.nd());

    updateHost();
#else
PRINTF("FLEXCOUNT : MASS -> make kernel\n");

#pragma omp parallel for
	for (int i=0; i<Y.nd(); i++) {
		for (int j=0; j<X.nd(); j++) {
			float x=X(j)+_deg/2;
			float y=Y(i)+_deg/2;
			Factor f=index_f(&G.host,&F.host,x,y,0.0);
			if (f.i1<0) continue;
			float p0=interpo2d_f(&f,&G.host,F.host.Ps);
			float z0=interpo3d_f(&f,&G.host,F.host.Z,0);
			float zl=interpo3d_f(&f,&G.host,F.host.Z,G.host.nl-1)-z0;
			M(0,i,j)=0;
			for (int k=1; k<Z.nd(); k++) {
				if (Z(k)>=zl) {
					M(k,i,j)=p0/9.81;
				}
				else {
					for (int l=1; l<G.host.nl; l++) {
						float z2=interpo3d_f(&f,&G.host,F.host.Z,l)-z0;
						if (z2>=Z(k)) {
							float z1=interpo3d_f(&f,&G.host,F.host.Z,l-1)-z0;
							double a=fabs((z2-Z(k))/(z2-z1));
							double b=fabs((z1-Z(k))/(z2-z1));
							double p;
							if (G.host.hybrid) {
								double p1=G.host.A[l-1]+G.host.B[l-1]*p0;
								double p2=G.host.A[l]+G.host.B[l]*p0;
								double p3;
								if (l<G.host.nl-1) {
									p3=G.host.A[l+1]+G.host.B[l+1]*p0;
									
								}
								else {
									p3=0.0;
								}
								p1=0.5*(p1+p2);
								p2=0.5*(p2+p3);
								p=a*log(p1)+b*log(p2);
							}
							else {
								p=a*log(G.host.P[l-1])+b*log(G.host.P[l]);
							}
							p=exp(p);
							M(k,i,j)=(p0-p)/9.81;
							break;
						}
					}
				}
			}
			if (M(Z.nd()-1,i,j)<M(Z.nd()-2,i,j)) M(Z.nd()-2,i,j)=M(Z.nd()-1,i,j);
		}
	}
#endif
}
/*----------------------------------------------------------------------------*/
void flexCount::count(const Class &Cs, flexCount &Ct, int sync)
{
	if (C.nd()==0) return;

	C.clear();

	int idx;
        double deg=Y(1)-Y(0);   
        
	for (idx=0; idx<Cs.np; idx++) {
		double x   = Cs.X[idx];             //longitude
		double y   = Cs.Y[idx];             //lataitude
		double z   = Cs.Z[idx];             //altitude
		double zb  = Cs.Zb[idx];            //boundary height
#if RADIOACTIVE
                double rad = Cs.Radioactivity[idx]; //radioactivity
#else   
                double rad = 1;
#endif
		        

		if (X(0)<0.0) {
			// grid longitude from -180 to 180
			if (x>180.0) x-=360.0f;
		}

		if (x<X(0) || y<Y(0)) continue;

		int i=double(y-Y(0))/deg;
		int j=double(x-X(0))/deg;

		if (i>=C.nr() || j>=C.nc()) continue;

		double dd=deg*deg;
		double d=0.5f*deg;
	
		double dx=x-(X(0)+j*deg);
		double dy=y-(Y(0)+i*deg);

		double dx1, dx2, dy1, dy2, cnt;

		if (dx<d) {
//left
			dx1=dx+d;
			dx2=d-dx;
			if (dy<d) {
//lower-left
				dy1=dy+d;
				dy2=d-dy;
				cnt=dx1*dy1/dd;
				cnt_f(i,j,cnt,z,zb, rad * cnt);
				cnt=dx1*dy2/dd;
				cnt_f(i-1,j,cnt,z,zb, rad * cnt);
				cnt=dx2*dy1/dd;
				cnt_f(i,j-1,cnt,z,zb, rad * cnt);
				cnt=dx2*dy2/dd;
				cnt_f(i-1,j-1,cnt,z,zb, rad * cnt);
			}
			else {
//upper-left
				dy1=deg+d-dy;
				dy2=dy-d;
				cnt=dx1*dy1/dd;
				cnt_f(i,j,cnt,z,zb, rad * cnt);
				cnt=dx1*dy2/dd;
				cnt_f(i+1,j,cnt,z,zb, rad * cnt);
				cnt=dx2*dy1/dd;
				cnt_f(i,j-1,cnt,z,zb, rad * cnt);
				cnt=dx2*dy2/dd;
				cnt_f(i+1,j-1,cnt,z,zb, rad * cnt);
			}
		}
		else {
// right
			dx1=deg+d-dx;
			dx2=dx-d;
			if (dy<d) {
//lower-right
				dy1=dy+d;
				dy2=d-dy;
				cnt=dx1*dy1/dd;
				cnt_f(i,j,cnt,z,zb, rad * cnt);
				cnt=dx1*dy2/dd;
				cnt_f(i-1,j,cnt,z,zb, rad * cnt);
				cnt=dx2*dy1/dd;
				cnt_f(i,j+1,cnt,z,zb, rad * cnt);
				cnt=dx2*dy2/dd;
				cnt_f(i-1,j+1,cnt,z,zb, rad * cnt);
			}
			else {
//upper-right
				dy1=deg+d-dy;
				dy2=dy-d;
				cnt=dx1*dy1/dd;
				cnt_f(i,j,cnt,z,zb, rad * cnt);
				cnt=dx1*dy2/dd;
				cnt_f(i+1,j,cnt,z,zb, rad * cnt);
				cnt=dx2*dy1/dd;
				cnt_f(i,j+1,cnt,z,zb, rad * cnt);
				cnt=dx2*dy2/dd;
				cnt_f(i+1,j+1,cnt,z,zb, rad * cnt);
			}
		}
	}

	for (idx=0; idx<C.nd(); idx++) {
		if (C(idx)>0.0) {
                    
                    //residence time
                    float res=C(idx)*sync/Cs.np;
                    R(idx)+=res;
                    
                    if (Ct.nd()==C.nd()) {
                        Ct.add_rt(res,idx);
                        Ct.add_cn(C(idx),idx);
#if RADIOACTIVE
                        Ct.add_radiation(Radiation(idx), idx);
#endif
                    }
		}
	}
}
/*----------------------------------------------------------------------------*/
void flexCount::save(const char *fname, const Parameter &P, int age, double ctime, int len)
{
	if (C.nd()==0 || P.outsec>age || age%P.outsec!=0) return;

	if (len==0) len=P.lensec/P.outsec;
	int idx=age/P.outsec-1;
	int ncid, tid=-1, zid, yid, xid, aid, mid, rid, radiation_id,dimid[4];

	const char	*value=NULL,
				*xname="x",
				*yname="y",
				*zname="z",
				*tname="t",
				*rname="rt",
                                *radiation_name = "radiation",
				*aname="ca",
				*mname="ma",
				*lname="long_name",
				*units="units",
				*comment="comment",
				*mode="direction";

	printf("save res: %s\n",fname);

	if (nc_open(fname,NC_WRITE,&ncid)!=NC_NOERR) {
	
		if (nc_create(fname,NC_NOCLOBBER,&ncid)!=NC_NOERR) errmsg("Failed to open ",fname," for saving data");
		
		nc_redef(ncid);
		
		value="FLEXCPP by Jiye Zeng <zeng@nies.go.jp>";
		nc_put_att_text(ncid,NC_GLOBAL,comment,strlen(value),value);

		if (len>1) {
			// define time coordinate variable only when len > 1.
			nc_def_dim(ncid,tname,len,&tid);
			dimid[0]=tid;
			nc_def_var(ncid,tname,NC_INT,1,dimid,&tid);
			value="time";
			nc_put_att_text(ncid,tid,lname,strlen(value),value);
			int yy, mm, dd, hh, mi;
			char str[64];
			jul2cal(yy,mm,dd,hh,mi,ctime-double(P.ldirect*P.outsec)/DAYSEC);
			sprintf(str,"seconds since %d-%02d-%02d %02d:%02d:00",yy,mm,dd,hh,mi);
			nc_put_att_text(ncid,tid,units,strlen(str),str);
			nc_put_att_int(ncid,tid,mode,NC_INT,1,&P.ldirect);
		}

		nc_def_dim(ncid,zname,Z.nd(),&zid);
		dimid[0]=zid;
		nc_def_var(ncid,zname,NC_FLOAT,1,dimid,&zid);
		value="height_above_surface";
		nc_put_att_text(ncid,zid,lname,strlen(value),value);
		value="m";
		nc_put_att_text(ncid,zid,units,strlen(value),value);

		nc_def_dim(ncid,yname,Y.nd(),&yid);
		dimid[0]=yid;
		nc_def_var(ncid,yname,NC_FLOAT,1,dimid,&yid);
		value="latitude";
		nc_put_att_text(ncid,yid,lname,strlen(value),value);
		value="degree_north";
		nc_put_att_text(ncid,yid,units,strlen(value),value);
		value="lower boundary";
		nc_put_att_text(ncid,yid,comment,strlen(value),value);

		nc_def_dim(ncid,xname,X.nd(),&xid);
		dimid[0]=xid;
		nc_def_var(ncid,xname,NC_FLOAT,1,dimid,&xid);
		value="longitude";
		nc_put_att_text(ncid,xid,lname,strlen(value),value);
		value="degree_east";
		nc_put_att_text(ncid,xid,units,strlen(value),value);
		value="left boundary";
		nc_put_att_text(ncid,xid,comment,strlen(value),value);

		dimid[0]=yid;
		nc_def_var(ncid,aname,NC_FLOAT,1,dimid,&aid);
		value="cell_area";
		nc_put_att_text(ncid,aid,lname,strlen(value),value);
		value="m2";
		nc_put_att_text(ncid,aid,units,strlen(value),value);

		dimid[0]=zid; dimid[1]=yid; dimid[2]=xid;
		nc_def_var(ncid,mname,NC_FLOAT,3,dimid,&mid);
		value="atmosphere_mass_of_air";
		nc_put_att_text(ncid,mid,lname,strlen(value),value);
		value="kg m-2";
		nc_put_att_text(ncid,mid,units,strlen(value),value);
		value="atmosphere mass of air under the height (under the planetary boundary layer for heigh=0)";
		nc_put_att_text(ncid,mid,comment,strlen(value),value);
		float fvalue=0;
		nc_put_att_float(ncid,mid,"_FillValue",NC_FLOAT,1,&fvalue);


		int ndim;
		if (len>1) {
			ndim=4;
			dimid[0]=tid; dimid[1]=zid; dimid[2]=yid; dimid[3]=xid;
		}
		else {
			ndim=3;
			dimid[0]=zid; dimid[1]=yid; dimid[2]=xid;
		}

		nc_def_var(ncid,rname,NC_FLOAT,ndim,dimid,&rid);
		value="residence_time";
		nc_put_att_text(ncid,rid,lname,strlen(value),value);
		value="s";
		nc_put_att_text(ncid,rid,units,strlen(value),value);
		value="residence time under the height at output time (height=0 means under PBL)";
		nc_put_att_text(ncid,rid,comment,strlen(value),value);
#if RADIOACTIVE
                //save radioactive data
                nc_def_var(ncid, radiation_name, NC_FLOAT, ndim, dimid, &radiation_id);
                value = "Radioactivity";
                nc_put_att_text(ncid, radiation_id, lname, strlen(value), value);
		value="%";
		nc_put_att_text(ncid, radiation_id, units, strlen(value), value);
		value="percentage of radioactive particulate still in the air as a function of the origional concentration";
		nc_put_att_text(ncid, radiation_id, comment, strlen(value), value);
#endif 
		nc_enddef(ncid);

		if (len>1) {
			flexArr<int> arr(len);
			arr.clear();
			nc_put_var_int(ncid,tid,arr.ptr());
		}
		nc_put_var_float(ncid,zid,Z.ptr());
		nc_put_var_float(ncid,yid,Y.ptr());
		nc_put_var_float(ncid,xid,X.ptr());
		nc_put_var_float(ncid,aid,A.ptr());
		nc_put_var_float(ncid,mid,M.ptr());

	}
	else {
		size_t tlen, zlen, ylen, xlen;
		
		if (len>1) {
			if (nc_inq_varid(ncid,tname,&tid)!=NC_NOERR) errmsg("Failed to get output time ID");
                        if (nc_inq_dimlen(ncid,tid,&tlen)!= NC_NOERR) errmsg("Failed to get output time value");
		}
		else {
			tlen=len;
		}

		if (nc_inq_varid(ncid,zname,&zid)!=NC_NOERR ||
			nc_inq_varid(ncid,yname,&yid)!=NC_NOERR ||
			nc_inq_varid(ncid,xname,&xid)!=NC_NOERR ||
			nc_inq_varid(ncid,aname,&aid)!=NC_NOERR ||
			nc_inq_varid(ncid,mname,&mid)!=NC_NOERR ||
			nc_inq_varid(ncid,rname,&rid)!=NC_NOERR 
#if RADIOACTIVE
                        || nc_inq_varid(ncid,radiation_name, &radiation_id) != NC_NOERR
#endif
                        ) errmsg("Failed to get variable IDs");

		nc_inq_dimlen(ncid,zid,&zlen);
		nc_inq_dimlen(ncid,yid,&ylen);
		nc_inq_dimlen(ncid,xid,&xlen);

		if (tlen!=len || zlen!=Z.nd() || ylen!=Y.nd() || xlen!=X.nd()){
                    PRINTF("tlen %lu, len  %d\n", tlen, len);
                    PRINTF("tlen %lu, Z.nd %d\n", zlen, Z.nd());
                    PRINTF("tlen %lu, ylen %d\n", ylen, Y.nd());
                    PRINTF("tlen %lu, xlen %d\n", xlen, X.nd());
                    errmsg("CDF dimensions != array dimensions");
                }

		if (idx==0) {
// overwrite coordinate data to an old file.
			if (len>1) {
				nc_put_att_int(ncid,tid,mode,NC_INT,1,&P.ldirect);
				flexArr<int> arr(len);
				arr.clear();
				nc_put_var_int(ncid,tid,arr.ptr());
			}
			nc_put_var_float(ncid,zid,Z.ptr());
			nc_put_var_float(ncid,yid,Y.ptr());
			nc_put_var_float(ncid,xid,X.ptr());
			nc_put_var_float(ncid,aid,A.ptr());
			nc_put_var_float(ncid,mid,M.ptr());
		}
	}

	size_t start[4], count[4];

	if (len>1) {
		start[0]=idx;
		start[1]=0;
		start[2]=0;
		start[3]=0;
		count[0]=1;
		count[1]=Z.nd();
		count[2]=Y.nd();
		count[3]=X.nd();
		nc_put_var1_int(ncid,tid,start,&age);
	}
	else {
		start[0]=0;
		start[1]=0;
		start[2]=0;
		count[0]=Z.nd();
		count[1]=Y.nd();
		count[2]=X.nd();
	}

	if (nc_put_vara_float(ncid, rid, start,count,R.ptr())!=NC_NOERR) errmsg("Failed to save resident time");
#if RADIATION        
        if (nc_put_vara_float(ncid, radiation_id, start, count, Radiation.ptr())!=NC_NOERR) errmsg("Failed to save radiation");
#endif
	nc_close(ncid);
}
/*----------------------------------------------------------------------------*/
void flexCount::cnt_f(int i, int j, double cnt, double z, double zb, double rad)
{
	if (j<0 || j>=C.nc()) return;

	if (i<0) {
// one row down at the poles
		if (!_global) return;
		i=0;
		j=(j+C.nc()/2)%C.nc();
	}
	else if (i>=C.nr()) {
// one row up at the poles
		if (!_global) return;
		i=C.nr()-1;
		j=(j+C.nc()/2)%C.nc();
	}

	if (z<=zb){
            C(i,j) += cnt;
#if RADIOACTIVE
            Radiation(i, j) += rad;
#endif
        }
	
	for (int k=1; k<Z.nd(); k++) {
		if (z<=Z(k)){
                    C(k,i,j) += cnt;
#if RADIOACTIVE
                    Radiation(k, i, j) += rad;
#endif
                }
	}
}
/*----------------------------------------------------------------------------*/
void flexCount::updateDevice(){

    PRINTF("FLEXCOUNT: updateDevice\n");
    if (!d_X) mallocOnDevice(&d_X, X.nd() * sizeof(float), X.ptr());
    setDeviceData(d_X, X.ptr(), X.nd() * sizeof(float));
    
    if (!d_Y) mallocOnDevice(&d_Y, Y.nd() * sizeof(float), Y.ptr());
    setDeviceData(d_Y, Y.ptr(), Y.nd() * sizeof(float));
    
    if (!d_Z) mallocOnDevice(&d_Z, Z.nd() * sizeof(float), Z.ptr());
    setDeviceData(d_Z, Z.ptr(), Z.nd() * sizeof(float));
    
    if (!d_M) mallocOnDevice(&d_M, M.nd() * sizeof(float), M.ptr());
    setDeviceData(d_M, M.ptr(), M.nd() * sizeof(float));
    
}
/*----------------------------------------------------------------------------*/
void flexCount::updateHost(){
    
    PRINTF("FLEXCOUNT: updateHost\n");
    
    //looks like M is the only thing changed, hence need to come back
    //getDeviceData(X.ptr(), d_X, X.nd() * sizeof(float));
    //getDeviceData(Y.ptr(), d_Y, Y.nd() * sizeof(float));
    //getDeviceData(Z.ptr(), d_Z, Z.nd() * sizeof(float));
    getDeviceData(M.ptr(), d_M, M.nd() * sizeof(float));
}
/*----------------------------------------------------------------------------*/
void flexCount::clearDevice(){
    
     freeOnDevice(d_X);
    d_X = NULL;
    freeOnDevice(d_Y);
    d_Y = NULL;
    freeOnDevice(d_Z);
    d_Z = NULL;
    freeOnDevice(d_M);
    d_M = NULL;
    
}
/*----------------------------------------------------------------------------*/
void flexCount::runMassKernel(cl_mem G, DeviceField *F, int itterations){
 
    PRINTF("FLEXCOUNT : mass kernel\n");
    
    if (!G|| !F)
        errmsg("Not all device pointers have been set properly");
    
    //cl_int errorNum; 
    cl_uint var = 0;
    //cl_kernel kernel = getSuperKernel();
    float dummyVal = 0.0;
    double dummyDouble;
    
    x_len = X.nd();
    y_len = Y.nd();
    z_len = Z.nd();
    m_len = M.nd();
    m_row = M.nr();
    m_column = M.nc();
    static_deg = _deg;
    
    
    int choice = MASS_KERNEL;
    //the first argument indicates the kernel code to run
    setKernelArgs(choice, var++, sizeof(int), &choice);                 //0

    var = 61;
    //The grid, just pass directly as static
    setKernelArgs(choice, var++, sizeof(cl_mem), &G);                   //61
    

    var = 96;
    //field variables
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->U);                //97
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->V);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->W);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Z);                //100
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Rho);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->DRho);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->T);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->RH);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->PBL);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Tropo);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Ustar);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Wstar);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Oli);
    setKernelArgs(choice, var++, sizeof(cl_mem), &F->Ps);               //110

    var = 114;
    
    //Mass Parameters
    setKernelArgs(choice, var++, sizeof(cl_mem), &d_X);                 //115
    setKernelArgs(choice, var++, sizeof(cl_mem), &d_Y);
    setKernelArgs(choice, var++, sizeof(cl_mem), &d_Z);
    setKernelArgs(choice, var++, sizeof(cl_mem), &d_M);
    setKernelArgs(choice, var++, sizeof(int), &x_len);
    setKernelArgs(choice, var++, sizeof(int), &y_len);                  //120
    setKernelArgs(choice, var++, sizeof(int), &z_len);
    setKernelArgs(choice, var++, sizeof(int), &m_len);
    setKernelArgs(choice, var++, sizeof(int), &m_row);
    setKernelArgs(choice, var++, sizeof(int), &m_column);
    setKernelArgs(choice, var++, sizeof(float), &static_deg);
    
    //printf("This should be calculated by querying the device!!\n");
    const size_t local_ws = 256;	// Number of work-items per work-group
    
    // shrRoundUp returns the smallest multiple of local_ws bigger than size
    // in this case 
    size_t global_ws = shrRoundUp(local_ws, itterations);	// Total number of work-items
    
    
    runKernelBlocking(&global_ws, local_ws);
}
/*----------------------------------------------------------------------------*/
