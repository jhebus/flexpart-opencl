/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXTRAJ_KERNEL_H__
#define __FLEXTRAJ_KERNEL_H__

#include "flexocl.h"
#include <math.h>
#include "flexutility.h"                                //for max

#ifndef __device__
#define __device__ static inline
#endif

/*! \addtogroup traj_kernel
 Documentation for trajjectory kernel functions used by
 host computer and GPU device.
 @{
 */

/** Interpolates 2D field data
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 */
__device__
float interpo2d_f(const Factor *f, const Grid *g, const float* v)
{
	int n1=f->i1*g->nc;
	int n2=n1+g->nc;
	float d=f->f1*v[n1+f->j1]+
			f->f2*v[n1+f->j2]+
			f->f3*v[n2+f->j2]+
			f->f4*v[n2+f->j1];
	return d;
}


/** Interpolates 3D field data on a level.
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 @param k Level index of v.
 */
__device__
float interpo3d_f(const Factor *f, const Grid *g, const float* v, int k)
{
	int n1=k*g->nxy+f->i1*g->nc;
	int n2=n1+g->nc;
	float d=f->f1*v[n1+f->j1]+
			f->f2*v[n1+f->j2]+
			f->f3*v[n2+f->j2]+
			f->f4*v[n2+f->j1];
	return d;
}

/** Calculates standard deviation for a grid box.
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to 3D field data.
 */
__device__
float std3d_f(const Factor *f, const Grid *g, const float* v)
{
	int n1, n2, nxy;
	float vm, v1, v2, v3, v4, v5, v6, v7, v8;

	nxy=g->nxy;

	n1=f->k1*nxy+f->i1*g->nc+f->j1;
	n2=n1+nxy;
	v1=v[n1];
	v2=v[n2];

	n1=f->k2*nxy+f->i1*g->nc+f->j2;
	n2=n1+nxy;
	v3=v[n1];
	v4=v[n2];

	n1=f->k3*nxy+f->i2*g->nc+f->j2;
	n2=n1+nxy;
	v5=v[n1];
	v6=v[n2];

	n1=f->k4*nxy+f->i2*g->nc+f->j1;
	n2=n1+nxy;
	v7=v[n1];
	v8=v[n2];

	vm=(v1+v2+v3+v4+v5+v6+v7+v8)/8.0f;

	v1-=vm;
	v2-=vm;
	v3-=vm;
	v4-=vm;
	v5-=vm;
	v6-=vm;
	v7-=vm;
	v8-=vm;

	return sqrt((v1*v1+v2*v2+v3*v3+v4*v4+v5*v5+v6*v6+v7*v7+v8*v8)/8.0f);
}

/** Interpolate 3D field data
 @param f Factor object.
 @param g Grid object.
 @param v Pointer to field data.
 */
__device__
float field3d_f(const Factor *f, const Grid *g, const float* v)
{
	int n1, n2, nxy;
	float v1, v2, v3, v4;

	nxy=g->nxy;
	
	n1=f->k1*nxy+f->i1*g->nc+f->j1;
	n2=n1+nxy;
	v1=f->fa1*v[n1]+f->fb1*v[n2];
	
	n1=f->k2*nxy+f->i1*g->nc+f->j2;
	n2=n1+nxy;
	v2=f->fa2*v[n1]+f->fb2*v[n2];

	n1=f->k3*nxy+f->i2*g->nc+f->j2;
	n2=n1+nxy;
	v3=f->fa3*v[n1]+f->fb3*v[n2];

	n1=f->k4*nxy+f->i2*g->nc+f->j1;
	n2=n1+nxy;
	v4=f->fa4*v[n1]+f->fb4*v[n2];

	return f->f1*v1+f->f2*v2+f->f3*v3+f->f4*v4;
}


/** Calculate vertical interpolation factors.
 @param g Grid object.
 @param f Field object.
 @param z Altitude.
 @param i Field row index.
 @param j Field column index.
 */
__device__
Factor zindex_f(const Grid *g, const Field *f, float z, int i, int j)
{
	// todo: implementation for hybrid or sigma vertical grid.

	int k, n, nxy;
	float z1, z2, dz;
	Factor fc;

	if (z<0.0f) {
#ifdef __FLEX__
	errmsg("z<0 problem");
#else
		z=-z;
#endif
	}

	n=i*g->nc+j;
	nxy=g->nxy;
	z1=f->Z[n];
	z+=z1+1.0f;
	
	for (k=1; k<g->nl; k++) {
		z2=f->Z[n+k*nxy];
		if (z2>z) {
			dz=z2-z1;
			fc.k1=k-1;
			fc.fa1=fabs((z2-z)/dz);
			fc.fb1=fabs((z1-z)/dz);
			return fc;
		}
		z1=z2;
	}

	if (g->gl || z-z2<100.0f) {
		fc.k1=g->nl-2;
		fc.fa1=0.0;
		fc.fb1=1.0;
	}
	else {
		fc.k1=-1;
		fc.fa1=1.0;
		fc.fb1=0.0;
	}

	return fc;
}


/** Finds x-y grid indices and calculates interpolation factors (z refer to surface)
 @param g Grid object.
 @param f Field object.
 @param x Longitude.
 @param y Latitude.
 @param z Altitude.
 */
__device__
Factor index_f(const Grid *g, const Field *f, float x, float y, float z)
{
	Factor fc, f2;

	fc.j1=int((x-g->x0)/g->dx);
	fc.j2=fc.j1+1;
	
	if (fc.j1<0 || fc.j2>=g->nc) {
		// x goes out of range
		if (!g->gl) {
			// for non-global grid, return negative index.
			fc.i1=-1;
			fc.j1=-1;
			return fc;
		}
		// it must be embraced by the two end grids along x-axis.
		fc.j1=g->nc-1;
		fc.j2=0;
	}

	float r=fabs((x-(g->x0+fc.j1*g->dx))/g->dx);

	fc.i1=int((y-g->y0)/g->dy);
	fc.i2=fc.i1+1;

	if (fc.i1<0 || fc.i2>=g->nr) {
		if (!g->gl) {
			fc.i1=-1;
			fc.j1=-1;
			return fc;
		}
#ifdef __FLEX__
		errmsg("i-index problem");
#endif
		fc.i1=0;
		fc.i2=1;
	}

	float s=fabs((y-(g->y0+fc.i1*g->dy))/g->dy);

	fc.f1=(1.0f-r)*(1.0f-s);
	fc.f2=r*(1.0f-s);
	fc.f3=r*s;
	fc.f4=(1.0f-r)*s;

// vertical indexes and factors

	f2=zindex_f(g,f,z,fc.i1,fc.j1);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k1=f2.k1;
	fc.fa1=f2.fa1;
	fc.fb1=f2.fb1;

	f2=zindex_f(g,f,z,fc.i1,fc.j2);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k2=f2.k1;
	fc.fa2=f2.fa1;
	fc.fb2=f2.fb1;

	f2=zindex_f(g,f,z,fc.i2,fc.j2);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k3=f2.k1;
	fc.fa3=f2.fa1;
	fc.fb3=f2.fb1;

	f2=zindex_f(g,f,z,fc.i2,fc.j1);
	if (f2.k1<0) {
		fc.i1=-1;
		fc.j1=-1;
		return fc;
	}
	fc.k4=f2.k1;
	fc.fa4=f2.fa1;
	fc.fb4=f2.fb1;

	return fc;
}


/** Reflect excessive altitude displacement on boundaries (z refer to the surface)
 @param z Altitude.
 @param zmax Altitude maximum.
 */
__device__
float reflect_f(float z, float zmax)
{
	if (z<0.0f) z=-z;
	if (z>zmax) {
		z=fmod(z,zmax);
		z=zmax-z;
	}
	return z;
}


//! Hanna function. Refer to hanna.f of flexpart6.4-gfs version
__device__
Turb hanna_f(float z, float h, float ol, float ust, float wst)
{
	Turb t;

	if (h/fabs(ol)<1.0) {
// Neutral conditions
		ust=max(ust,1.e-4f);
		float corr=z/ust;
		t.sigu=1.e-2f+2.0f*ust*exp(-3.e-4f*corr);
		t.sigw=1.3f*ust*exp(-2.e-4f*corr);
		t.dsigwdz=-2.e-4f*t.sigw;
		t.sigw=t.sigw+1.e-2f;
		t.sigv=t.sigw;
        t.tlu=0.5f*z/t.sigw/(1.0f+1.5e-3f*corr);
        t.tlv=t.tlu;
        t.tlw=t.tlu;
	}
	else if (ol<0.0f) {
// Unstable conditions
		float zeta=z/h;
		t.sigu=1.e-2f+ust*pow(12.0f-0.5f*h/ol,0.33333f);
		t.sigv=t.sigu;
		t.sigw=sqrt(1.2f*wst*wst*(1.0f-0.9f*zeta)*pow(zeta,0.66666f)+(1.8f-1.4f*zeta)*ust*ust)+1.e-2f;
		t.dsigwdz=0.5f/t.sigw/h*(-1.4f*ust*ust+wst*wst*(0.8f*pow(max(zeta,1.e-3f),-.33333f)-1.8f*pow(zeta,0.66666f)));
		t.tlu=0.15f*h/t.sigu;
        t.tlv=t.tlu;
		if (z<fabs(ol))
			t.tlw=0.1f*z/(t.sigw*(0.55f-0.38f*fabs(z/ol)));
		else if (zeta<0.1f)
			t.tlw=0.59f*z/t.sigw;
		else
			t.tlw=0.15f*h/t.sigw*(1.0f-exp(-5.0f*zeta));
	}
	else {
// Stable conditions
		float zeta=z/h;
		t.sigu=1.e-2f+2.0f*ust*(1.0f-zeta);
		t.sigv=1.e-2f+1.3f*ust*(1.0f-zeta);
		t.sigw=t.sigv;
		t.dsigwdz=-1.3f*ust/h;
		t.tlu=0.15f*h/t.sigu*sqrt(zeta);
		t.tlv=0.467f*t.tlu;
		t.tlw=0.1f*h/t.sigw*pow(zeta,0.8f);
	}

	t.tlu=max(10.0f,t.tlu);
	t.tlv=max(10.0f,t.tlv);
	t.tlw=max(30.0f,t.tlw);
	
	if (t.dsigwdz==0.0f) t.dsigwdz=1.e-10f;

	return t;
}

/** Converts distance change in m to degree.
 @param g Grid object.
 @param lon Longitude [deg]
 @param lat Latitude [deg]
 @param dx Est-west displacement [m]
 @oaram dy North-south displacement [m]
 @param zt Altitude [m]
 */
__device__
XY xy2ll_f(const Grid *g, float lon, float lat, float dx, float dy, float zt)
{
	const float R2D=57.2957795f;		// radius to degree 180/pi;
	const float D2R=0.0174532925f;		// radius to degree pi/180;

	float a=lon*D2R,
		  b=lat*D2R,
		  re=6378137.0f+zt,				// earth radius (m)
		  r=re*cos(b),
		  x, y;

	if (lat>75.0f) {
		// point near north pole
		x=r*cos(a)-dx*sin(a)-dy*cos(a);
		y=r*sin(a)+dx*cos(a)-dy*sin(a);
		lon=R2D*atan2(y,x);
		lat=R2D*acos(sqrt(x*x+y*y)/re);
	} else if (lat<-75.0f) {
		// point near south pole
		x=r*cos(a)-dx*sin(a)+dy*cos(a);
		y=r*sin(a)+dx*cos(a)+dy*sin(a);
		lon=R2D*atan2(y,x);
		lat=-R2D*acos(sqrt(x*x+y*y)/re);
	} else {
		lon+=R2D*dx/r;
		lat+=R2D*dy/re;
	}

	if (lat>90.0f) {
		lat=180.0f-lat;
		lon+=180.0f;
	}
	else if (lat<-90.0f) {
		lat=-(180.0f+lat);
		lon+=180.0f;
	}

	if (g->gl) {
		if (lon<g->x0) {
			lon+=360.0f;
		}
		else if (lon>g->x1+g->dx) {
			lon-=360.0f;
		}
	}

	XY xy={lon,lat};

	return xy;
}


/** Advances one synchronize time step for dispersion (refer to advance.f of flexpart6.4-gfs version)
 @param C Class object.
 @param G Grid object.
 @param P Parameter object.
 @param F Field obejct.
 */
__device__
int dispersion_f(Class *C, int idx, const Grid *G, const Parameter *P, const Field *F)
{
	int k=C->Idx[idx]%P->ng;
	if (k<0) k=0;

// particle position
	float x0=C->X[idx];
	float y0=C->Y[idx];
	float z0=C->Z[idx];
	float xt=x0;
	float yt=y0;
	float zt=z0;

// turbulent wind components
	float up=C->Up[idx];
	float vp=C->Vp[idx];
	float wp=C->Wp[idx];

// time steps
	float dt=P->ldt;
	float dtf=dt/P->ifine;
	int sdt=P->ldt;

// interpolation factors
	Factor fc=index_f(G,F,xt,yt,zt);

	if (fc.i1<0 || fc.j1<0) return 0;

	int ldt;
	float u, v, w;
	float cosphi, sinphi;
	float r, zsfc, zpbl, ztop, trop;
	float dx=0.0f, dy=0.0f, daw=0.0f, dcw=0.0f;
	XY xy;

	for (ldt=0; ldt<P->lsynctime; ldt+=sdt) {
// boundaries
		zsfc=interpo3d_f(&fc,G,F->Z,0);
		ztop=interpo3d_f(&fc,G,F->Z,G->nl-1)-zsfc;
		zpbl=interpo2d_f(&fc,G,F->PBL);
		trop=interpo2d_f(&fc,G,F->Tropo);

// winds
		u=field3d_f(&fc,G,F->U);
		v=field3d_f(&fc,G,F->V);
		w=field3d_f(&fc,G,F->W);
		
		if (P->ctl>0) {
// re-estimate time step for integration
			sdt=int(zpbl/(1.e-10+P->ctl*fabs(w)));
			if (sdt<1) sdt=1;
			if (sdt>P->ldt) sdt=P->ldt;
			if (sdt>P->lsynctime-ldt) sdt=P->lsynctime-ldt;
			dt=sdt;
			dtf=sdt/P->ifine;
		}

		dx+=u*dt*P->ldirect;
		dy+=v*dt*P->ldirect;
		zt+=w*dt*P->ldirect;

		if (zt>=zpbl) {
// z goes out of PBL, complete the current interval above PBL.
			ldt+=sdt;
			break;
		}

		if (zt<0.0f) zt=-zt;

// turbulent components
		float oli=interpo2d_f(&fc,G,F->Oli);
		float ust=interpo2d_f(&fc,G,F->Ustar);
		float wst=interpo2d_f(&fc,G,F->Wstar);
		float rhoaux=field3d_f(&fc,G,F->DRho)/(1.e-10f+field3d_f(&fc,G,F->Rho));

		Turb tb=hanna_f(zt,zpbl,oli,ust,wst);

// horizontal
		r=dt/tb.tlu;
		if (r<0.5f) {
			up=(1.0f-r)*up+P->Gauss[++k%P->ng]*tb.sigu*sqrt(2.0f*r);
		}
		else {
			r=exp(-r);
			up=r*up+P->Gauss[++k%P->ng]*tb.sigu*sqrt(1.0f-r*r);
		}

		r=dt/tb.tlv;
		if (r<0.5f) {
			vp=(1.0f-r)*vp+P->Gauss[++k%P->ng]*tb.sigv*sqrt(2.0f*r);
		}
		else {
			r=exp(-r);
			vp=r*vp+P->Gauss[++k%P->ng]*tb.sigv*sqrt(1.0f-r*r);
		}

		daw+=up*dt;
		dcw+=vp*dt;

// vertical
		float cbt=C->Fg[idx];
		int ifine;

		for (ifine=0; ifine<P->ifine; ifine++) {

			if (ifine>0) tb=hanna_f(zt,zpbl,oli,ust,wst);

			r=dtf/tb.tlw;
			if (r<0.5f) {
				wp=cbt*((1.0f-r)*wp+P->Gauss[++k%P->ng]*sqrt(2.f*r)+dtf*(tb.dsigwdz+rhoaux*tb.sigw));
			}
			else {
				r=exp(-r);
				wp=cbt*(r*wp+P->Gauss[++k%P->ng]*sqrt(1.f-r*r)+tb.tlw*(1.f-r)*(tb.dsigwdz+rhoaux*tb.sigw));
			}

			zt+=wp*tb.sigw*dtf;

			if (zt<0.f || zt>zpbl) {
				cbt=-1.0f;
			}
			else {
				cbt=1.0f;
			}

			zt=reflect_f(zt,zpbl);
		}

		C->Fg[idx]=cbt;

		fc=index_f(G,F,xt,yt,zt);

		if (fc.i1<0 || fc.j1<0) {
			C->X[idx]=x0;
			C->Y[idx]=y0;
			C->Z[idx]=z0;
			return 0;
		}
	}

// save random disturbances

	C->Up[idx]=up;
	C->Vp[idx]=vp;
	C->Wp[idx]=wp;

// convert along/cross wind dispplacement to dx and dy

	r=max(1.e-5f,sqrt(dx*dx+dy*dy));
	cosphi=dx/r;
	sinphi=dy/r;

	dx+=cosphi*daw-sinphi*dcw;
	dy+=sinphi*daw+cosphi*dcw;

// advance to next sync time for particle outside mixing layer

	if (ldt<P->lsynctime) {
		dt=P->lsynctime-ldt;

		if (zt<trop) {
			r=sqrt(2.*P->d_trop/dt);
			up=P->Gauss[++k%P->ng]*r;
			vp=P->Gauss[++k%P->ng]*r;
//			wp=0.0f;
			// different from flexpart: vertical perturbation even in troposphere.
			r=sqrt(2.*P->d_trop*0.01/dt);
			wp=P->Gauss[++k%P->ng]*r;
		}
		else if (zt<trop+1000.0f) {
			float weight=(zt-trop)/1000.0f;
			r=sqrt(2.*P->d_trop*(1.-weight)/dt);
			up=P->Gauss[++k%P->ng]*r;
			vp=P->Gauss[++k%P->ng]*r;
//			r=sqrt(2.*P->d_strat*weight/dt);
			// different from flexpart.
			r=sqrt(2.*P->d_strat*weight/dt)+sqrt(2.*P->d_trop*0.01*(1.-weight)/dt);
			wp=P->Gauss[++k%P->ng]*r;
		}
		else {
			up=0.0f;
			vp=0.0f;
			r=sqrt(2.*P->d_strat/dt);
			wp=P->Gauss[++k%P->ng]*r;
		}

		dx+=(u+up)*dt*P->ldirect;
		dy+=(v+vp)*dt*P->ldirect;
		zt+=(w+wp)*dt*P->ldirect;
	}

// add mesoscale random disturbances

	r=exp(-2.f*P->lsynctime/G->ns);
	float rs=sqrt(1.-r*r);

	float sig=std3d_f(&fc,G,F->U);
	C->Um[idx]=r*C->Um[idx]+rs*P->Gauss[++k%P->ng]*sig*P->turbmesoscale;

	sig=std3d_f(&fc,G,F->V);
	C->Vm[idx]=r*C->Vm[idx]+rs*P->Gauss[++k%P->ng]*sig*P->turbmesoscale;

	sig=std3d_f(&fc,G,F->W);
	C->Wm[idx]=r*C->Wm[idx]+rs*P->Gauss[++k%P->ng]*sig*P->turbmesoscale;
	
	dx+=C->Um[idx]*P->lsynctime*P->ldirect;
	dy+=C->Vm[idx]*P->lsynctime*P->ldirect;
	zt+=C->Wm[idx]*P->lsynctime*P->ldirect;

	zt=reflect_f(zt,ztop);

	xy=xy2ll_f(G,xt,yt,dx,dy,zt+zsfc);
	xt=xy.x;
	yt=xy.y;

// save coordinates

	C->X[idx]=xt;
	C->Y[idx]=yt;
	C->Z[idx]=zt;
	C->Zs[idx]=zsfc;
	C->Zb[idx]=zpbl;
	C->Idx[idx]=k;

// update grid index

	if (P->lconv!=0) {
		int i=int((yt+0.5*G->dy-G->y0)/G->dy);
		int j=int((xt+0.5*G->dx-G->x0)/G->dx);
#ifdef __FLEX__
		if (i<0 || j<0) errmsg("Bad grid index!");
#endif
		if (i>=G->nr) i=G->nr-1;
		if (j>=G->nc) j=G->nc-1;
		C->Gdx[idx]=i*G->nc+j;
	}

	return 1;
}


/** Advances one synchronize time step for trjaectory calculation using mean wind fields.
 @param C Class object.
 @param idx Particle index.
 @param G Grid object.
 @param P Parameter object.
 @param F Field obejct.
 */
__device__
int trajectory_f(Class *C, int idx, const Grid *G, const Parameter *P, const Field *F)
{
	float dt=P->ldt;
	int sdt=P->ldt;

	float x0=C->X[idx];
	float y0=C->Y[idx];
	float z0=C->Z[idx];

	for (int ldt=0; ldt<P->lsynctime; ldt+=sdt) {
// first guess, save winds.
		float xt=C->X[idx];
		float yt=C->Y[idx];
		float zt=C->Z[idx];
		
		Factor fc=index_f(G,F,xt,yt,zt);

		if (fc.i1<0 || fc.j1<0) {
			C->X[idx]=x0;
			C->Y[idx]=y0;
			C->Z[idx]=z0;
			return 0;
		}

		float zs=interpo3d_f(&fc,G,F->Z,0);
		float zb=interpo2d_f(&fc,G,F->PBL);
		float ztop=interpo3d_f(&fc,G,F->Z,G->nl-1)-zs;

		float u=field3d_f(&fc,G,F->U);
		float v=field3d_f(&fc,G,F->V);
		float w=field3d_f(&fc,G,F->W);

		if (P->ctl>0) {
// re-estimate time step for integration
			sdt=int(zb/(1.e-10+P->ctl*fabs(w)));
			if (sdt<1) sdt=1;
			if (sdt>P->ldt) sdt=P->ldt;
			if (sdt>P->lsynctime-ldt) sdt=P->lsynctime-ldt;
			dt=sdt;
		}
		
		XY xy=xy2ll_f(G,xt,yt,dt*u*P->ldirect,dt*v*P->ldirect,zs+zt);
		float ze=reflect_f(zt+dt*w*P->ldirect,ztop);

// final position
		fc=index_f(G,F,xy.x,xy.y,ze);

		if (fc.i1<0 || fc.j1<0) {
			C->X[idx]=x0;
			C->Y[idx]=y0;
			C->Z[idx]=z0;
			return 0;
		}

		u=0.5*(u+field3d_f(&fc,G,F->U));
		v=0.5*(v+field3d_f(&fc,G,F->V));
		w=0.5*(w+field3d_f(&fc,G,F->W));

		xy=xy2ll_f(G,xt,yt,dt*u*P->ldirect,dt*v*P->ldirect,zs+zt);
		zt=reflect_f(zt+dt*w*P->ldirect,ztop);

		C->X[idx]=xy.x;
		C->Y[idx]=xy.y;
		C->Z[idx]=zt;
		C->Zs[idx]=interpo3d_f(&fc,G,F->Z,0);
		C->Zb[idx]=interpo2d_f(&fc,G,F->PBL);
	}

	return 1;
}


#endif