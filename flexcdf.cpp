/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES,
 JAPAN, BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL,
 INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS
 SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE AUTHOR AND THE COPYRIGHT
 HOLDER HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexcdf.h"
#include "flexutility.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


static void decode_gdas1(float *p, unsigned char *b, int nr, int nc, double scale, double offset)
{
	if (scale!=0.0) {
		int i, j, l=0;
		for (i=0; i<nr; i++) {
			for (j=0; j<nc; j++) {
				p[l]=offset+(b[l]-127.0)/scale;
		        offset=p[l];
				l++;
			}
	        offset=p[i*nc];
		}
	}
	else {
		memset(p,0,nr*nc*sizeof(float));
	}
}

static void decode_short(float *p, short *s, int n, int missing, double scale, double offset)
{
	if (s[0]==missing) {
		memset(p,0,n*sizeof(float));
	}
	else {
		for (int i=0; i<n; i++) {
			p[i]=offset+s[i]*scale;
		}
	}
}

flexCDF::flexCDF() : _ncid(0), _buf(0), _size(0)
{
}

flexCDF::~flexCDF()
{
	if(_ncid) nc_close(_ncid);
	if (_buf) free(_buf);
}

void flexCDF::open(const char *fname)
{
	if(_ncid) nc_close(_ncid);
	if (nc_open(fname,NC_NOWRITE,&_ncid)!=NC_NOERR)
		errmsg("Failed to open ",fname," for reading");
}

bool flexCDF::isVariable(const char *vname)
{
	return nc_inq_varid(_ncid,vname,&_varid)==NC_NOERR;
}

int flexCDF::readGrid(const char *vname, double *ptr, int np)
{
	checkVariable(vname);

	if (np<_np) errmsg("ptr memory is too small for reading grid");

	if (_type==NC_DOUBLE) {
		if (nc_get_var_double(_ncid,_varid,ptr)!=NC_NOERR)
			errmsg("Failed to read grid (double)");
	}
	else if (_type==NC_FLOAT) {
		checkBuffer(_np*sizeof(float));
		if (nc_get_var_float(_ncid,_varid,(float*)_buf)!=NC_NOERR)
			errmsg("Failed to read grid (float)");
		for (int i=0; i<_np; i++) {
			ptr[i]=((float*)_buf)[i];
		}
	}
	else {
		errmsg("Unexpected grid type");
	}

	return _np;
}

void flexCDF::readVariable(const char *vname, float *ptr, int np)
{
	checkVariable(vname);

	if (np!=_np) errmsg("ptr np != variable np for ",vname);

	if (_type==NC_FLOAT) {
		if (nc_get_var_float(_ncid,_varid,ptr)!=NC_NOERR)
			errmsg("Failed to read variable (float)");
	}
	else if (_type==NC_SHORT) {
		checkBuffer(_np*sizeof(short));
		if (nc_get_var_short(_ncid,_varid,(short*)_buf)!=NC_NOERR)
			errmsg("Failed to read variable (short)");
		if (np==_np) {
			for (int i=0; i<_nl; i++) {
				int off=i*_nr*_nc;
				decode_short(ptr+off,(short*)_buf+off,_nr*_nc,_missing,_scale,_offset);
			}
		}
		else if (np==_nr*_nc) {
			decode_short(ptr,(short*)_buf,np,_missing,_scale,_offset);
		}
		else {
			errmsg("np!=_np && np!=_nr*_nc");
		}
	}
	else if (_type==NC_BYTE) {
		checkBuffer(_np*sizeof(unsigned char));
		if (nc_get_var_uchar(_ncid,_varid,(unsigned char*)_buf)!=NC_NOERR)
			errmsg("Failed to read variable (byte)");
		if (np==_np) {
			for (int i=0; i<_nl; i++) {
				int off=i*_nr*_nc;
				decode_gdas1(ptr+off,(unsigned char*)_buf+off,_nr,_nc,_gdas1_scale[i],_gdas1_offset[i]);
			}
		}
		else if (np==_nr*_nc) {
			decode_gdas1(ptr,(unsigned char*)_buf,_nr,_nc,_gdas1_scale[0],_gdas1_offset[0]);
		}
		else {
			errmsg("np!=_np && np!=_nr*_nc");
		}
		if (_scale!=1.0 || _offset!=0.0) {
			for (int i=0; i<np; i++) ptr[i]=ptr[i]*_scale+_offset;
		}
	}
	else {
		errmsg("Unexpected variable type");
	}
#ifdef _DEBUG
	for (int i=0; i<_nl; i++) {
		int off=i*_nr*_nc;
		print_variable(vname,ptr+off,_nr*_nc,i+1);
	}
#endif
}

void flexCDF::checkVariable(const char *vname)
{
	if (nc_inq_varid(_ncid,vname,&_varid)!=NC_NOERR)
		errmsg("Failed to get variable ID for ",vname);
	
	if (nc_inq_vartype(_ncid,_varid,&_type)!=NC_NOERR)
		errmsg("Failed to get variable type for ",vname);

	if (nc_inq_varndims(_ncid,_varid,&_ndims)!=NC_NOERR)
		errmsg("Failed to get variable dimension for ",vname);
	
	if (_ndims<1 || _ndims>3)
		errmsg("Unexpected variable dimension for ",vname);

	if (nc_inq_vardimid(_ncid,_varid,_dimids)!=NC_NOERR)
		errmsg("Failed to get variable dimension ID for ",vname);

	for (int i=0; i<_ndims; i++) {
		if (nc_inq_dimlen(_ncid,_dimids[i],&_dimlen[i])!=NC_NOERR)
			errmsg("Failed to get dimension length for ",vname);
	}

	if (_ndims==1) {
		_nl=_dimlen[0];
		_nr=1;
		_nc=1;
	}
	else if (_ndims==2) {
		_nl=1;
		_nr=_dimlen[0];
		_nc=_dimlen[1];
	}
	else if (_ndims==3) {
		_nl=_dimlen[0];
		_nr=_dimlen[1];
		_nc=_dimlen[2];
	}
	else {
		errmsg("Unexpected dimensions of ",vname);
	}

	_np=_nl*_nr*_nc;

	_scale=1;
	getAttribute("scale_factor",&_scale,0,1);
	
	_offset=0;
	getAttribute("add_offset",&_offset,0,1);

	_missing=-32768;
	getAttribute("missing_value",0,&_missing,1);
	
	memset(_gdas1_scale,0,MAX_ATT_LEN*sizeof(double));
	getAttribute("gdas1_scale",_gdas1_scale,0,MAX_ATT_LEN);
	
	memset(_gdas1_offset,0,MAX_ATT_LEN*sizeof(double));
	getAttribute("gdas1_offset",_gdas1_offset,0,MAX_ATT_LEN);
}

void flexCDF::checkBuffer(int size)
{
	if (_size<size) {
		if (_buf) free(_buf);
		_buf=malloc(size);
		if (!_buf) errmsg("Failed to allocate memory");
		_size=size;
	}
}

void flexCDF::getAttribute(const char *aname, double *value, int *ivalue, int n)
{
	size_t len;
	nc_type type;
	char name[32];

	strcpy(name,aname);

	if (nc_inq_att(_ncid,_varid,name,&type,&len)==NC_NOERR) {
		if (len>n) 
			errmsg("Attribute length > n for ",name);
		if (len>1 && len!=_nl)
			errmsg("Attributes != _nl for ",name);
		if (type==NC_DOUBLE) {
			if (!value) errmsg("*value is NULL for ",name);
			nc_get_att_double(_ncid,_varid,name,value);
		}
		else if (type==NC_FLOAT) {
			if (!value) errmsg("*value is NULL for ",name);
			float v[MAX_ATT_LEN];
			nc_get_att_float(_ncid,_varid,name,v);
			for (int i=0; i<len; i++) value[i]=v[i];
		}
		else if (type==NC_INT) {
			if (!ivalue) errmsg("*ivalue is NULL for ",name);
			nc_get_att_int(_ncid,_varid,name,ivalue);
		}
		else {
			errmsg("Unexpected attribute type for ",name);
		}
	}
}

