/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#ifndef __FLEXMODEL_H__
#define __FLEXMODEL_H__

#include "flexgrid.h"
#include "flexparam.h"
#include "flexfield.h"
#include "flexcount.h"
#include "flexlist.h"
//#include "flexgl.h"

#include <string>
#include <vector>

#define MAX_GMT 256

#include "flexocl.h"




/**
 The flexModel contains all necessary object for modeling particle dispersion or
 calculating trajectory using mean wind fields.
 <br>
 <br>
 Refer to flexmain.cpp for how to set model options and start modeling.
 <br>
 <br>
 Refer to flexgl.cpp for how to set a new position interactively and start modeling.
 */

class flexModel
{
public:
    //! Constructor
	flexModel();

    //! Destructor 
	~flexModel();

    //! Starts modeling.
	void start();

    /** Sets model options.
     @param nargs Number of arguments.
     @param args Argument values.
     */
	bool setOption(int nargs, char *args[]);

    /** Sets initial latitude and longitude (for flexGL use).
     @param x Longitude [degree].
     @param y Latitude [degree].
     */
	void glxy(float x, float y)	{ L.newXY(x,y); }

private:

// Reset starting time

	void reset(double julian);

// Update fields
	
	void update(double julian);

//private:

	bool setOption(const std::vector<std::string> &vs);

#ifdef POINTER_PARAMS
	flexCount *C;                                    // particle count

	flexGrid *G;					// Grid definition of data (global)
	flexGrid *Gn;                                    // Nested grid definition of data (regional)

	flexParam *P;                                    // Model parameters

	flexField *F, *F1, *F2;                            // Data field (must be global)
	flexField *Fn, *F1n, *F2n;         		// Nested data field (must be regional)

	flexList *L;					// particles

        //flexOCL ocl;                                    // The openCL device and params
        
	//flexGL GL;					// display
#else
        flexCount C;                                    // particle count

	flexGrid G;					// Grid definition of data (global)
	flexGrid Gn;                                    // Nested grid definition of data (regional)

	flexParam P;                                    // Model parameters

	flexField F, F1, F2;                            // Data field (must be global)
	flexField Fn, F1n, F2n;         		// Nested data field (must be regional)

	flexList L;					// particles

        //flexOCL ocl;                                    // The openCL device and params
        
	//flexGL GL;					// display
#endif
        
	std::string
		_input,					// input path
		_input2,				// nested input path
		_output;				// output paths

	std::vector<double>
		_jul1,					// start time (Julian date & time)
		_jul2;					// end time

	int
		_itvsec,				// continuous release interval [s]
		_dump,					// dumping flag
		_mobile,				// mobile release flag
		_minute,				// minute flag
		_loop,					// loop status (interactive mode)
		_addsec,				// interval of adding release [s]
		_insec;					// input interval (time resolution of field) [s]
};


#endif
