/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexmodel.h"
#include "flexutility.h"
#include "flexocl.h"
#include "flexparam.h"
#include "flexfield.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <omp.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#endif

#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

flexModel::flexModel()
{
    _input=std::string("./input");
    _output=std::string("./output");
    _itvsec=0;
    _dump=0;
    _mobile=0;
    _loop=0;
    _addsec=0;
    _insec=3*3600;
#if POINTER_PARAMS
    L = new flexList();
    C = new flexCount();                                    // particle count

    G = new flexGrid();					// Grid definition of data (global)
    Gn = new flexGrid();                                    // Nested grid definition of data (regional)

    P = new flexParam();                                    // Model parameters

    F = new flexField();
    F1  = new flexField();
    F2  = new flexField();                            // Data field (must be global)
    
    Fn = new flexField();
    F1n = new flexField();
    F2n = new flexField();         		// Nested data field (must be regional)
#endif
    //Create the context, and command queue
    //initOpenCL(0, 0);
}

flexModel::~flexModel()
{
}

void flexModel::start()
{
	int i, k;

// check start/end times

	if (_jul1.size()==0) return;

	if (_mobile) {
		if (_jul1.size() != L.count()) errmsg("start/end time counts are different from release count");
		_addsec=0;
		_itvsec=0;
		_jul2.clear();
		for (i=0; i<_jul1.size(); i++) {
			_jul2.push_back(_jul1[i] + P.host.lensec/DAYSEC);
		}
	}
	else {
		if (_addsec || _itvsec) {
			if (_jul1.size()!=_jul2.size()) errmsg("start/end time counts are different");
			for (i=0; i<_jul1.size(); i++) {
				if (_jul1[i]>_jul2[i]) errmsg("End time < start time");
			}
		}
		else {
			_jul2.clear();
			for (i=0; i<_jul1.size(); i++) {
				_jul2.push_back(_jul1[i] + P.host.lensec/DAYSEC);
			}
		}
	}

// check parameters

	if (P.host.ldt <= 0 ||
		P.host.lsynctime<=0 ||
		P.host.lsynctime<P.host.ldt ||
		P.host.lsynctime%P.host.ldt!=0 ||
		3600%P.host.lsynctime!=0) errmsg("Bad time steps");
/*
	if (GL.viewport()) {
//		interactive mode
		if (_loop==0) {
			_loop=1;
			if (L.count()!=1) errmsg("Release > 1 in interactive mode");
			GL.initialize(this,_jul1[0]);
			_addsec=0;
			_itvsec=0;
			double a=_jul1[0];
			_jul1.clear();
			_jul1.push_back(a);
			_jul2.clear();
			_jul2.push_back(a);
			GL.loop();
			return;
		}
	}
*/
// if -itv is set, disable -add and reset the start/end times.

	if (_itvsec>0) {
		_addsec=0;
		std::vector<double> jul1=_jul1;
		std::vector<double> jul2=_jul2;
		_jul1.clear();
		_jul2.clear();
		for (i=0; i<jul1.size(); i++) {
			double a=jul1[i];
			double b=jul2[i];
			double c=a;
			k=1;
			while (c<=b) {
				_jul1.push_back(c);
				_jul2.push_back(c);
				c=a+k*_itvsec/DAYSEC;
				k++;
			}
		}
	}

// alwasy save xyz for non-dispersion

	if (!P.host.dispersion) _dump=1;

// force synchronize time to 60 if start/end minute is not zero

	if (_minute!=0) P.host.lsynctime=60;

// cannot use GPU in nested mode

	if (G.nested) P.host.openCL=0;

// set the number of threads for OpenMP

	omp_set_num_threads(P.host.openmp); 

	if (P.host.openCL) printf("use openCL\n");
	printf("processors: %d\n",omp_get_num_procs( ));
	printf("threads: %d\n",P.host.openmp);
        
    
// make output directory

#ifdef _WIN32
	CreateDirectory(_output.c_str(),NULL);
#else
	mkdir(_output.c_str(),0775);
#endif

// start modeling for all GMT *** START OF SIMULATION LOOP *****

	for (int igmt=0; igmt<_jul1.size(); igmt++) {
// Set all kernel argumnets to 0 or NULL
            if(P.host.openCL){
                initKernelArgs();
            }
// set end time to start time if no continuous release

		if (_addsec<1) _jul2[igmt]=_jul1[igmt];

// release particles and reset fields => clear the linked list

		L.clear();

// set starting reference time

		double rtime;
		if (_addsec>0 && P.host.ldirect<0) {
			rtime=_jul2[igmt];
		}
		else {
			rtime=_jul1[igmt];
		}

// add releases and reset field data

		L.add(rtime, C, _mobile?igmt:-1);
		reset(rtime);

// calculate air mass in particle counting grids

		L.mass(G, F1);

// save initial positions

		if (P.host.outsec>0 /*&& !GL.viewport()*/ && _dump) {
			printf("save xyz\n");
			L.saveXYZ(_output.c_str(), G.host, P.host, F.host, rtime);
		}

// set integration seconds

		int nsec=P.host.lensec;

		if (_addsec>0) {
			nsec+=int((_jul2[igmt]-_jul1[igmt]+1.0e-11)*DAYSEC);
		}

// start integration

		time_t tick=time(NULL);
                
                //loop doing dispersion/trajectory for duration synctime
		for (int isec=P.host.lsynctime; isec<=nsec; isec+=P.host.lsynctime) {
		
                    if (isec%3600==0) {
                        printf("execution for hour %d\n",isec/3600);
                    }

// integration
                    if (P.host.dispersion) {
			    L.dispersion(G, P, F, C);
		    }
                    else {
                        L.trajectory(G, P, F);
                    }

//calculate the new particle mass, based on radioactive decay
#if RADIOACTIVE
                   L.radioactive(*G, *P, *F, *C);
#endif

// field update fields
                    double ctime=rtime+P.host.ldirect*isec/DAYSEC;
                    update(ctime);

// output
                    if (P.host.outsec>0) {
                        /*
                            if (GL.viewport()) {
                                    if (isec%P.host.outsec==0) {
                                            printf("output: %d\n",isec);
                                            L.show(GL,ctime, P.host.openCL != 0);
                                    }
                            }
                            else 
                         */ 
                            {
                                    if (_dump) {
                                            L.saveXYZ(_output.c_str(), G.host, P.host, F.host, ctime);
                                    }
                                    if (P.host.dispersion) {
                                            L.saveCNT(_output.c_str(),P.host,ctime);
                                            if (_addsec>0) {
                                                    char fname[1024];
                                                    sprintf(fname,"%s/Accumulate.res.cdf",_output.c_str());
                                                    C.save(fname, P.host, isec, ctime, nsec/P.host.outsec);
                                                    C.clear();
                                            }
                                    }
                            }
                    }

                    if (isec%3600==0) {
                        int yy, mm, dd, hh, mi;
                        jul2cal(yy,mm,dd,hh,mi,ctime);
                        printf("finished to %d-%02d-%02d %02d:%02d\n",yy,mm,dd,hh,mi);
                    }

// removal and release 
                    if (_addsec>0) {
                        L.remove(P.host.lensec);
                        if (isec%_addsec==0 && P.host.lensec<=nsec-isec) {
                            int nc = L.add(ctime, C, -1);
                            printf("particle classes: %d\n",nc);
                            if (_dump) {
                                L.saveXYZ(_output.c_str(), G.host, P.host, F.host, ctime);
                            }
                        }
                    }
		}

		printf("total execution seconds: %.1f\n",difftime(time(NULL),tick));

		L.clear();
		G.clearDevice();
		P.clearDevice();
		F.clearDevice();
		F1.clearDevice();
		F2.clearDevice();
	}
        //tearDownOpenCL();
       
}

//reset the starting time
void flexModel::reset(double julian)
{
    
    PRINTF("MODEL : reset\n");
    
// get the times that enclose julian
	double dfhr=_insec/DAYSEC;
	double jul2=floor(julian);
	while (jul2 < julian) jul2+=dfhr;
	double jul1=jul2-dfhr;
	char fname[1024];
	char fname2[1024];

	if (jul2==julian && P.host.ldirect>0) jul1=jul2+dfhr;

// input files must be named path/yyyymm/yyyymmddhh.cdf
	int yy, mm, dd, hh,mi;
	jul2cal(yy,mm,dd,hh,mi,jul1);
	sprintf(fname,"%s/%04d%02d/%04d%02d%02d%02d.cdf",_input.c_str(),yy,mm,yy,mm,dd,hh);
        printf("Read file %s\n", fname);

// get grid info first
	G.readData(fname,_insec,P.host.openCL!=0);
	if (G.nested) {
		sprintf(fname2,"%s/%04d%02d/%04d%02d%02d%02d.cdf",_input2.c_str(),yy,mm,yy,mm,dd,hh);
		G.nested->readData(fname2,_insec,P.host.openCL!=0);
	}

// read fields
	F1.readData(fname, jul1, G, P);
	if (G.nested) {
		F1.nested->readData(fname2, jul1, *G.nested, P);
	}

	jul2cal(yy,mm,dd,hh,mi,jul2);
	sprintf(fname,"%s/%04d%02d/%04d%02d%02d%02d.cdf",_input.c_str(),yy,mm,yy,mm,dd,hh);

	F2.readData(fname, jul2, G, P);
	if (G.nested) {
		sprintf(fname2,"%s/%04d%02d/%04d%02d%02d%02d.cdf",_input2.c_str(),yy,mm,yy,mm,dd,hh);
		F2.nested->readData(fname2, jul2, *G.nested, P);
	}

// update the device        
	P.update();

// interpolate fields
	F.interpolate(F1, F2, julian, G, P, true);
	if (G.nested) {
		F.nested->interpolate(*F1.nested, *F2.nested, julian, *G.nested, P, true);
	}
}

void flexModel::update(double julian)
{
        PRINTF("MODEL : update\n");

        
	int yy, mm, dd, hh, mi;
	char fname[1024];
	char fname2[1024];

	if (fabs(julian-F1.time()) <= 1.e-6) {
		double jul=julian+_insec*P.host.ldirect/DAYSEC;
		jul2cal(yy,mm,dd,hh,mi,jul);
		sprintf(fname,"%s/%04d%02d/%04d%02d%02d%02d.cdf",_input.c_str(),yy,mm,yy,mm,dd,hh);
		F2.readData(fname, jul, G, P);
		if (G.nested) {
			sprintf(fname2,"%s/%04d%02d/%04d%02d%02d%02d.cdf",_input2.c_str(),yy,mm,yy,mm,dd,hh);
			F2.nested->readData(fname2, jul, *G.nested, P);
		}
	}
	if (fabs(julian-F2.time()) <= 1.e-6) {
		double jul=julian+_insec*P.host.ldirect/DAYSEC;
		jul2cal(yy,mm,dd,hh,mi,jul);
		sprintf(fname,"%s/%04d%02d/%04d%02d%02d%02d.cdf",_input.c_str(),yy,mm,yy,mm,dd,hh);
		F1.readData(fname, jul, G, P);
		if (G.nested) {
			sprintf(fname2,"%s/%04d%02d/%04d%02d%02d%02d.cdf",_input2.c_str(),yy,mm,yy,mm,dd,hh);
			F1.nested->readData(fname2, jul, *G.nested, P);
		}
	}
	F.interpolate(F1, F2, julian, G, P);
	if (G.nested) {
		F.nested->interpolate(*F1.nested, *F2.nested, julian, *G.nested, P);
	}
}

bool flexModel::setOption(const std::vector<std::string> &vs)
{
	const char *msg1="Insufficient number of inputs for ";
	const char *msg3=" option";
	        
	int i=0;
	
	_minute=0;
	
	_jul1.clear();
	_jul2.clear();
	
	L.reset();

	while (i<vs.size()) {
		if (vs[i]=="-in") {
			if (i+2<vs.size()) {
				_input=vs[i+1];
				if (_input.at(_input.length()-1)=='\\' ||
					_input.at(_input.length()-1)=='/')
					_input.erase(_input.length()-1,1);
				int hour=atol(vs[i+2].c_str());
				if (hour<1 || hour>=24 || 24%hour!=0){
					printf("hour: %d\n", hour);
					errmsg("Bad field time resolution - options");
				}
				_insec=hour*3600;
				G.host.ns=_insec;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=3;
			_input2="";
			if (i<vs.size()) {
				_input2=vs[i];
				if (_input2.at(0)!='-') {
					// use nested input
					if (_input2.at(_input2.length()-1)=='\\' ||
						_input2.at(_input2.length()-1)=='/')
						_input2.erase(_input2.length()-1,1);
					i++;
					G.nested = &Gn;
					F.nested = &Fn;
					F1.nested = &F1n;
					F2.nested = &F2n;
				}
				else {
					_input2="";
					G.nested=NULL;
					F.nested=NULL;
					F1.nested=NULL;
					F2.nested=NULL;
				}
			}
		}
		else if (vs[i]=="-out") {
			if (i+2<vs.size()) {
				_output=vs[i+1];
				if (_output.at(_output.length()-1)=='\\' ||
					_output.at(_output.length()-1)=='/')
					_output.erase(_output.length()-1,1);
				int hour=atol(vs[i+2].c_str());
				P.host.outsec=hour*3600;
#ifdef _WIN32
				CreateDirectory(_output.c_str(),NULL);
#else
		        mkdir(_output.c_str(),0775);
#endif
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=3;
                }
                else if(vs[i] == "-dev"){
			if (i + 2 < vs.size()) {
				int platform = atol(vs[i + 1].c_str());
                                int device   = atol(vs[i + 2].c_str());
                                
                                bool list_devices = (platform < 0) || (device < 0);

                                initOpenCL(platform, device, list_devices);
                                
                                P.host.openCL = 1;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=3;
                }
		else if (vs[i]=="-t1") {
			if (i+5<vs.size()) {
				_jul1.push_back(cal2jul(atol(vs[i+1].c_str()),atol(vs[i+2].c_str()),atol(vs[i+3].c_str()))+
								atol(vs[i+4].c_str())/24.0+atol(vs[i+5].c_str())/1440.0);
				if (atol(vs[i+5].c_str())!=0) _minute=1;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=6;
		}
		else if (vs[i]=="-t2") {
			if (i+5<vs.size()) {
				_jul2.push_back(cal2jul(atol(vs[i+1].c_str()),atol(vs[i+2].c_str()),atol(vs[i+3].c_str()))+
								atol(vs[i+4].c_str())/24.0+atol(vs[i+5].c_str())/1440.0);
				if (atol(vs[i+5].c_str())!=0) _minute=1;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=6;
		}
		else if (vs[i]=="-itv") {
			if (i+1<vs.size()) {
				_itvsec=atol(vs[i+1].c_str())*60*60;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-len") {
			if (i+1<vs.size()) {
				P.host.lensec=atol(vs[i+1].c_str())*60*60;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-add") {
			if (i+1<vs.size()) {
				_addsec=atol(vs[i+1].c_str())*60*60;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-bw") {
			if (i+1<vs.size()) {
				if (atol(vs[i+1].c_str())!=0) {
					P.host.ldirect=-1;
				}
				else {
					P.host.ldirect=1;
				}
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-dsp") {
			if (i+1<vs.size()) {
				P.host.dispersion=atol(vs[i+1].c_str())!=0;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-omp") {
			if (i+1<vs.size()) {
				P.host.openmp=atol(vs[i+1].c_str());
				if (P.host.openmp<1) ;//P.host.openmp=omp_get_num_procs();
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-tm") {
			if (i+1<vs.size()) {
				P.host.timing=atol(vs[i+1].c_str())!=0;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
                /*
		else if (vs[i]=="-gl") {
			if (i+3<vs.size()) {
				GL.viewport(atol(vs[i+1].c_str()),atol(vs[i+2].c_str()));
				GL.image(vs[i+3].c_str());
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=4;
		}
                 */ 
		else if (vs[i]=="-cv") {
			if (i+1<vs.size()) {
				P.host.lconv=atol(vs[i+1].c_str())!=0;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-xyz") {
			if (i+1<vs.size()) {
				_dump=atol(vs[i+1].c_str())!=0;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-dt") {
			if (i+2<vs.size()) {
				P.host.lsynctime=atol(vs[i+1].c_str());
				P.host.ldt=atol(vs[i+2].c_str());
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=3;
		}
		else if (vs[i]=="-ctl") {
			if (i+1<vs.size()) {
				P.host.ctl=atol(vs[i+1].c_str());
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-rel") {
			if (i+9 <vs.size()) {
				L.release(vs[i+1].c_str(),
                                                atol(vs[i+2].c_str()), atof(vs[i+3].c_str()),
                                                atof(vs[i+4].c_str()), atof(vs[i+5].c_str()),
                                                atof(vs[i+6].c_str()), atof(vs[i+7].c_str()),
                                                atof(vs[i+8].c_str()), atoi(vs[i+9].c_str()));
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=10;
		}
		else if (vs[i]=="-rls") {
			if (i+10<vs.size()) {
				L.relgrid(vs[i+1].c_str(),
						atof(vs[i+2].c_str()),atof(vs[i+3].c_str()),atol(vs[i+4].c_str()),
						atof(vs[i+5].c_str()),atof(vs[i+6].c_str()),atol(vs[i+7].c_str()),
						atof(vs[i+8].c_str()),atof(vs[i+9].c_str()),atol(vs[i+10].c_str()));
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=11;
		}
		else if (vs[i]=="-mb") {
			if (i+1<vs.size()) {
				_mobile=atol(vs[i+1].c_str())!=0;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-flx") {
			if (i+1<vs.size()) {
				P.host.lflx=atol(vs[i+1].c_str())!=0;
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=2;
		}
		else if (vs[i]=="-vn") {
			if (i+2<vs.size()) {
				int iname;
				for (iname=P_NAME; iname<END_NAME; iname++) {
					if (vs[i+1]==G.name(iname)) {
						G.setName(iname,vs[i+2].c_str());
						break;
					}
				}
				if (iname>=END_NAME) errmsg("Cannot find ",vs[i+1].c_str()," in the variable name list.");
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=3;
		}
		else if (vs[i]=="-ct") {
			if (i+6<vs.size()) {
				C.set(atof(vs[i+1].c_str()),atol(vs[i+2].c_str()),atof(vs[i+3].c_str()),
					  atol(vs[i+4].c_str()),atof(vs[i+5].c_str()),atof(vs[i+6].c_str()));
			}
			else {
				errmsg(msg1,vs[i].c_str(),msg3);
			}
			i+=7;
		}
		else {
			errmsg(">Unexpected ",vs[i].c_str(),msg3);
		}
	}
	return true;
}

bool flexModel::setOption(int argc, char *argv[])
{
	int i;
	std::vector<std::string> vs;
	for (i=1; i<argc; i++) {
		if (strcmp(argv[i],"-ini")==0) {
// get options from initial file
			if (i+1>=argc) errmsg("Not a file name following -ini option");

			char buf[1024];
			FILE *fp=fopen(argv[i+1],"r");
			if (!fp) errmsg("Failed to open initial file");

			while (!feof(fp)) {

				if (!fgets(buf,1023,fp)) continue;
				if (buf[0]=='#') continue;

				int loc=0;
				std::string s(buf);
				while (loc<s.size()) {
					while (loc<s.size() && isspace(s.at(loc))) loc++;
					if (loc>=s.size()) break;
					int loc2=loc+1;
					if (s.at(loc)=='"') {
						loc++;
						loc2++;
						while (loc2<s.size() && s.at(loc2)!='"') loc2++;
						if (loc2>=s.size()) errmsg("Incomplete quoted string in initial file");
					}
					else {
						while (loc2<s.size() && !isspace(s.at(loc2))) loc2++;
					}
					vs.push_back(s.substr(loc,loc2-loc));
					loc=loc2+1;
				}
			}

			fclose(fp);

			i++;
		}
		else {
			vs.push_back(argv[i]);
		}
	}

	return setOption(vs);
}
