#include "flexsim.h"

#include <stdio.h>
#include <stdlib.h>

flexSim::flexSim(){
    
    initOpenCL(0, 0, true);
}

flexSim::~flexSim(){
    
}


void flexSim::start(){
    printf("start\n");
}

bool flexSim::setOptions(int nargs, char* args[]){
    printf("setOptions - simple\n");
}

bool flexSim::setOption(const std::vector<std::string>& vs){
    printf("setOptions - complex\n");
}

void flexSim::reset(double julian){
    printf("reset\n");
}

void flexSim::update(double julian){
    printf("update\n");
}
