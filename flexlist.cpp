/*
 Lagrangian particle dispersion program based on FLEXPART

 Version: 1.0 Beta, May 2010

 Author: Jiye Zeng <zeng@nies.go.jp>, Ph.D.

 Copyright (c) May 2010
 National Institute for Environmental Studies, Japan
 All rights reserved.

 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.

 IN NO EVENT SHALL THE AUTHOR (JIYE ZENG) AND THE COPYRIGHT HOLDER
 (THE NATIONAL INSTITUTE FOR ENVIRONMENTAL STUDIES, JAPAN) BE LIABLE
 TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 EVEN IF THE AUTHOR AND THE COPYRIGHT HOLDER HAVE BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

 THE AUTHOR AND THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR AND
 THE COPYRIGHT HOLDER HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

#include "flexlist.h"
#include "flexutility.h"
#include <stdio.h>

#define DEBUG 0

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

flexList::flexList() : _node(NULL)
{
	_reltype=0;
        
        Cv = new flexConv();
}

flexList::~flexList()
{
	clear();
}

void flexList::clear()
{
    PRINTF("LIST : clear\n");
    
	while(_node) {
		node *tmp=_node->next;
		delete _node->ptr;
		delete _node;
		_node=tmp;
	}
}

void flexList::release(const char *name, int n, float x0, float y0, float ds, float z0, float dz, float mass, int halflife)
{
	if (_reltype==0) {
		_reltype=1;
	}
	else {
		if (_reltype!=1) errmsg("Only one release type is allowed");
	}
        
        if(mass > 0)
            _mass.push_back(mass);
        if(halflife > 0)
            _half_life.push_back(halflife);
        
	_n.push_back(n);
	_x0.push_back(x0);
	_y0.push_back(y0);
	_ds.push_back(ds);
	_z0.push_back(z0);
	_dz.push_back(dz);
	_name.push_back(name);
}

void flexList::relgrid(const char *name, float x0, float dx, int nx, float y0, float dy, int ny, float z0, float dz, int nz)
{
	if (_reltype==0) {
		_reltype=-1;
	}
	else {
		if (_reltype!=-1) errmsg("Only one release type is allowed");
	}
	_x0.push_back(x0);
	_dx.push_back(dx);
	_nx.push_back(nx);
	_y0.push_back(y0);
	_dy.push_back(dy);
	_ny.push_back(ny);
	_z0.push_back(z0);
	_dz.push_back(dz);
	_nz.push_back(nz);
	_name.push_back(name);
}

void flexList::remove(int sec)
{
	if (!_node) return;
	node *prev=_node;
	node *next=_node->next;
	while(next) {
		if (next->ptr->age()>sec) {
			prev->next=next->next;
			delete next->ptr;
			delete next;
			next=prev->next;
		}
		else {
			prev=next;
			next=next->next;
		}
	}
	if (_node->ptr->age()>sec) {
		next=_node->next;
		delete _node->ptr;
		delete _node;
		_node=next;
	}
}

int flexList::add(double ctime, const flexCount &Ct, int idx)
{
	char s[512];
	int i, i1, i2, yy, mm, dd, hh, mi;
	jul2cal(yy,mm,dd,hh,mi,ctime);
        
        //mobile simulation
	if (idx>=0 && idx<_name.size()) {
		i1=idx;
		i2=idx+1;
	}
	else {
		i1=0;
		i2=_name.size();
	}
        
	for (i=i1; i<i2; i++) {
		sprintf(s,"%s.%d.%02d.%02d.%02d.%02d",_name[i].c_str(),yy,mm,dd,hh,mi);
		printf("release: %s\n",s);
		flexClass *c=new flexClass;
		c->id(s);
		if (_reltype==1) {
                    c->release(_n[i],_x0[i],_y0[i],_ds[i],_z0[i],_dz[i], _mass[i], _half_life[i]);
		}
		else {
                    printf("Warning, this does not yet have mass or radiation\n");
                    c->relgrid(_x0[i],_dx[i],_nx[i],_y0[i],_dy[i],_ny[i],_z0[i],_dz[i],_nz[i]);
		}
		c->cgrid(Ct);
		_node=new node(c,_node);
	}
	int n=0;
	node *tmp=_node;
	while(tmp) {
		n++;
		tmp=tmp->next;
	}
	return n;
}

void flexList::cgrid(const flexCount &C)
{
	node *tmp=_node;
	while(tmp) {
		tmp->ptr->cgrid(C);
		tmp=tmp->next;
	}
}

void flexList::mass(const flexGrid &G, const flexField &F)
{
	node *tmp=_node;
	while(tmp) {
		tmp->ptr->mass(G,F);
		tmp=tmp->next;
	}
}

void flexList::dispersion(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &C)
{
// To use OpenMPI, consider executing one relrease by one node here.
	node *tmp=_node;
	while(tmp) {
		tmp->ptr->dispersion(G,P,F,C);
		if (P.host.lconv) {
			Cv->convection(*tmp->ptr,G,P,F);
		}
		tmp=tmp->next;
	}
}

void flexList::trajectory(const flexGrid &G, const flexParam &P, const flexField &F)
{
	node *tmp=_node;
	while(tmp) {
		tmp->ptr->trajectory(G,P,F);
		tmp=tmp->next;
	}
}

void flexList::radioactive(const flexGrid &G, const flexParam &P, const flexField &F, flexCount &C){
    node *tmp=_node;
    while(tmp) {
            tmp->ptr->radioactive_decay(G,P,F,C);
            tmp=tmp->next;
    }
}

/*
void flexList::show(flexGL &GL, double ctime, bool cuda)
{
	if (_node) {
		if (cuda) _node->ptr->xyz();
		GL.update(ctime,_node->ptr->host.X,_node->ptr->host.Y,_node->ptr->host.np);
	}
}
*/
void flexList::saveXYZ(const char *path, const Grid &G, const Parameter &P, const Field &F, double ctime)
{
	char fname[1024];
	node *tmp=_node;
	while(tmp) {
		sprintf(fname,"%s/%s.xyz.cdf",path,tmp->ptr->id());
		tmp->ptr->saveXYZ(fname,G,P,F,ctime);
		tmp=tmp->next;
	}
}


void flexList::saveCNT(const char *path, const Parameter &P, double ctime)
{
	char fname[1024];
	node *tmp=_node;
	while(tmp) {
		sprintf(fname,"%s/%s.res.cdf",path,tmp->ptr->id());
		tmp->ptr->saveCNT(fname,P,ctime);
		tmp=tmp->next;
	}
}


void flexList::reset() {
	clear();
        
	_name.clear();
	_n.clear();
	_nx.clear();
	_ny.clear();
	_nz.clear();
	_x0.clear();
	_dx.clear();
	_y0.clear();
	_dy.clear();
	_ds.clear();
	_z0.clear();
	_dz.clear();
        _mass.clear();
        _half_life.clear();
}
